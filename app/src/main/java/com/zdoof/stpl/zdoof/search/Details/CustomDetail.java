package com.zdoof.stpl.zdoof.search.Details;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.zdoof.stpl.zdoof.R;

import java.util.ArrayList;

/**
 * Created by stpl on 25/6/16.
 */
public class CustomDetail extends BaseAdapter{
    View_dish_Deatails view_dish_deatails;
    ArrayList<String> values;
    public CustomDetail(View_dish_Deatails view_dish_deatails, int spiner_item, ArrayList<String> values) {
        this.view_dish_deatails=view_dish_deatails;
        this.values=values;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vgrid;
        LayoutInflater inflater = view_dish_deatails.getLayoutInflater();
        vgrid = inflater.inflate(R.layout.custom_resturant, null);

        return vgrid;
    }
}
