package com.zdoof.stpl.zdoof.activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.webservice.WebserviceCallpost;
import com.zdoof.stpl.zdoof.zshare.Chekin;
import com.zdoof.stpl.zdoof.zshare.Enjoyeddish;
import com.zdoof.stpl.zdoof.zshare.Post;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by stpl on 29/2/16.
 */
public class My_experience extends Activity {
    int[]img={R.drawable.friendicon,R.drawable.friends_frnds,R.drawable.pblic};
    String[]catgory={"Friends","Friends of friend","Public"};
    String imageFilePath,imageFilePath1,imageFilePath2,Expdate;
    int SELECT_FILE = 1;
    int REQUEST_CAMERA = 2;
    int SELECT_FILE1 = 3;
    int REQUEST_CAMERA1 = 4;
    LinearLayout camera;
    EditText exper;
    String imageName,imageName1,imageName2;
    String yourexp,uid;
    int uerid,dishid=0,posttype=2,placetype=0,createdby,divicetype=1;
    Spinner friends;
    ImageView im1,im2,ip1,ip2,is1,is2,chek,active1,active2,active3,active4,send2;
    ImageView enjoish,postdish,dishnrcp,checkin,close1;
    ImageView close,send,addimage,files,files1,files2,files3,enjoydish,Enjoydish,frsticn,sndicn,trdicn;
    static String aResponse;
    String placename="",dishname="",postcomment,createdon,ImageNName;
    SharedPreferenceClass sharedPreferenceClass;
    final WebserviceCallpost comp = new WebserviceCallpost();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.my_experience);
        setContentView(R.layout.myexperiences);
        sharedPreferenceClass=new SharedPreferenceClass(this);
        uid=sharedPreferenceClass.getValue_string("UID");
        friends = (Spinner) findViewById(R.id.spinner4);
       // close1= (ImageView) findViewById(R.id.close1);
        //send=(ImageView) findViewById(R.id.pst);
        enjoish= (ImageView) findViewById(R.id.enjd);
        dishnrcp= (ImageView) findViewById(R.id.postd);
        checkin= (ImageView) findViewById(R.id.chkin);
        chek= (ImageView) findViewById(R.id.imageView1);
        camera= (LinearLayout) findViewById(R.id.rl);
        exper= (EditText) findViewById(R.id.editTextRefractor1);
        frsticn= (ImageView) findViewById(R.id.frst);
        sndicn= (ImageView) findViewById(R.id.secnd);
        trdicn= (ImageView) findViewById(R.id.trd);
        close = (ImageView) findViewById(R.id.close);
       // send1 = (RelativeLayout) findViewById(R.id.pst);
        active1= (ImageView) findViewById(R.id.act1);
        active2=(ImageView) findViewById(R.id.act2);
        active3=(ImageView) findViewById(R.id.act3);
        active4=(ImageView) findViewById(R.id.act4);
      //  unit = (Spinner) findViewById(R.id.spinner5);
        //addimage = (ImageView) findViewById(R.id.addimg);
        files = (ImageView) findViewById(R.id.image);
        files1 = (ImageView) findViewById(R.id.images);
        files2 = (ImageView) findViewById(R.id.images1);
        send2= (ImageView) findViewById(R.id.pst1);
        active1.setVisibility(View.VISIBLE);
        uerid=Integer.parseInt(uid);
        createdby=uerid;
       /* Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        Expdate = df.format(c.getTime());*/
        String format = "dd/MMM/yyyy H:mm:ss";
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Expdate = sdf.format(new Date());
        createdon=Expdate;
       /* CusAdapters2 adapter4=new CusAdapters2(My_experience.this,R.layout.spiner_item,catgory,img);
        friends.setAdapter(adapter4);*/

        enjoish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                active1.setVisibility(View.INVISIBLE);
                sharedPreferenceClass.setValue_boolean("MYEX", true);
                Intent intent = new Intent(My_experience.this, Enjoyeddish.class);
                startActivity(intent);
                finish();
            }
        });
        dishnrcp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                active1.setVisibility(View.INVISIBLE);
                active3.setVisibility(View.VISIBLE);
                sharedPreferenceClass.setValue_boolean("DISH", true);
                Intent intent = new Intent(My_experience.this, Post.class);
                startActivity(intent);

            }
        });
        checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               active1.setVisibility(View.INVISIBLE);
                active4.setVisibility(View.VISIBLE);
                sharedPreferenceClass.setValue_boolean("CHK",true);
                Intent intent = new Intent(My_experience.this, Chekin.class);
                startActivity(intent);
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send.setVisibility(View.INVISIBLE);
                send2.setVisibility(View.VISIBLE);
                yourexp=exper.getText().toString();
                if(yourexp.equals("") /*|| imageFilePath==null *//*|| imageFilePath1==null || imageFilePath2==null*/){
                    Toast.makeText(My_experience.this,"This Field is requried",Toast.LENGTH_SHORT).show();
                }else{
                    AsyncCallWS task = new AsyncCallWS();
                    task.execute();
                }
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(My_experience.this,Home.class);
                startActivity(intent);
                finish();
            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        if(imageFilePath==null || imageFilePath1==null || imageFilePath2==null) {
                            final CharSequence[] items = {"Take Photo", "Choose from Library",
                                    "Cancel"};
                            AlertDialog.Builder builder = new AlertDialog.Builder(My_experience.this);
                            builder.setTitle("Add Photo!");
                            builder.setItems(items, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int item) {
                                    if (items[item].equals("Take Photo")) {
                                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                        startActivityForResult(intent, REQUEST_CAMERA);
                                    } else if (items[item].equals("Choose from Library")) {
                                        Intent intent = new Intent(
                                                Intent.ACTION_PICK,
                                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        intent.setType("image/*");
                                        startActivityForResult(
                                                Intent.createChooser(intent, "Select File"), SELECT_FILE);
                                    } else if (items[item].equals("Cancel")) {
                                        dialog.dismiss();
                                    }
                                }
                            });
                            builder.show();
                        }else{
                            Toast.makeText(My_experience.this, "You Have already Choose Three Images", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
            @Override
            protected void onActivityResult(int requestCode, int resultCode, Intent data) {

                if (resultCode == Activity.RESULT_OK && requestCode == SELECT_FILE){
                    onSelectFromGalleryResult(data);
                }
       /*else if(resultCode == Activity.RESULT_OK && requestCode == SELECT_FILE1){
            onSelectFromGalleryResult1(data);
        }*/
                else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CAMERA){
                    onCaptureImageResult(data);
                }
        /*else if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CAMERA1){
            onCaptureImageResult1(data);
        }*/
                else{
                    Toast.makeText(My_experience.this, "Please try again !!!", Toast.LENGTH_SHORT).show();
                }

                super.onActivityResult(requestCode, resultCode, data);
            }

            private void onCaptureImageResult(Intent data) {

                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");
                if(imageFilePath==null){
                    imageFilePath = destination.toString();

                   File imagefile = new File(imageFilePath);
                    FileInputStream fis = null;
                   /* try {
                        fis = new FileInputStream(imagefile);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    Bitmap bm = BitmapFactory.decodeStream(fis);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100 , baos);
                    byte[] b = baos.toByteArray();
                    imageName = Base64.encodeToString(b, Base64.DEFAULT);*/
                   FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    files.setImageBitmap(thumbnail);
                    byte[] b = bytes.toByteArray();
                    imageName = Base64.encodeToString(b, Base64.DEFAULT);

                    frsticn.setVisibility(View.VISIBLE);
                    frsticn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            files.setImageBitmap(null);
                            imageFilePath=null;
                            frsticn.setVisibility(View.INVISIBLE);
                        }
                    });
                }else if(imageFilePath1==null){
                    imageFilePath1 = destination.toString();


                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    files1.setImageBitmap(thumbnail);
                    sndicn.setVisibility(View.VISIBLE);
                    sndicn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            files1.setImageBitmap(null);
                            imageFilePath1=null;
                            sndicn.setVisibility(View.INVISIBLE);
                        }
                    });
                }else if(imageFilePath2==null){
                    imageFilePath2 = destination.toString();


                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    files2.setImageBitmap(thumbnail);
                    trdicn.setVisibility(View.VISIBLE);
                    trdicn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            files2.setImageBitmap(null);
                            imageFilePath2=null;
                            trdicn.setVisibility(View.INVISIBLE);
                        }
                    });
                }

            }

            /* private void onSelectFromGalleryResult(Intent data) {
                 Uri selectedImageUri = data.getData();
                 String[] projection = {MediaStore.MediaColumns.DATA};
                 Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                         null);
                 int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                 cursor.moveToFirst();

                 imageFilePath = cursor.getString(column_index).toString();

                 BitmapFactory.decodeFile(imageFilePath);

                 Bitmap bm = BitmapFactory.decodeFile(imageFilePath);

                 files.setImageBitmap(bm);

             }*/
            private void onSelectFromGalleryResult(Intent data) {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                        null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                if(imageFilePath==null){
                    imageFilePath = cursor.getString(column_index).toString();
                    BitmapFactory.decodeFile(imageFilePath);

                    Bitmap bm1 = BitmapFactory.decodeFile(imageFilePath);
                    File imagefile = new File(imageFilePath);
                    FileInputStream fis = null;
                    try {
                        fis = new FileInputStream(imagefile);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    Bitmap bm = BitmapFactory.decodeStream(fis);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] b = baos.toByteArray();
                    imageName = Base64.encodeToString(b, Base64.DEFAULT);
                   // ImageNName="@" + imageName;
                    files.setImageBitmap(bm1);
                    frsticn.setVisibility(View.VISIBLE);
                    frsticn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            files.setImageBitmap(null);
                            imageFilePath=null;
                            frsticn.setVisibility(View.INVISIBLE);
                        }
                    });
                }
                else if(imageFilePath1==null){
                    imageFilePath1 = cursor.getString(column_index).toString();

                    BitmapFactory.decodeFile(imageFilePath1);

                    Bitmap bm = BitmapFactory.decodeFile(imageFilePath1);

                    files1.setImageBitmap(bm);
                    sndicn.setVisibility(View.VISIBLE);
                    sndicn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            files1.setImageBitmap(null);
                            imageFilePath1=null;
                            sndicn.setVisibility(View.INVISIBLE);
                        }
                    });
                }
                else if(imageFilePath2==null){
                    imageFilePath2 = cursor.getString(column_index).toString();

                    BitmapFactory.decodeFile(imageFilePath2);

                    Bitmap bm = BitmapFactory.decodeFile(imageFilePath2);

                    files2.setImageBitmap(bm);
                    trdicn.setVisibility(View.VISIBLE);
                    trdicn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            files2.setImageBitmap(null);
                            imageFilePath2=null;
                            trdicn.setVisibility(View.INVISIBLE);
                        }
                    });

                }
       /*else if(imageFilePath3.equals(""){
           imageFilePath3 = cursor.getString(column_index).toString();

           BitmapFactory.decodeFile(imageFilePath);

           Bitmap bm = BitmapFactory.decodeFile(imageFilePath);

           files3.setImageBitmap(bm);
       }*/
            }
    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

           // aResponse = comp.Experience("insert_post", uerid, placename,dishid,dishname,Expdate,posttype,yourexp,imageName,placetype,divicetype,createdby,createdon);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");


            if (aResponse.equals("0")) {
                Toast.makeText(getApplicationContext(), "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (aResponse.equals("1")) {

                //Toast.makeText(getApplicationContext(), "You have successfully Posted !", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(My_experience.this, Home.class);
                startActivity(intent);
                finish();
            } else {

            }
        }

    }

        }






