package com.zdoof.stpl.zdoof.adapters;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.activites.PostListActivity;
import com.zdoof.stpl.zdoof.R;

import java.util.ArrayList;

/**
 * Created by stpl on 10/6/16.
 */
public class Custom_postlist extends BaseAdapter{
    PostListActivity postList;
    ArrayList<Detail> post;
    public Custom_postlist(PostListActivity postList, int spiner_item, ArrayList<Detail> post) {
        this.postList=postList;
        this.post=post;
    }

    @Override
    public int getCount() {
        return post.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = postList.getLayoutInflater();
        view = inflater.inflate(R.layout.custom_post, null);
        TextView text= (TextView) view.findViewById(R.id.likto);
        TextView text1= (TextView) view.findViewById(R.id.ype);
        TextView text2= (TextView) view.findViewById(R.id.liedon);
        text.setText(post.get(position).getPostname());
        text1.setText(Html.fromHtml(post.get(position).getPostedcomments()));
        text2.setText(post.get(position).getPcreaton());
        return view;
    }
}
