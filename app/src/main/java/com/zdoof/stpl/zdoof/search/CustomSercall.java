package com.zdoof.stpl.zdoof.search;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.webservice.PicConstant;

import java.util.ArrayList;

/**
 * Created by stpl on 13/3/16.
 */
public class CustomSercall extends BaseAdapter{
    Search_all search_all;
    ArrayList<Detail> values;
    String pic,pic1;
    public CustomSercall(Search_all search_all, int spiner_item, ArrayList<Detail> values) {
        this.search_all=search_all;
        this.values=values;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vgrid;
        LayoutInflater inflater = search_all.getLayoutInflater();
        vgrid = inflater.inflate(R.layout.food_search, null);
        ImageView img= (ImageView) vgrid.findViewById(R.id.imageView13);
        TextView text= (TextView) vgrid.findViewById(R.id.textView18);
        TextView text1= (TextView) vgrid.findViewById(R.id.textView19);
        TextView text2= (TextView) vgrid.findViewById(R.id.textView20);
        pic=values.get(position).getDish_pic();
        pic1= PicConstant.PROFILE_URL1+pic;
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(search_all).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.noimg)
                .showImageOnLoading(R.drawable.uute).build();
        imageLoader.displayImage(pic1, img, options);
        /*Uri myUri = Uri.parse(pic2);
        Picasso.with(search_dish)
                .load(myUri)
                .placeholder(R.drawable.load)
                .error(R.drawable.noimg)
                .into(img);*/

        text.setText(values.get(position).getDish_name());
        text1.setText(values.get(position).getReviews());
        text2.setText(values.get(position).getZlikes());

        return vgrid;
    }
}
