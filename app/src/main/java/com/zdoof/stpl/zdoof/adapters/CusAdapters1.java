package com.zdoof.stpl.zdoof.adapters;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zdoof.stpl.zdoof.R;

/**
 * Created by stpl on 26/2/16.
 */
public class CusAdapters1 extends BaseAdapter{
    FragmentActivity enjoyeddish;
    String[] catgory;
    int[] img;
    public CusAdapters1(FragmentActivity enjoyeddish, int spiner_item, String[] catgory, int[] img) {
      this.catgory=catgory;
        this.img=img;
        this.enjoyeddish=enjoyeddish;
    }

    @Override
    public int getCount() {
        return catgory.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = enjoyeddish.getLayoutInflater();
        view = inflater.inflate(R.layout.customview, null);
        TextView text= (TextView) view.findViewById(R.id.textView16);
        ImageView image= (ImageView) view.findViewById(R.id.imageView25);
        text.setText(catgory[position]);
        text.setTextColor(Color.WHITE);
        image.setImageResource(img[position]);
        return view;
    }
}
