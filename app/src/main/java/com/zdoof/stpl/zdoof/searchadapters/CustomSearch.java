package com.zdoof.stpl.zdoof.searchadapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zdoof.stpl.zdoof.activites.Home;
import com.zdoof.stpl.zdoof.R;

/**
 * Created by stpl on 18/4/16.
 */
public class CustomSearch extends BaseAdapter{
    Home home;
    String[] sertype;
    int[] sercimg;
    public CustomSearch(Home home, int spiner_item, String[] sertype, int[] sercimg) {
        this.home=home;
        this.sertype=sertype;
        this.sercimg=sercimg;
    }

    @Override
    public int getCount() {
        return sertype.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vgrid;
        LayoutInflater inflater = home.getLayoutInflater();
        vgrid = inflater.inflate(R.layout.custom_serdesign, null);
        ImageView image= (ImageView) vgrid.findViewById(R.id.imageView29);
        TextView text= (TextView) vgrid.findViewById(R.id.textView27);
        image.setImageResource(sercimg[position]);
        text.setText(sertype[position]);

        return vgrid;
    }
}
