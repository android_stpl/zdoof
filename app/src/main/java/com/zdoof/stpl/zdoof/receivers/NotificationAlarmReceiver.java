package com.zdoof.stpl.zdoof.receivers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.services.ConnectionNotificationService;

/**
 * Created by Shiftu Bd team1 on 10-Mar-2017.
 */

public class NotificationAlarmReceiver extends WakefulBroadcastReceiver {

    public static final int ALARM_REQUEST_CODE = 9627;
    public static final String ALARM_ACTION = "com.zdoof.stpl.zdoof.receivers.alarm";
    public static final String PREFS_ACTION = "com.zdoof.stpl.zdoof.receivers.NotificationAlarmReceiver.PREFS_FILTER";

    SharedPreferenceClass sharedPreferenceClass;
    // Triggered by the Alarm periodically (starts the service to run task)
    @Override
    public void onReceive(Context context, Intent intent) {
        sharedPreferenceClass = new SharedPreferenceClass(context);

        int login_id = Integer.parseInt(sharedPreferenceClass.getValue_string("UIDD"));
        int pending_conn_size = sharedPreferenceClass.getValue_int("PENDING_CONN_SIZE");
        Intent i = new Intent(context, ConnectionNotificationService.class);
        i.setAction(ALARM_ACTION);
        i.putExtra("LOGIN_ID", login_id);
        i.putExtra("PENDING_CONN_SIZE", pending_conn_size);
        context.startService(i);
    }
}
