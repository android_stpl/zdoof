package com.zdoof.stpl.zdoof.activites;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.fragments.About;
import com.zdoof.stpl.zdoof.views.Contropanel;

public class AboutApp extends Activity {

    String versionName=null;
    PackageInfo pi;
    TextView appVname;
    LinearLayout l1,l2,l3,l4;
    ImageView back;
    SharedPreferenceClass sharedPreferenceClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_app);

        sharedPreferenceClass = new SharedPreferenceClass(AboutApp.this);
        appVname = (TextView) findViewById(R.id.appVersionName);
        l1 = (LinearLayout) findViewById(R.id.aboutll1);
        l2 = (LinearLayout) findViewById(R.id.aboutll2);
        l3 = (LinearLayout) findViewById(R.id.aboutll3);
        l4 = (LinearLayout) findViewById(R.id.aboutll4);
        back = (ImageView) findViewById(R.id.aboutBack);

        try {
            pi = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = pi.versionName;
        }
        catch (Exception e) {
            e.printStackTrace();
            versionName = null;
        }

        if (versionName!=null) {
            appVname.setText(versionName);
        }

        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AboutApp.this, Home.class);
                startActivity(intent);
                finish();
            }
        });

        l2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AboutApp.this, Connecton.class);
                startActivity(intent);
                finish();
            }
        });

        l3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AboutApp.this, Notifications.class);
                startActivity(intent);
                finish();
            }
        });

        l4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AboutApp.this, Contropanel.class);
                startActivity(intent);
                finish();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        String back = sharedPreferenceClass.getValue_string("BACKPAGE");
        if (back.equals("controlpanel")) {
            Intent intent = new Intent(AboutApp.this, Contropanel.class);
            sharedPreferenceClass.setValue_string("BACKPAGE", "");
            startActivity(intent);
            finish();
        }
        else {
            super.onBackPressed();
        }
    }
}
