package com.zdoof.stpl.zdoof.activites;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.database.DatabaseHelper;
import com.zdoof.stpl.zdoof.webservice.WebserviceCallpost;

/**
 * Created by stpl on 28/4/16.
 */
public class Edit_Comments extends Activity{
    EditText edits;
    String comment,postid;
    Button update,cancel;
    DatabaseHelper helper;
    int postreview=0;
    final WebserviceCallpost com1 = new WebserviceCallpost();
    static String dResponse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editcomment);
        edits = (EditText) findViewById(R.id.editText4);
        update = (Button) findViewById(R.id.button3);
        cancel = (Button) findViewById(R.id.button4);
        helper = new DatabaseHelper(this);
        try {
            postid = getIntent().getStringExtra("POST");

            comment = getIntent().getStringExtra("CMMNT");
            edits.setText(comment);
            postreview=Integer.parseInt(postid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment = edits.getText().toString().trim();
                AsyncCallWS3 task = new AsyncCallWS3();
                task.execute();

                helper.upDatebyid(postid, comment);

                // finish();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(Edit_Comments.this, Comments.class);
                startActivity(in);
            }
        });
    }
        class AsyncCallWS3 extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... params) {
                Log.i("TAG", "doInBackground");

                dResponse = com1.Update_cmnt("upd_post_comment", postreview,comment);
                return null;
            }
            @Override
            protected void onPostExecute(Void result) {
                Log.i("TAG", "onPostExecute");
              /*  try {
                    JSONArray jr = new JSONArray(dResponse);
                    for (int i=0;i<jr.length();i++) {
                        JSONObject jsonObject = jr.getJSONObject(i);
                        totalpost = jsonObject.getString("total_post").toString();
                        totalcount = Integer.parseInt(totalpost);

                        if (totalcount > count) {
                            AsyncCallWS2 task1 = new AsyncCallWS2();
                            task1.execute();
                            // progressDialog = ProgressDialog.show(Home.this, "", "Loading...");
                        } else if (totalcount==count) {
                            // progressDialog.dismiss();
                        }
                        // progressDialog.dismiss();
                    }

                }catch (Exception e){

                }*/

                if (dResponse.equals("0")) {
                    Toast.makeText(Edit_Comments.this, "Sorry try again!", Toast.LENGTH_LONG).show();

                } else if (dResponse.equals("1")) {

                    Toast.makeText(Edit_Comments.this, "Updated successfully !", Toast.LENGTH_LONG).show();
                    Intent in = new Intent(Edit_Comments.this, Comments.class);
                    startActivity(in);
                } else {

                    // progressDialog.dismiss();

                }

            }
    }
}
