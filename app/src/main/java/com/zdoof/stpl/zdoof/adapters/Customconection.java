package com.zdoof.stpl.zdoof.adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.CircularNetworkImageView;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.activites.Connecton;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.webservice.PicConstant;

import java.util.ArrayList;

/**
 * Created by stpl on 12/2/16.
 */
public class Customconection extends BaseAdapter{
    Connecton connecton;
    ArrayList<Detail> detl;
    String pic1,pic2,pic3,pic4;
    String userid,name,pfpic,totalcnt;
    public Customconection(Connecton connecton, int simple_list_item_1, ArrayList<Detail> detl) {
        this.connecton=connecton;
        this.detl=detl;
    }

    @Override
    public int getCount() {
        return detl.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = connecton.getLayoutInflater();
        view = inflater.inflate(R.layout.customconection, null);
        CircularNetworkImageView circularImageView = (CircularNetworkImageView) view.findViewById(R.id.profilePictureView1);
        TextView userType = (TextView) view.findViewById(R.id.connUsertype);
        ImageView uTYPimg = (ImageView) view.findViewById(R.id.connUTYPimg);
        String utype = detl.get(position).getUtype();
        pfpic=detl.get(position).getPfimage();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(connecton).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                        // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.blankpeople)
                .showImageOnLoading(R.drawable.blankpeople).build();

        if(pfpic.contains("../../zdoof-admin/")) {
             pic1 = pfpic.replace("../../zdoof-admin/", "");
            pic3= PicConstant.PROFILE_URL+pic1;
           /* Uri myUri = Uri.parse(pic3);
            Picasso.with(connecton)
                    .load(myUri)
                    .placeholder(R.drawable.load)
                    .error(R.drawable.blankpeople)
                    .into(circularImageView);*/
            imageLoader.displayImage(pic3, circularImageView, options);



//initialize image view


//download and display image from url

        }else if(pfpic.contains("../../images/")){
             pic2=pfpic.replace("../../images/","");
            imageLoader.displayImage(pic2, circularImageView, options);
            /*Uri myUri = Uri.parse(pic2);
            Picasso.with(connecton)
                    .load(myUri)
                    .placeholder(R.drawable.load)
                    .error(R.drawable.blankpeople)
                    .into(circularImageView);*/
        }else{
            pic4=PicConstant.PROFILE_URL1+pfpic;
            imageLoader.displayImage(pic4, circularImageView, options);
           /* Uri myUri = Uri.parse(pic4);
            Picasso.with(connecton)
                    .load(myUri)
                    .placeholder(R.drawable.load)
                    .error(R.drawable.blankpeople)
                    .into(circularImageView);*/


        }

        TextView name1= (TextView) view.findViewById(R.id.name1);
        name=detl.get(position).getName();
        if(name.equals("null")){
            name1.setText("N/A");
        }else{
            name1.setText(name);
        }

        userType.setText(utype);

        if(utype.equals("Foodie")) {
            uTYPimg.setImageResource(R.drawable.utype2);
        }
        else if (utype.equals("Culinary hobbyist")) {
            uTYPimg.setImageResource(R.drawable.utype3);
        }
        else if (utype.equals("Chef")) {
            uTYPimg.setImageResource(R.drawable.utype1);
        }

        return view;
    }

}
