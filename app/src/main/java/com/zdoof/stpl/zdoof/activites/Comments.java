package com.zdoof.stpl.zdoof.activites;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.SliderLayout;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.CircularNetworkImageView;
import com.zdoof.stpl.zdoof.adapters.Custom_Comment;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.commons.Tables;
import com.zdoof.stpl.zdoof.database.DatabaseHelper;
import com.zdoof.stpl.zdoof.utils.AppUtil;
import com.zdoof.stpl.zdoof.webservice.PicConstant;
import com.zdoof.stpl.zdoof.webservice.WebserviceCallpost;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by sujit on 22/4/16.
 */
public class Comments extends Activity {
    ListView list;
    EditText coment;
    ImageView send;
    CircularNetworkImageView circularImageView, profimg;
    ImageView imm, like, likeactive,close;
    LinearLayout ll;
    LinearLayout ll1;
    LinearLayout webContainer;
    WebView webBlog;
    TextView nam, texth,texth_red, title;
    TextView likes, cment, descrip, dshares, dcrton,place_dish,place_rest,comnts,yums;
    ImageView imd,workimg,smile;
    SliderLayout mDemoSlider;
    TextView types;
    LinearLayout allclick;
    ListView cmntlist;
    RelativeLayout rll;
    ArrayList<Detail> cmt;
    //float Star_review= (float) 00.00;
    int post_id, creatby, ptype;
    String creat_on, commment, profurl, profilepic, names,Review_post_id,create_on;
    private static String cResponse,eResponse;
    DatabaseHelper helper;
    int count;
    String spinnervalue, postidd, comment,comments;
    ArrayList<Detail> hh;
    ArrayList<Detail>cmmnt;
    String[] gg = {"Edit", "Delete"};
    String postid, posttype, createb,post_name,profile_image,style,imd_img,pic4,share,details,blogImg,placeDish,comntCount,yumsCount,ifYumed;
    final WebserviceCallpost com = new WebserviceCallpost();
    SharedPreferenceClass sharedPreferenceClass;
    ProgressDialog progressDialog;
    ImageLoader imageLoader;
    DisplayImageOptions options1;
    private ArrayList<String> tableImages = new ArrayList<String>();
    LinearLayout imgScroller;
    String youtubeID;
    RelativeLayout youtubeContainer;
    ImageView youtubeThumb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.comment_page);
        title = (TextView) findViewById(R.id.comment_title);
        smile = (ImageView) findViewById(R.id.sndYum);
        comnts = (TextView) findViewById(R.id.comment_sum);
        yums = (TextView) findViewById(R.id.yum_sum);
        list = (ListView) findViewById(R.id.listView3);
        close = (ImageView) findViewById(R.id.comment_close);
        webContainer = (LinearLayout) findViewById(R.id.webContainer);
        webBlog = (WebView) findViewById(R.id.webBlog);
        youtubeContainer = (RelativeLayout) findViewById(R.id.youtubeContainer);
        youtubeThumb = (ImageView) findViewById(R.id.youtubeComthumb);
        imgScroller = (LinearLayout) findViewById(R.id.imgScroller);
        profimg = (CircularNetworkImageView) findViewById(R.id.profimg);
        coment = (EditText) findViewById(R.id.editText3);
        send = (ImageView) findViewById(R.id.sndd);
        Typeface custom_font = Typeface.createFromAsset(Comments.this.getAssets(), "OpenSans-Italic.ttf");
        imd = (ImageView) findViewById(R.id.imageView23);
        nam = (TextView) findViewById(R.id.textVie1);
        types = (TextView) findViewById(R.id.textV1);
        imm = (ImageView) findViewById(R.id.img);
        descrip = (TextView) findViewById(R.id.tcc);
        dshares = (TextView) findViewById(R.id.textyV1);
        dcrton = (TextView) findViewById(R.id.postTime);
        likeactive = (ImageView) findViewById(R.id.btnYum);
        texth = (TextView) findViewById(R.id.text7);
        texth_red = (TextView) findViewById(R.id.textred);
        likes = (TextView) findViewById(R.id.likes);
        cmntlist = (ListView) findViewById(R.id.lsst);
        cment=(TextView) findViewById(R.id.coments);
        place_dish=(TextView) findViewById(R.id.place);
        place_rest=(TextView) findViewById(R.id.resturant);

        workimg= (ImageView) findViewById(R.id.workimg);

        post_id = getIntent().getIntExtra("POST_ID", 0);
        postid = String.valueOf(post_id);
        creatby = getIntent().getIntExtra("_USER", 0);
        createb = String.valueOf(creatby);
        ptype = getIntent().getIntExtra("PTYPE", 0);
        creat_on = getIntent().getStringExtra("CREATE_ON");
        posttype = String.valueOf(ptype);
        profilepic = getIntent().getStringExtra("PFIMG");
        profurl = PicConstant.PROFILE_URL1 + profilepic;
        names = getIntent().getStringExtra("NMA");
        style=getIntent().getStringExtra("STYL");
        imd_img=getIntent().getStringExtra("MID");
        share=getIntent().getStringExtra("DSHR");
        details=getIntent().getStringExtra("DTL");
        tableImages = (ArrayList<String>) getIntent().getSerializableExtra("TBL_IMG");
        youtubeID = getIntent().getStringExtra("YT_ID");
        blogImg = getIntent().getStringExtra("BLOG_IMG");
        placeDish = getIntent().getStringExtra("PLACE_DISH");
        comntCount = getIntent().getStringExtra("COMMENTS");
        yumsCount = getIntent().getStringExtra("YUMS");
        ifYumed = getIntent().getStringExtra("YUMS_STATUS");
        //if (youtubeID!=null && youtubeID!="" && youtubeID!="null")

        title.setText(names+share);
            ImageLoaderConfiguration config1 = new ImageLoaderConfiguration.Builder(Comments.this).build();
        imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config1);
        options1 = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .resetViewBeforeLoading(true)
                .showImageOnFail(R.drawable.blankpeople)
                .showImageOnLoading(R.drawable.uute).build();

        imageLoader.displayImage(profurl, profimg, options1);

        comnts.setText(comntCount);
        yums.setText(yumsCount);
        if (ifYumed.equals("blog-btn choose")) {
            smile.setImageResource(R.drawable.smiling_green);
        }
        else {
            smile.setImageResource(R.drawable.smiling_gray);
        }
        if (!youtubeID.equals(null) && !youtubeID.equals("") && !youtubeID.equals("null")) {
            youtubeContainer.setVisibility(View.VISIBLE);
            String link = "http://img.youtube.com/vi/"+youtubeID+"/default.jpg";
            imageLoader.displayImage(link, youtubeThumb, options1);
        } else {youtubeContainer.setVisibility(View.GONE);}

        if (tableImages!=null && tableImages.size()>0){
            for (int i=0; i<tableImages.size(); i++) {
                View v = getLayoutInflater().inflate(R.layout.comment_img, null);
                ImageView img = (ImageView) v.findViewById(R.id.imageChild);
                String link = PicConstant.PROFILE_URL1+tableImages.get(i);
                imageLoader.displayImage(link, img, options1);
                imgScroller.addView(v);
            }
        } else {imgScroller.setVisibility(View.GONE);}

        youtubeThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Comments.this, YoutubePlayer.class);
                intent.putExtra("youtubeID", youtubeID);
                startActivity(intent);
            }
        });

        imgScroller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String>  imgURLs = new ArrayList<String>();
                for(String t:tableImages) {
                    imgURLs.add(PicConstant.PROFILE_URL1+t);
                }
                Intent intent = new Intent(Comments.this, AndroidImageSlider.class);
                intent.putExtra("ImageURLS", ""+imgURLs);
                startActivity(intent);
            }
        });
        hh = new ArrayList<Detail>();

        sharedPreferenceClass = new SharedPreferenceClass(this);
        helper = new DatabaseHelper(this);
        AsyncCallWS4 task4 = new AsyncCallWS4();
        task4.execute();


        if(style==null){

        }
        else if (style.equals("display: none")) {

        } else if (style.equals("")) {
            if (imd_img.contains("/zdoof-admin/dish/post-image/")) {
                imm.setVisibility(View.VISIBLE);
                pic4 = PicConstant.PROFILE_URL1 + imd_img;
                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Comments.this).build();
                ImageLoader imageLoader = ImageLoader.getInstance();
                ImageLoader.getInstance().init(config);
                DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                        .cacheOnDisc(true).resetViewBeforeLoading(true)
                        // .showImageForEmptyUri(fallback)
                        .showImageOnFail(R.drawable.blankpeople)
                        .showImageOnLoading(R.drawable.uute).build();
                imageLoader.displayImage(pic4, imm, options);
                // post_pic=Integer.parseInt(pic4);
            } else if (imd_img.contains("/zdoof-admin/dish/dish-image/")) {
                imm.setVisibility(View.VISIBLE);
                pic4 = PicConstant.PROFILE_URL1 + imd_img;
                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Comments.this).build();
                ImageLoader imageLoader = ImageLoader.getInstance();
                ImageLoader.getInstance().init(config);
                DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                        .cacheOnDisc(true).resetViewBeforeLoading(true)

                        .showImageOnFail(R.drawable.blankpeople)
                        .showImageOnLoading(R.drawable.uute).build();
                         imageLoader.displayImage(pic4, imm, options);

            }
        }

        dcrton.setText(creat_on);
        nam.setText(names);
        dshares.setTypeface(custom_font);
        dshares.setText(share);
        //descrip.setText(Html.fromHtml(details));

        if (posttype.equals("7")) {
            descrip.setVisibility(View.GONE);
            webContainer.setVisibility(View.VISIBLE);
            webBlog.getSettings().setJavaScriptEnabled(true);
            String data;
            if (!blogImg.equals("") && !blogImg.equals("null") && blogImg.length()>9) {
                data = "<html><body>"+"<p><h2>"+placeDish+"</h2></p>"+"<p><img width=100% height=auto src=\""+PicConstant.PROFILE_URL1+blogImg+"\" /></p>"+details+"</body></html>";
            }
            else {
                data = "<html><body>"+"<p><h2>"+placeDish+"</h2></p>"+details+"</body></html>";
            }
            webBlog.loadData(data, "text/html", "UTF-8");
//            SpannableString ss;
//            ss = new SpannableString(Html.fromHtml(details));
        }
        else {
            descrip.setText(details);
        }

        smile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                commment = coment.getText().toString().trim();
                if (commment.equals("")) {
                    Toast.makeText(Comments.this, "Give comment", Toast.LENGTH_SHORT).show();
                } else {


                    AsyncCallWS2 task = new AsyncCallWS2();
                    task.execute();

                    coment.setText("");
                }
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(Comments.this, Home.class);
//        startActivity(intent);
        finish();
    }

    class AsyncCallWS2 extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(Comments.this, "", "Loading...");
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            cResponse = com.Addcomment("add_post_comment", commment, post_id, ptype, creatby, creat_on);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            progressDialog.dismiss();

            try {

                JSONArray jr = new JSONArray(cResponse);

                for (int i = 0; i < jr.length(); i++) {

                    JSONObject jsonObject = jr.getJSONObject(i);

                }

            } catch (Exception e) {
                Log.v("EDC",e.getMessage());
            }

            if (cResponse.equals("0")) {
                Toast.makeText(Comments.this, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (cResponse.equals("1")) {
                AsyncCallWS4 task4 = new AsyncCallWS4();
                task4.execute();

                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    }
                });

            } else {


            }
        }
    }
            class AsyncCallWS4 extends AsyncTask<Void, Void, Void> {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }
                @Override
                protected Void doInBackground(Void... params) {
                    Log.i("TAG", "doInBackground");

                    eResponse = com.comment("ret_comment", post_id, ptype);
                    return null;
                }
                private String convertTime(String jTime) {
                    String returnString = null;
                    //returnString = jTime.substring(0, jTime.length()-6);
                    String[] temp = jTime.split("T");
                    returnString = temp[0] + " " + temp[1];
                    try {
                        long epoch = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(returnString).getTime();
                        //epoch = epoch+19800000L;
                        Date date = new Date(epoch);
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy 'at' HH:mm:ss.SSS");
                        String timezoneID = TimeZone.getDefault().getID();
                        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                        String t1 = sdf.format(date);
                        sdf.setTimeZone(TimeZone.getTimeZone(timezoneID));
                        String t2 = sdf.format(date);
                        long diff = ((new SimpleDateFormat("dd-MMM-yyyy 'at' HH:mm:ss.SSS").parse(t2).getTime())-(new SimpleDateFormat("dd-MMM-yyyy 'at' HH:mm:ss.SSS").parse(t1).getTime()))*1L;
                        long c_poch = epoch + diff;
                        Date c_date = new Date(c_poch);
                        sdf = new SimpleDateFormat("dd-MMM-yyyy 'at' HH:mm:ss aaa");
                        returnString = sdf.format(c_date);
                    } catch (ParseException e) {
                        returnString = "0";
                    }
                    return returnString;
                }

                @Override
                protected void onPostExecute(Void result) {
                    Log.i("TAG", "onPostExecute");
                    if (eResponse.equals("anyType{}")) {

                    } else {

                        try {
                            JSONArray jr = new JSONArray(eResponse);
                            cmt = new ArrayList<Detail>();
                            for (int i = 0; i < jr.length(); i++) {
                                Detail productdto = new Detail();
                                JSONObject jsonObject = jr.getJSONObject(i);
                                comments = jsonObject.getString("Comment");
                                Review_post_id = jsonObject.getString("Review_post_id");
                                create_on = jsonObject.getString("Created_on_cmnt_and");
//                                SimpleDateFormat sdfSource = new SimpleDateFormat("MMM dd yyyy HH:mmaaa");
//                                Date date = sdfSource.parse(create_on);
//                                SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
//                                create_on = sdfDestination.format(date);
                                create_on = convertTime(create_on);
                                //create_on = jsonObject.getString("Created_on_cmnt");
                                //create_on = AppUtil.getDateAndTimeFromTimestamp(Comments.this,create_on);
                                post_name = jsonObject.getString("Name_cmnt");
                                profile_image = jsonObject.getString("profimg_cmnt");
                                productdto.setComment(comments);
                                productdto.setProfile(profile_image);
                                productdto.setNames(post_name);
                                productdto.setCre(create_on);
                                cmt.add(productdto);
                            }
                               // helper.Addcomnt(commment, postid, posttype, createb, creat_on, profilepic, names);
                            list.setVisibility(View.VISIBLE);
                            Custom_Comment adapter = new Custom_Comment(Comments.this, R.layout.spiner_item, cmt);
                            list.setAdapter(adapter);
                            setListViewHeightBasedOnItems(list);
                               list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                   @Override
                                   public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                   }
                               });


                        } catch (Exception e) {
                            Log.v("EXC",e.getMessage());
                        }

                    }

                    if (eResponse.equals("0")) {
                        //Toast.makeText(Comments.this, "No comments yet!", Toast.LENGTH_LONG).show();

                    } else if (eResponse.equals("1")) {
                        //Toast.makeText(Comments.this, "You have successfully Registered !", Toast.LENGTH_LONG).show();

                    } else {

                        // progressDialog.dismiss();

                    }

                }
            }

    private class PostYums extends AsyncTask<Void, Void, Void> {

        String Expdate="";
        @Override
        protected void onPreExecute() {
            String format = "dd/MMM/yyyy H:mm:ss";
            final SimpleDateFormat sdf = new SimpleDateFormat(format);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Expdate = sdf.format(new Date());
        }

        @Override
        protected Void doInBackground(Void... params) {
            cResponse = com.Addlike("post_like", post_id, ptype, creatby, Expdate);
            Log.i("HomeAdapter Response", cResponse);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String[] Liked = cResponse.split(",");
            if (Liked[0].equals("1")) {
                ifYumed = "blog-btn choose";
                smile.setImageResource(R.drawable.smiling_green);
            }
            else if (Liked[0].equals("2")) {
                smile.setImageResource(R.drawable.smiling_gray);
            }
        }
    }

    public  boolean setListViewHeightBasedOnItems(ListView cmntlist) {

        ListAdapter listAdapter = cmntlist.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.

            int totalItemsHeight = 0;

            for (int itemPos = 0; itemPos < numberOfItems;itemPos++) {
                View item = listAdapter.getView(itemPos, null, cmntlist);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.

            int totalDividersHeight = cmntlist.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.

            ViewGroup.LayoutParams params = cmntlist.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            cmntlist.setLayoutParams(params);
            cmntlist.requestLayout();

            return true;

        } else {

            return false;

        }

    }
        }
