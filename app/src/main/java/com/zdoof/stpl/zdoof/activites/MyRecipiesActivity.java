package com.zdoof.stpl.zdoof.activites;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.CircularNetworkImageView;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.adapters.Custom_receipe;
import com.zdoof.stpl.zdoof.adapters.Customrepc;
import com.zdoof.stpl.zdoof.search.Search_all;
import com.zdoof.stpl.zdoof.search.Search_dish;
import com.zdoof.stpl.zdoof.search.Search_gstore;
import com.zdoof.stpl.zdoof.search.Search_people;
import com.zdoof.stpl.zdoof.search.Search_post;
import com.zdoof.stpl.zdoof.search.Search_resturant;
import com.zdoof.stpl.zdoof.views.Contropanel;
import com.zdoof.stpl.zdoof.webservice.PicConstant;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall;
import com.zdoof.stpl.zdoof.webservice.Webservicereceipe_post;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by stpl on 1/3/16.
 */
public class MyRecipiesActivity extends Activity implements LocationListener{
    ImageView home,conection,notification,Zpanel;
    LinearLayout ll1, ll2, ll3, ll4;
    EditText description;
    boolean isGPSEnabled = false;

    DrawerLayout drawerMainSearch;
    // flag for network status
    boolean isNetworkEnabled = false;
    LinearLayout headerview,ziploc,vieew;
    // flag for GPS status
    boolean canGetLocation = false;
    TextView Share;
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    // private static final long MY_PERMISSION_LOCATION = 10;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    ImageView back;
    ListView list1,drawerList;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    TextView code,username;
    Button submit;
    ProgressDialog progressDialog;
    CircularNetworkImageView prfimg;
    ArrayList<Detail> Dish;
    final WebserviceCall com = new WebserviceCall();
    int Loggin_id,User_id;
    int Sub_user_type, Main_user_type;
    String _name, Last_name="", Createdon, Updated_on, Ip_address,First_name="";
    final Webservicereceipe_post com1 = new Webservicereceipe_post();
    static String bResponse,cResponse;
    ImageView close,plus,serch;
    SharedPreferenceClass sharedPreferenceClass;
    String result,zipcode,locations,pos,des,desc,login_id,user_id,profileimage,fname,lastname,name,pic;
    String sertype[]={"Search Post","Search Foodie","Search Recipe/Dish","Search Restaurant","Search Grocery Store"};
    int sercimg[]={R.drawable.searhicon,R.drawable.sst,R.drawable.ff,R.drawable.sahlist,R.drawable.crt,R.drawable.uur};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_recipe);
        back = (ImageView) findViewById(R.id.imageViewBack);
        ll1 = (LinearLayout) findViewById(R.id.hmj);
        ll2 = (LinearLayout) findViewById(R.id.cnn);
        ll3 = (LinearLayout) findViewById(R.id.ntf);
        ll4 = (LinearLayout) findViewById(R.id.ct);
        drawerMainSearch = (DrawerLayout) findViewById(R.id.drawerMainSearch);
        close = (ImageView) findViewById(R.id.close);
        plus = (ImageView) findViewById(R.id.imageView3);
        serch = (ImageView) findViewById(R.id.serc);
        list1 = (ListView) findViewById(R.id.imagelist);
        drawerList = (ListView) findViewById(R.id.drawerItemList);
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header1 = (ViewGroup) inflater.inflate(R.layout.search_view, drawerList, false);
        drawerList.addHeaderView(header1, null, false);
        code= (TextView) findViewById(R.id.code);
        description = (EditText) findViewById(R.id.imageView11);
        LayoutInflater inflater1 = getLayoutInflater();
        ViewGroup header2 = (ViewGroup) inflater1.inflate(R.layout.recipe_header, list1, false);
        list1.addHeaderView(header2, null, false);
        prfimg= (CircularNetworkImageView) findViewById(R.id.profilePictureView);
        username= (TextView) findViewById(R.id.name);
        sharedPreferenceClass = new SharedPreferenceClass(this);
        login_id = sharedPreferenceClass.getValue_string("UIDD");

        Loggin_id=Integer.parseInt(login_id);
        User_id = Loggin_id;

        Customrepc drawerAdapter = new Customrepc(MyRecipiesActivity.this, R.layout.spiner_item, sertype, sercimg);
        drawerList.setAdapter(drawerAdapter);
//        AsyncCallWS1 task = new AsyncCallWS1();
//        task.execute();
       /* profileimage = sharedPreferenceClass.getValue_string("PHOTO");
        fname = sharedPreferenceClass.getValue_string("FRSTN");
        lastname = sharedPreferenceClass.getValue_string("LSTN");
        name=fname+lastname;*/

        AsyncCallWS2 task1 = new AsyncCallWS2();
        task1.execute();
        LocationManager locationManager = (LocationManager)
                MyRecipiesActivity.this.getSystemService(MyRecipiesActivity.this.LOCATION_SERVICE);
        ActivityCompat.requestPermissions(MyRecipiesActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        // getting GPS status
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
        isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


        if (!isGPSEnabled && !isNetworkEnabled) {
            // no network provider is enabled
        } else {
            this.canGetLocation = true;

            if (isNetworkEnabled) {
                // checkLocationPermission();
                if (ActivityCompat.checkSelfPermission(MyRecipiesActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(MyRecipiesActivity.this,
                                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                        if (location != null) {

                            Geocoder gcd = new Geocoder(MyRecipiesActivity.this, Locale.getDefault());
                            List<Address> list = null;
                            try {
                                list = gcd.getFromLocation(location
                                        .getLatitude(), location.getLongitude(), 1);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if (list != null) {
                                if (list.size() > 0) {
                                    Address address = list.get(0);
                                    result = address.getLocality();
                                    if (address.getPostalCode() != null) {
                                        zipcode = address.getPostalCode();
                                    }
                                    locations = result;
                                }

                            }
                        }
                    }

                }

                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        //checkLocationPermission();
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                Geocoder gcd = new Geocoder(MyRecipiesActivity.this, Locale.getDefault());
                                List<Address> list = null;
                                try {
                                    list = gcd.getFromLocation(location
                                            .getLatitude(), location.getLongitude(), 1);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                if (list != null) {
                                    if (list.size() > 0) {
                                        Address address = list.get(0);
                                        result = address.getLocality();
                                        if (address.getPostalCode() != null) {
                                            zipcode = address.getPostalCode();
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
                // postcomment = zipcode;
            }
        }
        String zzip = sharedPreferenceClass.getValue_string("ZZIP");
        if (!zzip.equals(null) && !zzip.equals("") && !zzip.equals("0")) {
            code.setText(zzip);
        }
        else {
            code.setText(zipcode);
            sharedPreferenceClass.setValue_string("ZZIP", zipcode);
        }
        zipcode = sharedPreferenceClass.getValue_string("ZZIP");
        /*submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });*/
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll1.setBackgroundColor(Color.parseColor("#026D8A"));
                Intent intent = new Intent(MyRecipiesActivity.this, Home.class);
                startActivity(intent);
                finish();
            }

        });
        ll3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ll2.setBackgroundColor(Color.parseColor("#026D8A"));
                Intent intent = new Intent(MyRecipiesActivity.this, Notifications.class);
                startActivity(intent);
                finish();
            }

        });
        ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll2.setBackgroundColor(Color.parseColor("#026D8A"));
                Intent intent = new Intent(MyRecipiesActivity.this, Connecton.class);
                startActivity(intent);
                finish();
            }

        });
        ll4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll4.setBackgroundColor(Color.parseColor("#026D8A"));
                Intent intent = new Intent(MyRecipiesActivity.this, Contropanel.class);
                startActivity(intent);
                finish();
            }

        });
        serch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawerMainSearch.isDrawerOpen(GravityCompat.END)) {
                    drawerMainSearch.openDrawer(GravityCompat.END);
                }
                else {
                    drawerMainSearch.closeDrawer(GravityCompat.END);
                }
            }
        });
        description.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //Log.e("KeyCode:::",""+actionId);
                if (actionId== EditorInfo.IME_ACTION_SEARCH) {
                    serch.performClick();
                    return true;
                }
                return false;
            }
        });
        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                drawerMainSearch.closeDrawer(GravityCompat.END);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setCursorVisible(true);
            }
        });
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                desc = description.getText().toString().trim();
                if (desc.length()<3) {
                    Toast.makeText(MyRecipiesActivity.this, "Please enter atleast 3 letters to search!", Toast.LENGTH_SHORT).show();
                }
                else {
                    pos = drawerList.getItemAtPosition(position).toString();
                    if (pos.equals("0")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(MyRecipiesActivity.this, Search_post.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("1")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(MyRecipiesActivity.this, Search_people.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("2")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(MyRecipiesActivity.this, Search_dish.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("3")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(MyRecipiesActivity.this, Search_resturant.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("4")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(MyRecipiesActivity.this, Search_gstore.class);
                        startActivity(intent);
                        //finish();
                    }
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (drawerMainSearch.isDrawerOpen(GravityCompat.END)) {
            drawerMainSearch.closeDrawer(GravityCompat.END);
        }
        else {
            String back = sharedPreferenceClass.getValue_string("BACKPAGE");
            if (back.equals("controlpanel")) {
                Intent intent = new Intent(MyRecipiesActivity.this, Contropanel.class);
                startActivity(intent);
                finish();
            }
            else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        drawerMainSearch.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    /*class AsyncCallWS1 extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MyRecipiesActivity.this);
            progressDialog.setMessage("loading....");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");
            progressDialog.dismiss();
            cResponse = com.Usersearch("user_search", Loggin_id, First_name, Last_name, Main_user_type, Sub_user_type, Createdon, Updated_on, Ip_address);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            Log.i("TAG", "onPostExecute");
            try {
                JSONArray jr = new JSONArray(cResponse);

                for (int i = 0; i < jr.length(); i++) {

                    JSONObject jsonObject = jr.getJSONObject(i);
                   String userid = jsonObject.getString("User_id").toString();
                    // username = jsonObject.getString("User_name").toString();
                    String totalconnection = jsonObject.getString("total_conn").toString();
                    fname = jsonObject.getString("First_name").toString();
                    lastname = jsonObject.getString("Last_name").toString();
                    name = jsonObject.getString("name").toString();
                    String email = jsonObject.getString("User_name").toString();
                    String wrkas = jsonObject.getString("Main_utype").toString().toUpperCase();
                    profileimage = jsonObject.getString("profimg").toString();

                    zipcode = jsonObject.getString("Zip_code").toString();


                }
                pic= PicConstant.PROFILE_URL1+profileimage;
                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(MyRecipiesActivity.this).build();
                ImageLoader imageLoader = ImageLoader.getInstance();
                ImageLoader.getInstance().init(config);
                DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                        .cacheOnDisc(true).resetViewBeforeLoading(true)
                        // .showImageForEmptyUri(fallback)
                        .showImageOnFail(R.drawable.blankpeople)
                        .showImageOnLoading(R.drawable.uute).build();
//initialize image view
//download and display image from url
                imageLoader.displayImage(pic,prfimg, options);
                name=fname+lastname;
                username.setText(name);
            }catch (Exception e) {
                e.printStackTrace();
            }
        }

    } */
    class AsyncCallWS2 extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... params) {
                Log.i("TAG", "doInBackground");

                bResponse = com1.receipe("MyRecipes", Loggin_id, User_id);
                return null;
            }


            @Override
            protected void onPostExecute(Void result) {
                Log.i("TAG", "onPostExecute");
                try {
                    JSONArray jr = new JSONArray(bResponse);
                   Dish = new ArrayList<Detail>();

                    for (int i = 0; i < jr.length(); i++) {
                        JSONObject jsonObject = jr.getJSONObject(i);
                        Detail pr=new Detail();
                        String Dish_Name = jsonObject.getString("Dish_name");
                        String Dish_picture = jsonObject.getString("Dish_picture");
                        String like=jsonObject.getString("totdish");
                        String dishID = jsonObject.getString("Dish_id");
                        String ifliked = jsonObject.getString("thumbclass");
                        String createdby = jsonObject.getString("Created_by");
                        pr.setDish_namee(Dish_Name);
                        pr.setDish_picture(Dish_picture);
                        pr.setPost_like(like);
                        pr.setDish_id(dishID);
                        pr.setIflike(ifliked);
                        pr.setPostedby(createdby);
                        Dish.add(pr);
                    }
                    Custom_receipe adapter=new Custom_receipe(MyRecipiesActivity.this,R.layout.spiner_item,Dish);
                    list1.setAdapter(adapter);

                   /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(Home.this, android.R.layout.simple_list_item_1, Dish);
                    item.setAdapter(adapter);
                    item.setThreshold(1);
                    item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            diSh_id = Dish1.get(position);
                            ddish_id = Integer.parseInt(diSh_id);
                        }
                    });*/

                    // progressDialog.dismiss();

                } catch (Exception e) {

                }

            }
        }


}
