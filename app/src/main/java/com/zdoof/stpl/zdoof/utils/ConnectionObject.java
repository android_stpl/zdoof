package com.zdoof.stpl.zdoof.utils;

/**
 * Created by Shiftu Bd team1 on 24-Feb-2017.
 */

public class ConnectionObject {
    private String connection_id;
    private String from_userid;
    private String to_userid;
    private String from_username;
    private String to_username;
    private String from_profimg;

    public ConnectionObject() {
    }

    public void setConnection_id(String connection_id) {
        this.connection_id = connection_id;
    }

    public void setFrom_userid(String from_userid) {
        this.from_userid = from_userid;
    }

    public void setTo_userid(String to_userid) {
        this.to_userid = to_userid;
    }

    public void setFrom_username(String from_username) {
        this.from_username = from_username;
    }

    public void setTo_username(String to_username) {
        this.to_username = to_username;
    }

    public void setFrom_profimg(String from_profimg) {
        this.from_profimg = from_profimg;
    }

    public String getConnection_id() {
        return connection_id;
    }

    public String getFrom_userid() {
        return from_userid;
    }

    public String getTo_userid() {
        return to_userid;
    }

    public String getFrom_username() {
        return from_username;
    }

    public String getTo_username() {
        return to_username;
    }

    public String getFrom_profimg() {
        return from_profimg;
    }
}
