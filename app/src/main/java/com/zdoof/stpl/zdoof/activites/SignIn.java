package com.zdoof.stpl.zdoof.activites;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.utils.AppUtil;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

/**
 * Created by sujit on 20/1/16.
 */
public class SignIn extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    EditText usernam, passwor;
    Button signin;
    String username, password;
    String userid, mainusertype, subusertype;
    TextView sigup, frgtpaswrd;
    String First_name, Last_name, Createdon, Updated_on, Ip_address;
    private SharedPreferenceClass sharedPreferences;
    String emailid;
    final WebserviceCall com = new WebserviceCall();
    static String aResponse;
    int  Sub_user_type, Main_user_type;
    String Regd_id;
    ProgressDialog progressdialog;
    private ImageView imgGooglePLus;
    private static final String TAG = SignIn.class.getSimpleName();
    private GoogleApiClient googleApiClient;
    private static final int RC_SIGN_IN = 9001;
    private String googleSignInResponse;
    int google_main_userType=4;
    int google_sub_usertype= 0;
    int google_regd_type = 4;
    int google_created_by = 0;
    String google_created_on;
    String google_ip_addr;
    int google_user_status= 1;
    //String google_zip_code;
    String google_version ;
    String google_device_name ;
    String google_imei_no;
    ProgressDialog googleDilog,facebookDialog;
    String googlePersonName;
    String googleEmail;
    String googleUserID;
    private ImageView imgFacebook;
    private CallbackManager callbackManager;
    String facebookEmail,facebookFName,facebookLName;
    int facebookRegdType = 2;
    int facebook_user_status = 1;
    private String facebookName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(SignIn.this);
        setContentView(R.layout.sign_in);
        String keyHash = AppUtil.printKeyHash(SignIn.this);
        Log.v("KEYHASSHHH",keyHash);
        googleDilog = new ProgressDialog(SignIn.this);
        googleDilog.setMessage("Signing with Google...");
        googleDilog.setCanceledOnTouchOutside(false);
        facebookDialog = new ProgressDialog(SignIn.this);
        facebookDialog.setMessage("Signing with Facebook...");
        facebookDialog.setCanceledOnTouchOutside(false);
        usernam = (EditText) findViewById(R.id.user);
        passwor = (EditText) findViewById(R.id.passwd);
        sigup = (TextView) findViewById(R.id.su);
        frgtpaswrd = (TextView) findViewById(R.id.textView2);
        imgGooglePLus = (ImageView)findViewById(R.id.imgGooglePLus);
        imgGooglePLus.setOnClickListener(this);
        progressdialog = new ProgressDialog(this);
        First_name = "";
        Last_name = "";
        Createdon = "";
        Updated_on = "";
        Ip_address = "";
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        google_created_on = df.format(c.getTime());
        WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
        google_ip_addr = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        google_version = Build.VERSION.RELEASE;
        google_device_name = Build.MODEL;
        google_imei_no = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        imgFacebook = (ImageView)findViewById(R.id.imgFacebook);
        imgFacebook.setOnClickListener(this);
        callbackManager = CallbackManager.Factory.create();
        frgtpaswrd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignIn.this, ForgotPasswordActivity.class);
                startActivity(intent);

            }
        });

        sharedPreferences = new SharedPreferenceClass(this);
        signin = (Button) findViewById(R.id.signin);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = usernam.getText().toString();
                password = passwor.getText().toString();
                if (username.equals("") && password.equals("")) {
                    Toast.makeText(SignIn.this, "Please Give  correct username and password signin", Toast.LENGTH_SHORT).show();
                } else {
                    AsyncCallWS task = new AsyncCallWS();
                    task.execute();
                    progressdialog = ProgressDialog.show(SignIn.this, "", "Loading...");
                }

            }
        });

        sigup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignIn.this, SignUp.class);
                startActivity(intent);
                finish();
            }
        });
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.imgGooglePLus) {
            signIn();
        }
        if (v.getId()==R.id.imgFacebook){
            loginWithFacebook();
        }
    }

    private void loginWithFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile","user_birthday"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e("Response>>",response.toString());
                        try {
                            String email = response.getJSONObject().getString("email");
                            String firstName = response.getJSONObject().getString("first_name");
                            String lastName = response.getJSONObject().getString("last_name");
                            String gender = response.getJSONObject().getString("gender");
                            //String bday= response.getJSONObject().getString("birthday");
                            String facebookUId = loginResult.getAccessToken().getUserId();
                            Profile profile = Profile.getCurrentProfile();
//                            String facebookUserId = profile.getId();
//                            Log.v("FACEBOOKID",facebookUserId);
//                            String link = profile.getLinkUri().toString();
//                            Log.i("FacebookLink",link);
                            if (Profile.getCurrentProfile()!=null)
                            {
                                Log.i("FacebookLogin", "ProfilePic" + Profile.getCurrentProfile().getProfilePictureUri(200, 200));
                            }

                            Log.e("FACEBOOUSERDATA", email+"//"+firstName+"//"+lastName+"//"+gender+"//"+facebookUId+"//");
                            facebookEmail = email;
                            facebookFName = firstName;
                            facebookLName = lastName;
                            facebookName = facebookFName+" " + facebookLName;
                            new FacebookSignINTask(SignIn.this).execute();
                        } catch (JSONException e) {
                            Log.e("ERRORT",e.getMessage());
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,email,first_name,last_name,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.v("Cancelled","Cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("ERR>>",error.getMessage());
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
            Log.v("Failed",connectionResult.toString());
    }


    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            aResponse = com.SignIn("user_login_search", username, password);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");

            try {
                JSONArray jr = new JSONArray(aResponse);

                for (int i = 0; i < jr.length(); i++) {

                    JSONObject jsonObject = jr.getJSONObject(i);
                    userid = jsonObject.getString("User_id").toString();
                    username = jsonObject.getString("User_name").toString();
                    emailid = jsonObject.getString("Email").toString();
                    mainusertype = jsonObject.getString("Main_user_type").toString();
                    subusertype = jsonObject.getString("Sub_user_type").toString();
                    sharedPreferences.setValue_string("UIDD", userid);
                    sharedPreferences.setValue_string("URN", username);
                    sharedPreferences.setValue_string("EML", emailid);
                }

            } catch (Exception e) {
            }

            if (aResponse.equals("0")) {
                progressdialog.dismiss();
                Toast.makeText(getApplicationContext(), "Please Give  correct username and password signin", Toast.LENGTH_LONG).show();

            }  else {
                Regd_id =userid;
                Sub_user_type = Integer.parseInt(subusertype);
                Main_user_type = Integer.parseInt(mainusertype);
                Intent i = new Intent(SignIn.this, Home.class);
                startActivity(i);

                sharedPreferences.setValue_boolean("Loginstatus", true);
                progressdialog.dismiss();
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
        //googleApiClient.connect();
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }else {
            callbackManager.onActivityResult(requestCode,resultCode,data);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            Log.e(TAG, "display name: " + acct.getDisplayName());

            googlePersonName = acct.getDisplayName();
            //String personPhotoUrl = acct.getPhotoUrl().toString();
             googleEmail = acct.getEmail();
             googleUserID = acct.getId();
            Log.e("GOOGLEPLUS", "Name: " + googlePersonName + ", email: " + googleEmail
                    + ", Image: " + ""+"ID::" +googleUserID);
            Regd_id = googleUserID;
            new GooglePlusSignInTask(this).execute();
        } else {
            Log.v("ERROR","error");
            // Signed out, show unauthenticated UI.
            //updateUI(false);
        }
    }
    private class GooglePlusSignInTask extends AsyncTask<Void,Void,String>{
        private WeakReference<SignIn> weakReference;
        public GooglePlusSignInTask(SignIn signIn){
            weakReference = new WeakReference<SignIn>(signIn);
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            googleDilog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            googleSignInResponse = com.Signup("signup_insert", googleEmail, "", google_main_userType, google_sub_usertype, google_regd_type, googlePersonName, "", google_created_by, google_created_on, google_ip_addr, google_user_status, "", google_version, google_device_name, google_imei_no);
//            String[] response = googleSignInResponse.split(",");
//            String response1 = response[0];
            return googleSignInResponse;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (weakReference.get()!=null){
                googleDilog.dismiss();
                if (!TextUtils.isEmpty(s)){
                    String[] stringParts = s.split(",");
                    String responseType = stringParts[0];
                    String generatedUserId = stringParts[1];
                    if (responseType.equals("1")) {
                        sharedPreferences.setValue_string("UIDD", generatedUserId);
                        sharedPreferences.setValue_string("URN", googlePersonName);
                        sharedPreferences.setValue_string("EML", googleEmail);
                        sharedPreferences.setValue_boolean("Loginstatus", true);
                        Intent intent = new Intent(SignIn.this, Home.class);
                        startActivity(intent);
                    }else if (responseType.equals("0")){
                        Toast.makeText(SignIn.this,"Please Try again!.",Toast.LENGTH_SHORT).show();
                    }else if (responseType.equals("5")){
                        Toast.makeText(SignIn.this,"User already exist.",Toast.LENGTH_SHORT).show();
                        sharedPreferences.setValue_string("UIDD", generatedUserId);
                        sharedPreferences.setValue_string("URN", googlePersonName);
                        sharedPreferences.setValue_string("EML", googleEmail);
                        sharedPreferences.setValue_boolean("Loginstatus", true);
                        Intent intent = new Intent(SignIn.this, Home.class);
                        startActivity(intent);
                    }
                }
            }
        }
    }
    private class FacebookSignINTask extends AsyncTask<Void,Void,String>{
        private WeakReference<SignIn> signInWeakReference;
        public FacebookSignINTask(SignIn signIn){
            signInWeakReference = new WeakReference<SignIn>(signIn);
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            facebookDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String facebookResultResponse = com.Signup("signup_insert", facebookEmail, "", google_main_userType, google_sub_usertype, facebookRegdType, facebookFName, facebookLName, google_created_by, google_created_on, google_ip_addr, facebook_user_status, "", google_version, google_device_name, google_imei_no);
            return facebookResultResponse;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            facebookDialog.dismiss();
            if (signInWeakReference.get()!=null){
                Log.v("RESPONSEFACEBOOK",result);
                if (!TextUtils.isEmpty(result)){
                    String[] stringParts = result.split(",");
                    String responseType = stringParts[0];
                    String generatedUserId = stringParts[1];
                    if (responseType.equals("1")) {
                        sharedPreferences.setValue_string("UIDD", generatedUserId);
                        sharedPreferences.setValue_string("URN", facebookName);
                        sharedPreferences.setValue_string("EML", facebookEmail);
                        sharedPreferences.setValue_boolean("Loginstatus", true);
                        Intent intent = new Intent(SignIn.this, Home.class);
                        startActivity(intent);
                    }else if (responseType.equals("0")){
                        Toast.makeText(SignIn.this,"Please Try again!.",Toast.LENGTH_SHORT).show();
                    }else if (responseType.equals("5")){
                        Toast.makeText(SignIn.this,"User already exist.",Toast.LENGTH_SHORT).show();
                        sharedPreferences.setValue_string("UIDD", generatedUserId);
                        sharedPreferences.setValue_string("URN", facebookName);
                        sharedPreferences.setValue_string("EML", facebookEmail);
                        sharedPreferences.setValue_boolean("Loginstatus", true);
                        Intent intent = new Intent(SignIn.this, Home.class);
                        startActivity(intent);
                    }
                }
            }
        }
    }
}
