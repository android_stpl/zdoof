package com.zdoof.stpl.zdoof;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

 
public class CircularNetworkImageView extends ImageView {
	//private int borderWidth=0;
    private int viewWidth;
    private int viewHeight;
    int defaultsize;
    int circleRadius;
	private Bitmap image;
	//private Paint paint;
	//private Paint paintBorder;
    private Boolean isScaled = false;

	public CircularNetworkImageView(final Context context) {
        super(context);
        setup();
	}

	public CircularNetworkImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
	}

	public CircularNetworkImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		// init paint
        setup();
	}

    private void setup()
    {
        isScaled = false;
        // init paint
//        paint = new Paint();
//        paint.setAntiAlias(true);
//
//        paintBorder = new Paint();
//        setBorderColor(Color.parseColor("#cccccc"));
//        paintBorder.setAntiAlias(true);
    }




	@Override
	public void onDraw(Canvas canvas) {
        Drawable d = getDrawable();
        if (image==null) {
            image = drawableToBitmap(getDrawable());
        }

        if (d!=null && getWidth()!=0 && getHeight()!=0 && image!=null) {
            int w = getWidth();
            int h = getHeight();
            defaultsize = w;
            if(h<defaultsize) {defaultsize =h;}
            circleRadius = defaultsize / 2;
            if(image.getWidth()!=defaultsize && image.getHeight()!=defaultsize) {
                image = scaleImage(image,defaultsize,defaultsize);
                //BitmapShader shader = new BitmapShader(image, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                //paint.setShader(shader);
            }

            // circleCenter is the x or y of the view's center
            // radius is the radius in pixels of the cirle to be drawn
            // paint contains the shader that will texture the shape
            canvas.drawBitmap(image, 0, 0, null);
        }
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int width = measureWidth(widthMeasureSpec);
		int height = measureHeight(heightMeasureSpec);
        viewWidth = width;
        viewHeight = height;
		setMeasuredDimension(width, height);
	}

	private int measureWidth(int measureSpec) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);

		if (specMode == MeasureSpec.EXACTLY) {
			// The parent has determined an exact size for the child.
			result = specSize;
		} else if (specMode == MeasureSpec.AT_MOST) {
			// The child can be as large as it wants up to the specified size.
			result = specSize;
		} else {
			// The parent has not imposed any constraint on the child.
			result = viewWidth;
		}

		return result;
	}

	private int measureHeight(int measureSpecHeight) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpecHeight);
		int specSize = MeasureSpec.getSize(measureSpecHeight);

		if (specMode == MeasureSpec.EXACTLY) {
			// We were told how big to be
			result = specSize;
		} else if (specMode == MeasureSpec.AT_MOST) {
			// The child can be as large as it wants up to the specified size.
			result = specSize;
		} else {
			// Measure the text (beware: ascent is a negative number)
			result = viewHeight;
		}

		return (result + 2);
	}

    private Bitmap scaleImage(Bitmap bitmap,int newWidth, int newHeight){
        /**
         * Scales the provided bitmap to have the height and width provided.
         * (Alternative method for scaling bitmaps
         * since Bitmap.createScaledBitmap(...) produces bad (blocky) quality bitmaps.)
         *
         * @param bitmap is the bitmap to scale.
         * @param newWidth is the desired width of the scaled bitmap.
         * @param newHeight is the desired height of the scaled bitmap.
         * @return the scaled bitmap.
         */
        Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Bitmap.Config.ARGB_8888);

        float scaleX = newWidth / (float) bitmap.getWidth();
        float scaleY = newHeight / (float) bitmap.getHeight();
        float pivotX = 0;
        float pivotY = 0;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));
        return scaledBitmap;
    }

	public Bitmap drawableToBitmap(Drawable drawable) {
		if (drawable == null) {
			return null;
		}
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int radius = 0;
        if (width>=height){
            bitmap = Bitmap.createBitmap(bitmap,(width/2)-(height/2),0,height,height);
        } else {
            bitmap = Bitmap.createBitmap(bitmap,0,(height/2)-(width/2),width,width);
        }
        width = bitmap.getWidth();
        height = bitmap.getHeight();
        radius = width/2;
        Bitmap circular = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        BitmapShader shader = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
        Paint paint = new Paint();
        paint.setShader(shader);
        paint.setAntiAlias(true);
        Canvas c = new Canvas(circular);
        c.drawCircle(width/2, height/2, radius, paint);
        return circular;
	}
}