package com.zdoof.stpl.zdoof.adapters;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.database.DatabaseHelper;

import java.util.ArrayList;

/**
 * Created by stpl on 25/2/16.
 */
public class Customdish extends BaseAdapter{
    FragmentActivity post;
    ArrayList<Detail> val;
    AutoCompleteTextView ingr;
    TextView uu;
    Spinner uts;
    ImageView imm;
    DatabaseHelper helper;
    Button cancel,editt;
    EditText qntyi,unts1;
    String[] Ingredient={"ZUCCHINI","CHANNA DAL","SPINCH PASTE","CHICKEN BONELESS","KASHMIRI MIRCH"};
    String[]units={"GMS","LTR","LBS","OZ","CUP","DROPS","OTHERS"};
    String id,ingrediants1,quantity1,units11;
    public Customdish(FragmentActivity post, int simple_list_item_1, ArrayList<Detail> val) {
        this.post=post;
        this.val=val;
    }

    @Override
    public int getCount() {
        return val.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = post.getLayoutInflater();
        view = inflater.inflate(R.layout.customdish, null);
        helper=new DatabaseHelper(post);
        TextView ing= (TextView) view.findViewById(R.id.textView17);
        TextView qnty=(TextView) view.findViewById(R.id.textView18);
        final TextView unts=(TextView) view.findViewById(R.id.textView22);
        final ImageView edit= (ImageView) view.findViewById(R.id.imageView19);
        ImageView del= (ImageView) view.findViewById(R.id.imageView26);

        ing.setText(val.get(position).getIngradiant());
        qnty.setText(val.get(position).getQuantity());
        unts.setText(val.get(position).getUnit());
        id=val.get(position).getId();

        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(post);
                alert.setTitle("Alert!!");
                alert.setMessage("Are you sure to remove this item");
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do your work here
                        if (helper.DeleteSingle(id)) {
                            //Toast.makeText(post, "removed successfull", Toast.LENGTH_SHORT).show();
                            post.recreate();

                        } else{

                        }
                        dialog.dismiss();

                    }
                });
                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                alert.show();

                //eturn true;
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(post);
                dialog.setContentView(R.layout.editdialog);
                dialog.setTitle("Edit Ingrediants !!");
                 ingr= (AutoCompleteTextView) dialog.findViewById(R.id.textView19);
                 qntyi= (EditText) dialog.findViewById(R.id.textView20);
                // unts1=(EditText) dialog.findViewById(R.id.textView21);
                 uu= (TextView) dialog.findViewById(R.id.ss);
                 uts= (Spinner) dialog.findViewById(R.id.spinner);
                imm= (ImageView) dialog.findViewById(R.id.imageView10);
                // cancel= (Button) dialog.findViewById(R.id.button2);
                 editt= (Button) dialog.findViewById(R.id.button2);
                 ingr.setText(val.get(position).getIngradiant());
                 qntyi.setText(val.get(position).getQuantity());
                uu.setText(val.get(position).getUnit());
                imm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        uts.setVisibility(View.VISIBLE);
                        ArrayAdapter adapter1 = new ArrayAdapter(post, android.R.layout.simple_list_item_1, units);
                        uts.setAdapter(adapter1);
                    }
                });

                 ArrayAdapter adapter = new ArrayAdapter(post,android.R.layout.simple_list_item_1,Ingredient);
                 ingr.setAdapter(adapter);
                 ingr.setThreshold(1);
                 /*cancel.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       dialog.dismiss();
                   }
               });*/
              editt.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {

                     /* ingrediants1=ingr.getText().toString();
                      quantity1=qntyi.getText().toString();
                      units11=unts1.getText().toString();
                      helper.upDate_byid(id,ingrediants1,quantity1,units11);
                      Toast.makeText(post,"Successfully Updated",Toast.LENGTH_SHORT).show();
                      notifyDataSetChanged();*/
                      dialog.dismiss();
                  }
              });
                dialog.show();
            }
        });
        return view;
    }
}
