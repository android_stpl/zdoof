package com.zdoof.stpl.zdoof.adapters;

import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;

import java.util.ArrayList;

/**
 * Created by stpl on 10/3/16.
 */
public class CustomZlikes extends BaseAdapter{
    FragmentActivity activity;
    ArrayList<Detail> detl;
    String pic,pic1,Likedto;
    public CustomZlikes(FragmentActivity activity, int spiner_item, ArrayList<Detail> detl) {
        this.activity=activity;
        this.detl=detl;

    }

    @Override
    public int getCount() {
        return detl.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = activity.getLayoutInflater();
        view = inflater.inflate(R.layout.customzlikes, null);
        TextView text= (TextView) view.findViewById(R.id.likto);
        TextView  img= (TextView ) view.findViewById(R.id.ype);
        TextView tm= (TextView) view.findViewById(R.id.liedon);
        Likedto=detl.get(position).getLikedto();
        if(Likedto.equals("null")){
            text.setText("NA");
        }else{
            text.setText(Html.fromHtml(Likedto));
        }

         pic=detl.get(position).getType();
         img.setText(pic);
       /* pic1= PicConstant.PROFILE_URL1+pic;
        Uri myUri = Uri.parse(pic1);
        Picasso.with(activity)
                .load(myUri)
                .placeholder(R.drawable.load)
                .error(R.drawable.blankpeople)
                .into(img);*/
        tm.setText(Html.fromHtml(detl.get(position).getLikedon()));
        return view;
    }
}
