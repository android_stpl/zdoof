package com.zdoof.stpl.zdoof.search;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zdoof.stpl.zdoof.R;

/**
 * Created by sujit on 23/4/16.
 */
public class CustomSearch12 extends BaseAdapter{
    Search_people search_people;
    String[] sertype;
    int[] sercimg;
    public CustomSearch12(Search_people search_people, int spiner_item, String[] sertype, int[] sercimg) {
        this.search_people=search_people;
        this.sertype=sertype;
        this.sercimg=sercimg;
    }

    @Override
    public int getCount() {
        return sertype.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vgrid;
        LayoutInflater inflater = search_people.getLayoutInflater();
        vgrid = inflater.inflate(R.layout.custom_serdesign, null);
        ImageView image= (ImageView) vgrid.findViewById(R.id.imageView29);
        TextView text= (TextView) vgrid.findViewById(R.id.textView27);
        image.setImageResource(sercimg[position]);
        text.setText(sertype[position]);

        return vgrid;
    }
}
