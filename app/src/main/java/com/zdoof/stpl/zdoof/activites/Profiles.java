package com.zdoof.stpl.zdoof.activites;
import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;
import com.zdoof.stpl.zdoof.CircularNetworkImageView;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.fragmentpageradapter.Profile_page_adapter;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.searchadapters.CustomSearch2;
import com.zdoof.stpl.zdoof.search.Search_dish;
import com.zdoof.stpl.zdoof.search.Search_gstore;
import com.zdoof.stpl.zdoof.search.Search_people;
import com.zdoof.stpl.zdoof.search.Search_post;
import com.zdoof.stpl.zdoof.search.Search_resturant;
import com.zdoof.stpl.zdoof.utils.AppUtil;
import com.zdoof.stpl.zdoof.utils.ImageListener;
import com.zdoof.stpl.zdoof.views.Contropanel;
import com.zdoof.stpl.zdoof.webservice.PicConstant;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall1;
import com.zdoof.stpl.zdoof.webservice.Webservicereceipe_post;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by stpl on 23/1/16.
 */
public class Profiles extends AppCompatActivity implements LocationListener,ImageListener {
    ImageView home, connection, notification, contlpnl, back,close;
    ImageView propic, serch, edit;
    Button b,b1;
    LinearLayout ll1, ll2, ll3, ll4;
    RelativeLayout editll;
    TextView textt;

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 6;
    SharedPreferenceClass sharedPreferenceClass;
    String fname, lname, name, pic, workas, gender, country, city1,Expdate;
    TextView nam, tcnt;
    String img, nname;
    ListView drawerList;
    Boolean A;
    String B;

    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;
    LinearLayout headerview,ziploc,vieew;
    // flag for GPS status
    boolean canGetLocation = false;
    TextView Share;
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    // private static final long MY_PERMISSION_LOCATION = 10;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    ImageView addbutton;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    Button connect;

    int Sub_user_type, Main_user_type;
    String _name, Last_name, Createdon, Updated_on, Ip_address;
    String c_status="";
    String[] c_connDtl = {"","","",""};
    String tcant,result,locations;
    ArrayList<Detail> detl;
    String rid, totalcont,Created_on;
    String userid,email,wrkas,profileimg,contry;
    Boolean editt;
    int Regd_id,from_id,Connection_type=1;
    static String bResponse;
    ProgressDialog progressDialog;
    Dialog dialog;
    String firstname, states, propict, mnutype, subutype, pic1, pic3, type, password, addrsline1, addressln2, zipcode, dob, Marital_status, Mobile;
    ImageView circularImageView;
    ImageView typeImage;
    static String aResponse;
    final WebserviceCall com = new WebserviceCall();
    private final int LOCATION_PERMISSION_CODE = 1;
    EditText description,zips;
    TextView code;
    Button submit;
    String First_name, emailid, pflpic, lastn, tcnt1, desc,city,searchvalue;
    String totalconnection;
    String sertype[]={"Search Post","Search Foodie","Search Recipe/Dish","Search Restaurant","Search Grocery Store"};
    int sercimg[]={R.drawable.searhicon,R.drawable.sst,R.drawable.ff,R.drawable.sahlist,R.drawable.crt,R.drawable.uur};
    android.support.v4.widget.DrawerLayout drawerMainSearch;
    String pos,des;
    public static Boolean aboutLoaded, postLoaded, zlikesLoaded, zfansLoaded, recipesLoaded;
    final WebserviceCall1 com1 = new WebserviceCall1();
    final Webservicereceipe_post com_post = new Webservicereceipe_post();
    static String cResponse;
    static String dResponse;
    int connID;
    int profImgPx;
    static String profLink;
    //TextView txtCoonectionResult ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        progressDialog = new ProgressDialog(Profiles.this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...Please wait!");

        profImgPx = getResources().getDimensionPixelSize(R.dimen.pxProfImg);
        home = (ImageView) findViewById(R.id.hme);
        connection = (ImageView) findViewById(R.id.hm);
        notification = (ImageView) findViewById(R.id.cnct);
        contlpnl = (ImageView) findViewById(R.id.ctrl);
        back = (ImageView) findViewById(R.id.imageView6);
        nam = (TextView) findViewById(R.id.name);
        tcnt = (TextView) findViewById(R.id.tcc);
        edit = (ImageView) findViewById(R.id.edit);
        ll1 = (LinearLayout) findViewById(R.id.hmj);
        ll2 = (LinearLayout) findViewById(R.id.cnn);
        ll3 = (LinearLayout) findViewById(R.id.ntf);
        ll4 = (LinearLayout) findViewById(R.id.ct);
        drawerMainSearch = (DrawerLayout) findViewById(R.id.drawerMainSearch);
        //txtCoonectionResult = (TextView)findViewById(R.id.txtCoonectionResult);
        connect = (Button) findViewById(R.id.connectBtn);
        // list= (ListView) findViewById(R.id.li);

        close= (ImageView) findViewById(R.id.close);
        //plus= (ImageView) findViewById(R.id.imageView3);
        //  b= (Button) findViewById(R.id.connect);
        // b= (Button) findViewById(R.id.connect1);
        //edit=(TextView) findViewById(R.id.lr);
        description = (EditText) findViewById(R.id.imageView11);
        serch = (ImageView) findViewById(R.id.serc);

        drawerList= (ListView) findViewById(R.id.drawerItemList);
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header1 = (ViewGroup)inflater.inflate(R.layout.search_view, drawerList, false);
        drawerList.addHeaderView(header1, null, false);
        CustomSearch2 drawerAdapter = new CustomSearch2(Profiles.this, R.layout.spiner_item, sertype, sercimg);
        drawerList.setAdapter(drawerAdapter);
        textt= (TextView) findViewById(R.id.textt);
        ziploc= (LinearLayout) findViewById(R.id.ziploc);
        vieew= (LinearLayout)findViewById(R.id.view);
        zips= (EditText) findViewById(R.id.zip);
        submit= (Button) findViewById(R.id.submit);
        code= (TextView) findViewById(R.id.code);
        circularImageView = (ImageView) findViewById(R.id.profilePictureView);
        typeImage = (ImageView) findViewById(R.id.imageView15);
        sharedPreferenceClass = new SharedPreferenceClass(this);
        searchvalue=sharedPreferenceClass.getValue_string("SEARCHVALUE");

        if(aboutLoaded==null) {
            aboutLoaded = false;
        }
        if (postLoaded==null) {
            postLoaded=false;
        }
        if (zlikesLoaded==null){
            zlikesLoaded=false;
        }
        if (zfansLoaded==null) {
            zfansLoaded=false;
        }
        if (recipesLoaded==null) {
            recipesLoaded=false;
        }

        if(searchvalue.equals("")){
            description.setText("");
            close.setVisibility(View.GONE);
        }else if(searchvalue.equals("0")){
            description.setText("");
            close.setVisibility(View.GONE);
        }
        else {
            description.setText(searchvalue);
            close.setVisibility(View.VISIBLE);
        }
        rid = sharedPreferenceClass.getValue_string("UIDD");
        from_id=Integer.parseInt(rid);
        Regd_id = Integer.parseInt(getIntent().getStringExtra("USERID"));
        if (from_id==Regd_id){
            //plus.setVisibility(View.GONE);
            connect.setVisibility(View.GONE);
            edit.setVisibility(View.VISIBLE);
        }else {
            //plus.setVisibility(View.VISIBLE);
            connect.setVisibility(View.VISIBLE);
            edit.setVisibility(View.GONE);
        }
        sharedPreferenceClass.setValue_string("UID",String.valueOf(Regd_id));
        A=getIntent().getBooleanExtra("VALUE",true);
        B=getIntent().getStringExtra("HTM");
        sharedPreferenceClass.setValue_string("UDDIDD", String.valueOf(Regd_id));
        First_name = "";
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText("ABOUT"));
        tabLayout.addTab(tabLayout.newTab().setText("my Posts"));
        tabLayout.addTab(tabLayout.newTab().setText("my Yums"));
        tabLayout.addTab(tabLayout.newTab().setText("my Fans"));
        tabLayout.addTab(tabLayout.newTab().setText("my Recipes"));
        // tabLayout.addTab(tabLayout.newTab().setText("Experience"));
        //  tabLayout.addTab(tabLayout.newTab().setText("Make Payment"));
        tabLayout.setTabGravity(TabLayout.MODE_SCROLLABLE);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        Profile_page_adapter adapter = new Profile_page_adapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(5);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }
        });
        //Task For all users which is not necesssary
        AsyncCallWS1 task = new AsyncCallWS1();
        //task.execute();


        String format = "dd/MMM/yyyy H:mm:ss";
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Expdate = sdf.format(new Date());
        Created_on=Expdate;
        //connection Work
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(c_status.equals("0")) {
                    AsyncCallWS3 task = new AsyncCallWS3();
                    task.execute();
                }
                else if(c_status.equals("3")) {
                    if (from_id==Integer.parseInt(c_connDtl[1])) {
                        //request sent
                        requestSentDialog();
                    }
                    else if (from_id==Integer.parseInt(c_connDtl[2])) {
                        //accept request
                        acceptRequestDialog();
                    }
                }
                else if(c_status.equals("1")) {
                    //disconnect
                    disconnectDialog();
                }


            }
        });


        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Profiles.this, EditMyDetails.class);
                startActivity(intent);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zipcode =zips.getText().toString();
                code.setText(zipcode);
                sharedPreferenceClass.setValue_string("ZZIP", zipcode);
                ziploc.setVisibility(View.GONE);
                vieew.setVisibility(View.GONE);
            }
        });

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                desc = description.getText().toString().trim();
                if (desc.length()<3) {
                    Toast.makeText(Profiles.this, "Please enter atleast 3 letters to search!", Toast.LENGTH_SHORT).show();
                }
                else {
                    pos = drawerList.getItemAtPosition(position).toString();
                    if (pos.equals("0")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(Profiles.this, Search_post.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("1")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(Profiles.this, Search_people.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("2")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(Profiles.this, Search_dish.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("3")) {
                        Intent intent = new Intent(Profiles.this, Search_resturant.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("4")) {
                        Intent intent = new Intent(Profiles.this, Search_gstore.class);
                        startActivity(intent);
                        //finish();
                    }
                }
            }
        });
        textt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ziploc.setVisibility(View.VISIBLE);
                vieew.setVisibility(View.VISIBLE);
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setText("");
                sharedPreferenceClass.setValue_string("SEARCHVALUE","0");
                close.setVisibility(View.GONE);
            }
        });
        serch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawerMainSearch.isDrawerOpen(GravityCompat.END)) {
                    drawerMainSearch.openDrawer(GravityCompat.END);
                }
                else {
                    drawerMainSearch.closeDrawer(GravityCompat.END);
                }
            }
        });
        description.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId== EditorInfo.IME_ACTION_SEARCH) {
                    serch.performClick();
                    return true;
                }
                return false;
            }
        });
        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                drawerMainSearch.closeDrawer(GravityCompat.END);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setCursorVisible(true);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ll1.setBackgroundColor(Color.parseColor("#026D8A"));
                Intent intent = new Intent(Profiles.this, Home.class);
                startActivity(intent);
                finish();
            }

        });
        ll3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ll2.setBackgroundColor(Color.parseColor("#026D8A"));
                Intent intent = new Intent(Profiles.this, Notifications.class);
                startActivity(intent);
                finish();
            }

        });
        ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ll2.setBackgroundColor(Color.parseColor("#026D8A"));
                Intent intent = new Intent(Profiles.this, Connecton.class);
                startActivity(intent);
                finish();
            }

        });
        ll4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ll4.setBackgroundColor(Color.parseColor("#026D8A"));
                Intent intent = new Intent(Profiles.this, Contropanel.class);
                startActivity(intent);
                finish();
            }

        });

        String z_zip = sharedPreferenceClass.getValue_string("ZZIP");
        if (!z_zip.equals(null) && !z_zip.equals("") && !(z_zip.length()<2)) {
            code.setText(z_zip);
            zipcode = z_zip;
        } else {
            if (AppUtil.GetLocationPermission(this)) {
                GetZipcodeTask zipTask = new GetZipcodeTask();
                zipTask.execute();
            }
            else {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CODE);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        drawerMainSearch.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onBackPressed() {
        if (drawerMainSearch.isDrawerOpen(GravityCompat.END)) {
            drawerMainSearch.closeDrawer(GravityCompat.END);
        }
        else {
            String back = sharedPreferenceClass.getValue_string("BACKPAGE");
            if (back.equals("controlpanel")) {
                Intent intent = new Intent(Profiles.this, Contropanel.class);
                sharedPreferenceClass.setValue_string("BACKPAGE","");
                startActivity(intent);
                finish();
            }
            else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==LOCATION_PERMISSION_CODE) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //getZipcodeOfLocation();
                GetZipcodeTask zipcodeTask = new GetZipcodeTask();
                zipcodeTask.execute();
            } else {
                Toast.makeText(Profiles.this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        }
    }

    private class GetZipcodeTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... params) {
            getZipcodeOfLocation();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String zzip = sharedPreferenceClass.getValue_string("ZZIP");
            if (!zzip.equals(null) && !zzip.equals("") && !zzip.equals("0")) {
                code.setText(zzip);
            }
            else {
                code.setText(zipcode);
                sharedPreferenceClass.setValue_string("ZZIP", zipcode);
            }
            zipcode = sharedPreferenceClass.getValue_string("ZZIP");
        }
    }

    private void getZipcodeOfLocation() {
        LocationManager manager = (LocationManager) Profiles.this.getSystemService(LOCATION_SERVICE);
        // getting GPS status
        isGPSEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        isNetworkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Geocoder geocoder = new Geocoder(Profiles.this, Locale.getDefault());
        if (
                ActivityCompat.checkSelfPermission(Profiles.this, Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(Profiles.this, Manifest.permission.ACCESS_COARSE_LOCATION)==PackageManager.PERMISSION_GRANTED) {
            if (isNetworkEnabled) {
                location = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location!=null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    try {
                        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 5);
                        if (addresses!=null) {
                            if (addresses.size()>0) {
                                for (int i=0; i<addresses.size(); i++){
                                    if (addresses.get(i).getPostalCode()!=null) {
                                        if (addresses.get(i).getPostalCode().length()>1) {
                                            zipcode = addresses.get(i).getPostalCode();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        else {
            ActivityCompat.requestPermissions(Profiles.this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CODE);
        }
    }

    private void requestSentDialog() {
        dialog = new Dialog(Profiles.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.request_sent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        Button yes = (Button) dialog.findViewById(R.id.sentYes);
        Button no = (Button) dialog.findViewById(R.id.sentNo);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisconnectTask task = new DisconnectTask();
                task.execute();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void acceptRequestDialog() {
        dialog = new Dialog(Profiles.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setContentView(R.layout.accept_request);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        Button yes = (Button) dialog.findViewById(R.id.acceptYes);
        Button no = (Button) dialog.findViewById(R.id.acceptNo);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connID = Integer.parseInt(c_connDtl[0]);
                AcceptRequestTask task = new AcceptRequestTask();
                task.execute();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void disconnectDialog() {
        dialog = new Dialog(Profiles.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setContentView(R.layout.disconnect_request);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        Button yes = (Button) dialog.findViewById(R.id.acceptYes);
        Button no = (Button) dialog.findViewById(R.id.acceptNo);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisconnectTask task = new DisconnectTask();
                task.execute();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    @Override
    public void loadImage(String s) {
        profLink = s;
        ImageLoadingTask imgTask = new ImageLoadingTask();
        imgTask.execute();
//        if (!TextUtils.isEmpty(s)){
//            Picasso.with(Profiles.this).load(s).resize(400,400).placeholder(R.drawable.profile).error(R.drawable.profile).into(circularImageView);
//        }else {
//            circularImageView.setImageDrawable(getResources().getDrawable(R.drawable.profile));
//        }
    }

    @Override
    public void loadName(String name) {
        nam.setText(name);
    }

    @Override
    public void connectionStatus(String status, String[] connDtl) {
        c_status = status;
        if (connDtl.length==4) {
            c_connDtl = connDtl;
        }
        if(status.equals("3")) {
            if(c_status.equals("3")) {
                if (from_id==Integer.parseInt(c_connDtl[1])) {
                    connect.setText(R.string.connect_sent);
                }
                else if (from_id==Integer.parseInt(c_connDtl[2])) {
                    connect.setText(R.string.connect_accept);
                }
            }
        }
        else if (status.equals("1")) {
            connect.setText(R.string.disconnect);
        }
        else if (status.equals("0")) {
            connect.setText(R.string.connect);
        }
    }

    @Override
    public ProgressDialog getProgressDialog() {
        return this.progressDialog;
    }

    @Override
    public void dismissProgressDialog() {
//        if (aboutLoaded && postLoaded && zlikesLoaded && zfansLoaded && recipesLoaded) {
//            this.progressDialog.dismiss();
//        }
        if (this.progressDialog.isShowing()) {
            this.progressDialog.dismiss();
        }
    }

    @Override
    public void loadUType(String utype) {
        String uTYP = utype;
        if(uTYP.toLowerCase().equals("foodie")) {
            typeImage.setImageResource(R.drawable.utype2);
        }
        else if (uTYP.toLowerCase().equals("culinary hobbyist")) {
            typeImage.setImageResource(R.drawable.utype3);
        }
        else if (uTYP.toLowerCase().equals("chef")) {
            typeImage.setImageResource(R.drawable.utype1);
        }
    }

    class AsyncCallWS1 extends AsyncTask<Void, Void, Void> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(Profiles.this, "Loading", "Please wait");
            progressDialog.setCancelable(false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");
            bResponse = com.Usersearch("user_search", Regd_id, First_name, Last_name, Main_user_type, Sub_user_type, Createdon, Updated_on, Ip_address);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {



            Log.i("TAG", "onPostExecute");
            try {
                JSONArray jr = new JSONArray(bResponse);

                for (int i = 0; i < jr.length(); i++) {

                    JSONObject jsonObject = jr.getJSONObject(i);
                    userid = jsonObject.getString("User_id").toString();
                    // username = jsonObject.getString("User_name").toString();
                    totalconnection = jsonObject.getString("My_connection").toString();
                    firstname = jsonObject.getString("First_name").toString();
                    lname = jsonObject.getString("Last_name").toString();
                    name = jsonObject.getString("name").toString();
                    email = jsonObject.getString("User_name").toString();
                    wrkas = jsonObject.getString("UTYPE").toString().toUpperCase();
                    profileimg = jsonObject.getString("profimg").toString();
                    contry = jsonObject.getString("Country").toString();
                    gender = jsonObject.getString("gen").toString();
                    city = jsonObject.getString("City").toString();
                    password = jsonObject.getString("Password").toString();
                    addrsline1 = jsonObject.getString("Address_line1").toString();
                    addressln2 = jsonObject.getString("Address_line2").toString();
                    zipcode = jsonObject.getString("Zip_code").toString();
                    dob = jsonObject.getString("DOB").toString();
                    states = jsonObject.getString("State").toString();
                    Marital_status = jsonObject.getString("mstat").toString();
                    Mobile = jsonObject.getString("Mobile").toString();
                    mnutype = jsonObject.getString("Main_user_type").toString();
                    subutype = jsonObject.getString("Sub_user_type").toString();
                    String creaton = jsonObject.getString("Created_on").toString();
                    //String creatby = jsonObject.getString("Created_by").toString();
                    String maritaltext = jsonObject.getString("mstat").toString();

                    sharedPreferenceClass.setValue_string("MRTX", maritaltext);
                    sharedPreferenceClass.setValue_string("CREBY", userid);
                    sharedPreferenceClass.setValue_string("CREAT", creaton);
                    sharedPreferenceClass.setValue_string("FRSTN", firstname);
                    sharedPreferenceClass.setValue_string("LSTN", lname);
                    sharedPreferenceClass.setValue_string("ADR1", addrsline1);
                    sharedPreferenceClass.setValue_string("ADR2", addressln2);
                    sharedPreferenceClass.setValue_string("ZIP", zipcode);
                    sharedPreferenceClass.setValue_string("DOB", dob);
                    sharedPreferenceClass.setValue_string("MSTS", Marital_status);
                    sharedPreferenceClass.setValue_string("PHONE", Mobile);
                    sharedPreferenceClass.setValue_string("WRK", wrkas);
                    sharedPreferenceClass.setValue_string("EMID", email);
                    sharedPreferenceClass.setValue_string("PHOTO", profileimg);
                    sharedPreferenceClass.setValue_string("GEN", gender);
                    sharedPreferenceClass.setValue_string("CON", contry);
                    sharedPreferenceClass.setValue_string("CTY", city);
                    sharedPreferenceClass.setValue_string("STS", states);
                    sharedPreferenceClass.setValue_string("UDDIDD", userid);
                    sharedPreferenceClass.setValue_string("LTN", name);
                    sharedPreferenceClass.setValue_string("PSW", password);
                    sharedPreferenceClass.setValue_string("MNUTYPE", mnutype);
                    sharedPreferenceClass.setValue_string("SUBUTYPE", subutype);
                    sharedPreferenceClass.setValue_string("TC", totalconnection);

                    try {
                        editt = getIntent().getBooleanExtra("Profiletask", false);
                        img = getIntent().getStringExtra("PIX");
                        nname = getIntent().getStringExtra("NM");
                        tcant = sharedPreferenceClass.getValue_string("TC");
                        if (tcant.equals("null")) {
                            tcant = "0";
                        }

          /*  workas=getIntent().getStringExtra("WRKAS");
            gender=getIntent().getStringExtra("GEND");
            country=getIntent().getStringExtra("CTC");
            city1=getIntent().getStringExtra("CITY");*/

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (editt) {
                        edit.setVisibility(View.VISIBLE);
                    } else {
                        edit.setVisibility(View.GONE);
                    }
                    if (img == null) {
                        // propic= (ImageView) findViewById(R.id.profilePictureView14);

                        // fname=sharedPreferenceClass.getValue_string("FSN");
                        lname = sharedPreferenceClass.getValue_string("LTN");
                        //name=fname + lname;
                        propict = sharedPreferenceClass.getValue_string("PHOTO");
                        pic = PicConstant.PROFILE_URL1 + propict;
                        //nam.setText(lname);
                        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Profiles.this).build();
                        ImageLoader imageLoader = ImageLoader.getInstance();
                        ImageLoader.getInstance().init(config);
                        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                                .cacheOnDisc(true).resetViewBeforeLoading(true)
                                // .showImageForEmptyUri(fallback)
                                .showImageOnFail(R.drawable.blankpeople)
                                .showImageOnLoading(R.drawable.blankpeople).build();
                        imageLoader.displayImage(pic, circularImageView, options);
          /*  Uri myUri = Uri.parse(pic);
            Picasso.with(Profiles.this)
                    .load(myUri)
                    .placeholder(R.drawable.load)
                    .error(R.drawable.blankpeople)
                    .into(circularImageView);*/
                        if (lname.equals("null")) {
                            nam.setText("");
                        } else {
                            nam.setText(lname);
                        }
                        tcnt.setText(tcant);
                    } else {
                        String img1 = PicConstant.PROFILE_URL + img;
                        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Profiles.this).build();
                        ImageLoader imageLoader = ImageLoader.getInstance();
                        ImageLoader.getInstance().init(config);
                        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                                .cacheOnDisc(true).resetViewBeforeLoading(true)
                                // .showImageForEmptyUri(fallback)
                                .showImageOnFail(R.drawable.blankpeople)
                                .showImageOnLoading(R.drawable.blankpeople).build();
                        imageLoader.displayImage(img1, circularImageView, options);
           /* Uri myUri = Uri.parse(img1);
            Picasso.with(this)
                    .load(myUri)
                    .placeholder(R.drawable.load)
                    .error(R.drawable.blankpeople)
                    .into(circularImageView);*/

                    }

                }

            } catch (Exception e) {

            }

            if (bResponse.equals("0")) {
                Toast.makeText(getApplicationContext(), "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (bResponse.equals("1")) {
                Toast.makeText(getApplicationContext(), "You have successfully Registered !", Toast.LENGTH_LONG).show();
              /*  Intent i = new Intent(SignUp.this, Home.class);
                startActivity(i);
                // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                //save auth key
                finish();*/
            } else {

                //progressDialog.dismiss();

            }

            try{
                if(progressDialog != null){
                    progressDialog.dismiss();
                }
            } catch (Exception exception){
                if(progressDialog != null) {
                    progressDialog.dismiss();
                }
            }


        }

    }
/////
    //webservice for connect people

    class DisconnectTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            dResponse = com_post.disconnect("disconnect", from_id, Regd_id);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (dResponse!=null) {
                progressDialog.dismiss();
                if (dResponse.equals("3")) {
                    dialog.dismiss();
                    Toast.makeText(Profiles.this,"Request deleted successfully!",Toast.LENGTH_SHORT).show();
                    connect.setText(R.string.connect);
                    c_status = "0";
                }
                else {
                    Toast.makeText(Profiles.this,"Something went wrong please try again!",Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(Profiles.this,"Something went wrong please try again!",Toast.LENGTH_SHORT).show();
            }
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    class AcceptRequestTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            cResponse = com_post.acceptConnection("acceptConnection", connID);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (cResponse!=null) {
                progressDialog.dismiss();
                String[] responseCell = cResponse.split(",");
                if (responseCell[0].equals("2")) {
                    dialog.dismiss();
                    Toast.makeText(Profiles.this,"Connected successfully!",Toast.LENGTH_SHORT).show();
                    connect.setText(R.string.disconnect);
                    c_status = "1";
                }
                else {
                    Toast.makeText(Profiles.this,"Something went wrong please try again!",Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(Profiles.this,"Something went wrong please try again!",Toast.LENGTH_SHORT).show();
            }
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    class AsyncCallWS3 extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            aResponse = com1.connecto_foddie("add_connection",from_id, Regd_id,Connection_type,Created_on);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            String[] arrResp = aResponse.split(",");
            if (arrResp.length>0 && arrResp[0].equals("1")) {
                Toast.makeText(Profiles.this, "Connection request sent!", Toast.LENGTH_LONG).show();
                c_status = "3";
                c_connDtl[1] = ""+from_id;
                c_connDtl[2] = ""+Regd_id;
                connect.setText(R.string.connect_sent);
            }
            else {Toast.makeText(Profiles.this, "Something went wrong please try again!", Toast.LENGTH_LONG).show();}
            progressDialog.dismiss();
        }
    }

    private class ImageLoadingTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... params) {
            if (Home.profileCache.get(""+Regd_id)==null) {
                try {
                    URL url = new URL(profLink);
                    Home.lodingCache.put(Home.LOADING_CACHE, BitmapFactory.decodeStream(url.openConnection().getInputStream()));
                } catch (Exception e) {
                    e.printStackTrace();
                    Home.lodingCache.put(Home.LOADING_CACHE,null);
                }
                if (Home.lodingCache.get(Home.LOADING_CACHE)==null) {
                    Home.lodingCache.put(Home.LOADING_CACHE,BitmapFactory.decodeResource(getResources(),R.drawable.blankpeople));
                }
                Home.lodingCache.put(Home.LOADING_CACHE,scaleImage(Home.lodingCache.get(Home.LOADING_CACHE), profImgPx));
                //Values.get(i).setProfBitmap(lodingCache.get(LOADING_CACHE).copy(lodingCache.get(LOADING_CACHE).getConfig(),lodingCache.get(LOADING_CACHE).isMutable()));
                Home.profileCache.put(""+Regd_id,Home.lodingCache.get(Home.LOADING_CACHE).copy(Home.lodingCache.get(Home.LOADING_CACHE).getConfig(),Home.lodingCache.get(Home.LOADING_CACHE).isMutable()));
                Home.lodingCache.get(Home.LOADING_CACHE).recycle();
                //lodingCache.put(LOADING_CACHE,null);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            circularImageView.setImageBitmap(Home.profileCache.get(""+Regd_id));
        }
    }
    //////
    private Bitmap scaleImage(Bitmap bitmap, int newDpHeight) {
        int newHeight = newDpHeight;
        int newWidth = (int) (newHeight * bitmap.getWidth() / ((double) bitmap.getHeight()));
        return Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
    }
}

