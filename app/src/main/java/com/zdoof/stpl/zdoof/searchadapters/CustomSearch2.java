package com.zdoof.stpl.zdoof.searchadapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zdoof.stpl.zdoof.activites.Profiles;
import com.zdoof.stpl.zdoof.R;

/**
 * Created by stpl on 20/4/16.
 */
public class CustomSearch2 extends BaseAdapter{
    Profiles profiles;
    String[] sertype;
    int[] sercimg;
    public CustomSearch2(Profiles profiles, int spiner_item, String[] sertype, int[] sercimg) {
        this.profiles=profiles;
        this.sertype=sertype;
        this.sercimg=sercimg;


    }

    @Override
    public int getCount() {return sertype.length;}



    @Override
    public Object getItem(int position) {return position;

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vgrid;
        LayoutInflater inflater = profiles.getLayoutInflater();
        vgrid = inflater.inflate(R.layout.custom_serdesign, null);
        ImageView image= (ImageView) vgrid.findViewById(R.id.imageView29);
        TextView text= (TextView) vgrid.findViewById(R.id.textView27);
        image.setImageResource(sercimg[position]);
        text.setText(sertype[position]);

        return vgrid;
    }

}
