package com.zdoof.stpl.zdoof.activites;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.adapters.CustomSearch_frlist;
import com.zdoof.stpl.zdoof.adapters.Custom_postlist;

import com.zdoof.stpl.zdoof.search.Search_dish;
import com.zdoof.stpl.zdoof.search.Search_gstore;
import com.zdoof.stpl.zdoof.search.Search_people;
import com.zdoof.stpl.zdoof.search.Search_post;
import com.zdoof.stpl.zdoof.search.Search_resturant;
import com.zdoof.stpl.zdoof.utils.AppUtil;
import com.zdoof.stpl.zdoof.views.Contropanel;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall;
import com.zdoof.stpl.zdoof.webservice.Webservicereceipe_post;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by stpl on 29/2/16.
 */
public class PostListActivity extends Activity implements LocationListener{
    ImageView PostList,conection,notification,Zpanel,serc,close,back;
  
    String searchvalue,rid,pos;
    int Regd_id;
    SharedPreferenceClass sharedPreferenceClass;
    String desc = "", loATION,des;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    String rest, dish, pep, gros, posts,result,zipcode;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    DrawerLayout drawerMainSearch;
    ListView list1, drawerList;
    LinearLayout ll1,ll2,ll3,ll4,ll5,ziploc,vieew;
    EditText description,zips;
    TextView code,textt;
    Button submit;
    int Sub_user_type, Main_user_type;
    String _name, Last_name="", Createdon, Updated_on, Ip_address,First_name="";
    final Webservicereceipe_post com1 = new Webservicereceipe_post();
    final WebserviceCall com = new WebserviceCall();
    static String bResponse,cResponse;
    int Loggin_id;
    ProgressDialog progressDialog;
    ArrayList<Detail> Post;
    String sertype[]={"Search Post","Search Foodie","Search Recipe/Dish","Search Restaurant","Search Grocery Store"};
    int sercimg[]={R.drawable.searhicon,R.drawable.sst,R.drawable.ff,R.drawable.sahlist,R.drawable.crt,R.drawable.uur};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.postlist);
        PostList= (ImageView) findViewById(R.id.hme);
        conection= (ImageView) findViewById(R.id.hm);
        notification= (ImageView) findViewById(R.id.cnct);
        Zpanel= (ImageView) findViewById(R.id.ctrl);
        back = (ImageView) findViewById(R.id.imageViewpost);
        ll1= (LinearLayout) findViewById(R.id.hmj);
        ll2= (LinearLayout) findViewById(R.id.cnn);
        ll3=(LinearLayout) findViewById(R.id.ntf);
        ll4=(LinearLayout) findViewById(R.id.ct);
        ll5= (LinearLayout) findViewById(R.id.lays);
        drawerMainSearch = (DrawerLayout) findViewById(R.id.drawerMainSearch);
        list1= (ListView) findViewById(R.id.list);
        drawerList = (ListView) findViewById(R.id.drawerItemList);
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header1 = (ViewGroup)inflater.inflate(R.layout.search_view, drawerList, false);
        drawerList.addHeaderView(header1, null, false);
        code= (TextView) findViewById(R.id.code);
        submit= (Button) findViewById(R.id.submit);
        textt= (TextView) findViewById(R.id.textt);
        ziploc= (LinearLayout) findViewById(R.id.ziploc);
        vieew= (LinearLayout) findViewById(R.id.view);
        zips= (EditText) findViewById(R.id.zip);
        description=(EditText) findViewById(R.id.imageView11);
        serc= (ImageView) findViewById(R.id.serc);
        close= (ImageView) findViewById(R.id.close);
        //rch= (ImageView) findViewById(R.id.sec);
        /*AsyncCallWS1 task = new AsyncCallWS1();
        task.execute();*/

        CustomSearch_frlist drawerAdapter = new CustomSearch_frlist(PostListActivity.this, R.layout.spiner_item, sertype, sercimg);
        drawerList.setAdapter(drawerAdapter);
        sharedPreferenceClass=new SharedPreferenceClass(this);
        searchvalue=sharedPreferenceClass.getValue_string("SEARCHVALUE");
        if(searchvalue.equals("0")){
            description.setText("");
        }else {
            description.setText(searchvalue);
        }
        rid =getIntent().getStringExtra("USERID");
        Regd_id=Integer.parseInt(rid);

        //String rid1 = sharedPreferenceClass.getValue_string("UIDD").toString();
        String rid1 = sharedPreferenceClass.getValue_string("UIDD").toString();
        Loggin_id=Integer.parseInt(rid1);

        LocationManager locationManager = (LocationManager)
                PostListActivity.this.getSystemService(PostListActivity.this.LOCATION_SERVICE);
        ActivityCompat.requestPermissions(PostListActivity.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
        // getting GPS status
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
        isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


        if (!isGPSEnabled && !isNetworkEnabled) {
            // no network provider is enabled
        } else {
            this.canGetLocation = true;

            if (isNetworkEnabled) {
                // checkLocationPermission();
                if (ActivityCompat.checkSelfPermission(PostListActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(PostListActivity.this,
                                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                        if (location != null) {

                            Geocoder gcd = new Geocoder(PostListActivity.this, Locale.getDefault());
                            List<Address> list = null;
                            try {
                                list = gcd.getFromLocation(location
                                        .getLatitude(), location.getLongitude(), 1);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if (list != null) {
                                if(list.size() > 0) {
                                    Address address = list.get(0);
                                    result = address.getLocality();
                                    if (address.getPostalCode() != null) {
                                        zipcode = address.getPostalCode();
                                    }
                                    // locaqtions = result;
                                    loATION = result;
                                }
                                //location1.setText("Checking in to" + " " + result + "?");
   /* // Setting latitude and longitude in the TextView tv_location
    location.setText("Latitude:" +  latitude  + ", Longitude:"+ longitude );*/
                            }
                        }
                    }

                }


                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        //checkLocationPermission();
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                Geocoder gcd = new Geocoder(PostListActivity.this, Locale.getDefault());
                                List<Address> list = null;
                                try {
                                    list = gcd.getFromLocation(location
                                            .getLatitude(), location.getLongitude(), 1);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                if (list != null) {
                                    if (list.size() > 0) {
                                        Address address = list.get(0);
                                        result = address.getLocality();
                                        if (address.getPostalCode() != null) {
                                            zipcode = address.getPostalCode();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // postcomment = zipcode;
            }
        }
        String zzip = sharedPreferenceClass.getValue_string("ZZIP");
        if (!zzip.equals(null) && !zzip.equals("") && !zzip.equals("0")) {
            code.setText(zzip);
        }
        else {
            code.setText(zipcode);
            sharedPreferenceClass.setValue_string("ZZIP", zipcode);
        }
        zipcode = sharedPreferenceClass.getValue_string("ZZIP");
        AsyncCallWS2 task1 = new AsyncCallWS2();
        task1.execute();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zipcode =zips.getText().toString();
                code.setText(zipcode);
                sharedPreferenceClass.setValue_string("ZZIP", zipcode);
                ziploc.setVisibility(View.GONE);
                vieew.setVisibility(View.GONE);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PostListActivity.this,Home.class);
                startActivity(intent);
                finish();
            }
        });
        ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PostListActivity.this,Connecton.class);
                startActivity(intent);
                finish();
            }
        });
        ll3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PostListActivity.this,Notifications.class);
                startActivity(intent);
                finish();
            }
        });
        ll4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PostListActivity.this, Contropanel.class);
                startActivity(intent);
                finish();
            }
        });
        serc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawerMainSearch.isDrawerOpen(GravityCompat.END)) {
                    drawerMainSearch.openDrawer(GravityCompat.END);
                }
                else {
                    drawerMainSearch.closeDrawer(GravityCompat.END);
                }
            }
        });

        description.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //Log.e("KeyCode:::",""+actionId);
                if (actionId== EditorInfo.IME_ACTION_SEARCH) {
                    serc.performClick();
                    return true;
                }
                return false;
            }
        });
        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                drawerMainSearch.closeDrawer(GravityCompat.END);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setCursorVisible(true);

            }
        });

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                des = description.getText().toString().trim();
                if (des.length()<3) {
                    Toast.makeText(PostListActivity.this, "Please enter atleast 3 letters to search!", Toast.LENGTH_SHORT).show();
                }
                else {
                    pos = drawerList.getItemAtPosition(position).toString();
                    String tem = pos;
                    if (pos.equals("0")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(PostListActivity.this, Search_post.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("1")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(PostListActivity.this, Search_people.class);
                        startActivity(intent);
                    } else if (pos.equals("2")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(PostListActivity.this, Search_dish.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("3")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(PostListActivity.this, Search_resturant.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("4")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(PostListActivity.this, Search_gstore.class);
                        startActivity(intent);
                        //finish();
                    }
                }
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setText("");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        drawerMainSearch.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onLocationChanged(Location location) {
        
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    class AsyncCallWS2 extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(PostListActivity.this, "", "Loading...");
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            bResponse = com1.post("post_report",Loggin_id);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            try {
                JSONArray jr = new JSONArray(bResponse);
                Post = new ArrayList<Detail>();

                for (int i = 0; i < jr.length(); i++) {
                    JSONObject jsonObject = jr.getJSONObject(i);
                    Detail pr = new Detail();
                    String post_type = jsonObject.getString("Post_type");
                    String Post_cmmnt = jsonObject.getString("Details");
                    String Creton = jsonObject.getString("Created_on");
                    //Creton = AppUtil.getDateAndTimeFromTimestamp(PostListActivity.this, Creton);
                    Creton = convertTime(Creton);
                    pr.setPostname(post_type);
                    pr.setPostedcomments(Post_cmmnt);
                    pr.setPcreaton(Creton);
                    Post.add(pr);
                }
                Custom_postlist adapter = new Custom_postlist(PostListActivity.this, R.layout.spiner_item, Post);
                list1.setAdapter(adapter);


                   /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(Home.this, android.R.layout.simple_list_item_1, Dish);
                    item.setAdapter(adapter);
                    item.setThreshold(1);
                    item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            diSh_id = Dish1.get(position);
                            ddish_id = Integer.parseInt(diSh_id);
                        }
                    });*/

                // progressDialog.dismiss();

            } catch (Exception e) {
            }
            progressDialog.dismiss();
        }
       private String convertTime(String jTime) {
           String returnString = null;
           returnString = jTime.substring(6, jTime.length()-2);
           long epoch = Long.parseLong(returnString);
           //returnString = new java.text.SimpleDateFormat("dd/MMM/yyyy 'at' HH:mm:ss aaa").format(new java.util.Date (epoch));
           Date date = new Date(epoch-5400000);
           SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy 'at' HH:mm:ss aaa");
           sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
           returnString = sdf.format(date);
           return returnString;
       }
    }
    @Override
    public void onBackPressed() {
        if (drawerMainSearch.isDrawerOpen(GravityCompat.END)) {
            drawerMainSearch.closeDrawer(GravityCompat.END);
        }else {
           String back = sharedPreferenceClass.getValue_string("BACKPAGE");
            if (back.equals("controlpanel")) {
                Intent intent = new Intent(PostListActivity.this, Contropanel.class);
                startActivity(intent);
                finish();
            }
            else {
                super.onBackPressed();
            }
        }
//        sharedPreferenceClass.setValue_boolean("Loginstatus", false);
//        sharedPreferenceClass.clearData();
    }
}
