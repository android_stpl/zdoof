package com.zdoof.stpl.zdoof.utils;

/**
 * Created by Shiftu_Android on 16-01-2017.
 */

public class ZdoofConstants {
    public static final String EXTRA_USER_PREFF = "EXTRA_USER_PREFF";
    public static final String EXTRA_USER_ID = "User_id";
    public static final String EXTRA_MAIN_USER_TYPE = "Main_user_type";
    public static final String EXTRA_SUBUSER_TYPE = "Sub_user_type";
    public static final String EXTRA_USER_STATUS = "User_status";
    public static final String EXTRA_USER_FIRST_NAME = "First_name";
    public static final String EXTRA_USER_LAST_NAME = "Last_name";
    public static final String EXTRA_USER_EMAIL = "Email";

    public static final String EXTRA_USER_NAME = "name";
    public static final String EXTRA_USER_ADDRESS_ONE = "Address_line1";
    public static final String EXTRA_USER_ADDRESS_TWO = "Address_line2";
    public static final String EXTRA_USER_ZIP_CODE = "Zip_code";
    public static final String EXTRA_USER_CUTY = "City";
    public static final String EXTRA_USER_STATE = "State";
    public static final String EXTRA_USER_COUNTRY = "Country";
    public static final String EXTRA_USER_GENDER = "gen";
    public static final String EXTRA_USER_MOBILE = "Mobile";
    public static final String EXTRA_USER_DOB = "DOB";
    public static final String EXTRA_USER_PROFILE_IMAGE = "profimg";
    public static final String EXTRA_USER_TYPE = "UTYPE";
    public static final String EXTRA_USER_CREATED_ON = "Created_on";
    public static final String EXTRA_USER_MARITIAL_STATUS = "mstat";
    public static final String EXTRA_USER_ABOUT = "About_user";
}
