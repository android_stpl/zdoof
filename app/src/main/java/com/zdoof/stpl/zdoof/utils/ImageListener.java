package com.zdoof.stpl.zdoof.utils;

import android.app.ProgressDialog;

/**
 * Created by Shiftu_Android on 10-01-2017.
 */



public interface ImageListener {
    public void loadImage(String s);
    public void loadName(String name);
    public void connectionStatus(String status, String[] connDtl);
    public void loadUType(String utype);
    public ProgressDialog getProgressDialog();
    public void dismissProgressDialog();
}