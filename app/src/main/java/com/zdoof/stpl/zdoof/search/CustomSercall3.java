package com.zdoof.stpl.zdoof.search;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;

import java.util.ArrayList;

/**
 * Created by stpl on 29/3/16.
 */
public class CustomSercall3 extends BaseAdapter{
    Search_all search_all;
    ArrayList<Detail> values;
    public CustomSercall3(Search_all search_all, int spiner_item, ArrayList<Detail> values) {
        this.search_all=search_all;
        this.values=values;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vgrid;
        LayoutInflater inflater = search_all.getLayoutInflater();
        vgrid = inflater.inflate(R.layout.custom_searchpeopl, null);
        ImageView img= (ImageView) vgrid.findViewById(R.id.image);

        TextView text= (TextView) vgrid.findViewById(R.id.textView2);
        TextView text1= (TextView) vgrid.findViewById(R.id.textView4);
        TextView text2= (TextView) vgrid.findViewById(R.id.textView);
        /* TextView text3= (TextView) vgrid.findViewById(R.id.textView13);
        TextView text4= (TextView) vgrid.findViewById(R.id.textView15);
        TextView text5= (TextView) vgrid.findViewById(R.id.textView17);*/

       String pic=values.get(position).getUserpfimg();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(search_all).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                        // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.blankpeople)
                .showImageOnLoading(R.drawable.uute).build();
        imageLoader.displayImage(pic, img, options);
      /* String pic1= PicConstant.PROFILE_URL1+pic;
        Uri myUri = Uri.parse(pic1);
        Picasso.with(search_all)
                .load(myUri)
                .placeholder(R.drawable.load)
                .error(R.drawable.blankpeople)
                .into(img);*/

        text2.setText(values.get(position).getU_Name());
        text.setText(values.get(position).getUser_type());
        text1.setText(values.get(position).getUser_Zlike());

        return vgrid;
    }
}
