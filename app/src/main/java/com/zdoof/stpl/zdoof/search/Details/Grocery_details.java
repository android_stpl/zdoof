package com.zdoof.stpl.zdoof.search.Details;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.webservice.PicConstant;
import com.zdoof.stpl.zdoof.webservice.webservice_searchdetails;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by stpl on 1/7/16.
 */
public class Grocery_details extends Activity {

    EditText ur_review;
    //AutoCompleteTextView dishname;
    TextView name, address, reviews, contact_no, massage, url, location, likes;
    TextView rtng,ratess,postrvew;
    Button post_review;
    RatingBar ratingBar,ratings;
    ImageView dishpic;
    String ratingg,restplc_id="",grocer_plcid="",creatby,resturant_id,Restaurant_name,
            Restaurant_phone,Address_line1,Address_line2,Website,
            Google_map_url,Google_place_id,thurest,
            reviewrating,Restaurant_image,pic,pic1,dishnames;
    String Dish_Name;
    String Dish_id;
    String diSh_id,grocid;
    ListView rev_list;
    int ddish_id;
    String yrreviw,gResponse;
    ProgressDialog progressDialog;
    String Expdate,cresponse,eresponse,fresponse,Gstore_phone,Google_map_ur,thugroc,
            Gstore_image,Gsrore_id,Gstore_address,Gstore_address2,totalreview,jresponse,comment,review_name,review_image;
    String[] dresponse;
    int resturantid,Grocery_id;
    int createdby;
    SharedPreferenceClass sharedPreferenceClass;
    ArrayList<Detail>Gstore;
    final webservice_searchdetails com2 = new webservice_searchdetails();
    String bResponse,hResponse;
    String rest_plid="",Grocery_place_id="",Gstore_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grocery_details);
        dishpic = (ImageView) findViewById(R.id.imageView13);
        name = (TextView) findViewById(R.id.textView18);
        postrvew = (TextView) findViewById(R.id.postreview);
        address = (TextView) findViewById(R.id.address);
        reviews = (TextView) findViewById(R.id.textView19);
        contact_no = (TextView) findViewById(R.id.contact);
        massage = (TextView) findViewById(R.id.sms);
        url = (TextView) findViewById(R.id.url);
        location = (TextView) findViewById(R.id.location);
        likes = (TextView) findViewById(R.id.textView20);
        rtng = (TextView) findViewById(R.id.ratetyp);
        ratess= (TextView) findViewById(R.id.ratess);
        ur_review = (EditText) findViewById(R.id.review);
        ratingBar= (RatingBar) findViewById(R.id.rating);
        ratings=(RatingBar) findViewById(R.id.ratin);
        ratings.setIsIndicator(true);
        rev_list= (ListView) findViewById(R.id.rev_list);
        post_review =(Button) findViewById(R.id.viw);

        //dishname = (AutoCompleteTextView) findViewById(R.id.dishname);
        sharedPreferenceClass=new SharedPreferenceClass(Grocery_details.this);
        creatby=sharedPreferenceClass.getValue_string("UIDD");
        createdby=Integer.parseInt(creatby);
       /* resturant_id=getIntent().getStringExtra("RID");
        resturantid=Integer.parseInt(resturant_id);*/
        String format = "dd/MMM/yyyy H:mm:ss";
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Expdate = sdf.format(new Date());

        grocid=getIntent().getStringExtra("GCD");
        Grocery_id=Integer.parseInt(grocid);

        post_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yrreviw=ur_review.getText().toString().trim();
                Float rate = ratingBar.getRating();
                ratingg = String.format("%.1f",rate);
                if (yrreviw.length()>150) {
                    Toast.makeText(Grocery_details.this,"Your review should be less than 150 charecters!",Toast.LENGTH_SHORT).show();
                }
                else {
                    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Expdate = sdf.format(new Date());
                    ur_review.setText("");
                    //dishname.setText("");
                    ratingBar.setRating(Float.parseFloat("0.0"));
                    AsyncCallWS task = new AsyncCallWS();
                    task.execute();
                }


            }
        });
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rtng.setText(String.valueOf(rating));
                //ratingg = String.valueOf(rating);
            }
        });

        AsyncCallWS2 task = new AsyncCallWS2();
        task.execute();
    }
    class AsyncCallWS2 extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            bResponse = com2.Groc_detail("ret_groc_details", Grocery_id,rest_plid,createdby,Grocery_place_id);
            dresponse=bResponse.split("\\$");
            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");

            try {
                  cresponse=dresponse[0];
                  hResponse=dresponse[1];
                  eresponse=dresponse[2];
                  fresponse=dresponse[3];
                  jresponse=dresponse[4];

                JSONArray jr = new JSONArray(cresponse);

                for (int i = 0; i < jr.length(); i++) {

                    JSONObject jsonObject = jr.getJSONObject(i);
                    Gstore_name = jsonObject.getString("Gstore_name").toString();
                    Gstore_address = jsonObject.getString("Address_line1").toString();
                    Gstore_address2 = jsonObject.getString("Address_line2").toString();
                    // username = jsonObject.getString("User_name").toString();
                    Gsrore_id = jsonObject.getString("Gstore_id").toString();

                    Gstore_phone = jsonObject.getString("Gstore_phone").toString();
                    Google_map_ur=jsonObject.getString("Google_map_url").toString();
                    thugroc=jsonObject.getString("thugroc").toString();
                    pic= PicConstant.PROFILE_URL1+"images/blank-grocery.jpg";
                   // pic= PicConstant.PROFILE_URL1+Gstore_image;
                    ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Grocery_details.this).build();
                    ImageLoader imageLoader = ImageLoader.getInstance();
                    ImageLoader.getInstance().init(config);
                    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                            .cacheOnDisc(true).resetViewBeforeLoading(true)
                            // .showImageForEmptyUri(fallback)
                            .showImageOnFail(R.drawable.noimg)
                            .showImageOnLoading(R.drawable.uute).build();
                    imageLoader.displayImage(pic, dishpic, options);
                    name.setText(Gstore_name);
                    address.setText(Gstore_address + Gstore_address2);
                     if(Gstore_phone==null){
                         contact_no.setText("N/A");
                     }else{
                         contact_no.setText(Gstore_phone);
                     }
                   // contact_no.setText(Gstore_phone);
                    likes.setText(thugroc);
                    location.setText(Html.fromHtml(Google_map_ur));
                }

            }catch (Exception e){

            }
           /* try {

                JSONArray jr = new JSONArray(hResponse);

                for (int i = 0; i < jr.length(); i++) {

                    JSONObject jsonObject = jr.getJSONObject(i);

                    Gstore_image = jsonObject.getString("Gstore_Image").toString();
                    // pic= PicConstant.PROFILE_URL1+"images/blank-grocery.jpg";
                    pic= PicConstant.PROFILE_URL1+Gstore_image;
                    ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Grocery_details.this).build();
                    ImageLoader imageLoader = ImageLoader.getInstance();
                    ImageLoader.getInstance().init(config);
                    DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                            .cacheOnDisc(true).resetViewBeforeLoading(true)
                            // .showImageForEmptyUri(fallback)
                            .showImageOnFail(R.drawable.noimg)
                            .showImageOnLoading(R.drawable.uute).build();
                    imageLoader.displayImage(pic, dishpic, options);

                }

            }catch (Exception e){

            }
*/
          try {

                JSONArray jr = new JSONArray(eresponse);

                for (int i = 0; i < jr.length(); i++) {

                    JSONObject jsonObject = jr.getJSONObject(i);
                    totalreview = jsonObject.getString("totalreview").toString();
                    reviews.setText(totalreview);
                }

            }catch (Exception e){

            }

            try {

                JSONArray jr = new JSONArray(fresponse);

                for (int i = 0; i < jr.length(); i++) {

                    JSONObject jsonObject = jr.getJSONObject(i);

                    reviewrating = jsonObject.getString("reviewrating").toString();
                    ratess.setText(reviewrating);
                    ratings.setRating(Float.parseFloat(reviewrating));
                }

            }catch (Exception e){

            }

            try {

                JSONArray jr = new JSONArray(jresponse);
                Gstore=new ArrayList<Detail>();
                for (int i = 0; i < jr.length(); i++) {
                    Detail pro=new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);

                    comment = jsonObject.getString("Comment").toString();
                    review_name=jsonObject.getString("Name").toString();
                    review_image=jsonObject.getString("UTYPIMG").toString();
                    pro.setComment(comment);
                    pro.setName(review_name);
                    pro.setPfimage(review_image);
                    Gstore.add(pro);

                }
                Cut_gstor_review adapter=new Cut_gstor_review(Grocery_details.this,R.layout.spiner_item,Gstore);
                rev_list.setAdapter(adapter);
                setListViewHeightBasedOnItems(rev_list);
            }catch (Exception e){

            }

            if (bResponse.equals("0")) {
                Toast.makeText(Grocery_details.this, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (bResponse.equals("1")) {
                //Toast.makeText(View_dish_Deatails.this, "S!", Toast.LENGTH_LONG).show();

            } else {

            }

        }
    }
    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            gResponse = com2.Select_grocery("add_review_grocery",Grocery_id,ratingg,yrreviw,createdby,Expdate);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            if (gResponse.equals("0")) {
                Toast.makeText(Grocery_details.this, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (gResponse.equals("1")) {

            } else {

            }

        }

    }

    public  boolean setListViewHeightBasedOnItems(ListView cmntlist) {

        ListAdapter listAdapter = cmntlist.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems;itemPos++) {
                View item = listAdapter.getView(itemPos, null, cmntlist);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = cmntlist.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = cmntlist.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            cmntlist.setLayoutParams(params);
            cmntlist.requestLayout();
            return true;

        } else {

            return false;

        }

    }

}
