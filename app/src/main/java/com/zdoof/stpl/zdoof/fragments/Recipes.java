package com.zdoof.stpl.zdoof.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.zdoof.stpl.zdoof.activites.Profiles;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.adapters.Custom_receipe1;
import com.zdoof.stpl.zdoof.utils.ImageListener;
import com.zdoof.stpl.zdoof.webservice.Webservicereceipe_post;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by stpl on 8/2/16.
 */
public class Recipes extends Fragment{
    ListView list1;
    SharedPreferenceClass sharedPreferenceClass;
    String rid,userid;
    int Loggin_id,User_id;
    ProgressDialog progressDialog;
    final Webservicereceipe_post com1 = new Webservicereceipe_post();
    static String bResponse;
    ArrayList<Detail> Dish;
    ImageListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (ImageListener) ((Activity) context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.recipe,null);
        list1= (ListView) rootView.findViewById(R.id.imagelist);
        sharedPreferenceClass= new SharedPreferenceClass(getActivity());
        rid = sharedPreferenceClass.getValue_string("UDDIDD").toString();
        Loggin_id=Integer.parseInt(rid);
        userid=sharedPreferenceClass.getValue_string("UID");
        User_id=Integer.parseInt(userid);
        AsyncCallWS2 task = new AsyncCallWS2();
        task.execute();
        return rootView;
    }
  /*  @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            AsyncCallWS2 task = new AsyncCallWS2();
            task.execute();

            if (!isVisibleToUser) {
                Log.d("MyFragment", "Not visible anymore.  Stopping audio.");
                // TODO stop audio playback
            }
        }
    }*/
    class AsyncCallWS2 extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //progressDialog = ProgressDialog.show(getActivity(), "", "Loading...");
        }
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            bResponse = com1.receipe("MyRecipes", Loggin_id, User_id);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            //progressDialog.dismiss();
            try {
                JSONArray jr = new JSONArray(bResponse);
                Dish = new ArrayList<Detail>();

                for (int i = 0; i < jr.length(); i++) {
                    JSONObject jsonObject = jr.getJSONObject(i);
                    Detail pr=new Detail();
                    String Dish_Name = jsonObject.getString("Dish_name");
                    String Dish_picture = jsonObject.getString("Dish_picture");
                    String like=jsonObject.getString("totdish");
                    String dishID = jsonObject.getString("Dish_id");
                    String ifliked = jsonObject.getString("thumbclass");
                    String createdby = jsonObject.getString("Created_by");
                    pr.setDish_namee(Dish_Name);
                    pr.setDish_picture(Dish_picture);
                    pr.setPost_like(like);
                    pr.setDish_id(dishID);
                    pr.setIflike(ifliked);
                    pr.setPostedby(createdby);
                    Dish.add(pr);
                }
                Custom_receipe1 adapter=new Custom_receipe1(getActivity(),R.layout.spiner_item,Dish);
                list1.setAdapter(adapter);
                setListViewHeightBasedOnChildren(list1);

                   /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(Home.this, android.R.layout.simple_list_item_1, Dish);
                    item.setAdapter(adapter);
                    item.setThreshold(1);
                    item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            diSh_id = Dish1.get(position);
                            ddish_id = Integer.parseInt(diSh_id);
                        }
                    });*/

                // progressDialog.dismiss();

            } catch (Exception e) {
            }

        }

    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

}
