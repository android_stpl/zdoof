package com.zdoof.stpl.zdoof.search.Details;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.CircularNetworkImageView;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.webservice.PicConstant;

import java.util.ArrayList;

/**
 * Created by stpl on 1/7/16.
 */
public class Cut_rest_review extends BaseAdapter{
    Resturant_design resturant_design;
    ArrayList<Detail> dish2;
    String pic,pic1,comment,names,dates,star_riview;
    public Cut_rest_review(Resturant_design resturant_design, int spiner_item, ArrayList<Detail> dish2) {
        this.resturant_design=resturant_design;
        this.dish2=dish2;
    }

    @Override
    public int getCount() {
        return dish2.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = resturant_design.getLayoutInflater();
        view = inflater.inflate(R.layout.custom_coment_detaitail, null);
        CircularNetworkImageView circularImageView = (CircularNetworkImageView) view.findViewById(R.id.profilePictureVie);
        TextView nam = (TextView) view.findViewById(R.id.textVie1);
        TextView types = (TextView) view.findViewById(R.id.textV1);
        TextView zlikes=(TextView) view.findViewById(R.id.textView20);
        TextView rst= (TextView) view.findViewById(R.id.textV11);
        TextView datess=(TextView) view.findViewById(R.id.textV12);
        RatingBar ratingBar= (RatingBar) view.findViewById(R.id.ratings);
        //ImageView down= (ImageView) view.findViewById(R.id.fdd);
        TextView cmty=(TextView) view.findViewById(R.id.tb);

        pic=dish2.get(position).getPfimage();
        pic1= PicConstant.PROFILE_URL1 + pic;

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(resturant_design).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.blankpeople)
                .showImageOnLoading(R.drawable.uute).build();
        imageLoader.displayImage(pic1, circularImageView, options);
        comment=dish2.get(position).getComment();
        names=dish2.get(position).getName();
        dates=dish2.get(position).getDcreateon();
        star_riview=dish2.get(position).getAt();
        nam.setText(names);
        types.setText(dish2.get(position).getWeight());
        rst.setText(dish2.get(position).getRestaurant_name());
        datess.setText("on"+ " " +dates);
        cmty.setText(comment);
        zlikes.setText(dish2.get(position).getPost_like());
        ratingBar.setRating(Float.parseFloat(star_riview));
        ratingBar.setIsIndicator(true);
      return view;
    }
}
