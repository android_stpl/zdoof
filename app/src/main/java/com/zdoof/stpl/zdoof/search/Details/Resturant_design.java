package com.zdoof.stpl.zdoof.search.Details;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.webservice.PicConstant;
import com.zdoof.stpl.zdoof.webservice.WebserviceCallpost;
import com.zdoof.stpl.zdoof.webservice.webservice_searchdetails;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by stpl on 27/6/16.
 */
public class Resturant_design extends Activity {
    EditText ur_review;
    AutoCompleteTextView dishname;
    TextView name, address, reviews, contact_no, massage, url, location, likes;
    TextView rtng,ratess,postrvew;
    Button post_review;
    RatingBar ratingBar,ratings;
    ImageView dishpic;
    String ratingg,restplc_id="",grocer_plcid="",creatby,resturant_id,Restaurant_name,
            Restaurant_phone,Address_line1,Address_line2,Website,
            Google_map_url,Google_place_id,thurest,totalreview,
            reviewrating,Restaurant_image,pic,pic1,dishnames;
    String Dish_Name;
    String Dish_id;
    String diSh_id;
    ListView rev_list;
   int ddish_id;
    String yrreviw;
    ProgressDialog progressDialog;
    String Expdate;
   ArrayList<String> Dish,Dish1;
   ArrayList<Detail>Dish2;
    int resturantid;
    int createdby;
    static String bResponse,gResponse,cResponse,aResponse;
    SharedPreferenceClass sharedPreferenceClass;
    final webservice_searchdetails com = new webservice_searchdetails();
    final WebserviceCallpost com1 = new WebserviceCallpost();
 String names,comments,type,zlikes,date,pfimg,restneme,Star_review;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    private GoogleApiClient client;
    //private static final String GOOGLE_API_KEY = "AIzaSyClVSlqhlOZvvYhN_707ZoP70PGmAJJP6s";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.resturant_design);
        dishpic = (ImageView) findViewById(R.id.imageView13);
        name = (TextView) findViewById(R.id.textView18);
        postrvew = (TextView) findViewById(R.id.postreview);
        address = (TextView) findViewById(R.id.address);
        reviews = (TextView) findViewById(R.id.textView19);
        contact_no = (TextView) findViewById(R.id.contact);
        massage = (TextView) findViewById(R.id.sms);
        url = (TextView) findViewById(R.id.url);
        location = (TextView) findViewById(R.id.location);
        likes = (TextView) findViewById(R.id.textView20);
        rtng = (TextView) findViewById(R.id.ratetyp);
        ratess= (TextView) findViewById(R.id.ratess);
        ur_review = (EditText) findViewById(R.id.review);
        ratings= (RatingBar) findViewById(R.id.ratings);
        ratings.setIsIndicator(true);
        rev_list= (ListView) findViewById(R.id.rev_list);
        post_review =(Button) findViewById(R.id.viw);
        dishname = (AutoCompleteTextView) findViewById(R.id.dishname);
        sharedPreferenceClass=new SharedPreferenceClass(Resturant_design.this);
        creatby=sharedPreferenceClass.getValue_string("UIDD");
        createdby=Integer.parseInt(creatby);
        resturant_id=getIntent().getStringExtra("RID");
        resturantid=Integer.parseInt(resturant_id);


        ratingBar = (RatingBar) findViewById(R.id.rating);
        AsyncCallWS7 task1 = new AsyncCallWS7();
        task1.execute();

       AsyncCallWS2 task = new AsyncCallWS2();
        task.execute();
        AsyncCallWS4 task2 = new AsyncCallWS4();
        task2.execute();
        String format = "dd/MMM/yyyy H:mm:ss";
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Expdate = sdf.format(new Date());
        post_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yrreviw=ur_review.getText().toString().trim();
                Float rate = ratingBar.getRating();
                ratingg = String.format("%.01f",rate);
                dishnames = dishname.getText().toString().trim();
                if (yrreviw.length()>150 || dishnames.length()>150) {
                    Toast.makeText(Resturant_design.this,"Your review should be less than 150 charecters!",Toast.LENGTH_SHORT).show();
                }
                else {
                    ur_review.setText("");
                    dishname.setText("");
                    ratingBar.setRating(Float.parseFloat("0.0"));
                    AsyncCallWS3 task = new AsyncCallWS3();
                    task.execute();
                }
            }
        });

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rtng.setText(String.valueOf(rating));
                //ratingg = String.valueOf(rating);
            }
        });

       /* Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId)
                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                    @Override
                    public void onResult(PlaceBuffer places) {
                        if (places.getStatus().isSuccess()) {
                            final Place myPlace = places.get(0);
                            LatLng queriedLocation = myPlace.getLatLng();
                            Log.v("Latitude is", "" + queriedLocation.latitude);
                            Log.v("Longitude is", "" + queriedLocation.longitude);
                        }
                        places.release();
                    }
                });*/

    }

    class AsyncCallWS2 extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Resturant_design.this);
            progressDialog.setMessage("loading....");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

           bResponse = com.ResturantReview("ret_rest_details", resturantid, restplc_id, grocer_plcid, createdby);
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
progressDialog.dismiss();
            try {
                JSONArray jr = new JSONArray(bResponse);
                //Dish=new ArrayList<Detail>();

                for (int i = 0; i < jr.length(); i++) {
                    // Detail productdto = new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);
                    Restaurant_name = jsonObject.getString("Restaurant_name");
                    Restaurant_phone=jsonObject.getString("Restaurant_phone");
                    Address_line1=jsonObject.getString("Address_line1");
                    Address_line2 = jsonObject.getString("Address_line2");
                    Google_map_url=jsonObject.getString("Google_map_url");
                    Google_place_id=jsonObject.getString("Google_place_id");
                    thurest = jsonObject.getString("thurest");
                    totalreview=jsonObject.getString("totalreview");
                    reviewrating=jsonObject.getString("reviewrating");
                    Restaurant_image=jsonObject.getString("Restaurant_image");
                }
                pic= PicConstant.PROFILE_URL1+Restaurant_image;
                ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Resturant_design.this).build();
                ImageLoader imageLoader = ImageLoader.getInstance();
                ImageLoader.getInstance().init(config);
                DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                        .cacheOnDisc(true).resetViewBeforeLoading(true)
                        // .showImageForEmptyUri(fallback)
                        .showImageOnFail(R.drawable.noimg)
                        .showImageOnLoading(R.drawable.uute).build();
                imageLoader.displayImage(pic, dishpic, options);
                name.setText(Restaurant_name);
                address.setText(Address_line1+Address_line2);
                reviews.setText(totalreview);
                contact_no.setText(Restaurant_phone);
                likes.setText(thurest);
                location.setText(Html.fromHtml(Google_map_url));
                ratess.setText(reviewrating);
                ratings.setRating(Float.parseFloat(reviewrating));


                // progressDialog.dismiss();

            } catch (Exception e) {

            }
            if (bResponse.equals("0")) {
                Toast.makeText(Resturant_design.this, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (bResponse.equals("1")) {
                //Toast.makeText(View_dish_Deatails.this, "S!", Toast.LENGTH_LONG).show();
            } else {

            }

        }

    }
    class AsyncCallWS3 extends AsyncTask<Void, Void, Void> {
       @Override
       protected void onPreExecute() {
           super.onPreExecute();

       }

       @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            cResponse = com.ResturantRview("add_review_rest",ratingg, yrreviw,resturantid, ddish_id,dishnames,createdby,Expdate);
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            progressDialog.dismiss();

            if (cResponse.equals("0")) {
                Toast.makeText(Resturant_design.this, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (cResponse.equals("1")) {

                AsyncCallWS4 task = new AsyncCallWS4();
                task.execute();
            } else {

            }

        }
    }
    class AsyncCallWS7 extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            gResponse = com1.Selectdish("select_all_dish");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            try {
                JSONArray jr = new JSONArray(gResponse);
                Dish=new ArrayList<String>();
                Dish1=new ArrayList<String>();
                for (int i = 0; i < jr.length(); i++) {
                    JSONObject jsonObject = jr.getJSONObject(i);
                    Dish_Name = jsonObject.getString("Dish_name");
                    Dish_id=jsonObject.getString("Dish_id");
                    Dish.add(Dish_Name);
                    Dish1.add(Dish_id);
                }
                ArrayAdapter<String> adapter=new ArrayAdapter<String>(Resturant_design.this,android.R.layout.simple_list_item_1,Dish);
                dishname.setAdapter(adapter);
                dishname.setThreshold(1);
                dishname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        dishnames=Dish.get(position).toString();
                        diSh_id=Dish1.get(position);
                        ddish_id=Integer.parseInt(diSh_id);
                    }
                });

                // progressDialog.dismiss();

            } catch (Exception e) {

            }

        }

    }
    class AsyncCallWS4 extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");


            aResponse = com.reviewss("ret_rest_revw",resturantid,createdby);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            try {
                JSONArray jr = new JSONArray(aResponse);
                Dish2=new ArrayList<Detail>();
                for (int i = 0; i < jr.length(); i++) {
                    Detail pro=new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);
                    names = jsonObject.getString("Name");
                    comments = jsonObject.getString("Comment");
                    type = jsonObject.getString("DRvtext");
                    if (jsonObject.has("thurevwdish")) {
                        zlikes = jsonObject.getString("thurevwdish");
                    }
                    date=jsonObject.getString("Date");
                    pfimg=jsonObject.getString("profimg");
                    restneme=jsonObject.getString("Dish_name");
                    Star_review=jsonObject.getString("Star_review");
                    pro.setName(names);
                    pro.setComment(comments);
                    pro.setWeight(type);
                    pro.setDcreateon(date);
                    pro.setPost_like(zlikes);
                    pro.setPfimage(pfimg);
                    pro.setRestaurant_name(restneme);
                    pro.setAt(Star_review);
                    Dish2.add(pro);
                }

               Cut_rest_review adapter=new Cut_rest_review(Resturant_design.this,R.layout.spiner_item,Dish2);
                rev_list.setAdapter(adapter);
                setListViewHeightBasedOnItems(rev_list);
            }catch (Exception e){
                Log.v("JsonErr",e.getMessage());
            }

            if (aResponse.equals("0")) {
                Toast.makeText(Resturant_design.this, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (aResponse.equals("1")) {
                //Toast.makeText(View_dish_Deatails.this, "S!", Toast.LENGTH_LONG).show();


            } else {

            }
        }
    }

    public  boolean setListViewHeightBasedOnItems(ListView cmntlist) {

        ListAdapter listAdapter = cmntlist.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems;itemPos++) {
                View item = listAdapter.getView(itemPos, null, cmntlist);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = cmntlist.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = cmntlist.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            cmntlist.setLayoutParams(params);
            cmntlist.requestLayout();
            return true;

        } else {
            return false;
        }

    }

}



