package com.zdoof.stpl.zdoof.search;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.webservice.PicConstant;

import java.util.ArrayList;

/**
 * Created by stpl on 26/3/16.
 */
public class Customres extends BaseAdapter{
    Search_all search_all;
    ArrayList<Detail> values;
    public Customres(Search_all search_all, int spiner_item, ArrayList<Detail> values) {
        this.search_all=search_all;
        this.values=values;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vgrid;
        LayoutInflater inflater = search_all.getLayoutInflater();
        vgrid = inflater.inflate(R.layout.custom_grocery, null);
        ImageView img= (ImageView) vgrid.findViewById(R.id.img);
        ImageView img1= (ImageView) vgrid.findViewById(R.id.img1);
        TextView text= (TextView) vgrid.findViewById(R.id.textView8);
        TextView text1= (TextView) vgrid.findViewById(R.id.textView10);
        TextView text2= (TextView) vgrid.findViewById(R.id.textView12);
        TextView text3= (TextView) vgrid.findViewById(R.id.textView13);
        TextView text4= (TextView) vgrid.findViewById(R.id.textView15);
        TextView text5= (TextView) vgrid.findViewById(R.id.textView17);

        String pic=values.get(position).getRest_img();

       String pic2= PicConstant.PROFILE_URL1+pic;
        Uri myUri = Uri.parse(pic2);
        Picasso.with(search_all)
                .load(myUri)
                .placeholder(R.drawable.load)
                .error(R.drawable.blankpeople)
                .into(img);

        text.setText(values.get(position).getRestaurant_name());
        text1.setText(values.get(position).getRest_address());
        text2.setText(values.get(position).getRest_phone());
       String pic3=values.get(position+1).getRest_img();
       String pic4=PicConstant.PROFILE_URL1+pic3;
        Uri myUri1 = Uri.parse(pic4);
        Picasso.with(search_all)
                .load(myUri1)
                .placeholder(R.drawable.load)
                .error(R.drawable.blankpeople)
                .into(img1);
        text3.setText(values.get(position+1).getRestaurant_name());
        text4.setText(values.get(position+1).getRest_address());
        text5.setText(values.get(position+1).getRest_phone());
        return vgrid;
    }
}
