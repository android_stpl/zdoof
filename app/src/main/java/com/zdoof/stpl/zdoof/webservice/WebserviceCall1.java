package com.zdoof.stpl.zdoof.webservice;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;

/**
 * @author AndroidExample DotNetWebService Class
 *
 */
public class WebserviceCall1 {

    /**
     * Variable Decleration................
     *
     */
    String namespace = "http://tempuri.org/";
    private String url ="https://www.zdoof.com/web-services-android/zdoof-and-connection.asmx";
    SoapPrimitive resultString;
    String SOAP_ACTION;
    SoapObject request = null, objMessages = null;
    SoapSerializationEnvelope envelope;
    AndroidHttpTransport androidHttpTransport;

    public WebserviceCall1() {
    }


    /**
     * Set Envelope
     */
    protected void SetEnvelope() {

        try {

            // Creating SOAP envelope			
            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            //You can comment that line if your web service is not .NET one.
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            androidHttpTransport = new AndroidHttpTransport(url);
            androidHttpTransport.debug = true;
           /* androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapObject result = (SoapObject)envelope.getResponse();*/

        } catch (Exception e) {
            System.out.println("Soap Exception---->>>" + e.toString());
        }
    }

   /* public String Signup(String MethodName,String username,String password,int mainusertype,int subusertype,int regtype,String firstname,String lastname,
                         int createby,String createdon,String ipadres,int usestatus,String zip,String version,String devicename,String imieno)

    {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();
            PropertyInfo weightProp1 =new PropertyInfo();
            PropertyInfo weightProp2 =new PropertyInfo();
            PropertyInfo weightProp3 =new PropertyInfo();
            PropertyInfo weightProp4 =new PropertyInfo();
            PropertyInfo weightProp5 =new PropertyInfo();
            PropertyInfo weightProp6 =new PropertyInfo();
            PropertyInfo weightProp7 =new PropertyInfo();
            PropertyInfo weightProp8 =new PropertyInfo();
            PropertyInfo weightProp9 =new PropertyInfo();
            PropertyInfo weightProp10 =new PropertyInfo();
            PropertyInfo weightProp11 =new PropertyInfo();
            PropertyInfo weightProp12 =new PropertyInfo();
            PropertyInfo weightProp13 =new PropertyInfo();
            PropertyInfo weightProp14 =new PropertyInfo();


            weightProp.setName("User_name");
            weightProp.setValue(username);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("Password");
            weightProp1.setValue(password);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Main_user_type");
            weightProp2.setValue(mainusertype);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("Sub_user_type");
            weightProp3.setValue(subusertype);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            weightProp4.setName("Reg_type");
            weightProp4.setValue(regtype);
            weightProp4.setType(String.class);
            request.addProperty(weightProp4);

            weightProp5.setName("First_name");
            weightProp5.setValue(firstname);
            weightProp5.setType(String.class);
            request.addProperty(weightProp5);

            weightProp6.setName("Last_name");
            weightProp6.setValue(lastname);
            weightProp6.setType(String.class);
            request.addProperty(weightProp6);

            weightProp7.setName("Created_by");
            weightProp7.setValue(createby);
            weightProp7.setType(String.class);
            request.addProperty(weightProp7);

            weightProp8.setName("Created_on");
            weightProp8.setValue(createdon);
            weightProp8.setType(String.class);
            request.addProperty(weightProp8);

            weightProp9.setName("Ip_address");
            weightProp9.setValue(ipadres);
            weightProp9.setType(String.class);
            request.addProperty(weightProp9);

            weightProp10.setName("User_Status");
            weightProp10.setValue(usestatus);
            weightProp10.setType(String.class);
            request.addProperty(weightProp10);

            weightProp11.setName("Zip_code");
            weightProp11.setValue(zip);
            weightProp11.setType(String.class);
            request.addProperty(weightProp11);

            weightProp12.setName("Version");
            weightProp12.setValue(version);
            weightProp12.setType(String.class);
            request.addProperty(weightProp12);

            weightProp13.setName("Device_name");
            weightProp13.setValue(devicename);
            weightProp13.setType(String.class);
            request.addProperty(weightProp13);

            weightProp14.setName("Imei_no");
            weightProp14.setValue(imieno);
            weightProp14.setType(String.class);
            request.addProperty(weightProp14);




            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
    // MethodName variable is d*///efine for which webservice function  will call

    public String Connection(String MethodName, String frstn,int uid)

    {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();
            PropertyInfo weightProp1 =new PropertyInfo();


            weightProp.setName("firstname");
            weightProp.setValue(frstn);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("User_id");
            weightProp1.setValue(uid);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }

    public String Zlikes(String MethodName, int ztype,String frmdat,int uid,String todt) {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();
            PropertyInfo weightProp2 = new PropertyInfo();
            PropertyInfo weightProp3 = new PropertyInfo();



            weightProp.setName("zLike_type");
            weightProp.setValue(ztype);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("fromdate");
            weightProp1.setValue(frmdat);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Created_by");
            weightProp2.setValue(uid);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("todate");
            weightProp3.setValue(todt);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);


            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
    //////calling Zfans
    public String Zfans(String MethodName, int ztype,String frmdat,int Created_by,String todt) {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();
            PropertyInfo weightProp2 = new PropertyInfo();
            PropertyInfo weightProp3 = new PropertyInfo();



            weightProp.setName("zLike_type");
            weightProp.setValue(ztype);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("fromdate");
            weightProp1.setValue(frmdat);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Created_by");
            weightProp2.setValue(Created_by);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("todate");
            weightProp3.setValue(todt);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);


            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }

    public String connectioncount(String MethodName, int rid)
    {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();
            weightProp.setName("From_user_id");
            weightProp.setValue(0);
            weightProp.setType(String.class);
            weightProp.setName("User_id");
            weightProp.setValue(rid);
            weightProp.setType(String.class);
            request.addProperty(weightProp);


            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }

    public String connecto_foddie(String MethodName, int from_id,int Regd_id,int Connection_type,String todt) {
        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);
            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();
            PropertyInfo weightProp2 = new PropertyInfo();
            PropertyInfo weightProp3 = new PropertyInfo();

            weightProp.setName("From_user_id");
            weightProp.setValue(from_id);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("To_user_id");
            weightProp1.setValue(Regd_id);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Connection_type");
            weightProp2.setValue(Connection_type);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("Created_on");
            weightProp3.setValue(todt);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);


            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
    //////////////////////
  /*  public String AdminassignTask(String MethodName, String department,String name,String empid,String Date,String task,String type) {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();
            PropertyInfo weightProp2 = new PropertyInfo();
            PropertyInfo weightProp3 = new PropertyInfo();
            PropertyInfo weightProp4 = new PropertyInfo();
            PropertyInfo weightProp5 = new PropertyInfo();


            weightProp.setName("Dep");
            weightProp.setValue(department);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("EmpName");
            weightProp1.setValue(name);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("EmpID");
            weightProp2.setValue(empid);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("Date");
            weightProp3.setValue(Date);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            weightProp4.setName("WhichTask");
            weightProp4.setValue(task);
            weightProp4.setType(String.class);
            request.addProperty(weightProp4);

            weightProp5.setName("EntryBy");
            weightProp5.setValue(type);
            weightProp5.setType(String.class);
            request.addProperty(weightProp5);

            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
    ///////////////////////////////////
    public String AddDelNotice(String MethodName, String action,String id,String date,String sub,String descrip,String enterby)
    {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();
            PropertyInfo weightProp1 =new PropertyInfo();
            PropertyInfo weightProp2 =new PropertyInfo();
            PropertyInfo weightProp3 =new PropertyInfo();
            PropertyInfo weightProp4 =new PropertyInfo();
            PropertyInfo weightProp5 =new PropertyInfo();

            weightProp.setName("Action");
            weightProp.setValue(action);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("ID");
            weightProp1.setValue(id);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Date");
            weightProp2.setValue(date);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("Subject");
            weightProp3.setValue(sub);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            weightProp4.setName("Description");
            weightProp4.setValue(descrip);
            weightProp4.setType(String.class);
            request.addProperty(weightProp4);

            weightProp5.setName("EntryBy");
            weightProp5.setValue(enterby);
            weightProp5.setType(String.class);
            request.addProperty(weightProp5);

            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }*/
    /////////////////////////////////////////////////
    /////
	/*public String AddDelNotice1(String MethodName, String action,String id,String date,String sub,String descrip,String enterby)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();
			PropertyInfo weightProp1 =new PropertyInfo();
			PropertyInfo weightProp2 =new PropertyInfo();
			PropertyInfo weightProp3 =new PropertyInfo();
			PropertyInfo weightProp4 =new PropertyInfo();
			PropertyInfo weightProp5 =new PropertyInfo();

			weightProp.setName("Action");
			weightProp.setValue(action);
			weightProp.setType(String.class);
			request.addProperty(weightProp);

			weightProp1.setName("ID");
			weightProp1.setValue(id);
			weightProp1.setType(String.class);
			request.addProperty(weightProp1);

			weightProp2.setName("Date");
			weightProp2.setValue(date);
			weightProp2.setType(String.class);
			request.addProperty(weightProp2);

			weightProp3.setName("Subject");
			weightProp3.setValue(sub);
			weightProp3.setType(String.class);
			request.addProperty(weightProp3);

			weightProp4.setName("Description");
			weightProp4.setValue(descrip);
			weightProp4.setType(String.class);
			request.addProperty(weightProp4);

			weightProp5.setName("EntryBy");
			weightProp5.setValue(enterby);
			weightProp5.setType(String.class);
			request.addProperty(weightProp5);

			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	////

//////////
public String Logout(String MethodName,String emp_id)

{

	try {
		SOAP_ACTION = namespace + MethodName;

		//Adding values to request object
		request = new SoapObject(namespace, MethodName);

		//Adding Double value to request object
		PropertyInfo weightProp =new PropertyInfo();



		weightProp.setName("UserID");
		weightProp.setValue(emp_id);
		weightProp.setType(String.class);
		request.addProperty(weightProp);



		SetEnvelope();

		try {

			//SOAP calling webservice
			androidHttpTransport.call(SOAP_ACTION, envelope);

			//Got Webservice response
			String result = envelope.getResponse().toString();

			return result;

		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}
	} catch (Exception e) {
		// TODO: handle exception
		return e.toString();
	}

}
	//////

	//////////////
	public String EmDeptWiseName(String MethodName, String dpt)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("Department");
			weightProp.setValue(dpt);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	///////////
	public String EmpeNameWiseid(String MethodName, String name)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("Name");
			weightProp.setValue(name);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	///
	public String Alldata(String MethodName,String dd)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("RegID");
			weightProp.setValue(dd);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}*/
    ///package


    /************************************/
}
