package com.zdoof.stpl.zdoof.activites;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.security.ProviderInstaller;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.database.TempStorage;
import com.zdoof.stpl.zdoof.facebook.PrefUtils;
import com.zdoof.stpl.zdoof.facebook.User;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by stpl on 20/1/16.
 */
public class SignUp extends Activity implements View.OnClickListener {
    ContentResolver ct;
    TextView tx;
    Button signup;
    ImageView facebk_login;
    EditText fstname, lname, eml, psw;
    SharedPreferenceClass sharedPreferenceClass;
    String firstname, lastname, emailid, password, Version, Device_name, Ip_address, Imei_no, Created_on, Zip_code;
    int Main_user_type, Sub_user_type, Reg_type, Created_by, User_Status;
    final WebserviceCall com = new WebserviceCall();
    static String aResponse;
    String[] response;
    String response1, responce2;
    String formattedDate;
    CallbackManager callbackManager;
    ProgressDialog progressDialog;
    User user;
    private static final String REQUIRED_MSG = "required";
    private  ImageView imgGooglePLus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        updateAndroidSecurityProvider(this);
        tx = (TextView) findViewById(R.id.txt);
        fstname = (EditText) findViewById(R.id.fn);
        facebk_login = (ImageView) findViewById(R.id.fb);
        sharedPreferenceClass = new SharedPreferenceClass(this);
        imgGooglePLus = (ImageView)findViewById(R.id.imgGooglePLus);
        fstname.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                hasText(fstname);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        lname = (EditText) findViewById(R.id.ln);
        lname.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                hasText(lname);
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        eml = (EditText) findViewById(R.id.el);
        psw = (EditText) findViewById(R.id.ps);
        signup = (Button) findViewById(R.id.button);

        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        formattedDate = df.format(c.getTime());

        tx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUp.this, SignIn.class);
                startActivity(intent);
                finish();
            }
        });
       /* facebk_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstname = fstname.getText().toString().trim();
                lastname = lname.getText().toString().trim();
                emailid = eml.getText().toString().trim();
                password = psw.getText().toString().trim();
                Main_user_type = 4;
                Sub_user_type = 0;
                Reg_type = 1;
                Created_by = 0;
                User_Status = 1;
                Created_on = formattedDate;
                WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
                Ip_address = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
                Zip_code = "";
                sharedPreferenceClass.setValue_string("FNAME", firstname);
                sharedPreferenceClass.setValue_string("LASTNAME", lastname);
                sharedPreferenceClass.setValue_string("IPADDRESS", Ip_address);
                // Device_name = Settings.Global.getUniqueId(SignUp.this);
                Version = Build.VERSION.RELEASE;
               /* TelephonyManager tm=(TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
                Imei_no=tm.getDeviceId();*/
                Imei_no = Settings.Secure.getString(getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                Device_name = Build.MODEL;
                if (((password.equals("") || password.length() == 0)) || ((firstname.equals("") || firstname.length() == 0)) || ((lastname.equals("") || lastname.length() == 0))
                        || ((emailid.equals("") || emailid.length() == 0)))

                {
                    Toast.makeText(getApplicationContext(), "Field should not be blank.", Toast.LENGTH_LONG).show();

                } else if (!isValidEmail(emailid)) {
                    eml.setError("Invalid Email");
                    eml.requestFocus();
                } else if (!isValidPassword(password)) {
                    psw.setError("Please enter at least 6 alpha-numeric & special characters(#,$,@,% etc).");
                    psw.requestFocus();

                } else {

                    AsyncCallWS task = new AsyncCallWS();
                    task.execute();
                }
                //Toast.makeText(SignUp.this,"Field Should Not be blank",Toast.LENGTH_SHORT).show();
            }
        });
        imgGooglePLus.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

    }

    class AsyncCallWS extends AsyncTask<Void, Void, Void> {

        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(SignUp.this, null, "Please wait...");
            dialog.setCancelable(false);
            dialog.show();
        }
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");
            aResponse = com.Signup("signup_insert", emailid, password, Main_user_type, Sub_user_type, Reg_type, firstname, lastname, Created_by, Created_on, Ip_address, User_Status, Zip_code, Version, Device_name, Imei_no);
            response = aResponse.split(",");
            response1 = response[0];
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            /**
             *  Dismiss dialog if display
             */

            if(dialog!=null) {
                dialog.dismiss();
            }


            if (response1.equals("0")) {
                Toast.makeText(getApplicationContext(), "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (response1.equals("5")) {
                Toast.makeText(getApplicationContext(), "Username already exists !", Toast.LENGTH_LONG).show();

            } else if (response1.equals("1")) {


                /**
                 *  Strore USER Details (UTTAM)
                 */
                storeUserDetails(firstname,lastname,emailid);

                ///////////////////////////////

                Toast.makeText(getApplicationContext(), "You have successfully Registered !", Toast.LENGTH_LONG).show();
                Intent i = new Intent(SignUp.this, Home.class);
                startActivity(i);
                finish();
            } else {

            }
        }
    }

    /**
     *  Store signup user details in tempStorage for EDIT PROFILE (UTTAM)
     */
    public void storeUserDetails(String first_name,String last_name,String e_mail){
        TempStorage tempStorage = new TempStorage(SignUp.this);
        tempStorage.putString("FIRST_NAME",first_name);
        tempStorage.putString("LAST_NAME",last_name);
        tempStorage.putString("EMAIL",e_mail);
    }
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password
    public boolean isValidPassword(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean hasText(EditText editText) {

        String text = editText.getText().toString().trim();
        editText.setError(null);

        // length 0 means there is no text
        if (text.length() == 0) {
            editText.setError(REQUIRED_MSG);
            return false;
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        callbackManager = CallbackManager.Factory.create();

        //facebk_login.setReadPermissions("public_profile", "email","user_friends");

        /*facebk_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = new ProgressDialog(SignUp.this);
                progressDialog.setMessage("Loading...");
                progressDialog.show();

                facebk_login.performClick();

                facebk_login.setPressed(true);

                facebk_login.invalidate();

                //facebk_login.registerCallback(callbackManager, mCallBack);

                facebk_login.setPressed(false);

                facebk_login.invalidate();
            }
        });*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            progressDialog.dismiss();

            // App code
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {
                            Log.e("response: ", response + "");
                            try {
                                user = new User();
                                user.facebookID = object.getString("id").toString();
                                user.email = object.getString("email").toString();
                                user.name = object.getString("name").toString();
                                user.gender = object.getString("gender").toString();
                                PrefUtils.setCurrentUser(user, SignUp.this);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Toast.makeText(SignUp.this, "welcome " + user.name, Toast.LENGTH_LONG).show();
                           /* Intent intent=new Intent(SignUp.this,LogoutActivity.class);
                            startActivity(intent);
                            finish();*/
                        }

                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender, birthday");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            progressDialog.dismiss();
        }

        @Override
        public void onError(FacebookException e) {
            progressDialog.dismiss();
        }
    };
    private void updateAndroidSecurityProvider(Activity callingActivity) {
        try {
            ProviderInstaller.installIfNeeded(this);
        } catch (GooglePlayServicesRepairableException e) {
            // Thrown when Google Play Services is not installed, up-to-date, or enabled
            // Show dialog to allow users to install, update, or otherwise enable Google Play services.
            GooglePlayServicesUtil.getErrorDialog(e.getConnectionStatusCode(), callingActivity, 0);
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e("SecurityException", "Google Play Services not available.");
        }
    }
}
