package com.zdoof.stpl.zdoof.activites;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.views.Contropanel;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by stpl on 19/2/16.
 */
public class ChangePasswordActivity extends Activity{
    EditText oldp,newp,cnfp;
    SharedPreferenceClass sharedPreferenceClass;
    Button submit;
    String rid,oldpassword,newpassword,confrmpassword,pass;
    int Regd_id;
    final WebserviceCall com = new WebserviceCall();
    static String aResponse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changepassword);
        oldp= (EditText) findViewById(R.id.oldp);
        newp=(EditText) findViewById(R.id.newpas);
        cnfp=(EditText) findViewById(R.id.confrm);
        submit= (Button) findViewById(R.id.submt);
        sharedPreferenceClass=new SharedPreferenceClass(this);
        rid = sharedPreferenceClass.getValue_string("UIDD").toString();
        Regd_id=Integer.parseInt(rid);
        pass=sharedPreferenceClass.getValue_string("PSW").toString();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newpassword=newp.getText().toString().trim();
                confrmpassword=cnfp.getText().toString().trim();
                oldpassword=oldp.getText().toString().trim();
                if(newpassword.equals("") || (confrmpassword.equals(""))){
                    Toast.makeText(ChangePasswordActivity.this,"Please Fill All the fields",Toast.LENGTH_SHORT).show();
                }
                    else if (!isValidPassword(newpassword)) {
                    newp.setError("At least 6 alpha-numeric and special characters(#,$,@,% etc)");
                    newp.requestFocus();

                    }
                else if(newpassword.equals(confrmpassword)) {
                    AsyncCallWS task = new AsyncCallWS();
                    task.execute();
                } else{
                   Toast.makeText(ChangePasswordActivity.this,"New password & confirm password shoul be same",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    @Override
    public void onBackPressed() {
        Intent in=new Intent(ChangePasswordActivity.this,Contropanel.class);
        startActivity(in);
        finish();
    }
    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            aResponse = com.ChngPass("cng_password", Regd_id, newpassword, oldpassword);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            if (aResponse.equals("4")) {
                Toast.makeText(getApplicationContext(), "Please give correct oldpassword!", Toast.LENGTH_LONG).show();


            } else if (aResponse.equals("2")) {
                Toast.makeText(getApplicationContext(), "You have Changed Your password successfully !", Toast.LENGTH_LONG).show();
                Intent i = new Intent(ChangePasswordActivity.this, Contropanel.class);
                startActivity(i);
                finish();
                // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                //save auth key

            } else {
                Toast.makeText(getApplicationContext(), "Sorry Try again!", Toast.LENGTH_LONG).show();
            }

        }
    }
    public boolean isValidPassword( String password) {

        Pattern pattern;
        Matcher matcher;

        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

}
