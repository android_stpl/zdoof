package com.zdoof.stpl.zdoof.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zdoof.stpl.zdoof.R;

/**
 * Created by stpl on 8/2/16.
 */
public class Experience extends Fragment{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.experience,null);
    }
}
