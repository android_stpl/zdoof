package com.zdoof.stpl.zdoof.search;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.webservice.PicConstant;

import java.util.ArrayList;

/**
 * Created by stpl on 29/3/16.
 */
public class CustomSercall2 extends BaseAdapter{
    Search_all search_all;
    ArrayList<Detail> values;
    String pic,pic2;
    public CustomSercall2(Search_all search_all, int spiner_item, ArrayList<Detail> values) {
        this.search_all=search_all;
        this.values=values;

    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vgrid;
        LayoutInflater inflater = search_all.getLayoutInflater();
        vgrid = inflater.inflate(R.layout.custom_gstoree, null);
        ImageView img= (ImageView) vgrid.findViewById(R.id.imageView13);

        TextView text= (TextView) vgrid.findViewById(R.id.textView8);
        TextView text1= (TextView) vgrid.findViewById(R.id.textView10);
        TextView text2= (TextView) vgrid.findViewById(R.id.textView12);
      /*  TextView text3= (TextView) vgrid.findViewById(R.id.textView13);
        TextView text4= (TextView) vgrid.findViewById(R.id.textView15);
        TextView text5= (TextView) vgrid.findViewById(R.id.textView17);*/

        pic=values.get(position).getGstore_img();

        pic2= PicConstant.PROFILE_URL1+pic;
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(search_all).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.noimg)
                .showImageOnLoading(R.drawable.uute).build();
        imageLoader.displayImage(pic2, img, options);
//        Uri myUri = Uri.parse(pic2);
//        Picasso.with(search_gstore)
//                .load(myUri)
//                .placeholder(R.drawable.load)
//                .error(R.drawable.no)
//                .into(img);

        text.setText(values.get(position).getGstore_name());
        text1.setText(values.get(position).getGstore_address());
        text2.setText(values.get(position).getGstore_phone());
        return  vgrid;
    }
}
