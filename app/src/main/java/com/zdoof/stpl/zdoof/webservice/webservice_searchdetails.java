package com.zdoof.stpl.zdoof.webservice;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;

/**
 * Created by stpl on 24/6/16.
 */
public class webservice_searchdetails {
    String namespace = "http://tempuri.org/";
    private String url ="https://www.zdoof.com/web-services-android/zdoof-and-search-details.asmx";
    //"http://zdoof-debug.azurewebsites.net/web-services-android/zdoof-and-search-details.asmx";
    SoapPrimitive resultString;
    String SOAP_ACTION;
    SoapObject request = null, objMessages = null;
    SoapSerializationEnvelope envelope;
    AndroidHttpTransport androidHttpTransport;

    public webservice_searchdetails() {

    }

    /**
     * Set Envelope
     */
    protected void SetEnvelope() {

        try {

            // Creating SOAP envelope
            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            //You can comment that line if your web service is not .NET one.
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            androidHttpTransport = new AndroidHttpTransport(url);
            androidHttpTransport.debug = true;
           /* androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapObject result = (SoapObject)envelope.getResponse();*/

        } catch (Exception e) {
            System.out.println("Soap Exception---->>>" + e.toString());
        }
    }
    public String resturant_autocomp(String MethodName) {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);




            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
    public String Viewdetails(String MethodName, int dishid,int user_id) {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();



            weightProp.setName("Dish_id");
            weightProp.setValue(dishid);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("Created_by");
            weightProp1.setValue(user_id);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);


            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }

    public String AddReview(String MethodName, int disid, int resturant_idd, String resturant_namee,String star_riview, String comment, int riid1, String expdate) {



            try {
                SOAP_ACTION = namespace + MethodName;

                //Adding values to request object
                request = new SoapObject(namespace, MethodName);

                //Adding Double value to request object
                PropertyInfo weightProp = new PropertyInfo();
                PropertyInfo weightProp1 = new PropertyInfo();
                PropertyInfo weightProp2 = new PropertyInfo();
                PropertyInfo weightProp3 = new PropertyInfo();
                PropertyInfo weightProp4 = new PropertyInfo();
                PropertyInfo weightProp5= new PropertyInfo();
                PropertyInfo weightProp6= new PropertyInfo();


                weightProp.setName("Dish_id");
                weightProp.setValue(disid);
                weightProp.setType(String.class);
                request.addProperty(weightProp);

                weightProp1.setName("Restaurant_id");
                weightProp1.setValue(resturant_idd);
                weightProp1.setType(String.class);
                request.addProperty(weightProp1);

                weightProp2.setName("Restaurant_name");
                weightProp2.setValue(resturant_namee);
                weightProp2.setType(String.class);
                request.addProperty(weightProp2);

                weightProp3.setName("Star_review");
                weightProp3.setValue(star_riview);
                weightProp3.setType(String.class);
                request.addProperty(weightProp3);

                weightProp4.setName("Comment");
                weightProp4.setValue(comment);
                weightProp4.setType(String.class);
                request.addProperty(weightProp4);

                weightProp5.setName("Created_by");
                weightProp5.setValue(riid1);
                weightProp5.setType(String.class);
                request.addProperty(weightProp5);

                weightProp6.setName("Created_on");
                weightProp6.setValue(expdate);
                weightProp6.setType(String.class);
                request.addProperty(weightProp6);

                SetEnvelope();

                try {

                    //SOAP calling webservice
                    androidHttpTransport.call(SOAP_ACTION, envelope);

                    //Got Webservice response
                    String result = envelope.getResponse().toString();

                    return result;

                } catch (Exception e) {
                    // TODO: handle exception
                    return e.toString();
                }
            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }

        }

    public String review(String MethodName, int disid) {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();




            weightProp.setName("Dish_id");
            weightProp.setValue(disid);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }

    public String ResturantReview(String MethodName, int resturantid, String restplc_id, String grocer_plcid, int createdby) {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();
            PropertyInfo weightProp2 = new PropertyInfo();
            PropertyInfo weightProp3 = new PropertyInfo();


            weightProp.setName("Restaurant_id");
            weightProp.setValue(resturantid);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("Restaurant_place_id");
            weightProp1.setValue(restplc_id);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Grocery_place_id");
            weightProp2.setValue(grocer_plcid);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("Created_by");
            weightProp3.setValue(createdby);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);


            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }
    }

    public String ResturantRview(String MethodName, String ratingg, String yrreviw, int resturantid, int ddish_id, String dishname, int createdby, String expdate) {
        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();
            PropertyInfo weightProp2 = new PropertyInfo();
            PropertyInfo weightProp3 = new PropertyInfo();
            PropertyInfo weightProp4 = new PropertyInfo();
            PropertyInfo weightProp5= new PropertyInfo();
            PropertyInfo weightProp6 = new PropertyInfo();


            weightProp.setName("Star_review");
            weightProp.setValue(ratingg);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("Comment");
            weightProp1.setValue(yrreviw);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Restaurant_id");
            weightProp2.setValue(resturantid);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("Dish_id");
            weightProp3.setValue(ddish_id);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            weightProp4.setName("Dish_name");
            weightProp4.setValue(dishname);
            weightProp4.setType(String.class);
            request.addProperty(weightProp4);

            weightProp5.setName("Created_by");
            weightProp5.setValue(createdby);
            weightProp5.setType(String.class);
            request.addProperty(weightProp5);

            weightProp6.setName("Created_on");
            weightProp6.setValue(expdate);
            weightProp6.setType(String.class);
            request.addProperty(weightProp6);


            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }
    }

    public String reviewss(String MethodName, int resturantid, int createdby) {
        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();


            weightProp.setName("Restaurant_id");
            weightProp.setValue(resturantid);
            weightProp.setType(String.class);
            request.addProperty(weightProp);


            weightProp1.setName("Created_by");
            weightProp1.setValue(createdby);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }

    public String Groc_detail(String MethodName, int grocery_id, String rest_plid, int createdby, String grocery_place_id) {
        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();
            PropertyInfo weightProp2 = new PropertyInfo();
            PropertyInfo weightProp3= new PropertyInfo();



            weightProp.setName("Gstore_id");
            weightProp.setValue(grocery_id);
            weightProp.setType(String.class);
            request.addProperty(weightProp);


            weightProp1.setName("Restaurant_place_id");
            weightProp1.setValue(rest_plid);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);


            weightProp2.setName("User_id");
            weightProp2.setValue(createdby);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);


            weightProp3.setName("Grocery_place_id");
            weightProp3.setValue(grocery_place_id);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }

    public String Select_grocery(String MethodName, int grocery_id, String ratingg, String yrreviw, int createdby, String expdate) {
        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();
            PropertyInfo weightProp2 = new PropertyInfo();
            PropertyInfo weightProp3= new PropertyInfo();
            PropertyInfo weightProp4= new PropertyInfo();


            weightProp.setName("Grocery_id");
            weightProp.setValue(grocery_id);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("Star_review");
            weightProp1.setValue(ratingg);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Comment");
            weightProp2.setValue(yrreviw);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("Created_by");
            weightProp3.setValue(createdby);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            weightProp4.setName("Created_on");
            weightProp4.setValue(expdate);
            weightProp4.setType(String.class);
            request.addProperty(weightProp4);


            SetEnvelope();
            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }

    }


