package com.zdoof.stpl.zdoof.search;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zdoof.stpl.zdoof.R;

/**
 * Created by stpl on 21/4/16.
 */
public class CustomSearch7 extends BaseAdapter{
    Search_all search_all;
    String[] sertype;
    int[] sercimg;
    public CustomSearch7(Search_all search_all, int spiner_item, String[] sertype, int[] sercimg) {
        this.search_all=search_all;
        this.sertype=sertype;
        this.sercimg=sercimg;
    }

    @Override
    public int getCount() {
        return sertype.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vgrid;
        LayoutInflater inflater = search_all.getLayoutInflater();
        vgrid = inflater.inflate(R.layout.custom_serdesign, null);
        ImageView image= (ImageView) vgrid.findViewById(R.id.imageView29);
        TextView text= (TextView) vgrid.findViewById(R.id.textView27);
        image.setImageResource(sercimg[position]);
        text.setText(sertype[position]);

        return vgrid;
    }
}
