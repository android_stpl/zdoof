package com.zdoof.stpl.zdoof.activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.utils.AppUtil;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by stpl on 27/1/16.
 */
public class SplashActivity extends Activity {

     ImageView icon,logo;
SharedPreferenceClass sharedPreferenceClass;
        private static final int TIME = 1 * 1000;// 2 seconds
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.splash_screen);
            icon= (ImageView) findViewById(R.id.spoon);
            logo= (ImageView) findViewById(R.id.imageView16);
            sharedPreferenceClass=new SharedPreferenceClass(this);
            String tt = AppUtil.getDateAndTimeFromTimestamp(SplashActivity.this, "/Date(1481916368000)/");
            FacebookSdk.sdkInitialize(getApplicationContext());
            try {
                PackageInfo info = getPackageManager().getPackageInfo(
                        "com.example.android.facebookloginsample",  // replace with your unique package name
                        PackageManager.GET_SIGNATURES);
                for (Signature signature : info.signatures) {
                    MessageDigest md = MessageDigest.getInstance("SHA");
                    md.update(signature.toByteArray());
                    Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                }
            } catch (PackageManager.NameNotFoundException e) {

            } catch (NoSuchAlgorithmException e) {

            }
            if (sharedPreferenceClass.getValue_boolean("Loginstatus") && isNetworkAvailable()) {
                Intent intent = new Intent(SplashActivity.this, Home.class);
                startActivity(intent);
                finish();
                return;
            }

            final Animation Rotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoomout);
            final Animation Rotat = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
            final Animation Rotat1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
            logo.startAnimation(Rotate);
            icon.startAnimation(Rotat);
            icon.startAnimation(Rotat1);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (sharedPreferenceClass.getValue_boolean("Loginstatus") && isNetworkAvailable()) {


                        Intent intent = new Intent(SplashActivity.this, Home.class);
                        startActivity(intent);
                        SplashActivity.this.finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    } else if(!isNetworkAvailable()) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                SplashActivity.this);

                        // set title
                        alertDialogBuilder.setTitle("No Internet!!!");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("No Internet Connection available!")
                                .setCancelable(false)
                                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                       if(isNetworkAvailable()){
                                           Intent intent = new Intent(SplashActivity.this, Home.class);
                                           startActivity(intent);
                                           SplashActivity.this.finish();
                                           overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                       }


                                    }
                                })
                                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();

                        Toast.makeText(SplashActivity.this,"Please connect to network",Toast.LENGTH_LONG).show();

                    }

                     else{
                            Intent intent = new Intent(SplashActivity.this, SignIn.class);
                            startActivity(intent);
                            SplashActivity.this.finish();
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                    }
                }
            }, TIME);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() { } }, TIME);
        }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    }