package com.zdoof.stpl.zdoof.webservice;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;

/**
 * @author AndroidExample DotNetWebService Class
 * 
 */
public class WebserviceCall {
	
	/**
	 * Variable Decleration................
	 * 
	 */
	String namespace = "http://tempuri.org/";
	private String url = "https://www.zdoof.com/web-services-android/zdoof-and-account.asmx";
	
	String SOAP_ACTION;
	SoapObject request = null, objMessages = null;
	SoapSerializationEnvelope envelope;
	AndroidHttpTransport androidHttpTransport;
	
	public WebserviceCall() {
	}
	/**
	 * Set Envelope
	 */
	protected void SetEnvelope() {

		try {
			
            // Creating SOAP envelope			
			envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
			
			//You can comment that line if your web service is not .NET one.
			envelope.dotNet = true;

			envelope.bodyOut = request;
			envelope.encodingStyle = SoapSerializationEnvelope.ENC2001;
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			androidHttpTransport = new AndroidHttpTransport(url);
			androidHttpTransport.debug = true;
			
		} catch (Exception e) {
			System.out.println("Soap Exception---->>>" + e.toString());	
		}
	}

	public String Signup(String MethodName,String username,String password,int mainusertype,int subusertype,int regtype,String firstname,String lastname,
	            int createby,String createdon,String ipadres,int usestatus,String zip,String version,String devicename,String imieno)

	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();
			PropertyInfo weightProp1 =new PropertyInfo();
			PropertyInfo weightProp2 =new PropertyInfo();
			PropertyInfo weightProp3 =new PropertyInfo();
			PropertyInfo weightProp4 =new PropertyInfo();
			PropertyInfo weightProp5 =new PropertyInfo();
			PropertyInfo weightProp6 =new PropertyInfo();
			PropertyInfo weightProp7 =new PropertyInfo();
			PropertyInfo weightProp8 =new PropertyInfo();
			PropertyInfo weightProp9 =new PropertyInfo();
			PropertyInfo weightProp10 =new PropertyInfo();
			PropertyInfo weightProp11 =new PropertyInfo();
			PropertyInfo weightProp12 =new PropertyInfo();
			PropertyInfo weightProp13 =new PropertyInfo();
			PropertyInfo weightProp14 =new PropertyInfo();


			weightProp.setName("User_name");
			weightProp.setValue(username);
			weightProp.setType(String.class);
			request.addProperty(weightProp);

			weightProp1.setName("Password");
			weightProp1.setValue(password);
			weightProp1.setType(String.class);
			request.addProperty(weightProp1);

			weightProp2.setName("Main_user_type");
			weightProp2.setValue(mainusertype);
			weightProp2.setType(String.class);
			request.addProperty(weightProp2);

			weightProp3.setName("Sub_user_type");
			weightProp3.setValue(subusertype);
			weightProp3.setType(String.class);
			request.addProperty(weightProp3);

			weightProp4.setName("Reg_type");
			weightProp4.setValue(regtype);
			weightProp4.setType(String.class);
			request.addProperty(weightProp4);

			weightProp5.setName("First_name");
			weightProp5.setValue(firstname);
			weightProp5.setType(String.class);
			request.addProperty(weightProp5);

			weightProp6.setName("Last_name");
			weightProp6.setValue(lastname);
			weightProp6.setType(String.class);
			request.addProperty(weightProp6);

			weightProp7.setName("Created_by");
			weightProp7.setValue(createby);
			weightProp7.setType(String.class);
			request.addProperty(weightProp7);

			weightProp8.setName("Created_on");
			weightProp8.setValue(createdon);
			weightProp8.setType(String.class);
			request.addProperty(weightProp8);

			weightProp9.setName("Ip_address");
			weightProp9.setValue(ipadres);
			weightProp9.setType(String.class);
			request.addProperty(weightProp9);

			weightProp10.setName("User_Status");
			weightProp10.setValue(usestatus);
			weightProp10.setType(String.class);
			request.addProperty(weightProp10);

			weightProp11.setName("Zip_code");
			weightProp11.setValue(zip);
			weightProp11.setType(String.class);
			request.addProperty(weightProp11);

			weightProp12.setName("Version");
			weightProp12.setValue(version);
			weightProp12.setType(String.class);
			request.addProperty(weightProp12);

			weightProp13.setName("Device_name");
			weightProp13.setValue(devicename);
			weightProp13.setType(String.class);
			request.addProperty(weightProp13);

			weightProp14.setName("Imei_no");
			weightProp14.setValue(imieno);
			weightProp14.setType(String.class);
			request.addProperty(weightProp14);




			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	/////////connection cont
	public String connectioncount(String MethodName,int userid)

	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("Imei_no");
			weightProp.setValue(userid);
			weightProp.setType(String.class);
			request.addProperty(weightProp);




			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	// MethodName variable is define for which webservice function  will call

	public String SignIn(String MethodName, String username,String pass)
	{
		try {
			SOAP_ACTION = namespace + MethodName;
			//Adding values to request object
			request = new SoapObject(namespace, MethodName);
			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();
			PropertyInfo weightProp1 =new PropertyInfo();
			weightProp.setName("User_name");
	        weightProp.setValue(username);
			weightProp.setType(String.class);
			request.addProperty(weightProp);
			weightProp1.setName("Password");
			weightProp1.setValue(pass);
			weightProp1.setType(String.class);
			request.addProperty(weightProp1);
			SetEnvelope();
			try {
				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);
				
				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;
				
			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}


	public String Usersearch(String MethodName, int rid,String fstname,String lname,int mainusertype,int subtype,String creton,String updton,String ip) {

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp = new PropertyInfo();
			PropertyInfo weightProp1 = new PropertyInfo();
			PropertyInfo weightProp2 = new PropertyInfo();
			PropertyInfo weightProp3 = new PropertyInfo();
			PropertyInfo weightProp4 = new PropertyInfo();
			PropertyInfo weightProp5 = new PropertyInfo();
			PropertyInfo weightProp6 = new PropertyInfo();
			PropertyInfo weightProp7 = new PropertyInfo();


			weightProp.setName("User_id");
			weightProp.setValue(rid);
			weightProp.setType(String.class);
			request.addProperty(weightProp);

			weightProp1.setName("First_name");
			weightProp1.setValue(fstname);
			weightProp1.setType(String.class);
			request.addProperty(weightProp1);

			weightProp2.setName("Last_name");
			weightProp2.setValue(lname);
			weightProp2.setType(String.class);
			request.addProperty(weightProp2);

			weightProp3.setName("Main_user_type");
			weightProp3.setValue(mainusertype);
			weightProp3.setType(String.class);
			request.addProperty(weightProp3);

			weightProp4.setName("Sub_user_type");
			weightProp4.setValue(subtype);
			weightProp4.setType(String.class);
			request.addProperty(weightProp4);

			weightProp5.setName("Created_on");
			weightProp5.setValue(creton);
			weightProp5.setType(String.class);
			request.addProperty(weightProp5);

			weightProp6.setName("Updated_on");
			weightProp6.setValue(updton);
			weightProp6.setType(String.class);
			request.addProperty(weightProp6);

			weightProp7.setName("Ip_address");
			weightProp7.setValue(ip);
			weightProp7.setType(String.class);
			request.addProperty(weightProp7);

			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	//////////////////////
	public String ChngPass(String MethodName, int rid,String nwpassword,String oldpass) {

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp = new PropertyInfo();
			PropertyInfo weightProp1 = new PropertyInfo();
			PropertyInfo weightProp2 = new PropertyInfo();


			weightProp.setName("User_id");
			weightProp.setValue(rid);
			weightProp.setType(String.class);
			request.addProperty(weightProp);

			weightProp1.setName("Password");
			weightProp1.setValue(nwpassword);
			weightProp1.setType(String.class);
			request.addProperty(weightProp1);

			weightProp2.setName("Confpwd");
			weightProp2.setValue(oldpass);
			weightProp2.setType(String.class);
			request.addProperty(weightProp2);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	///////////////////////////////////
	public String Editprofile(String MethodName, int Regd_id, String First_name, String Last_name, String Address_line1, String Address_line2, String Zip_code, String City,
							  String State, String Country, String Gender, String DOB, String Marital_status, String Mobile, String Email, String Profile_image,
							  int Main_user_type, int Sub_user_type, int Updated_by, String Updated_on)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();
			PropertyInfo weightProp1 =new PropertyInfo();
			PropertyInfo weightProp2 =new PropertyInfo();
			PropertyInfo weightProp3 =new PropertyInfo();
			PropertyInfo weightProp4 =new PropertyInfo();
			PropertyInfo weightProp5 =new PropertyInfo();
			PropertyInfo weightProp6 =new PropertyInfo();
			PropertyInfo weightProp7 =new PropertyInfo();
			PropertyInfo weightProp8 =new PropertyInfo();
			PropertyInfo weightProp9 =new PropertyInfo();
			PropertyInfo weightProp10 =new PropertyInfo();
			PropertyInfo weightProp11 =new PropertyInfo();
			PropertyInfo weightProp12 =new PropertyInfo();
			PropertyInfo weightProp13 =new PropertyInfo();
			PropertyInfo weightProp14 =new PropertyInfo();
			PropertyInfo weightProp15 =new PropertyInfo();
			PropertyInfo weightProp16 =new PropertyInfo();
			PropertyInfo weightProp17 =new PropertyInfo();
			PropertyInfo weightProp18 =new PropertyInfo();



			weightProp.setName("Regd_id");
			weightProp.setValue(Regd_id);
			weightProp.setType(String.class);
			request.addProperty(weightProp);

			weightProp1.setName("First_name");
			weightProp1.setValue(First_name);
			weightProp1.setType(String.class);
			request.addProperty(weightProp1);

			weightProp2.setName("Last_name");
			weightProp2.setValue(Last_name);
			weightProp2.setType(String.class);
			request.addProperty(weightProp2);

			weightProp3.setName("Address_line1");
			weightProp3.setValue(Address_line1);
			weightProp3.setType(String.class);
			request.addProperty(weightProp3);

			weightProp4.setName("Address_line2");
			weightProp4.setValue(Address_line2);
			weightProp4.setType(String.class);
			request.addProperty(weightProp4);

			weightProp5.setName("Zip_code");
			weightProp5.setValue(Zip_code);
			weightProp5.setType(String.class);
			request.addProperty(weightProp5);


			weightProp6.setName("City");
			weightProp6.setValue(City);
			weightProp6.setType(String.class);
			request.addProperty(weightProp6);

			weightProp7.setName("State");
			weightProp7.setValue(State);
			weightProp7.setType(String.class);
			request.addProperty(weightProp7);

			weightProp8.setName("Country");
			weightProp8.setValue(Country);
			weightProp8.setType(String.class);
			request.addProperty(weightProp8);

			weightProp9.setName("Gender");
			weightProp9.setValue(Gender);
			weightProp9.setType(String.class);
			request.addProperty(weightProp9);

			weightProp10.setName("DOB");
			weightProp10.setValue(DOB);
			weightProp10.setType(String.class);
			request.addProperty(weightProp10);

			weightProp11.setName("Marital_status");
			weightProp11.setValue(Marital_status);
			weightProp11.setType(String.class);
			request.addProperty(weightProp11);


			weightProp12.setName("Mobile");
			weightProp12.setValue(Mobile);
			weightProp12.setType(String.class);
			request.addProperty(weightProp12);

			weightProp13.setName("Email");
			weightProp13.setValue(Email);
			weightProp13.setType(String.class);
			request.addProperty(weightProp13);

			weightProp14.setName("Profile_image");
			weightProp14.setValue(Profile_image);
			weightProp14.setType(String.class);
			request.addProperty(weightProp14);

			weightProp15.setName("Main_user_type");
			weightProp15.setValue(Main_user_type);
			weightProp15.setType(String.class);
			request.addProperty(weightProp15);

			weightProp16.setName("Sub_user_type");
			weightProp16.setValue(Sub_user_type);
			weightProp16.setType(String.class);
			request.addProperty(weightProp16);

			weightProp17.setName("Updated_by");
			weightProp17.setValue(Updated_by);
			weightProp17.setType(String.class);
			request.addProperty(weightProp17);

			weightProp18.setName("Updated_on");
			weightProp18.setValue(Updated_on);
			weightProp18.setType(String.class);
			request.addProperty(weightProp18);

			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}

	public String Forgot_pass(String MethodName, String email) {
		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp = new PropertyInfo();


			weightProp.setName("mailid");
			weightProp.setValue(email);
			weightProp.setType(String.class);
			request.addProperty(weightProp);





			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}


	/////////////////////////////////////////////////
	/////
	/*public String Addlike(String MethodName,int Post_id, int Created_by, string Created_on)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();
			PropertyInfo weightProp1 =new PropertyInfo();
			PropertyInfo weightProp2 =new PropertyInfo();

			weightProp.setName("Post_id");
			weightProp.setValue(Post_id);
			weightProp.setType(String.class);
			request.addProperty(weightProp);

			weightProp1.setName("Created_by");
			weightProp1.setValue(Created_by);
			weightProp1.setType(String.class);
			request.addProperty(weightProp1);

			weightProp2.setName("Created_on");
			weightProp2.setValue(Created_on);
			weightProp2.setType(String.class);
			request.addProperty(weightProp2);

			SetEnvelope();

			try {
				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	////

//////////
public String Addcomment(String MethodName,Double Star_review, string Comment, int Post_id, int Created_by, string Created_on)

{

	try {
		SOAP_ACTION = namespace + MethodName;

		//Adding values to request object
		request = new SoapObject(namespace, MethodName);

		//Adding Double value to request object
		PropertyInfo weightProp =new PropertyInfo();
        PropertyInfo weightProp1 =new PropertyInfo();
		PropertyInfo weightProp2 =new PropertyInfo();
       PropertyInfo weightProp3 =new PropertyInfo();
       PropertyInfo weightProp4 =new PropertyInfo();

		weightProp.setName("Star_review");
		weightProp.setValue(Star_review);
		weightProp.setType(String.class);
		request.addProperty(weightProp);

        weightProp1.setName("Comment");
		weightProp1.setValue(Comment);
		weightProp1.setType(String.class);
		request.addProperty(weightProp1);

        weightProp2.setName("Post_id");
		weightProp2.setValue(Post_id);
		weightProp2.setType(String.class);
		request.addProperty(weightProp2);

		weightProp3.setName("Created_by");
		weightProp3.setValue(Created_by);
		weightProp3.setType(String.class);
		request.addProperty(weightProp3);

        weightProp4.setName("Created_on");
		weightProp4.setValue(Created_on);
		weightProp4.setType(String.class);
		request.addProperty(weightProp4);
		SetEnvelope();

		try {

			//SOAP calling webservice
			androidHttpTransport.call(SOAP_ACTION, envelope);

			//Got Webservice response
			String result = envelope.getResponse().toString();

			return result;

		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}
	} catch (Exception e) {
		// TODO: handle exception
		return e.toString();
	}

}
	//////
	public String Push(String MethodName, String rid)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("RegID");
			weightProp.setValue(rid);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	//////////////
	public String EmDeptWiseName(String MethodName, String dpt)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("Department");
			weightProp.setValue(dpt);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	///////////
	public String EmpeNameWiseid(String MethodName, String name)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("Name");
			weightProp.setValue(name);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	///
	public String Alldata(String MethodName,String dd)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("RegID");
			weightProp.setValue(dd);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}*/
	///package

	/************************************/
}
