package com.zdoof.stpl.zdoof.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.util.Base64;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Shiftu_Android_10 on 15-12-2016.
 */

public class AppUtil {

    private static String TAG = "AppUtil";
    private static SimpleDateFormat  sdf=new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
    private static SimpleDateFormat  sdf1=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");


    //     String date_time = AppUtil.getDateAndTimeFromTimestamp(context, jsondatetime);
    public static String getDateAndTimeFromTimestamp(Context context, String timeStamp) {

        if (timeStamp != null && timeStamp.trim().length() > 0) {
            timeStamp = timeStamp.replace("/", "");

            if (timeStamp.trim().toLowerCase().contains(new String("Date").toLowerCase())) {
                String[] ss = timeStamp.split("Date");
                String rest = ss[1];
                rest = rest.replace("(", "");
                rest = rest.replace(")", "");
                try {
                    long tt = 0;
                    if (TextUtils.isDigitsOnly(rest)) {
                        tt = Long.parseLong(rest);
                        timeStamp = String.valueOf(tt);

                    }
                } catch (Exception exception) {
                    Log.e(TAG, "getDateAndTimeFromTimestamp()", exception);
                }
            }

        }

        try {
            Calendar calendar = Calendar.getInstance();

            calendar.setTimeInMillis(Long.parseLong(timeStamp));
            System.out.println(calendar.getTime());
           // sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            sdf=new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
            //Date date=sdf.parse(String.valueOf(calendar.getTime()));
            sdf1.setTimeZone(TimeZone.getDefault());
            return sdf1.format(calendar.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return timeStamp;
        }
    }

    public static String getDateCurrentTimeZone(long timeInMillis) {
        try{

            //time difference will check with server time
            long diff = 1000 * 60 * 60 * 7 ;

            Date date11=new Date(timeInMillis);
            date11.getTime();

            long addedtime=timeInMillis;
            long hour = (addedtime / 1000) / 60 / 60;

            Date date = new Date(timeInMillis-diff);

            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "dd-MM-yyyy HH:mm:ss", Locale.ENGLISH);
            dateFormat.setTimeZone(TimeZone.getDefault());
           String formattedDate = dateFormat.format(date);
            return formattedDate;
        }catch (Exception e) {
        }
        return "";
    }
    public static String printKeyHash(Context context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (android.content.pm.Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    public static Boolean GetLocationPermission(Context context) {
        Boolean result = false;
        if (
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)==PackageManager.PERMISSION_GRANTED) {
            result = true;
        }
        return result;
    }

    public static Boolean GetStoragePermission(Context context) {
        Boolean result = false;
        if (
                ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED) {
            result = true;
        }
        return result;
    }

    public static void dismissKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != activity.getCurrentFocus()) {
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getApplicationWindowToken(), 0);
        }
    }
}
