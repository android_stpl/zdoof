package com.zdoof.stpl.zdoof.search;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.search.Details.Grocery_details;
import com.zdoof.stpl.zdoof.webservice.PicConstant;

import java.util.ArrayList;

/**
 * Created by stpl on 13/3/16.
 */
public class CustomGstore extends BaseAdapter {
    Search_gstore search_gstore;
    ArrayList<Detail> values;
    String pic,pic2,pic3,pic4;
    String grocery_id;
    public CustomGstore(Search_gstore search_gstore, int spiner_item, ArrayList<Detail> values) {
        this.search_gstore=search_gstore;
        this.values=values;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final View vgrid;
        LayoutInflater inflater = search_gstore.getLayoutInflater();
        vgrid = inflater.inflate(R.layout.custom_gstoree, null);
        RatingBar rateBar = (RatingBar) vgrid.findViewById(R.id.store_ratings);
        ImageView img= (ImageView) vgrid.findViewById(R.id.imageView13);
        TextView text= (TextView) vgrid.findViewById(R.id.textView8);
        TextView text1= (TextView) vgrid.findViewById(R.id.textView10);
        TextView text2= (TextView) vgrid.findViewById(R.id.textView12);
        TextView viewdetails=(TextView) vgrid.findViewById(R.id.viewdetails);

      /*  TextView text3= (TextView) vgrid.findViewById(R.id.textView13);
        TextView text4= (TextView) vgrid.findViewById(R.id.textView15);
        TextView text5= (TextView) vgrid.findViewById(R.id.textView17);*/
        Float rating = Float.parseFloat(values.get(position).getRatings());
        rateBar.setRating(rating);
        rateBar.setIsIndicator(true);
        pic=values.get(position).getGstore_img();
        pic2= PicConstant.PROFILE_URL1+pic;
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(search_gstore).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                        // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.noimg)
                .showImageOnLoading(R.drawable.uute).build();
        imageLoader.displayImage(pic2, img, options);

//        Uri myUri = Uri.parse(pic2);
//        Picasso.with(search_gstore)
//                .load(myUri)
//                .placeholder(R.drawable.load)
//                .error(R.drawable.no)
//                .into(img);

        text.setText(values.get(position).getGstore_name());
        text1.setText(values.get(position).getGstore_address());
        text2.setText(values.get(position).getGstore_phone());

        viewdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                grocery_id=values.get(position).getGstore_id();
                Intent intent=new Intent(search_gstore, Grocery_details.class);
                intent.putExtra("GCD",grocery_id);
                search_gstore.startActivity(intent);

            }
        });

       /* try {
            pic3 = values.get(position + 1).getGstore_img();
            pic4 = PicConstant.PROFILE_URL1 + pic3;
            imageLoader.displayImage(pic4, img1, options);
           *//* Uri myUri1 = Uri.parse(pic4);
            Picasso.with(search_gstore)
                    .load(myUri1)
                    .placeholder(R.drawable.load)
                    .error(R.drawable.noimg)
                    .into(img1);*//*
            text3.setText(values.get(position + 1).getGstore_name());
            text4.setText(values.get(position + 1).getGstore_address());
            text5.setText(values.get(position + 1).getGstore_phone());
        }catch (Exception e){
            
        }*/
        return vgrid;
    }
    }

