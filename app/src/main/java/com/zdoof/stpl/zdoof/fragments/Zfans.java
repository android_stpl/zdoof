package com.zdoof.stpl.zdoof.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.activites.Profiles;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.adapters.CustomZfans;
import com.zdoof.stpl.zdoof.utils.AppUtil;
import com.zdoof.stpl.zdoof.utils.ImageListener;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall1;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by stpl on 2/3/16.
 */
public class Zfans extends Fragment {
    ListView list;
    String rid,First_name,zfrmdt="",ztodt="";
    int Regd_id,zliketype=0;
    SharedPreferenceClass shared;
    static String aResponse;
    ProgressDialog progressDialog;
    final WebserviceCall1 com1 = new WebserviceCall1();
    ArrayList<Detail> detl;
    ImageListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (ImageListener) ((Activity) context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.zfans,null);
        list= (ListView) rootView.findViewById(R.id.listView2);
        shared= new SharedPreferenceClass(getActivity());
        rid = shared.getValue_string("UDDIDD").toString();
        Regd_id=Integer.parseInt(rid);
        First_name="";

        return rootView;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {

            // If we are becoming invisible, then...
            AsyncCallWS task = new AsyncCallWS();
            task.execute();

            if (!isVisibleToUser) {
                Log.d("MyFragment", "Not visible anymore.  Stopping audio.");
                // TODO stop audio playback
            }
        }
    }
    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressDialog = ProgressDialog.show(getActivity(), "", "Loading...");
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            aResponse = com1.Zfans("user_zfans", zliketype, zfrmdt, Regd_id, ztodt);
            String a = aResponse;
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            //progressDialog.dismiss();
            try {
                JSONArray jr = new JSONArray(aResponse);

                detl = new ArrayList<Detail>();

                for (int i = 0; i < jr.length(); i++) {

                    Detail productdto = new Detail();

                    JSONObject jsonObject = jr.getJSONObject(i);
                    String likedto=jsonObject.getString("Like_to").toString();
                    String Zfan=jsonObject.getString("name").toString();
                    String type=jsonObject.getString("typetxt").toString();
                    String likedonn=jsonObject.getString("Created_on").toString();
                    likedonn = convertTime(likedonn);
                    //String ter = convertTime(likedonn);
                    //likedonn = AppUtil.getDateAndTimeFromTimestamp(getActivity(), likedonn);

                    productdto.setLikedto(likedto);
                    productdto.setZfan(Zfan);
                    productdto.setType(type);
                    productdto.setLikedon(likedonn);

                    detl.add(productdto);
                }
                CustomZfans adapter=new CustomZfans(getActivity(),R.layout.spiner_item,detl);
                list.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }catch (Exception e){
                Log.e("Zfans", "Exception", e);
            }

            if (aResponse.equals("0")) {
                Toast.makeText(getActivity(), "No zFans Availabe", Toast.LENGTH_LONG).show();

            } else if (aResponse.equals("1")) {
                Toast.makeText(getActivity(), "You have successfully Registered !", Toast.LENGTH_LONG).show();

            } else {

               /* Intent intent=new Intent(Profiles.this,Connecton.class);
                startActivity(intent);
                finish();*/
            }
        }

        private String convertTime(String jTime) {
            String returnString = null;
            returnString = jTime.substring(6, jTime.length()-2);
            long epoch = Long.parseLong(returnString);
            //returnString = new java.text.SimpleDateFormat("dd/MMM/yyyy 'at' HH:mm:ss aaa").format(new java.util.Date (epoch));
            Date date = new Date(epoch-5400000);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy 'at' HH:mm:ss aaa");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            returnString = sdf.format(date);
            return returnString;
        }
    }
}
