package com.zdoof.stpl.zdoof.search;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.utils.AppUtil;
import com.zdoof.stpl.zdoof.activites.Connecton;
import com.zdoof.stpl.zdoof.views.Contropanel;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.activites.Home;
import com.zdoof.stpl.zdoof.activites.Notifications;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall2;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by stpl on 13/3/16.
 */
public class Search_post extends Activity implements LocationListener{
    ImageView all,post,grocery,resturant,food,serc;
    ImageView close,home,connect,noti,icon,contlpnl,search_close;
    ImageView active1,active2,act3,act4,act5,act6,act7;
    private FloatingActionButton fab;
    ListView list;
    LinearLayout ll1,ll2,ll3,ll4,mainBody,ziploc,vieew;
    ProgressDialog progressDialog;
    ArrayList<Detail> Values;

    String zipcode;
    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    // String rest, dish, pep, gros, posts;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    String loATION,des;
    final WebserviceCall2 com2 = new WebserviceCall2();
    static String aResponse;
    SharedPreferenceClass sharedPreferenceClass;
    String description="",locationn="";
    String post_image,postdetails,postname,creaton,result;
    Dialog dialog;
    EditText descc,zips;
    TextView textt,code;
    String desc,profile_image,reviews;
    ImageView rch;
    Button submit;
    String pos,searchvalue="",style;
    ListView listt;
    String sertype[]={"Search Post","Search Foodie","Search Recipe/Dish","Search Restaurant","Search Grocery Store"};
    int sercimg[]={R.drawable.searhicon,R.drawable.sst,R.drawable.ff,R.drawable.sahlist,R.drawable.crt,R.drawable.uur};
    private final int LOCATION_PERMISSION_CODE=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_post);
        list= (ListView) findViewById(R.id.listviewf);
        listt= (ListView) findViewById(R.id.li);
        serc= (ImageView) findViewById(R.id.serc);
        rch= (ImageView) findViewById(R.id.sec);
       /* all= (ImageView) findViewById(R.id.image5);
        post= (ImageView) findViewById(R.id.imageVie);
        grocery=(ImageView) findViewById(R.id.grocery);
        resturant=(ImageView) findViewById(R.id.imageViwes);
        active1=(ImageView) findViewById(R.id.act1);
        active2=(ImageView) findViewById(R.id.act2);
        act3=(ImageView) findViewById(R.id.act3);
        act4=(ImageView) findViewById(R.id.act4);
        act5=(ImageView) findViewById(R.id.act5);
        act6=(ImageView) findViewById(R.id.act6);
        act7=(ImageView) findViewById(R.id.act7);*/
        search_close= (ImageView) findViewById(R.id.close);
        descc= (EditText) findViewById(R.id.imageView11);
        ll1= (LinearLayout) findViewById(R.id.hmj);
        ll2= (LinearLayout) findViewById(R.id.cnn);
        ll3=(LinearLayout) findViewById(R.id.ntf);
        ll4=(LinearLayout) findViewById(R.id.ct);
        mainBody = (LinearLayout) findViewById(R.id.postMainBody);
        food= (ImageView) findViewById(R.id.imageVies);
        home= (ImageView) findViewById(R.id.hme);
        connect= (ImageView) findViewById(R.id.hm);
        icon=(ImageView) findViewById(R.id.icon);
        contlpnl = (ImageView) findViewById(R.id.ctrl);
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header1 = (ViewGroup)inflater.inflate(R.layout.search_view, listt, false);
        listt.addHeaderView(header1, null, false);
        serc= (ImageView) findViewById(R.id.serc);
        textt= (TextView) findViewById(R.id.textt);
        ziploc= (LinearLayout) findViewById(R.id.ziploc);
        vieew= (LinearLayout) findViewById(R.id.view);
        zips= (EditText) findViewById(R.id.zip);
        code= (TextView) findViewById(R.id.code);
        submit= (Button) findViewById(R.id.submit);
        sharedPreferenceClass=new SharedPreferenceClass(this);
        searchvalue=sharedPreferenceClass.getValue_string("SEARCHVALUE");
        descc.setText(searchvalue);
        if(searchvalue.equals("")){
            search_close.setVisibility(View.GONE);
        }
        else if(searchvalue.equals("0")){
            search_close.setVisibility(View.GONE);
        }else {
            search_close.setVisibility(View.VISIBLE);
            sharedPreferenceClass.setValue_string("SEARCHVALUE",desc);
        }
        //fab= (FloatingActionButton) findViewById(R.id.fab);
        //active2.setVisibility(View.VISIBLE);
        progressDialog = ProgressDialog.show(Search_post.this, "", "Loading...");
        AsyncCallWS task = new AsyncCallWS();
        task.execute();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zipcode =zips.getText().toString();
                code.setText(zipcode);
                sharedPreferenceClass.setValue_string("ZZIP", zipcode);
                ziploc.setVisibility(View.GONE);
                vieew.setVisibility(View.GONE);
            }
        });
        textt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ziploc.setVisibility(View.VISIBLE);
                vieew.setVisibility(View.VISIBLE);
            }
        });
        search_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descc.setText("");
                sharedPreferenceClass.setValue_string("SEARCHVALUE", "0");
                search_close.setVisibility(View.GONE);
            }
        });
        ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll2.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent = new Intent(Search_post.this, Connecton.class);
                // intent.putExtra("EML",det);
                startActivity(intent);
                finish();

            }
        });
        mainBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listt.getVisibility()== View.VISIBLE) {
                    listt.setVisibility(View.GONE);
                }
            }
        });
        ll4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll4.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent = new Intent(Search_post.this, Contropanel.class);
                startActivity(intent);
                finish();
            }
        });
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll1.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent=new Intent(Search_post.this,Home.class);
                startActivity(intent);
                finish();
            }
        });
        ll3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ll1.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent=new Intent(Search_post.this,Notifications.class);
                startActivity(intent);
                finish();
            }
        });
        rch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listt.getVisibility()== View.VISIBLE) {
                    listt.setVisibility(View.GONE);
                }else {
                    listt.setVisibility(View.VISIBLE);
                    CustomSearch9 adapter = new CustomSearch9(Search_post.this, R.layout.spiner_item, sertype, sercimg);
                    listt.setAdapter(adapter);

                    listt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            desc = descc.getText().toString().trim();
                            if (desc.length()<3) {
                                Toast.makeText(Search_post.this, "Please enter atleast 3 letters to search!", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                pos = listt.getItemAtPosition(position).toString();
                                if (pos.equals("0")) {
                                    desc = descc.getText().toString().trim();
                                    descc.setText(desc);
                                    sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                                    Intent intent = new Intent(Search_post.this, Search_post.class);
                                    startActivity(intent);
                                    finish();
                                } else if (pos.equals("1")) {
                                    desc = descc.getText().toString().trim();
                                    descc.setText(desc);
                                    sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                                    Intent intent = new Intent(Search_post.this, Search_people.class);
                                    startActivity(intent);
                                    finish();
                                } else if (pos.equals("2")) {
                                    desc = descc.getText().toString().trim();
                                    descc.setText(desc);
                                    sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                                    Intent intent = new Intent(Search_post.this, Search_dish.class);
                                    startActivity(intent);
                                    finish();
                                } else if (pos.equals("3")) {
                                    desc = descc.getText().toString().trim();
                                    descc.setText(desc);
                                    sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                                    Intent intent = new Intent(Search_post.this, Search_resturant.class);
                                    startActivity(intent);
                                    finish();
                                } else if (pos.equals("4")) {
                                    desc = descc.getText().toString().trim();
                                    descc.setText(desc);
                                    sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                                    Intent intent = new Intent(Search_post.this, Search_gstore.class);
                                    startActivity(intent);
                                    finish();
                                } /*else if (pos.equals("5")) {}*/
                                listt.setVisibility(View.GONE);
                            }
                        }
                    });

                }
            }
        });
        descc.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            desc = descc.getText().toString().trim();

                            Intent intent = new Intent(Search_post.this, Search_post.class);
                            sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                            startActivity(intent);
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
        descc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descc.setCursorVisible(true);
            }
        });

        serc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //description.setCursorVisible(true);
                desc = descc.getText().toString().trim();
                if (desc.length()<3) {
                    Toast.makeText(Search_post.this, "Please enter atleast 3 letters to search!", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent(Search_post.this, Search_post.class);
                    sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                    startActivity(intent);
                }
            }

        });

        String z_zip = sharedPreferenceClass.getValue_string("ZZIP");
        if (!z_zip.equals(null) && !z_zip.equals("") && !(z_zip.length()<2)) {
            code.setText(z_zip);
            zipcode = z_zip;
        } else {
            if (AppUtil.GetLocationPermission(this)) {
                GetZipcodeTask zipTask = new GetZipcodeTask();
                zipTask.execute();
            }
            else {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CODE);
            }
        }
    }

    private class GetZipcodeTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... params) {
            getZipcodeOfLocation();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String zzip = sharedPreferenceClass.getValue_string("ZZIP");
            if (!zzip.equals(null) && !zzip.equals("") && !zzip.equals("0") && !(zzip.length()<2)) {
                code.setText(zzip);
            } else {
                code.setText(zipcode);
                sharedPreferenceClass.setValue_string("ZZIP", zipcode);
            }
            zipcode = sharedPreferenceClass.getValue_string("ZZIP");
        }
    }

    private void getZipcodeOfLocation() {
        LocationManager manager = (LocationManager) Search_post.this.getSystemService(LOCATION_SERVICE);
        // getting GPS status
        isGPSEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        isNetworkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Geocoder geocoder = new Geocoder(Search_post.this, Locale.getDefault());
        if (
                ActivityCompat.checkSelfPermission(Search_post.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(Search_post.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (isNetworkEnabled) {
                location = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    try {
                        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 5);
                        if (addresses != null) {
                            if (addresses.size() > 0) {
                                for (int i = 0; i < addresses.size(); i++) {
                                    if (addresses.get(i).getPostalCode() != null) {
                                        if (addresses.get(i).getPostalCode().length() > 1) {
                                            zipcode = addresses.get(i).getPostalCode();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        } else {
            ActivityCompat.requestPermissions(Search_post.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CODE);
        }
    }

    @Override
    public void onBackPressed() {
        if(listt.getVisibility()== View.VISIBLE) {
            listt.setVisibility(View.GONE);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_CODE) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                GetZipcodeTask zipcodeTask = new GetZipcodeTask();
                zipcodeTask.execute();
            } else {
                Toast.makeText(Search_post.this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            aResponse = com2.Serch_post("post_search", searchvalue, locationn);
            return null;
        }

        private String convertTime(String jTime) {
            String returnString = null;
            returnString = jTime.substring(6, jTime.length()-2);
            long epoch = Long.parseLong(returnString);
            //returnString = new java.text.SimpleDateFormat("dd/MMM/yyyy 'at' HH:mm:ss aaa").format(new java.util.Date (epoch));
            Date date = new Date(epoch-5400000);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy 'at' HH:mm:ss aaa");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            returnString = sdf.format(date);
            return returnString;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            try {
                JSONArray jr = new JSONArray(aResponse);

                Values=new ArrayList<Detail>();

                for (int i=0;i<20;i++) {

                    Detail productdto = new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);
                    if (jsonObject.has("UID")){
                        productdto.setUser_id(jsonObject.getString("UID"));
                    }
                    profile_image=jsonObject.getString("profimg").toString();
                    post_image = jsonObject.getString("mid_img").toString();
                    postdetails = jsonObject.getString("Details").toString();

                    creaton= jsonObject.getString("Created_on").toString();
                    creaton = convertTime(creaton);
                    //creaton = AppUtil.getDateAndTimeFromTimestamp(Search_post.this, creaton);

                    postname=jsonObject.getString("Name").toString();
                    //reviews=jsonObject.getString("Review").toString();

                    productdto.setPostimage(post_image);
                    productdto.setPostdetails(postdetails);
                    productdto.setPostname(postname);
                    productdto.setPcreaton(creaton);
                    productdto.setPfimage(profile_image);
                    // productdto.setReviews(reviews);
                    Values.add(productdto);


                    CustomSerchPost adapter=new CustomSerchPost(Search_post.this, R.layout.spiner_item,Values,Search_post.this);
                    list.setAdapter(adapter);
                    //setListViewHeightBasedOnItems(list);
                    progressDialog.dismiss();
                   /* grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        }
                    });*/
                    // progressDialog.dismiss();

                    // progressDialog.dismiss();
                }

            }catch (Exception e){
                Log.v("JsonEXCEPTION",e.getMessage());
            }



            if (aResponse.equals("0")) {
                Toast.makeText(getApplicationContext(), "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (aResponse.equals("1")) {
                Toast.makeText(getApplicationContext(), "You have successfully Registered !", Toast.LENGTH_LONG).show();
              /*  Intent i = new Intent(SignUp.this, Home.class);
                startActivity(i);
                // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                //save auth key
                finish();*/
            } else {

                //sharedPreferenceClass.setValue_boolean("Loginstatus", true);
            }

        }
        public  boolean setListViewHeightBasedOnItems(ListView cmntlist) {

            ListAdapter listAdapter = cmntlist.getAdapter();
            if (listAdapter != null) {

                int numberOfItems = listAdapter.getCount();
                // Get total height of all items.
                int totalItemsHeight = 0;
                for (int itemPos = 0; itemPos < numberOfItems;itemPos++) {
                    View item = listAdapter.getView(itemPos, null, cmntlist);
                    item.measure(0, 0);
                    totalItemsHeight += item.getMeasuredHeight();
                }
                // Get total height of all item dividers.
                int totalDividersHeight = cmntlist.getDividerHeight() *
                        (numberOfItems - 1);
                // Set list height.
                ViewGroup.LayoutParams params = cmntlist.getLayoutParams();
                params.height = totalItemsHeight + totalDividersHeight;
                cmntlist.setLayoutParams(params);
                cmntlist.requestLayout();
                return true;

            } else {
                return false;
            }

        }

    }
}
