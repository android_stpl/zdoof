package com.zdoof.stpl.zdoof.zshare;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;

import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.activites.Home;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.adapters.CusAdapters3;
import com.zdoof.stpl.zdoof.webservice.WebserviceCallpost;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by stpl on 29/2/16.
 */
public class Chekin extends Fragment implements LocationListener {

    int[] img = {R.drawable.friend, R.drawable.frnds_frnds, R.drawable.pblic};
    String[] catgory = {"Friends", "Frinds of friends", "Public"};
    Spinner friends;
    ImageView im1, im2, ip1, ip2, is1, is2, chekin, chek, close;
    TextView location1;
    String result = "", Expdate, imageFilePath = "";
    LinearLayout send;
    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;
    TextView Share;
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
   // private static final long MY_PERMISSION_LOCATION = 10;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    LinearLayout ll1, ll2, ll3, ll4;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    ImageView active1, active2, active3, active4, enjoish, dishnrcp, checkin, exp, send2;
    int uerid, dishid = 0, posttype = 4, placetype = 1, createdby, divicetype = 2;
    EditText denj, hdl, plce;
    String denjoyed = "", hwldulik, whreenjd = "";
    static String aResponse;
    String placename = "", dishname = "", postcomment, createdon, uid, zipcode = "";
    SharedPreferenceClass sharedPreferenceClass;
    final WebserviceCallpost comp = new WebserviceCallpost();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.checkins, null);
        //close = (ImageView) findViewById(R.id.close);
        location1 = (TextView) rootView.findViewById(R.id.editTextRefractor1);
        send = (LinearLayout) rootView.findViewById(R.id.pst);
        send2 = (ImageView) rootView.findViewById(R.id.pst1);
        exp = (ImageView) rootView.findViewById(R.id.myex);
        im2 = (ImageView) rootView.findViewById(R.id.imageView8);
        enjoish = (ImageView) rootView.findViewById(R.id.enjd);
        ip2 = (ImageView) rootView.findViewById(R.id.imageView9);
        dishnrcp = (ImageView) rootView.findViewById(R.id.postd);
        is2 = (ImageView) rootView.findViewById(R.id.imageVie);
        /*ll1 = (LinearLayout) rootView.findViewById(R.id.hmj);
        ll2 = (LinearLayout) rootView.findViewById(R.id.cnn);
        ll3 = (LinearLayout) rootView.findViewById(R.id.ntf);
        ll4 = (LinearLayout) rootView.findViewById(R.id.ct);*/
        chekin = (ImageView) rootView.findViewById(R.id.imageView21);
        chek = (ImageView) rootView.findViewById(R.id.imageView1);
        friends = (Spinner) rootView.findViewById(R.id.spinner4);
        Share= (TextView) rootView.findViewById(R.id.hgh);
        // active4.setVisibility(View.VISIBLE);


        sharedPreferenceClass = new SharedPreferenceClass(getActivity());
        uid = sharedPreferenceClass.getValue_string("UIDD");
        uerid = Integer.parseInt(uid);
        createdby = uerid;
        String format = "dd/MMM/yyyy H:mm:ss";
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Expdate = sdf.format(new Date());
        createdon = Expdate;
        createdon = Expdate;

        LocationManager locationManager = (LocationManager)
                getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
        // getting GPS status
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
        isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


        if (!isGPSEnabled && !isNetworkEnabled) {
            // no network provider is enabled
        } else {
            this.canGetLocation = true;

            if (isNetworkEnabled) {
                // checkLocationPermission();
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) getActivity());
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                        if (location != null) {

                            Geocoder gcd = new Geocoder(getActivity(), Locale.getDefault());
                            List<Address> list = null;
                            try {
                                list = gcd.getFromLocation(location
                                        .getLatitude(), location.getLongitude(), 1);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if (list != null & list.size() > 0) {
                                Address address = list.get(0);
                                result = address.getLocality();
                                if(address.getPostalCode()!=null) {
                                    zipcode = address.getPostalCode();
                                }
                                location1.setText("Checking in to" + " " + result + "?");
   /* // Setting latitude and longitude in the TextView tv_location
    location.setText("Latitude:" +  latitude  + ", Longitude:"+ longitude );*/
                            }
                        }
                    }
                    return location1;
                }


                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        //checkLocationPermission();
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                Geocoder gcd = new Geocoder(getActivity(), Locale.getDefault());
                                List<Address> list = null;
                                try {
                                    list = gcd.getFromLocation(location
                                            .getLatitude(), location.getLongitude(), 1);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                if (list != null & list.size() > 0) {
                                    Address address = list.get(0);
                                    result = address.getLocality();
                                        if(address.getPostalCode()!=null) {
                                            zipcode = address.getPostalCode();
                                        }
                                    location1.setText("Checking in to"+ " " + result +"?");
                                }
                            }
                        }
                    }
                }
                postcomment=zipcode;


send.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Share.setTextColor(Color.parseColor("#4CAF50"));
       /* AsyncCallWS task = new AsyncCallWS();
        task.execute();*/
    }
});


                CusAdapters3 adapter = new CusAdapters3(getActivity(), R.layout.spiner_item, catgory, img);
                friends.setAdapter(adapter);
               /* ll2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ll2.setBackgroundColor(Color.parseColor("#5ce480"));
                        Intent intent = new Intent(getActivity(), Connecton.class);
                        // intent.putExtra("EML",det);
                        getActivity().startActivity(intent);
                        getActivity().finish();

                    }
                });

                ll4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ll4.setBackgroundColor(Color.parseColor("#5ce480"));
                        Intent intent = new Intent(getActivity(), Contropanel.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                    }
                });
                ll1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ll1.setBackgroundColor(Color.parseColor("#5ce480"));
                        Intent intent = new Intent(getActivity(), Home.class);
                        getActivity().startActivity(intent);
                        getActivity().finish();
                    }
                });*/
            }


        }
        return rootView;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    /////////

    //////////
    /*public boolean checkLocationPermission()
    {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = getActivity().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }*/
    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

           // aResponse = comp.Checkin("insert_post", uerid, whreenjd, dishid, denjoyed, Expdate, posttype, postcomment, imageFilePath, placetype,divicetype,createdby, createdon);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");


            if (aResponse.equals("0")) {
                Toast.makeText(getActivity(), "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (aResponse.equals("1")) {

               // Toast.makeText(getActivity(), "You have successfully Posted !", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(),Home.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            } else {

            }
        }

    }
}
