package com.zdoof.stpl.zdoof.activites;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.adapters.ConnRequestAdapter;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.search.Search_dish;
import com.zdoof.stpl.zdoof.search.Search_gstore;
import com.zdoof.stpl.zdoof.search.Search_people;
import com.zdoof.stpl.zdoof.search.Search_post;
import com.zdoof.stpl.zdoof.search.Search_resturant;
import com.zdoof.stpl.zdoof.searchadapters.CustomSearch4;
import com.zdoof.stpl.zdoof.utils.AppUtil;
import com.zdoof.stpl.zdoof.utils.ConnectionObject;
import com.zdoof.stpl.zdoof.views.Contropanel;
import com.zdoof.stpl.zdoof.webservice.PicConstant;
import com.zdoof.stpl.zdoof.webservice.Webservicereceipe_post;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by sujit on 19/4/16.
 */
public class Notifications extends Activity{
    LinearLayout ll1,ll2,ll3,ll4;
    ImageView serc;

    String desc;
    ListView connList,drawerList;
    ImageView rch;
    String des,pos;
    EditText description,zips;
    final Webservicereceipe_post com = new Webservicereceipe_post();
    static String aResponse = "";
    SharedPreferenceClass sharedPreferenceClass;
    ProgressDialog progressDialog;
    ArrayList<ConnectionObject> connArray;
    ConnRequestAdapter reqAdapter;
    TextView textt,code;
    Button submit;
    LinearLayout ziploc,vieew;
    String sertype[]={"Search Post","Search Foodie","Search Recipe/Dish","Search Restaurant","Search Grocery Store"};
    int sercimg[]={R.drawable.sst,R.drawable.uur,R.drawable.ff,R.drawable.sahlist,R.drawable.crt};
    android.support.v4.widget.DrawerLayout drawerMainSearch;
    int login_id;
    int pxProfImg;
    private final int LOCATION_PERMISSION_CODE=1;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    Location location;
    double latitude=0;
    double longitude=0;
    String zipcode="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification);
        ll1= (LinearLayout) findViewById(R.id.hmj);
        ll2= (LinearLayout) findViewById(R.id.cnn);
        ll3=(LinearLayout) findViewById(R.id.ntf);
        ll4=(LinearLayout) findViewById(R.id.ct);
        drawerMainSearch = (DrawerLayout) findViewById(R.id.drawerMainSearch);
        serc= (ImageView) findViewById(R.id.serc);
        drawerList= (ListView) findViewById(R.id.drawerItemList);
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header1 = (ViewGroup)inflater.inflate(R.layout.search_view, drawerList, false);
        drawerList.addHeaderView(header1, null, false);
        CustomSearch4 drawerAdapter = new CustomSearch4(Notifications.this, R.layout.spiner_item, sertype, sercimg);
        drawerList.setAdapter(drawerAdapter);
        textt= (TextView) findViewById(R.id.textt);
        ziploc= (LinearLayout) findViewById(R.id.ziploc);
        vieew= (LinearLayout) findViewById(R.id.view);
        zips= (EditText) findViewById(R.id.zip);
        submit= (Button) findViewById(R.id.submit);
        code= (TextView) findViewById(R.id.code);
        connList = (ListView) findViewById(R.id.connReqList);
        pxProfImg = getResources().getDimensionPixelSize(R.dimen.pxProfImg);
        connArray = new ArrayList<ConnectionObject>();
        progressDialog = new ProgressDialog(Notifications.this);
        progressDialog.setMessage("Loading...Please wait!");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        rch= (ImageView) findViewById(R.id.sec);
        ll3.setBackgroundColor(Color.parseColor("#1D8168"));
        sharedPreferenceClass=new SharedPreferenceClass(this);
        login_id = Integer.parseInt(sharedPreferenceClass.getValue_string("UIDD"));

        description= (EditText) findViewById(R.id.imageView11);
        serc.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!drawerMainSearch.isDrawerOpen(GravityCompat.END)) {
                    drawerMainSearch.openDrawer(GravityCompat.END);
                }
                else {
                    drawerMainSearch.closeDrawer(GravityCompat.END);
                }
            }
        });

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                des = description.getText().toString().trim();
                if (des.length()<3) {
                    Toast.makeText(Notifications.this, "Please enter atleast 3 letters to search!", Toast.LENGTH_SHORT).show();
                }
                else {
                    pos = drawerList.getItemAtPosition(position).toString();
                    if (pos.equals("0")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(Notifications.this, Search_post.class);
                        startActivity(intent);
                        //finish();
                    }else if (pos.equals("1")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(Notifications.this, Search_people.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("2")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(Notifications.this, Search_dish.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("3")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(Notifications.this, Search_resturant.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("4")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(Notifications.this, Search_gstore.class);
                        startActivity(intent);
                        //finish();
                    }
                }
            }
        });
        ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Notifications.this, Connecton.class);
                startActivity(intent);
                finish();

            }


        });
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Notifications.this, Home.class);
                startActivity(intent);
                finish();

            }


        });

        ll4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 /* ll1.setBackgroundColor(Color.parseColor("#F63E48"));
                ll2.setBackgroundColor(Color.parseColor("#F63E48"));
                ll4.setBackgroundColor(Color.parseColor("#FFD25E41"));*/

                Intent intent = new Intent(Notifications.this, Contropanel.class);
                startActivity(intent);
                finish();

            }
               /* ll1.setBackgroundColor(Color.parseColor("#F63E48"));
                ll2.setBackgroundColor(Color.parseColor("#F63E48"));
                ll4.setBackgroundColor(Color.parseColor("#FFD25E41"));*/




        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zipcode =zips.getText().toString();
                code.setText(zipcode);
                sharedPreferenceClass.setValue_string("ZZIP", zipcode);
                ziploc.setVisibility(View.GONE);
                vieew.setVisibility(View.GONE);
            }
        });
        textt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ziploc.setVisibility(View.VISIBLE);
                vieew.setVisibility(View.VISIBLE);
            }
        });
        description.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId== EditorInfo.IME_ACTION_SEARCH) {
                    serc.performClick();
                    return true;
                }
                return false;
            }
        });
        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                drawerMainSearch.closeDrawer(GravityCompat.END);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setCursorVisible(true);
            }
        });

        RetriveConnectionTask retTask = new RetriveConnectionTask();
        retTask.execute();

        String z_zip = sharedPreferenceClass.getValue_string("ZZIP");
        if (!z_zip.equals(null) && !z_zip.equals("") && !(z_zip.length()<2)) {
            code.setText(z_zip);
            zipcode = z_zip;
        } else {
            if (AppUtil.GetLocationPermission(this)) {
                GetZipcodeTask zipTask = new GetZipcodeTask();
                zipTask.execute();
            }
            else {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode==LOCATION_PERMISSION_CODE) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //getZipcodeOfLocation();
                GetZipcodeTask zipcodeTask = new GetZipcodeTask();
                zipcodeTask.execute();
            } else {
                Toast.makeText(Notifications.this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        drawerMainSearch.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onBackPressed() {
        if (drawerMainSearch.isDrawerOpen(GravityCompat.END)) {
            drawerMainSearch.closeDrawer(GravityCompat.END);
        }
        else {
            description.setText("");
            sharedPreferenceClass.setValue_string("SEARCHVALUE","0");
            Intent intent = new Intent(Notifications.this, Home.class);
            startActivity(intent);
            finish();
        }
    }

    private class GetZipcodeTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... params) {
            getZipcodeOfLocation();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String zzip = sharedPreferenceClass.getValue_string("ZZIP");
            if (!zzip.equals(null) && !zzip.equals("") && !(zzip.length()<2)) {
                code.setText(zzip);
            }
            else {
                code.setText(zipcode);
                sharedPreferenceClass.setValue_string("ZZIP", zipcode);
            }
            zipcode = sharedPreferenceClass.getValue_string("ZZIP");
        }
    }

    private void getZipcodeOfLocation() {
        LocationManager manager = (LocationManager) Notifications.this.getSystemService(LOCATION_SERVICE);
        // getting GPS status
        isGPSEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        isNetworkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Geocoder geocoder = new Geocoder(Notifications.this, Locale.getDefault());
        if (
                ActivityCompat.checkSelfPermission(Notifications.this, Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(Notifications.this, Manifest.permission.ACCESS_COARSE_LOCATION)==PackageManager.PERMISSION_GRANTED) {
            if (isNetworkEnabled) {
                location = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location!=null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    try {
                        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 5);
                        if (addresses!=null) {
                            if (addresses.size()>0) {
                                for (int i=0; i<addresses.size(); i++){
                                    if (addresses.get(i).getPostalCode()!=null) {
                                        if (addresses.get(i).getPostalCode().length()>1) {
                                            zipcode = addresses.get(i).getPostalCode();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        else {
            ActivityCompat.requestPermissions(Notifications.this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CODE);
        }

    }

    private class RetriveConnectionTask extends AsyncTask<Void,Void,Void> {

        private Boolean connSuccess = false;
        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            aResponse = com.retConnection("retConnection",login_id);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            try {
                JSONArray jsonArray = new JSONArray(aResponse);
                connArray.clear();
                for (int i=0; i<jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ConnectionObject connObject = new ConnectionObject();
                    String c_id = jsonObject.getString("Connection_id");
                    String from_id = jsonObject.getString("From_user_id");
                    String to_id = jsonObject.getString("To_user_id");
                    String from_name = jsonObject.getString("from_user");
                    String to_name = jsonObject.getString("to_user");
                    String from_img = jsonObject.getString("Profile_image");
                    connObject.setConnection_id(c_id);
                    connObject.setFrom_userid(from_id);
                    connObject.setTo_userid(to_id);
                    connObject.setFrom_username(from_name);
                    connObject.setTo_username(to_name);
                    connObject.setFrom_profimg(from_img);
                    connArray.add(connObject);
                }
                reqAdapter = new ConnRequestAdapter(connArray,Notifications.this);
                connList.setAdapter(reqAdapter);
                ImageLoadingTask imgTask = new ImageLoadingTask();
                imgTask.execute();
                connSuccess = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!connSuccess) {
                progressDialog.dismiss();
            }
        }
    }

    private class ImageLoadingTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i=0; i<connArray.size(); i++) {
                ConnectionObject d = connArray.get(i);
                if (Home.profileCache.get(connArray.get(i).getFrom_userid())==null) {
                    try {
                        URL url = new URL(PicConstant.PROFILE_URL1+ d.getFrom_profimg());
                        Home.lodingCache.put(Home.LOADING_CACHE, BitmapFactory.decodeStream(url.openConnection().getInputStream()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        Home.lodingCache.put(Home.LOADING_CACHE,null);
                    }
                    if (Home.lodingCache.get(Home.LOADING_CACHE)==null) {
                        Home.lodingCache.put(Home.LOADING_CACHE,BitmapFactory.decodeResource(getResources(),R.drawable.blankpeople));
                    }
                    Home.lodingCache.put(Home.LOADING_CACHE,scaleImage(Home.lodingCache.get(Home.LOADING_CACHE), pxProfImg));
                    //Values.get(i).setProfBitmap(lodingCache.get(LOADING_CACHE).copy(lodingCache.get(LOADING_CACHE).getConfig(),lodingCache.get(LOADING_CACHE).isMutable()));
                    Home.profileCache.put(connArray.get(i).getFrom_userid(),Home.lodingCache.get(Home.LOADING_CACHE).copy(Home.lodingCache.get(Home.LOADING_CACHE).getConfig(),Home.lodingCache.get(Home.LOADING_CACHE).isMutable()));
                    Home.lodingCache.get(Home.LOADING_CACHE).recycle();
                    //lodingCache.put(LOADING_CACHE,null);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            reqAdapter.notifyDataSetChanged();
            progressDialog.dismiss();
        }
    }

    private Bitmap scaleImage(Bitmap bitmap, int newDpHeight) {
        int newHeight = newDpHeight;
        int newWidth = (int) (newHeight * bitmap.getWidth() / ((double) bitmap.getHeight()));
        return Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
    }
}

