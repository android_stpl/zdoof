package com.zdoof.stpl.zdoof.services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;

import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.activites.Notifications;
import com.zdoof.stpl.zdoof.receivers.NotificationAlarmReceiver;
import com.zdoof.stpl.zdoof.receivers.PreferenceReceiver;
import com.zdoof.stpl.zdoof.webservice.Webservicereceipe_post;

import org.json.JSONArray;
import org.json.JSONObject;

public class ConnectionNotificationService extends IntentService {

    public static final long REPEAT_INTERVAL = 10000L;

    private final Webservicereceipe_post com = new Webservicereceipe_post();

    public ConnectionNotificationService() {
        super("ConnectionNotificationService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (intent.getAction().equals(NotificationAlarmReceiver.ALARM_ACTION)) {
            int login_id = intent.getIntExtra("LOGIN_ID",-1);
            int pending_conn_size = intent.getIntExtra("PENDING_CONN_SIZE",-1);
            if (login_id==0 || login_id==-1 || pending_conn_size==-1) {
                Log.i("Watcher :", "error! login_id="+login_id+" | pending_conn_size="+pending_conn_size);
            }
            else {
                Log.i("Watcher :", "request started");
                String response = com.retConnection("retConnection",login_id);

                try {
                    if (response==null || response.length()<1) {
                        Intent in = new Intent();
                        in.setAction(PreferenceReceiver.PREFS_FILTER);
                        in.putExtra("PENDING_CONN_SIZE", 0);
                        getApplicationContext().sendBroadcast(in);
                        Log.i("Watcher :", "null response");
                    }
                    else if (response.equals("anyType{}")) {
                        Intent in = new Intent();
                        in.setAction(PreferenceReceiver.PREFS_FILTER);
                        in.putExtra("PENDING_CONN_SIZE", 0);
                        getApplicationContext().sendBroadcast(in);
                        Log.i("Watcher :", "anyType{}");
                    }
                    else {
                        JSONArray jsonArray = new JSONArray(response);
                        if (jsonArray.length()>pending_conn_size) {
                            JSONObject jsonObject = jsonArray.getJSONObject(jsonArray.length()-1);
                            String from_name = jsonObject.getString("from_user");
                            Spannable sb = new SpannableString(from_name+" send you a connection request!");
                            sb.setSpan(new ForegroundColorSpan(Color.parseColor("#d43f3a")), 0, 0 + from_name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                            sb.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), 0, 0 + from_name.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                            Intent myIntent = new Intent(getApplicationContext(), Notifications.class);
                            myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            PendingIntent pendingIntent = PendingIntent.getActivity(
                                    getApplicationContext(),
                                    0,
                                    myIntent,
                                    PendingIntent.FLAG_UPDATE_CURRENT);
                            NotificationCompat.Builder mBuilder =
                                    new NotificationCompat.Builder(getApplicationContext())
                                            .setContentTitle("Connection request Arrived")
                                            .setContentText(sb)
                                            .setSmallIcon(R.drawable.iconspoonn)
                                            .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.app_icon))
                                            .setContentIntent(pendingIntent).setAutoCancel(true);
                            Notification n = mBuilder.build();
                            n.flags |= Notification.FLAG_AUTO_CANCEL;
                            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                            Uri defaultRingtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            MediaPlayer mediaPlayer = new MediaPlayer();
                            mediaPlayer.setDataSource(getApplicationContext(), defaultRingtoneUri);
                            mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
                            mediaPlayer.prepare();
                            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    mp.release();
                                }
                            });
                            nm.notify((int) System.currentTimeMillis(),n);
                            mediaPlayer.start();
                        }
                        Intent in = new Intent();
                        in.setAction(PreferenceReceiver.PREFS_FILTER);
                        in.putExtra("PENDING_CONN_SIZE", jsonArray.length());
                        getApplicationContext().sendBroadcast(in);
                        Log.i("Watcher :", "success");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i("Watcher :", "failed");
                }
            }

            sheduleAlarm();
        }

    }

    private void sheduleAlarm() {
        Intent intent = new Intent(getApplicationContext(), NotificationAlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, NotificationAlarmReceiver.ALARM_REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarm.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+ConnectionNotificationService.REPEAT_INTERVAL, pIntent);
        }
        else {
            alarm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+ConnectionNotificationService.REPEAT_INTERVAL, pIntent);
        }
    }

}
