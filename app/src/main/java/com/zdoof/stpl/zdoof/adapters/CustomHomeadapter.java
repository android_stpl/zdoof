package com.zdoof.stpl.zdoof.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.CircularNetworkImageView;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.activites.AndroidImageSlider;
import com.zdoof.stpl.zdoof.activites.Comments;
import com.zdoof.stpl.zdoof.activites.Home;
import com.zdoof.stpl.zdoof.activites.Profiles;
import com.zdoof.stpl.zdoof.activites.YoutubePlayer;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.commons.Tables;
import com.zdoof.stpl.zdoof.database.DatabaseHelper;
import com.zdoof.stpl.zdoof.webservice.PicConstant;
import com.zdoof.stpl.zdoof.webservice.WebserviceCallpost;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by sujit on 3/3/16.
 */
public class CustomHomeadapter extends BaseAdapter {

    Home home;
    SharedPreferenceClass sharedPreferenceClass;
    Bitmap profBitmap = null;
    int riid1, Sub_user_type = 0, Main_user_type = 0, Post_id, pty;
    String name1, usertype_img, idd = "", ptype = "", ifliked = "", posted_comments = "", place_of_dish = "", place_of_rest = "";
    String email, likesss;
    Typeface typeface;
    Bitmap defaultimg;
    ViewHolder holder;
    String gg, post_userid, comments, share, riboncol, youtubeID, pic6;
    DatabaseHelper helper;
    int imgCount = 0;

    static String cResponse, eResponse, Expdate;
    final WebserviceCallpost com = new WebserviceCallpost();
    String imd_img, prfimg, name, type, description, pic3, style, pic4, pic5, shares, creaton, user, imd_img1, likess, Dtails;
    ArrayList<Detail> values;
    ArrayList<Detail> cmt;
    ArrayList<Detail> cmmnt;
    String lik, likk, comment, Review_post_id;
    String[] Liked;
    static int total;
    ArrayList<String> cmn;
    ImageLoader imageLoader;
    DisplayImageOptions options;
    private Boolean yumclicked;
    private ArrayList<Tables> tableImages = new ArrayList<Tables>();

    //int pos;

    public CustomHomeadapter(Home home, int spiner_item, ArrayList<Detail> values) {
        this.home = home;
        this.values = values;
        yumclicked = false;
        notifyDataSetChanged();
        sharedPreferenceClass = new SharedPreferenceClass(home);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(home).build();
        imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .resetViewBeforeLoading(true)

                // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.blankpeople)
                .showImageOnLoading(R.drawable.uute).build();
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void addToHorizentalScrollView(String url, LinearLayout linearlayout, final Context context, int imgCount) {

        View view = LayoutInflater.from(context).inflate(R.layout.img_upload_layout, null);
        ImageView image = (ImageView) view.findViewById(R.id.image);
        ImageView image1 = (ImageView) view.findViewById(R.id.image1);
        image1.setVisibility(View.GONE);
        /*switch (imgCount) {
            case 0:
                imageLoader.displayImage(url, image, options);
                break;
            case 1:
                image1.setImageResource(R.drawable.add_user);
                break;
        }*/



        imageLoader.displayImage(url, image, options);
        // imageLoader.displayImage(url, image1, options);
        ImageView image_close = (ImageView) view.findViewById(R.id.close);
        image_close.setVisibility(View.GONE);
        linearlayout.addView(view);
    }

    private void addToHorizentalScrollView(Context context, LinearLayout linearlayout, Bitmap bitmap) {
        View view = LayoutInflater.from(context).inflate(R.layout.img_upload_layout, null);
        ImageView image = (ImageView) view.findViewById(R.id.image);
        ImageView image1 = (ImageView) view.findViewById(R.id.image1);
        image1.setVisibility(View.GONE);

        image.setImageBitmap(bitmap);

        ImageView image_close = (ImageView) view.findViewById(R.id.close);
        image_close.setVisibility(View.GONE);
        linearlayout.addView(view);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        //if (convertView == null) {
        LayoutInflater inflater = home.getLayoutInflater();
        convertView = inflater.inflate(R.layout.custom_new_ztimeline, null);
        holder = new ViewHolder();
        helper = new DatabaseHelper(home);
        //pos=position;
        holder.circularImageView = (CircularNetworkImageView) convertView.findViewById(R.id.profilePictureVie);
        holder.viewMore = (TextView) convertView.findViewById(R.id.viewMoreItem);
        Typeface custom_font = Typeface.createFromAsset(home.getAssets(), "OpenSans-Italic.ttf");
        holder.imd = (ImageView) convertView.findViewById(R.id.imageView23);
        holder.nam = (TextView) convertView.findViewById(R.id.textVie1);
        holder.types = (TextView) convertView.findViewById(R.id.textV1);
        holder.descrip = (TextView) convertView.findViewById(R.id.tcc);
        holder.dshares = (TextView) convertView.findViewById(R.id.textyV1);
        holder.dcrton = (TextView) convertView.findViewById(R.id.postTime);
        holder.likeactive = (ImageView) convertView.findViewById(R.id.btnYum);
        holder.like = (ImageView) convertView.findViewById(R.id.kk);
        holder.texth = (TextView) convertView.findViewById(R.id.text7);
        holder.texth_red = (TextView) convertView.findViewById(R.id.textred);
        holder.likes = (TextView) convertView.findViewById(R.id.likes);
        holder.cmntlist = (ListView) convertView.findViewById(R.id.lsst);
        holder.cment = (TextView) convertView.findViewById(R.id.coments);
        holder.place_dish = (TextView) convertView.findViewById(R.id.place);
        holder.place_rest = (TextView) convertView.findViewById(R.id.resturant);
        holder.at = (TextView) convertView.findViewById(R.id.at);
        // holder.mDemoSlider = (SliderLayout)convertView.findViewById(R.id.slider);
        // RelativeLayout rl = (RelativeLayout) convertView.findViewById(R.id.rr);
        holder.orange = (ImageView) convertView.findViewById(R.id.orange);
        holder.blue = (ImageView) convertView.findViewById(R.id.blue);
        holder.red = (ImageView) convertView.findViewById(R.id.red);
        holder.green = (ImageView) convertView.findViewById(R.id.green);

        holder.youtubethumb = (ImageView) convertView.findViewById(R.id.youtubethumb);
        holder.youtubeplay = (ImageView) convertView.findViewById(R.id.youtubeplay);
        holder.workimg = (ImageView) convertView.findViewById(R.id.workimg);
        holder.ll = (LinearLayout) convertView.findViewById(R.id.ll);
        holder.ll1 = (LinearLayout) convertView.findViewById(R.id.ll1);
        holder.allclick = (LinearLayout) convertView.findViewById(R.id.newAllclick);
        //holder.relative = (RelativeLayout) convertView.findViewById(R.id.rll);

        holder.yumProgres = (ProgressBar) convertView.findViewById(R.id.yumprogress);
        holder.hsv = (HorizontalScrollView) convertView.findViewById(R.id.hsv);
        holder.layoutlinearPhotos = (LinearLayout) convertView.findViewById(R.id.layoutlinearPhotos);


        post_userid = sharedPreferenceClass.getValue_string("UIDD");


        user = values.get(position).getUserid();
        creaton = values.get(position).getDcreateon();
        usertype_img = values.get(position).getUtypeimage();
        prfimg = values.get(position).getUpdateimage();
        imd_img = values.get(position).getDprf();
        name1 = values.get(position).getDname();
        type = values.get(position).getDwrkas();
        likess = values.get(position).getPost_like();
        posted_comments = values.get(position).getPostedcomments();
        share = values.get(position).getDshare();
        Dtails = values.get(position).getDdtails();
        riboncol = values.get(position).getRibon();
        youtubeID = values.get(position).getYoutubeID();

        if (!youtubeID.equals(null) && !youtubeID.equals("") && !youtubeID.equals("null")) {
            holder.youtubethumb.setVisibility(View.VISIBLE);
            holder.youtubeplay.setVisibility(View.VISIBLE);
            String link = "http://img.youtube.com/vi/"+youtubeID+"/default.jpg";
            imageLoader.displayImage(link, holder.youtubethumb, options);
        } else {holder.youtubethumb.setVisibility(View.GONE); holder.youtubeplay.setVisibility(View.GONE);}

        if(type.trim().equals("Foodie")) {
            holder.workimg.setImageResource(R.drawable.utype2);
        }
        else if (type.trim().equals("Culinary hobbyist")) {
            holder.workimg.setImageResource(R.drawable.utype3);
        }
        else if (type.trim().equals("Chef")) {
            holder.workimg.setImageResource(R.drawable.utype1);
        }

        if (riboncol.equals("ribbon-blue")) {
            holder.blue.setVisibility(View.VISIBLE);
        } else if (riboncol.equals("ribbon-red")) {
            holder.red.setVisibility(View.VISIBLE);
        } else if (riboncol.equals("ribbon-green")) {
            holder.green.setVisibility(View.VISIBLE);
        } else {
            holder.orange.setVisibility(View.VISIBLE);
        }
        place_of_dish = values.get(position).getPlace_dish();
        place_of_rest = values.get(position).getPlace_rest();
        if (place_of_dish.equals("")) {
        } else if (place_of_dish.equals("null")) {

        } else {

            holder.place_dish.setVisibility(View.VISIBLE);

            holder.place_dish.setText(place_of_dish + " ");
        }

        if (!TextUtils.isEmpty(place_of_rest) && !place_of_rest.equals("null")){
            holder.place_rest.setVisibility(View.VISIBLE);
            holder.at.setVisibility(View.VISIBLE);
            holder.place_rest.setText(" " + place_of_rest);
        }else {
            holder.place_rest.setVisibility(View.GONE);
            holder.at.setVisibility(View.GONE);
        }
        // comments=values.get(pos).getComments();

        idd = values.get(position).getIdd();
        ptype = values.get(position).getPtype();

        try {
            Post_id = Integer.parseInt(idd);
            pty = Integer.parseInt(ptype);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        ifliked = values.get(position).getIflike();
        if (ifliked == null) {

        } else if (ifliked.equals("blog-btn choose")) {
            holder.likeactive.setVisibility(View.VISIBLE);
            holder.like.setVisibility(View.GONE);
            holder.texth.setVisibility(View.GONE);
            holder.texth_red.setVisibility(View.VISIBLE);

        } else {

            holder.likeactive.setVisibility(View.GONE);
            holder.like.setVisibility(View.VISIBLE);
            holder.texth.setVisibility(View.VISIBLE);
            holder.texth_red.setVisibility(View.GONE);
        }
        style = values.get(position).getStyle();
        user = values.get(position).getUserid();
        holder.likes.setText(likess);
        holder.cment.setText(posted_comments);

        try {
            riid1 = Integer.parseInt(post_userid);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

       /* pic5 = PicConstant.PROFILE_URL1 + usertype_img;
        imageLoader.displayImage(pic5, holder.workimg, options);*/

        /*pic3 = PicConstant.PROFILE_URL1 + prfimg;
        imageLoader.displayImage(pic3, holder.circularImageView, options);*/

        if (Home.profileCache.get(user)==null) {
        }
        else {
            holder.circularImageView.setImageBitmap(Home.profileCache.get(user));
        }

        ArrayList<Tables>tablesArrayList=values.get(position).getTableImages();


        /*if(tablesArrayList!=null && tablesArrayList.size()>0){
            for(Tables t:tablesArrayList){
                addToHorizentalScrollView(PicConstant.PROFILE_URL1+t.getMid_img(), holder.layoutlinearPhotos, home,imgCount);
                imgCount++;

            }
        }*/

        //ArrayList<Bitmap> bitmaps = values.get(position).getScrollBitmap();
        if (tablesArrayList!=null) {
            if (tablesArrayList.size()>0) {
                for (int i=0;i<tablesArrayList.size();i++) {
                    String cachekey = values.get(position).getPtype()+"."+values.get(position).getIdd()+"."+i;
                    if (Home.scrollCache.get(cachekey)!=null) {
                        addToHorizentalScrollView(home, holder.layoutlinearPhotos, Home.scrollCache.get(cachekey));
                    }
                }
            }
            else {
                holder.hsv.setVisibility(View.GONE);
            }
        }
        else {
            holder.hsv.setVisibility(View.GONE);
        }


        /*if(tablesArrayList != null){
            values.get(position).getTableImages();
        }*/

        holder.layoutlinearPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ArrayList<Tables>tablesArrayList1=values.get(position).getTableImages();
                ArrayList<String>  imgURLs = new ArrayList<String>();

                for(Tables t:tablesArrayList1){

                    imgURLs.add(PicConstant.PROFILE_URL1+t.getMid_img());
                }

                Intent intent = new Intent(Home.homeContext, AndroidImageSlider.class);
                intent.putExtra("ImageURLS", ""+imgURLs);
                Home.homeContext.startActivity(intent);

               // Toast.makeText(Home.homeContext, ""+imgURLs,Toast.LENGTH_SHORT).show();


            }
        });
        holder.nam.setText(values.get(position).getDname());
        String description = values.get(position).getDdtails();
        if (description.split("http://").length>1) {
        }
        else if (description.split("https://").length>1) {
        }

        //holder.descrip.setText(Html.fromHtml(values.get(position).getDdtails()));
        if (values.get(position).getPtype().equals("3")) {
            SpannableString ss = new SpannableString(values.get(position).connName1+" connected with "+values.get(position).connName2);
            ClickableSpan span1 = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    // do some thing
                    String rrid = values.get(position).connID1;
                    Intent intent = new Intent(home, Profiles.class);
                    intent.putExtra("USERID", rrid);
                    sharedPreferenceClass.setValue_string("BACKPAGE", "home");
                    home.startActivity(intent);
                }
            };
            ClickableSpan span2 = new ClickableSpan() {
                @Override
                public void onClick(View textView) {
                    // do another thing
                    String rrid = values.get(position).connID2;
                    Intent intent = new Intent(home, Profiles.class);
                    intent.putExtra("USERID", rrid);
                    sharedPreferenceClass.setValue_string("BACKPAGE", "home");
                    home.startActivity(intent);
                }
            };
            int l1 = values.get(position).connName1.length();
            ss.setSpan(span1, 0, l1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            ss.setSpan(span2, l1+16, ss.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            ss.setSpan(new ForegroundColorSpan(Color.parseColor("#0077cc")), 0, l1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            ss.setSpan(new ForegroundColorSpan(Color.parseColor("#0077cc")), l1+16, ss.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            holder.descrip.setText(ss);
            holder.descrip.setMovementMethod(LinkMovementMethod.getInstance());
        }
        else if (values.get(position).getPtype().equals("7")) {
            SpannableString ss;
            ss = new SpannableString(Html.fromHtml(description));
//            ss = new SpannableString(Html.fromHtml("<p>normal text</p>"+
//                    "<p><small><small><small>normal text</small></small></small></p>"+
//                    "<p><small><small>normal text</small></small></p>"+
//                    "<p><small>normal text</small></p>"+
//                    "<p>normal text</p>"+
//                    "<p><big>normal text</big></p>"+
//                    "<p><big><big>normal text</big></big></p>"+
//                    "<p><big><big><big>normal text</big></big></big></p>"));
            holder.descrip.setText(ss);
//            holder.descrip.setMaxLines(3);
//            holder.descrip.setEllipsize(TextUtils.TruncateAt.END);
        }
        else {
            holder.descrip.setText(description);
        }
        holder.dshares.setTypeface(custom_font);
        holder.dshares.setText(values.get(position).getDshare());
        //String dd = values.get(position).getDcreateon();
        holder.dcrton.setText(values.get(position).getDcreateon());
//        holder.relative.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String rrid = values.get(position).getUserid();
//                riid1 = Integer.parseInt(rrid);
//                Intent intent = new Intent(home, Profiles.class);
//                intent.putExtra("USERID", rrid);
//                sharedPreferenceClass.setValue_string("BACKPAGE", "home");
//                home.startActivity(intent);
//            }
//        });
        holder.youtubethumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(home, YoutubePlayer.class);
                youtubeID = values.get(position).getYoutubeID();
                intent.putExtra("youtubeID", youtubeID);
                home.startActivity(intent);
            }
        });
        holder.allclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String rrid = values.get(position).getUserid();
                riid1 = Integer.parseInt(rrid);
                Intent intent = new Intent(home, Profiles.class);
                intent.putExtra("USERID", rrid);
                sharedPreferenceClass.setValue_string("BACKPAGE", "home");
                home.startActivity(intent);
            }
        });

        String format = "dd/MMM/yyyy H:mm:ss";
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Expdate = sdf.format(new Date());

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!yumclicked) {
                    yumclicked = true;
                    //holder.yumProgres.setVisibility(View.VISIBLE);
                    AsyncCallWS2 task = new AsyncCallWS2(position);
                    task.execute();
                }
            }
        });

        holder.viewMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentClicked(position);
            }
        });

        holder.ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentClicked(position);
            }
        });

       /* try {
            idd = values.get(pos).getIdd();

            AsyncCallWS4 task4 = new AsyncCallWS4();
            task4.execute();
            cmt = helper.getcomments(idd);

            gg = cmt.get(pos).getComment();
            if (gg.equals("")) {

            } else {
                holder.cment.setVisibility(View.VISIBLE);
                holder.cment.setText(gg);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }*/

        return convertView;
    }

    private void commentClicked(int position) {
        ptype = values.get(position).getPtype();
        idd = values.get(position).getIdd();
        Post_id = Integer.parseInt(idd);
        pty = Integer.parseInt(ptype);
        prfimg = values.get(position).getUpdateimage();
        name1 = values.get(position).getDname();
        imd_img = values.get(position).getDprf();
        user = values.get(position).getUserid();
        creaton = values.get(position).getDcreateon();
        type = values.get(position).getDwrkas();
        style = values.get(position).getStyle();
        share = values.get(position).getDshare();
        Dtails = values.get(position).getDdtails();
        youtubeID = values.get(position).getYoutubeID();
        tableImages = values.get(position).getTableImages();
        ArrayList<String> imagelinks = new ArrayList<String>();
        if (tableImages!=null && tableImages.size()>0) {
            for (int i=0;i<tableImages.size();i++) {
                imagelinks.add(tableImages.get(i).getMid_img());
            }
        }

        // usertype_img = values.get(position).getUtypeimage();
        Intent intent = new Intent(home, Comments.class);

        intent.putExtra("POST_ID", Post_id);
        intent.putExtra("_USER", riid1);
        intent.putExtra("CREATE_ON", creaton);
        intent.putExtra("PTYPE", pty);
        intent.putExtra("PFIMG", prfimg);
        intent.putExtra("NMA", name1);
        intent.putExtra("MID", imd_img);
        intent.putExtra("USRID", user);
        intent.putExtra("WRK", user);
        intent.putExtra("STYL", style);
        intent.putExtra("DSHR", share);
        intent.putExtra("DTL", Dtails);
        intent.putExtra("TBL_IMG", imagelinks);
        intent.putExtra("YT_ID", youtubeID);
        intent.putExtra("BLOG_IMG", values.get(position).blogTitleImg);
        intent.putExtra("PLACE_DISH", values.get(position).getPlace_dish());
        intent.putExtra("COMMENTS", values.get(position).getPostedcomments());
        intent.putExtra("YUMS", values.get(position).getPost_like());
        intent.putExtra("YUMS_STATUS", values.get(position).getIflike());
        home.startActivity(intent);
        //home.finish();
        //  Toast.makeText(home, "Clicked on comment", Toast.LENGTH_SHORT).show();
    }

    public void add(ArrayList<Detail> val) {
        this.values = val;
    }

    public void clear() {
        values.clear();
        notifyDataSetChanged();
    }


    ////////////////


    class AsyncCallWS2 extends AsyncTask<Void, Void, Void> {

        int pos;

        public AsyncCallWS2(int pos) {
            this.pos = pos;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");
            ptype = values.get(pos).getPtype();
            pty = Integer.parseInt(ptype);
            idd = values.get(pos).getIdd();
            Post_id = Integer.parseInt(idd);
            cResponse = com.Addlike("post_like", Post_id, pty, riid1, Expdate);

            Log.i("HomeAdapter Response", cResponse);

            Liked = cResponse.split(",");
            //      lik = Liked[0];
            //          likess = Liked[1];
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            likess = values.get(pos).getPost_like();
            likess = likess.replace(" yums", "");
            if (Liked[0].equals("1")) {
                //total = Integer.parseInt(likess) + 1;
                values.get(pos).setIflike("blog-btn choose");
            }
            else if (Liked[0].equals("2")) {
                /*total = Integer.parseInt(likess) - 1;
                if(total < 0) {
                    total = 0;
                }*/
                values.get(pos).setIflike("");
            }
            values.get(pos).setPost_like(Liked[1]);
            likesss = values.get(pos).getPost_like();
            helper.upDate(idd, likesss, ifliked);
            //holder.yumProgres.setVisibility(View.GONE);
            yumclicked = false;
            notifyDataSetChanged();
            /*total = Integer.parseInt(likess) + 1;
            values.get(position).setPost_like(String.valueOf(total) + " yums");
            likesss = values.get(position).getPost_like();
            helper.upDate(idd, likesss, ifliked);

            notifyDataSetChanged();*/

            /*total = Integer.parseInt(likess) - 1;
            if(total < 0) {
                total = 0;
            }
            values.get(position).setPost_like(String.valueOf(total) + " yums");

            likesss = values.get(position).getPost_like();
            helper.upDate(idd, likesss, ifliked);

            notifyDataSetChanged();*/
        }
    }

    private static class ViewHolder {
        CircularNetworkImageView circularImageView;
        ImageView like, likeactive;
        LinearLayout ll;
        RelativeLayout relative;
        LinearLayout ll1;
        TextView nam, texth, texth_red;
        TextView likes, cment, descrip, dshares, dcrton, place_dish, place_rest, at, viewMore;
        ImageView imd, workimg, youtubethumb,youtubeplay;
        SliderLayout mDemoSlider;
        ImageView orange, blue, red, green;
        TextView types;
        LinearLayout allclick;
        ListView cmntlist;
        ProgressBar yumProgres;
        HorizontalScrollView hsv;
        LinearLayout layoutlinearPhotos;
    }


    public boolean setListViewHeightBasedOnItems(ListView cmntlist) {

        ListAdapter listAdapter = cmntlist.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, cmntlist);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = cmntlist.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = cmntlist.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            cmntlist.setLayoutParams(params);
            cmntlist.requestLayout();
            return true;

        } else {
            return false;
        }

    }
}