package com.zdoof.stpl.zdoof.fragmentpageradapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.zdoof.stpl.zdoof.fragments.About;
import com.zdoof.stpl.zdoof.fragments.Post;
import com.zdoof.stpl.zdoof.fragments.Recipes;
import com.zdoof.stpl.zdoof.fragments.Zfans;
import com.zdoof.stpl.zdoof.fragments.Zlikes;

/**
 * Created by PCIS-ANDROID on 21-01-2016.
 */
public class Profile_page_adapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    FragmentManager fm;
   /* public Profile_page_adapter(FragmentManager fm, int NumOfTabs) {
        super(fm);

    }*/


    public Profile_page_adapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                About tab1 = new About();
                return tab1;
            case 1:
                Post tab2 = new Post();
                return tab2;
            case 2:
                Zlikes tab3 = new Zlikes();
                return tab3;
            case 3:
                Zfans tab4 = new Zfans();
                return tab4;
            case 4:
                Recipes tab5 = new Recipes();
                return tab5;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

