package com.zdoof.stpl.zdoof.webservice;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;

/**
 * @author AndroidExample DotNetWebService Class
 *
 */
public class WebserviceCall2 {

    /**
     * Variable Decleration................
     *
     */
    String namespace = "http://tempuri.org/";
    private String url ="https://www.zdoof.com/web-services-android/zdoof-and-search.asmx";
    SoapPrimitive resultString;
    String SOAP_ACTION;
    SoapObject request = null, objMessages = null;
    SoapSerializationEnvelope envelope;
    AndroidHttpTransport androidHttpTransport;

    public WebserviceCall2() {
    }


    /**
     * Set Envelope
     */
    protected void SetEnvelope() {

        try {

            // Creating SOAP envelope			
            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            //You can comment that line if your web service is not .NET one.
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            androidHttpTransport = new AndroidHttpTransport(url);
            androidHttpTransport.debug = true;
           /* androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapObject result = (SoapObject)envelope.getResponse();*/

        } catch (Exception e) {
            System.out.println("Soap Exception---->>>" + e.toString());
        }
    }



    //////////////////////
   public String Serch_dish(String MethodName, String desc,String loction) {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();



            weightProp.setName("Description");
            weightProp.setValue(desc);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("Location");
            weightProp1.setValue(loction);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);


            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
    ///////////////////////////////////
	public String Serch_resturant(String MethodName, String desc,String loction) {

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp = new PropertyInfo();
			PropertyInfo weightProp1 = new PropertyInfo();



			weightProp.setName("Description");
			weightProp.setValue(desc);
			weightProp.setType(String.class);
			request.addProperty(weightProp);

			weightProp1.setName("Location");
			weightProp1.setValue(loction);
			weightProp1.setType(String.class);
			request.addProperty(weightProp1);


			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
    /////////////////////////////////////////////////
	public String Gstore(String MethodName, String desc,String loction) {

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp = new PropertyInfo();
			PropertyInfo weightProp1 = new PropertyInfo();



			weightProp.setName("Description");
			weightProp.setValue(desc);
			weightProp.setType(String.class);
			request.addProperty(weightProp);

			weightProp1.setName("Location");
			weightProp1.setValue(loction);
			weightProp1.setType(String.class);
			request.addProperty(weightProp1);


			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	///////////
	public String Serch_people(String MethodName, String desc,String loction) {

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp = new PropertyInfo();
			PropertyInfo weightProp1 = new PropertyInfo();



			weightProp.setName("Description");
			weightProp.setValue(desc);
			weightProp.setType(String.class);
			request.addProperty(weightProp);

			weightProp1.setName("Location");
			weightProp1.setValue(loction);
			weightProp1.setType(String.class);
			request.addProperty(weightProp1);


			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
    /////
	public String Serch_post(String MethodName, String desc,String loction) {

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp = new PropertyInfo();
			PropertyInfo weightProp1 = new PropertyInfo();



			weightProp.setName("Description");
			weightProp.setValue(desc);
			weightProp.setType(String.class);
			request.addProperty(weightProp);

			weightProp1.setName("Location");
			weightProp1.setValue(loction);
			weightProp1.setType(String.class);
			request.addProperty(weightProp1);


			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	////
	public String Serch_All(String MethodName, String description, String location, String descr, String loction) {

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp = new PropertyInfo();
			PropertyInfo weightProp1 = new PropertyInfo();
			PropertyInfo weightProp2 = new PropertyInfo();
			PropertyInfo weightProp3 = new PropertyInfo();

			weightProp.setName("Description");
			weightProp.setValue(description);
			weightProp.setType(String.class);
			request.addProperty(weightProp);

			weightProp1.setName("Location");
			weightProp1.setValue(location);
			weightProp1.setType(String.class);
			request.addProperty(weightProp1);

			weightProp2.setName("Restaurant_place_id");
			weightProp2.setValue(descr);
			weightProp2.setType(String.class);
			request.addProperty(weightProp2);

			weightProp3.setName("Grocery_place_id");
			weightProp3.setValue(loction);
			weightProp3.setType(String.class);
			request.addProperty(weightProp3);


			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();
                //result= (String[]) envelope.getResponse();
                return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	///
	/*public String AddDelNotice1(String MethodName, String action,String id,String date,String sub,String descrip,String enterby)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();
			PropertyInfo weightProp1 =new PropertyInfo();
			PropertyInfo weightProp2 =new PropertyInfo();
			PropertyInfo weightProp3 =new PropertyInfo();
			PropertyInfo weightProp4 =new PropertyInfo();
			PropertyInfo weightProp5 =new PropertyInfo();

			weightProp.setName("Action");
			weightProp.setValue(action);
			weightProp.setType(String.class);
			request.addProperty(weightProp);

			weightProp1.setName("ID");
			weightProp1.setValue(id);
			weightProp1.setType(String.class);
			request.addProperty(weightProp1);

			weightProp2.setName("Date");
			weightProp2.setValue(date);
			weightProp2.setType(String.class);
			request.addProperty(weightProp2);

			weightProp3.setName("Subject");
			weightProp3.setValue(sub);
			weightProp3.setType(String.class);
			request.addProperty(weightProp3);

			weightProp4.setName("Description");
			weightProp4.setValue(descrip);
			weightProp4.setType(String.class);
			request.addProperty(weightProp4);

			weightProp5.setName("EntryBy");
			weightProp5.setValue(enterby);
			weightProp5.setType(String.class);
			request.addProperty(weightProp5);

			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	////

//////////
public String Logout(String MethodName,String emp_id)

{

	try {
		SOAP_ACTION = namespace + MethodName;

		//Adding values to request object
		request = new SoapObject(namespace, MethodName);

		//Adding Double value to request object
		PropertyInfo weightProp =new PropertyInfo();



		weightProp.setName("UserID");
		weightProp.setValue(emp_id);
		weightProp.setType(String.class);
		request.addProperty(weightProp);



		SetEnvelope();

		try {

			//SOAP calling webservice
			androidHttpTransport.call(SOAP_ACTION, envelope);

			//Got Webservice response
			String result = envelope.getResponse().toString();

			return result;

		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}
	} catch (Exception e) {
		// TODO: handle exception
		return e.toString();
	}

}
	//////
	public String Push(String MethodName, String rid)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("RegID");
			weightProp.setValue(rid);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	//////////////
	public String EmDeptWiseName(String MethodName, String dpt)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("Department");
			weightProp.setValue(dpt);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	///////////
	public String EmpeNameWiseid(String MethodName, String name)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("Name");
			weightProp.setValue(name);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	///
	public String Alldata(String MethodName,String dd)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("RegID");
			weightProp.setValue(dd);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}*/
    ///package


    /************************************/
}
