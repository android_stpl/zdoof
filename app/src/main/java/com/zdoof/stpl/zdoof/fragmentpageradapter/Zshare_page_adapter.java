package com.zdoof.stpl.zdoof.fragmentpageradapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.zdoof.stpl.zdoof.fragments.Recipes;
import com.zdoof.stpl.zdoof.zshare.Chekin;
import com.zdoof.stpl.zdoof.zshare.Enjoyeddish;
import com.zdoof.stpl.zdoof.zshare.My_experiences;
import com.zdoof.stpl.zdoof.zshare.Post;

/**
 * Created by PCIS-ANDROID on 21-01-2016.
 */
public class Zshare_page_adapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    FragmentManager fm;
   /* public Profile_page_adapter(FragmentManager fm, int NumOfTabs) {
        super(fm);

    }*/


    public Zshare_page_adapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                My_experiences tab1 = new My_experiences();
                return tab1;
            case 1:
                Enjoyeddish tab2 = new Enjoyeddish();
                return tab2;
            case 2:
                Post tab3 = new Post();
                return tab3;
            case 3:
                Chekin tab4 = new Chekin();
                return tab4;
            case 4:
                Recipes tab5 = new Recipes();
                return tab5;
           /* case 5:
                Experience tab6 = new Experience();
                return tab6;*/
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}

