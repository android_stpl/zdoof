package com.zdoof.stpl.zdoof.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by UTTAM on 9/1/2017.
 */

public class TempStorage {

    Context mContext;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public TempStorage(Context context){
        this.mContext = context;
        preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        editor = preferences.edit();
    }
    public void putString(String key, String value) {

        editor.putString(key, value);
        editor.apply();
    }
    public String getString(String key) {
        return preferences.getString(key, null);
    }
}
