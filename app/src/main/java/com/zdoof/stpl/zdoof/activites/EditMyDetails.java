package com.zdoof.stpl.zdoof.activites;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.CircularNetworkImageView;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.utils.ZdoofConstants;
import com.zdoof.stpl.zdoof.views.Contropanel;
import com.zdoof.stpl.zdoof.webservice.PicConstant;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by stpl on 19/2/16.
 */
public class EditMyDetails extends Activity {
    Spinner utyp,countr,gen,status;
    ImageView Edit,close;
    RelativeLayout Edits,uty1,cont,gnn,stts,nam1;

    ImageView home,connection,notification,contlpnl,back;
    int Regd_id,mainutype1,subutype1,creaton1,createby1;
    Calendar mcurrentTime;
    CircularNetworkImageView circularview;
    private SimpleDateFormat dateformatter;
    private DatePickerDialog fromDatePickerDialog;
    final WebserviceCall com = new WebserviceCall();
    static String bResponse;
    ImageView calender;
    TextView nam;
    CircularNetworkImageView namm;
    TextView usertype1,contry1,gender1,status2;
    // ImageView uty,cont,gnn,stts;
    EditText fnam,lnam,addressln1,addressln2,zip,city,state,dob,email,phone;
    String[] utype={"FOODIE","CULINARY HOBBYIST","CHEF"};
    String[] Country={"India","United States"};
    String[] gender2={"Male","Female"};
    String[] status1={"Married","Unmarried"};
    String imageFilePath;
    String imageName=null;
    int SELECT_FILE = 1;
    int REQUEST_CAMERA = 2;
    ProgressDialog progressDialog;
    SharedPreferenceClass sharedPreferenceClass;
    SharedPreferences userSharedPreff;
    String mainutype,subutype,updatedon,updatedby,rid,meritaltext,formattedDate,frofimage;
    String usertype,fname,lastname,address1,addresln2,zipcode,cit,states,conty,gender,dateofb,emails,mobile,statuss,profileimage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);
        userSharedPreff = getSharedPreferences(ZdoofConstants.EXTRA_USER_PREFF, Context.MODE_PRIVATE);
        sharedPreferenceClass = new SharedPreferenceClass(this);
        circularview = (CircularNetworkImageView) findViewById(R.id.profilePictureView1);
        nam = (TextView) findViewById(R.id.name1);
        nam1= (RelativeLayout) findViewById(R.id.view1);
        namm= (CircularNetworkImageView) findViewById(R.id.profilePictureView1);
        // usertype1 = (TextView) findViewById(R.id.ss);
        //contry1 = (TextView) findViewById(R.id.ss1);
       /* gender1 = (TextView) findViewById(R.id.ss2);
        status2 = (TextView) findViewById(R.id.ss3);*/
        // Edit = (ImageView) findViewById(R.id.submt);
        Edits = (RelativeLayout) findViewById(R.id.submts);
        close = (ImageView) findViewById(R.id.close);
        uty1 = (RelativeLayout) findViewById(R.id.imageView10);
        cont = (RelativeLayout) findViewById(R.id.imageView11);
        gnn = (RelativeLayout) findViewById(R.id.imageView12);
        stts = (RelativeLayout) findViewById(R.id.imageView13);
        utyp = (Spinner) findViewById(R.id.spinner);
        countr = (Spinner) findViewById(R.id.spinner1);
        gen = (Spinner) findViewById(R.id.spinner2);
        fnam = (EditText) findViewById(R.id.lme);
        lnam = (EditText) findViewById(R.id.nme);
        home = (ImageView) findViewById(R.id.hme);
        connection = (ImageView) findViewById(R.id.hm);
        notification = (ImageView) findViewById(R.id.cnct);
        contlpnl = (ImageView) findViewById(R.id.ctrl);
        back = (ImageView) findViewById(R.id.imageView6);
        addressln1 = (EditText) findViewById(R.id.addrln);
        addressln2 = (EditText) findViewById(R.id.editText2);
        zip = (EditText) findViewById(R.id.zip);
        city = (EditText) findViewById(R.id.cty);
        state = (EditText) findViewById(R.id.state);
        dob = (EditText) findViewById(R.id.dobt);
        status = (Spinner) findViewById(R.id.spinner3);
        email = (EditText) findViewById(R.id.email);
        email.setEnabled(false);
        email.setKeyListener(null);
        phone = (EditText) findViewById(R.id.phone);
        calender = (ImageView) findViewById(R.id.clnder);
        /*usertype = sharedPreferenceClass.getValue_string("WRK");
        fname = sharedPreferenceClass.getValue_string("FRSTN");
        lastname = sharedPreferenceClass.getValue_string("LSTN");
        address1 = sharedPreferenceClass.getValue_string("ADR1");
        addresln2 = sharedPreferenceClass.getValue_string("ADR2");
        zipcode = sharedPreferenceClass.getValue_string("ZIP");
        cit = sharedPreferenceClass.getValue_string("CTY");
        states = sharedPreferenceClass.getValue_string("STS");
        conty = sharedPreferenceClass.getValue_string("CON");
        gender = sharedPreferenceClass.getValue_string("GEN");
        dateofb = sharedPreferenceClass.getValue_string("DOB");
        emails = sharedPreferenceClass.getValue_string("EMID");
        mobile = sharedPreferenceClass.getValue_string("PHONE");
        statuss = sharedPreferenceClass.getValue_string("MSTS");
        profileimage = sharedPreferenceClass.getValue_string("PHOTO");
        mainutype = sharedPreferenceClass.getValue_string("MNUTYPE");
        subutype = sharedPreferenceClass.getValue_string("SUBUTYPE");
        //updatedon=sharedPreferenceClass.getValue_string("CREAT");
        meritaltext = sharedPreferenceClass.getValue_string("meritaltext");*/
        usertype = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_TYPE,"");
        if(usertype.toLowerCase().equals("null")) {usertype = "";}
        fname = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_FIRST_NAME,"");
        if(fname.toLowerCase().equals("null")) {fname = "";}
        lastname = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_LAST_NAME,"");
        if(lastname.toLowerCase().equals("null")) {lastname = "";}
        address1 = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_ADDRESS_ONE,"");
        if(address1.toLowerCase().equals("null")) {address1 = "";}
        addresln2 = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_ADDRESS_TWO,"");
        if(addresln2.toLowerCase().equals("null")) {addresln2 = "";}
        zipcode = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_ZIP_CODE,"");
        if(zipcode.toLowerCase().equals("null")) {zipcode = "";}
        cit = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_CUTY,"");
        if(cit.toLowerCase().equals("null")) {cit = "";}
        states = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_STATE,"");
        if(states.toLowerCase().equals("null")) {states = "";}
        conty = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_COUNTRY,"");
        if(conty.toLowerCase().equals("null")) {conty = "";}
        gender = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_GENDER,"");
        if(gender.toLowerCase().equals("null")) {gender = "";}
        dateofb = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_DOB,"");
        if(dateofb.toLowerCase().equals("null")) {dateofb = "";}
        emails = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_EMAIL,"");
        if(emails.toLowerCase().equals("null")) {emails = "";}
        mobile = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_MOBILE,"");
        if(mobile.toLowerCase().equals("null")) {mobile = "";}
        statuss = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_STATUS,"");
        if(statuss.toLowerCase().equals("null")) {statuss = "";}
        profileimage = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_PROFILE_IMAGE,"");
        if(profileimage.toLowerCase().equals("null")) {profileimage = "";}
        mainutype = userSharedPreff.getString(ZdoofConstants.EXTRA_MAIN_USER_TYPE,"");
        if(mainutype.toLowerCase().equals("null")) {mainutype = "";}
        subutype = userSharedPreff.getString(ZdoofConstants.EXTRA_SUBUSER_TYPE,"");
        if(subutype.toLowerCase().equals("null")) {subutype = "";}
        meritaltext = userSharedPreff.getString(ZdoofConstants.EXTRA_USER_MARITIAL_STATUS,"");
        if(meritaltext.toLowerCase().equals("null")) {meritaltext = "";}

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditMyDetails.this, R.layout.spinner_itemes, utype);
        utyp.setAdapter(adapter);
        utyp.setSelection(adapter.getPosition(usertype));
        utyp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                usertype = utyp.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(EditMyDetails.this, R.layout.spinner_itemes, Country);
        countr.setAdapter(adapter1);
        countr.setSelection(adapter1.getPosition(conty));
        countr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                conty = countr.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(EditMyDetails.this, R.layout.spinner_itemes, status1);
        status.setAdapter(adapter2);
        status.setSelection(adapter2.getPosition(statuss));
        status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                statuss = status.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(EditMyDetails.this, R.layout.spinner_itemes, gender2);
        gen.setAdapter(adapter3);
        gen.setSelection(adapter3.getPosition(gender));
        gen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gender = gen.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        frofimage= PicConstant.PROFILE_URL1+profileimage;
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.noimg)
                .showImageOnLoading(R.drawable.load).build();
        imageLoader.displayImage(frofimage, circularview, options);
       /* Uri myUri = Uri.parse(frofimage);
        Picasso.with(this)
                .load(myUri)
                .placeholder(R.drawable.load)
                .error(R.drawable.load)
                .into(circularview);*/
        updatedby=sharedPreferenceClass.getValue_string("CREBY");
        rid=sharedPreferenceClass.getValue_string("UIDD");

        // nam.setText(fname +" "+ lastname);
        fnam.setText(fname);
        lnam.setText(lastname);
        //  utyp.setSelection(GetIndexOfArray(usertype,utype));
        // usertype1.setText(usertype);
        addressln1.setText(address1);
        addressln2.setText(addresln2);
        state.setText(states);
        // contry1.setText(conty);
        if(gender.equals("null")) {
            gender = "Select";

        }
        dob.setText(dateofb);
        email.setText(emails);
        phone.setText(mobile);
        if(statuss.equals("null")){
            statuss="Select";

        }
        // status2.setText(statuss);
        zip.setText(zipcode);
        city.setText(cit);
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
        formattedDate = df.format(c.getTime());
        updatedon=formattedDate;

       /* uty1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // utyp.setVisibility(View.VISIBLE);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditMyDetails.this, R.layout.spinner_itemes, utype);
                utyp.setAdapter(adapter);
                //utyp.setSelection(GetIndexOfArray(usertype,utype));


            }
        });
*//*
        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // countr.setVisibility(View.VISIBLE);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditMyDetails.this, R.layout.spinner_itemes, Country);
                countr.setAdapter(adapter);
                countr.setSelection(GetIndexOfArray(conty, Country));

            }
        });*/
       /* gnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //gen.setVisibility(View.VISIBLE);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditMyDetails.this, R.layout.spinner_itemes, gender2);
                gen.setAdapter(adapter);
               // gen.setSelection(GetIndexOfArray(gender, gender2));
                gen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        gender = gen.getItemAtPosition(position).toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });
        stts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // status.setVisibility(View.VISIBLE);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(EditMyDetails.this, R.layout.spinner_itemes, status1);
                status.setAdapter(adapter);
                status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        statuss=status.getItemAtPosition(position).toString();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        });*/
        mcurrentTime = Calendar.getInstance();
        dateformatter = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        calender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fromDatePickerDialog = new DatePickerDialog(EditMyDetails.this, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        String da_te = dateformatter.format(newDate.getTime());
                        dob.setText(da_te);
                        // Date = datew.getText().toString();
                    }

                }, mcurrentTime.get(Calendar.YEAR), mcurrentTime.get(Calendar.MONTH), mcurrentTime.get(Calendar.DAY_OF_MONTH));
                //fromDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                fromDatePickerDialog.show();
            }
        });
        home.setOnTouchListener(new View.OnTouchListener() {


            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent intent = new Intent(EditMyDetails.this, Home.class);
                startActivity(intent);
                finish();
                return true;
            }
        });
        connection.setOnTouchListener(new View.OnTouchListener() {


            @Override
            public boolean onTouch(View v, MotionEvent event) {
               /* AsyncCallWS task = new AsyncCallWS();
                task.execute();*/
                Intent intent = new Intent(EditMyDetails.this, Connecton.class);
                startActivity(intent);
                finish();
                return true;
            }
        });
        contlpnl.setOnTouchListener(new View.OnTouchListener() {


            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Intent intent = new Intent(EditMyDetails.this, Contropanel.class);
                startActivity(intent);
                finish();
                return true;
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Edits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fname = fnam.getText().toString().trim();
                lastname = lnam.getText().toString().trim();
                address1 = addressln1.getText().toString();
                addresln2 = addressln2.getText().toString();
                zipcode = zip.getText().toString().trim();
                cit = city.getText().toString().trim();
                states = state.getText().toString().trim();
                dateofb = dob.getText().toString().trim();
                emails = email.getText().toString().trim();
                if (statuss.equals("Married")) {
                    statuss = "1";
                } else if (statuss.equals("Unmarried")) {
                    statuss = "2";
                }
                if (gender.equals("Male")) {
                    gender = "1";
                } else if (gender.equals("Female")) {
                    gender = "2";
                }
                if(usertype.equals("FOODIE")){
                    mainutype1=4;
                }else if(usertype.equals("CULINARY HOBBYIST")){
                    mainutype1=6;
                }else if(usertype.equals("CHEF")){
                    mainutype1=5;
                }
                // statuss=status2.getText().toString().trim();
                mobile = phone.getText().toString().trim();
                Regd_id = Integer.parseInt(rid);
                if(subutype == ""){
                    subutype = "0";
                }
                subutype1 = Integer.parseInt(subutype);
                //creaton1=Integer.parseInt(updatedon);
                createby1 = Integer.parseInt(updatedby);
                if (usertype.equals("") || fname.equals("") || lastname.equals("") || zipcode.equals("") || cit.equals("") || state.equals("") || conty.equals("") || emails.equals("")) {
                    Toast.makeText(getApplicationContext(), "Every Fields are required", Toast.LENGTH_SHORT).show();


                } else {
                    if (imageName == null) {
                        Bitmap bit = ((BitmapDrawable) circularview.getDrawable()).getBitmap();
                        setImageName(bit);
                    }
                    AsyncCallWS task = new AsyncCallWS();
                    task.execute();
                    progressDialog = ProgressDialog.show(EditMyDetails.this, "", "Loading...");
                }
            }
        });

        nam1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //if (imageFilePath == null) {
                final CharSequence[] items = {"Take Photo", "Choose from Library",
                        "Cancel"};

                AlertDialog.Builder builder = new AlertDialog.Builder(EditMyDetails.this);
                builder.setTitle("Add Photo!");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Take Photo")) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, REQUEST_CAMERA);
                        } else if (items[item].equals("Choose from Library")) {
                            Intent intent = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            //intent.setType("image/*");
                            startActivityForResult(
                                    Intent.createChooser(intent, "Select File"), SELECT_FILE);

                        } else if (items[item].equals("Cancel")) {
                            dialog.dismiss();
                        }

                    }
                });

                builder.show();
               /* } else {
                    Toast.makeText(EditMyDetails.this, "You Have already Choose Three Images", Toast.LENGTH_SHORT).show();
                }*/

            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && requestCode == SELECT_FILE){
            onSelectFromGalleryResult(data);
        }
       /*else if(resultCode == Activity.RESULT_OK && requestCode == SELECT_FILE1){
            onSelectFromGalleryResult1(data);
        }*/
        else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CAMERA){
            onCaptureImageResult(data);
        }
        /*else if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CAMERA1){
            onCaptureImageResult1(data);
        }*/
        else{
            Toast.makeText(EditMyDetails.this, "Please try again !!!", Toast.LENGTH_SHORT).show();
        }


        super.onActivityResult(requestCode, resultCode, data);
    }
    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        //if(imageFilePath==null){
        imageFilePath = destination.toString();


        File imagefile = new File(imageFilePath);
        FileInputStream fis = null;
                   /* try {
                        fis = new FileInputStream(imagefile);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    Bitmap bm = BitmapFactory.decodeStream(fis);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100 , baos);
                    byte[] b = baos.toByteArray();
                    imageName = Base64.encodeToString(b, Base64.DEFAULT);*/
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        circularview.setImageBitmap(thumbnail);


        byte[] b = bytes.toByteArray();
        imageName = Base64.encodeToString(b, Base64.DEFAULT);

        // }



    }
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        //Cursor cursor = managedQuery(selectedImageUri, projection, null, null, null);
        Cursor cursor = getContentResolver().query(selectedImageUri, projection, null, null, null);
        if (cursor!=null){
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();

            //if(imageFilePath==null){
            imageFilePath = cursor.getString(column_index).toString();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
           // BitmapFactory.decodeFile(imageFilePath,options);
            Bitmap bm1 = BitmapFactory.decodeFile(imageFilePath,options);
            File imagefile = new File(imageFilePath);
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(imagefile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            Bitmap bm = BitmapFactory.decodeStream(fis);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 60, baos);
            byte[] b = baos.toByteArray();
            imageName = Base64.encodeToString(b, Base64.DEFAULT);
            //ImageNName="@" + imageName;
            circularview.setImageBitmap(bm1);

            //  }
        }
        else {Toast.makeText(this,"device not support choose image from gallary",Toast.LENGTH_SHORT).show();}

    }

    private void setImageName(Bitmap bit) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bit.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        imageName = Base64.encodeToString(b, Base64.DEFAULT);
    }

    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");


            bResponse = com.Editprofile("upd_user_regd", Regd_id, fname, lastname, address1, addresln2, zipcode, cit, states,conty,gender,dateofb,statuss,mobile,emails,imageName,mainutype1,subutype1,createby1,updatedon);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            progressDialog.dismiss();
            if (bResponse.equals("2")) {
                Toast.makeText(getApplicationContext(), "Your Profile Updated Successfully", Toast.LENGTH_LONG).show();
                Intent i = new Intent(EditMyDetails.this, Contropanel.class);
                startActivity(i);

                finish();
// why 6 there should be some meaningful name
            } else if (bResponse.equals("6")) {
                Toast.makeText(getApplicationContext(), "Email already exists!", Toast.LENGTH_LONG).show();

// why 7 there should be some meaningful name
            } else if (bResponse.equals("7")) {
                Toast.makeText(getApplicationContext(), "Mobileno already exists!", Toast.LENGTH_LONG).show();

            }
            else {

                Toast.makeText(getApplicationContext(), "Sorry try again!", Toast.LENGTH_LONG).show();

            }

        }


    }
    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(getApplicationContext(),Contropanel.class);
//        startActivity(intent);
        finish();
    }


}
