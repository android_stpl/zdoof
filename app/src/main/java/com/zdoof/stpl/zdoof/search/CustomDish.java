package com.zdoof.stpl.zdoof.search;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.search.Details.View_dish_Deatails;
import com.zdoof.stpl.zdoof.webservice.PicConstant;
import com.zdoof.stpl.zdoof.webservice.webservice_searchdetails;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by stpl on 12/3/16.
 */
public class CustomDish extends BaseAdapter{
    Search_dish search_dish;
    ArrayList<Detail> values;
    ArrayList<String>Ingradiant;
    //ArrayList<String>Stareview;

    String pic,pic2,postedby,userid,dish_id,Dish_Name,Preferred_time,Cooking_Method,Dish_desc,thudish,total_review_dish,
            Dish_picture,Name,Allergy_info,details,ingrediants,ingradiantdtl,qantity,wgt,Dish_id,Stareview,reviewstar;
    int riid1,dishid;

    private boolean viewDetailsclicked;

    ViewHolder holder;
    String dish,ingradiant,gros,createdby;
    final webservice_searchdetails com1 = new webservice_searchdetails();
    static String bResponse;
    String[] bresponce = {};
    public CustomDish(Search_dish search_dish, int spiner_item, ArrayList<Detail> values) {
        this.search_dish=search_dish;
        this.values=values;
        viewDetailsclicked = false;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vgrid;
        LayoutInflater inflater = search_dish.getLayoutInflater();
        vgrid = inflater.inflate(R.layout.food_search, null);
        holder = new ViewHolder();
        holder.rateBar = (RatingBar) vgrid.findViewById(R.id.dish_ratings);
        holder.img= (ImageView) vgrid.findViewById(R.id.imageView13);
        holder.text= (TextView) vgrid.findViewById(R.id.textView18);
        holder.text1= (TextView) vgrid.findViewById(R.id.textView19);
        holder.text2= (TextView) vgrid.findViewById(R.id.textView20);
        holder.text3=(TextView) vgrid.findViewById(R.id.textView);
        holder.viewdetails=(TextView) vgrid.findViewById(R.id.vd);
        postedby=values.get(position).getPostedby();
        holder.text3.setText(postedby);
       /* userid=values.get(position).getId();
        riid1=Integer.parseInt(userid);
        dish_id=values.get(position).getDish_id();
        dishid=Integer.parseInt(dish_id);*/
        Float rating = Float.parseFloat(values.get(position).getRatings());
        holder.rateBar.setRating(rating);
        holder.rateBar.setIsIndicator(true);
        pic=values.get(position).getDish_pic();
        pic2= PicConstant.PROFILE_URL1+pic;
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(search_dish).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                        // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.noimg)
                .showImageOnLoading(R.drawable.uute).build();
        imageLoader.displayImage(pic2, holder.img, options);
        /*Uri myUri = Uri.parse(pic2);
        Picasso.with(search_dish)
                .load(myUri)
                .placeholder(R.drawable.load)
                .error(R.drawable.noimg)
                .into(img);*/
        holder.text.setText(values.get(position).getDish_name());
        holder.text1.setText(values.get(position).getReviews());
        holder.text2.setText(values.get(position).getZlikes());

        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //riid1 = Integer.parseInt(userid);
//                userid=values.get(position).getId();
//                Intent intent = new Intent(search_dish, View_dish_Deatails.class);
//                intent.putExtra("USERID", userid);
//                search_dish.startActivity(intent);
                if (!viewDetailsclicked) {
                    viewDetailsclicked = true;
                    userid=values.get(position).getId();
                    riid1=Integer.parseInt(userid);
                    dish_id=values.get(position).getDish_id();
                    dishid=Integer.parseInt(dish_id);
                    AsyncCallWS2 task = new AsyncCallWS2();
                    task.execute();
                }
            }
        });
        holder.viewdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!viewDetailsclicked) {
                    viewDetailsclicked = true;
                    userid=values.get(position).getId();
                    riid1=Integer.parseInt(userid);
                    dish_id=values.get(position).getDish_id();
                    dishid=Integer.parseInt(dish_id);
                    AsyncCallWS2 task = new AsyncCallWS2();
                    task.execute();
                }

            }
        });
        return vgrid;
    }
    class AsyncCallWS2 extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            bResponse = com1.Viewdetails("ret_dish_details", dishid, riid1);
            bresponce = bResponse.split("\\$");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            dish = bresponce[0];
            ingradiant = bresponce[2];
            gros = bresponce[3];
            Stareview=bresponce[4];

            try {
                JSONArray jr = new JSONArray(dish);
                //Dish=new ArrayList<Detail>();

                for (int i = 0; i < jr.length(); i++) {
                    // Detail productdto = new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);
                    Dish_Name = jsonObject.getString("Dish_name");
                    Dish_id=jsonObject.getString("Dish_id");
                    createdby=jsonObject.getString("Created_by");
                    Preferred_time = jsonObject.getString("Preferred_time");
                    Cooking_Method = jsonObject.getString("Cooking_Method");
                    Dish_desc = jsonObject.getString("Dish_desc");
                    thudish = jsonObject.getString("thudish");
                    //total_review_dish = jsonObject.getString("total_review_dish");
                    Dish_picture = jsonObject.getString("mid_img");
                    Name = jsonObject.getString("Name");
                    Allergy_info = jsonObject.getString("Allergy_info");
                    details = jsonObject.getString("Dish_desc");
                }

                //CustomDetails adapter=new CustomDetails(search_dish,R.layout.spiner_item,Dish);


                // progressDialog.dismiss();

            } catch (Exception e) {

            }


            if (bResponse.equals("0")) {
                Toast.makeText(search_dish, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (bResponse.equals("1")) {

                // Toast.makeText(getActivity(), "You have successfully Posted !", Toast.LENGTH_LONG).show();

            } else {

            }
            try {
                JSONArray jr = new JSONArray(Stareview);
                //Dish=new ArrayList<Detail>();

                for (int i = 0; i < jr.length(); i++) {
                    // Detail productdto = new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);
                    reviewstar = jsonObject.getString("reviewrating");
                }

                //CustomDetails adapter=new CustomDetails(search_dish,R.layout.spiner_item,Dish);


                // progressDialog.dismiss();

            } catch (Exception e) {

            }
            try {
                JSONArray jr = new JSONArray(gros);
                //Dish=new ArrayList<Detail>();

                for (int i = 0; i < jr.length(); i++) {
                    // Detail productdto = new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);
                    total_review_dish = jsonObject.getString("totalreview");
                }

                //CustomDetails adapter=new CustomDetails(search_dish,R.layout.spiner_item,Dish);


                // progressDialog.dismiss();

            } catch (Exception e) {

            }


            if (bResponse.equals("0")) {
                Toast.makeText(search_dish, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (bResponse.equals("1")) {

                // Toast.makeText(getActivity(), "You have successfully Posted !", Toast.LENGTH_LONG).show();

            } else {

            }


            try {
                JSONArray jr = new JSONArray(ingradiant);
                Ingradiant=new ArrayList<String>();
                for (int i = 0; i < jr.length(); i++) {

                    JSONObject jsonObject = jr.getJSONObject(i);
                    ingrediants=jsonObject.getString("Ingredients_Name");
                     ingradiantdtl=jsonObject.getString("Ingrd_Dtl_id");
                     qantity=jsonObject.getString("Quantity");
                     wgt=jsonObject.getString("Weight");
                    String value=(ingrediants+" " + ingradiantdtl+" " +qantity+" " + wgt).toUpperCase();
                    Ingradiant.add(value);
                   /* Ingradiant.add(ingradiantdtl);
                    Ingradiant.add(qantity);
                    Ingradiant.add(wgt);
*/
                }
               /* CustomDetail adapter=new CustomDetail(search_dish,R.layout.spiner_item,Ingradiant);*/

                Intent in = new Intent(search_dish, View_dish_Deatails.class);
                in.putExtra("Dname", Dish_Name);
                in.putExtra("PFTM", Preferred_time);
                in.putExtra("CKM", Cooking_Method);
                in.putExtra("Dsc", Dish_desc);
                in.putExtra("Tl", thudish);
                in.putExtra("TR", total_review_dish);
                in.putExtra("Dp", Dish_picture);
                in.putExtra("Nm", Name);
                in.putExtra("algf", Allergy_info);
                in.putExtra("Details", details);
                in.putExtra("UID",createdby);
                in.putExtra("dsid", Dish_id);
                in.putExtra("ARRAY",Ingradiant);
                in.putExtra("STAR",reviewstar);
                search_dish.startActivity(in);
                viewDetailsclicked = false;
                //CustomDetails adapter=new CustomDetails(search_dish,R.layout.spiner_item,Dish);


                // progressDialog.dismiss();

            } catch (Exception e) {

            }


            if (bResponse.equals("0")) {
                Toast.makeText(search_dish, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (bResponse.equals("1")) {

                // Toast.makeText(getActivity(), "You have successfully Posted !", Toast.LENGTH_LONG).show();

            } else {

            }

        }
    }
    private static class ViewHolder {
        ArrayList<Detail>Dish;

        ImageView img;
        TextView text,text1,text2,text3,viewdetails;
        RatingBar rateBar;
    }
}
