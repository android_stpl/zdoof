package com.zdoof.stpl.zdoof.receivers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;

/**
 * Created by Shiftu Bd team1 on 11-Mar-2017.
 */

public class PreferenceReceiver extends WakefulBroadcastReceiver {

    public static final String PREFS_FILTER = "com.zdoof.stpl.zdoof.receivers.PreferenceReceiver.PREFS_FILTER";

    SharedPreferenceClass sharedPreferenceClass;

    @Override
    public void onReceive(Context context, Intent intent) {
        sharedPreferenceClass = new SharedPreferenceClass(context);
        int pending_conn_size = intent.getIntExtra("PENDING_CONN_SIZE",-1);

        if (pending_conn_size!=-1) {
            sharedPreferenceClass.setValue_int("PENDING_CONN_SIZE", pending_conn_size);
            Log.i("Watcher_prefs :", "PENDING_CONN_SIZE added, size : "+pending_conn_size);
        } else {Log.i("Watcher_prefs :", "error!");}
    }
}
