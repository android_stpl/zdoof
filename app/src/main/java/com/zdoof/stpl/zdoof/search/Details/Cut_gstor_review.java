package com.zdoof.stpl.zdoof.search.Details;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.CircularNetworkImageView;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;

import java.util.ArrayList;

/**
 * Created by stpl on 15/7/16.
 */
public class Cut_gstor_review extends BaseAdapter{
    Grocery_details grocery_details;
    ArrayList<Detail> gstore;

    String pic;
    public Cut_gstor_review(Grocery_details grocery_details, int spiner_item, ArrayList<Detail> gstore) {
        this.grocery_details=grocery_details;
        this.gstore=gstore;
    }

    @Override
    public int getCount() {
        return gstore.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {



        View view;
        LayoutInflater inflater = grocery_details.getLayoutInflater();
        view = inflater.inflate(R.layout.custom_coment_detaitail, null);
        CircularNetworkImageView circularImageView = (CircularNetworkImageView) view.findViewById(R.id.profilePictureVie);
        TextView nam = (TextView) view.findViewById(R.id.textVie1);
        TextView types = (TextView) view.findViewById(R.id.textV1);
        TextView zlikes=(TextView) view.findViewById(R.id.textView20);
        TextView rst= (TextView) view.findViewById(R.id.textV11);
        TextView datess=(TextView) view.findViewById(R.id.textV12);
       // RatingBar ratingBar= (RatingBar) view.findViewById(R.id.ratings);
        //ImageView down= (ImageView) view.findViewById(R.id.fdd);
        TextView cmty=(TextView) view.findViewById(R.id.tb);
        nam.setText(gstore.get(position).getName());
        types.setText(gstore.get(position).getComment());
        pic=gstore.get(position).getPfimage();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(grocery_details).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.noimg)
                .showImageOnLoading(R.drawable.uute).build();
                imageLoader.displayImage(pic, circularImageView,options);

     /*  zlikes=values.get(position).getThumbsup();
        review=values.get(position).getReviews();
        text3.setText(review);
        text4.setText(zlikes);

        pic=values.get(position).getRest_img();

        pic2= PicConstant.PROFILE_URL1+pic;
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(search_resturant).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.noimg)
                .showImageOnLoading(R.drawable.uute).build();
        imageLoader.displayImage(pic2, img, options);
       *//**//* Uri myUri = Uri.parse(pic2);
        Picasso.with(search_resturant)
                .load(myUri)
                .placeholder(R.drawable.load)
                .error(R.drawable.blankpeople)
                .into(img);
*//**//*
        text.setText(values.get(position).getRestaurant_name());
        text1.setText(values.get(position).getRest_address());
        text2.setText(values.get(position).getRest_phone());

        viewdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String resturant_id = values.get(position).getRest_id();
                Intent in = new Intent(search_resturant, Resturant_design.class);
                in.putExtra("RID", resturant_id);
                search_resturant.startActivity(in);


            }
        });*/

        return view;
    }
}

