package com.zdoof.stpl.zdoof.adapters;

import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.webservice.PicConstant;

import java.util.ArrayList;

/**
 * Created by stpl on 10/3/16.
 */
public class CustomZfans extends BaseAdapter {
    FragmentActivity activity;
    ArrayList<Detail> detl;
    String pic,pic1,Likedto,Zfanname;
    public CustomZfans(FragmentActivity activity, int spiner_item, ArrayList<Detail> detl) {
        this.activity=activity;
        this.detl=detl;

    }

    @Override
    public int getCount() {
        return detl.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        LayoutInflater inflater = activity.getLayoutInflater();
        view = inflater.inflate(R.layout.custom_zfans, null);

        TextView text= (TextView) view.findViewById(R.id.likdto);
        TextView text1= (TextView) view.findViewById(R.id.faof);
        TextView type= (TextView) view.findViewById(R.id.tkype);
        TextView tm= (TextView) view.findViewById(R.id.liketime);
        Zfanname=detl.get(position).getZfan();
        text.setText(detl.get(position).getZfan());
        text1.setText(detl.get(position).getLikedto());
        type.setText(detl.get(position).getType());
        tm.setText(detl.get(position).getLikedon());
        //Likedto=detl.get(position).getLikedto();

        /*if(Likedto==null){
            text1.setText("NA");
        }else{
            text1.setText(Likedto);
        }*/

        /*pic=detl.get(position).getType();
        pic1= PicConstant.PROFILE_URL1+pic;
        Uri myUri = Uri.parse(pic1);
        Picasso.with(activity)
                .load(myUri)
                .placeholder(R.drawable.load)
                .error(R.drawable.blankpeople)
                .into(img);*/
        //tm.setText(detl.get(position).getLikedon());
        return view;
    }
}
