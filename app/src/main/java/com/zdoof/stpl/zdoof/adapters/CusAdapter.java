package com.zdoof.stpl.zdoof.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zdoof.stpl.zdoof.activites.Connecton;
import com.zdoof.stpl.zdoof.R;

/**
 * Created by stpl on 17/2/16.
 */
public class CusAdapter extends BaseAdapter{
    Connecton connecton1;
    String[] option;
    public CusAdapter(Connecton connecton1, int support_simple_spinner_dropdown_item, String[] option) {
        this.connecton1=connecton1;
        this.option=option;
    }

    @Override
    public int getCount() {
        return option.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = connecton1.getLayoutInflater();
        view = inflater.inflate(R.layout.customoption, null);
        TextView text= (TextView) view.findViewById(R.id.textView7);
        text.setText(option[position]);
        return view;
    }
}
