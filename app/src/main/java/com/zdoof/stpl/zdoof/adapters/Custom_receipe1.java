package com.zdoof.stpl.zdoof.adapters;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.search.Details.View_dish_Deatails;
import com.zdoof.stpl.zdoof.webservice.PicConstant;
import com.zdoof.stpl.zdoof.webservice.WebserviceCallpost;
import com.zdoof.stpl.zdoof.webservice.webservice_searchdetails;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by stpl on 10/6/16.
 */
public class Custom_receipe1 extends BaseAdapter{
    FragmentActivity activity;
    ArrayList<Detail> dish;
    ArrayList<String> Ingradiant;
    SharedPreferenceClass sharedPreferenceClass;
    SimpleDateFormat sdf;
    String rcpimg,pic5;
    final WebserviceCallpost com = new WebserviceCallpost();
    final webservice_searchdetails com1 = new webservice_searchdetails();
    String cResponse;
    String[] cResponsArr;
    int Post_id, pty,dishid, riid1;
    String Expdate,uuid;
    String[] Liked;
    Boolean isYumClicked,viewDetailsclicked;
    public Custom_receipe1(FragmentActivity activity, int spiner_item, ArrayList<Detail> dish) {
        this.activity=activity;
        this.dish=dish;
        isYumClicked = false;
        viewDetailsclicked = false;
        sharedPreferenceClass = new SharedPreferenceClass(activity);
        uuid = sharedPreferenceClass.getValue_string("UDDIDD");
        //riid1 = Integer.parseInt(sharedPreferenceClass.getValue_string("UDDIDD"));
        pty = 2;
        String format = "dd/MMM/yyyy H:mm:ss";
        sdf = new SimpleDateFormat(format);
    }

    @Override
    public int getCount() {
        return dish.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view;
        LayoutInflater inflater = activity.getLayoutInflater();
        view = inflater.inflate(R.layout.custom_recipe, null);
        TextView text= (TextView) view.findViewById(R.id.rcpnme);
        final TextView text1= (TextView) view.findViewById(R.id.rcplyk);
        ImageView image= (ImageView) view.findViewById(R.id.imgrecp);
        ImageView yum_img = (ImageView) view.findViewById(R.id.like);
        String[] y_arr = dish.get(position).getPost_like().split(" ");
        String yum = y_arr[0] + " yums";
        text1.setText(yum);
        text.setText(dish.get(position).getDish_namee());
        rcpimg=dish.get(position).getDish_picture();
        pic5= PicConstant.PROFILE_URL1 + rcpimg;
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(activity).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.blankpeople)
                .showImageOnLoading(R.drawable.uute).build();
//initialize image view
//download and display image from url
        imageLoader.displayImage(pic5, image, options);
        String isliked = dish.get(position).getIflike();
        if (isliked==null) {
        } else if (isliked.equals("btn btn-primary btn-circle btn-xs csLikediv csLikedivsel")) {
            yum_img.setImageResource(R.drawable.smilegreen);
        } else {
            yum_img.setImageResource(R.drawable.smile);
        }

        yum_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isYumClicked) {
                    isYumClicked = true;
                    riid1 = Integer.parseInt(uuid);
                    pty = 2;
                    Post_id = Integer.parseInt(dish.get(position).getDish_id());
                    String timezoneID = TimeZone.getDefault().getID();
                    sdf.setTimeZone(TimeZone.getTimeZone(timezoneID));
                    Expdate = sdf.format(new Date());
                    ClickYumTask yumTask = new ClickYumTask(position);
                    yumTask.execute();
                }

            }
        });
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!viewDetailsclicked) {
                    viewDetailsclicked = true;
                    dishid = Integer.parseInt(dish.get(position).getDish_id());
                    riid1 = Integer.parseInt(dish.get(position).getPostedby());
                    DishDetailsTask detailsTask = new DishDetailsTask();
                    detailsTask.execute();
                }
            }
        });
        //image.setImageResource(Integer.parseInt(dish.get(position).getDish_picture()));
        return view;
    }

    class ClickYumTask extends AsyncTask <Void, Void, Void> {

        int pos;
        public ClickYumTask(int pos) {
            this.pos = pos;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            cResponse = com.Addlike("post_like", Post_id, pty, riid1, Expdate);
            Liked = cResponse.split(",");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (Liked[0].equals("1")) {
                dish.get(pos).setIflike("btn btn-primary btn-circle btn-xs csLikediv csLikedivsel");
            } else if (Liked[0].equals("2")) {
                dish.get(pos).setIflike("btn btn-primary btn-circle btn-xs csLikediv");
            }
            dish.get(pos).setPost_like(Liked[1]);
            cResponse = null;
            isYumClicked = false;
            notifyDataSetChanged();
        }
    }

    class DishDetailsTask extends AsyncTask<Void, Void, Void> {
        String Dish_Name="",
                Dish_id="",
                createdby="",
                Preferred_time="",
                Cooking_Method="",
                Dish_desc="",
                thudish="",
                Dish_picture="",
                Name="",
                Allergy_info="",
                details="",
                total_review_dish="",
                reviewstar="",
                ingrediants="",ingradiantdtl="",qantity="",wgt="";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            cResponse = com1.Viewdetails("ret_dish_details", dishid, riid1);
            cResponsArr = cResponse.split("\\$");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String dish = cResponsArr[0];
            String ingradiant = cResponsArr[2];
            String gros = cResponsArr[3];
            String Stareview=cResponsArr[4];

            try {
                JSONArray jr = new JSONArray(dish);
                //Dish=new ArrayList<Detail>();

                for (int i = 0; i < jr.length(); i++) {
                    // Detail productdto = new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);
                    Dish_Name = jsonObject.getString("Dish_name");
                    Dish_id=jsonObject.getString("Dish_id");
                    createdby=jsonObject.getString("Created_by");
                    Preferred_time = jsonObject.getString("Preferred_time");
                    Cooking_Method = jsonObject.getString("Cooking_Method");
                    Dish_desc = jsonObject.getString("Dish_desc");
                    thudish = jsonObject.getString("thudish");
                    //total_review_dish = jsonObject.getString("total_review_dish");
                    Dish_picture = jsonObject.getString("mid_img");
                    Name = jsonObject.getString("Name");
                    Allergy_info = jsonObject.getString("Allergy_info");
                    details = jsonObject.getString("Dish_desc");
                }

                //CustomDetails adapter=new CustomDetails(search_dish,R.layout.spiner_item,Dish);


                // progressDialog.dismiss();

            } catch (Exception e) {

            }


            if (cResponse.equals("0")) {
                Toast.makeText(activity, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (cResponse.equals("1")) {

                // Toast.makeText(getActivity(), "You have successfully Posted !", Toast.LENGTH_LONG).show();

            } else {

            }
            try {
                JSONArray jr = new JSONArray(Stareview);
                //Dish=new ArrayList<Detail>();

                for (int i = 0; i < jr.length(); i++) {
                    // Detail productdto = new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);
                    reviewstar = jsonObject.getString("reviewrating");
                }

                //CustomDetails adapter=new CustomDetails(search_dish,R.layout.spiner_item,Dish);


                // progressDialog.dismiss();

            } catch (Exception e) {

            }
            try {
                JSONArray jr = new JSONArray(gros);
                //Dish=new ArrayList<Detail>();

                for (int i = 0; i < jr.length(); i++) {
                    // Detail productdto = new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);
                    total_review_dish = jsonObject.getString("totalreview");
                }

                //CustomDetails adapter=new CustomDetails(search_dish,R.layout.spiner_item,Dish);


                // progressDialog.dismiss();

            } catch (Exception e) {

            }


            if (cResponse.equals("0")) {
                Toast.makeText(activity, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (cResponse.equals("1")) {

                // Toast.makeText(getActivity(), "You have successfully Posted !", Toast.LENGTH_LONG).show();

            } else {

            }


            try {
                JSONArray jr = new JSONArray(ingradiant);
                Ingradiant=new ArrayList<String>();
                for (int i = 0; i < jr.length(); i++) {

                    JSONObject jsonObject = jr.getJSONObject(i);
                    ingrediants=jsonObject.getString("Ingredients_Name");
                    ingradiantdtl=jsonObject.getString("Ingrd_Dtl_id");
                    qantity=jsonObject.getString("Quantity");
                    wgt=jsonObject.getString("Weight");
                    String value=(ingrediants+" " + ingradiantdtl+" " +qantity+" " + wgt).toUpperCase();
                    Ingradiant.add(value);
                   /* Ingradiant.add(ingradiantdtl);
                    Ingradiant.add(qantity);
                    Ingradiant.add(wgt);
*/
                }
               /* CustomDetail adapter=new CustomDetail(search_dish,R.layout.spiner_item,Ingradiant);*/

                Intent in = new Intent(activity, View_dish_Deatails.class);
                in.putExtra("Dname", Dish_Name);
                in.putExtra("PFTM", Preferred_time);
                in.putExtra("CKM", Cooking_Method);
                in.putExtra("Dsc", Dish_desc);
                in.putExtra("Tl", thudish);
                in.putExtra("TR", total_review_dish);
                in.putExtra("Dp", Dish_picture);
                in.putExtra("Nm", Name);
                in.putExtra("algf", Allergy_info);
                in.putExtra("Details", details);
                in.putExtra("UID",createdby);
                in.putExtra("dsid", Dish_id);
                in.putExtra("ARRAY",Ingradiant);
                in.putExtra("STAR",reviewstar);
                activity.startActivity(in);
                //CustomDetails adapter=new CustomDetails(search_dish,R.layout.spiner_item,Dish);


                // progressDialog.dismiss();

            } catch (Exception e) {

            }


            if (cResponse.equals("0")) {
                Toast.makeText(activity, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (cResponse.equals("1")) {

                // Toast.makeText(getActivity(), "You have successfully Posted !", Toast.LENGTH_LONG).show();

            } else {

            }
            viewDetailsclicked = false;
            cResponse = null;
            cResponsArr = null;
        }
    }
}
