package com.zdoof.stpl.zdoof.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zdoof.stpl.zdoof.activites.Home;
import com.zdoof.stpl.zdoof.R;

/**
 * Created by stpl on 19/5/16.
 */
public class CusAdapters5 extends BaseAdapter {
    Home my_experience;
    int spiner_item;
    String[] catgory;
    int[] img;
    public CusAdapters5(Home my_experience, int spiner_item, String[] catgory, int[] img) {
        this.my_experience=my_experience;

        this.catgory=catgory;
        this.img=img;

    }

    @Override
    public int getCount() {
        return catgory.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        LayoutInflater inflater = my_experience.getLayoutInflater();
        view = inflater.inflate(R.layout.customview, null);
        TextView text= (TextView) view.findViewById(R.id.textView16);
        ImageView image= (ImageView) view.findViewById(R.id.imageView25);
        text.setText(catgory[position]);
        text.setTextColor(Color.BLACK);
        image.setImageResource(img[position]);

        return view;
    }
}

