package com.zdoof.stpl.zdoof.activites;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.views.Contropanel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class AppSettings extends Activity {

    StringBuilder log;
    ToggleButton toggle;
    ImageView back;
    Boolean togglestate;
    LinearLayout l1,l2,l3,l4;
    SharedPreferenceClass sharedPreferenceClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_settings);
        //storing logs in appsettings.txt inside zdoof folder in mobile
        try {
            Process process = Runtime.getRuntime().exec("logcat -d");
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            log = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                log.append(line);
                log.append("\n");
            }
        } catch (IOException e) {
        }
        //convert log to string
        final String logString = new String(log.toString());

        //to create a Text file name "logcat.txt" in SDCard
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath() + "/zdoof");
        dir.mkdirs();
        File file = new File(dir, "appsettings.txt");

        try {
            //to write logcat in text file
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter osw = new OutputStreamWriter(fOut);

            // Write the string to the file
            osw.write(logString);
            osw.flush();
            osw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        sharedPreferenceClass = new SharedPreferenceClass(this);
        toggle = (ToggleButton) findViewById(R.id.logoutToggle);
        back = (ImageView) findViewById(R.id.appsetingBack);
        l1 = (LinearLayout) findViewById(R.id.settingsll1);
        l2 = (LinearLayout) findViewById(R.id.settingsll2);
        l3 = (LinearLayout) findViewById(R.id.settingsll3);
        l4 = (LinearLayout) findViewById(R.id.settingsll4);
        togglestate = sharedPreferenceClass.getValue_boolean("AUTO_LOGOUT");
        toggle.setChecked(togglestate);
        toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toggle.isChecked()) {
                    toggle.setChecked(true);
                    sharedPreferenceClass.setValue_boolean("AUTO_LOGOUT", true);
                } else {
                    toggle.setChecked(false);
                    sharedPreferenceClass.setValue_boolean("AUTO_LOGOUT", false);
                }
            }
        });

        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppSettings.this, Home.class);
                startActivity(intent);
                finish();
            }
        });

        l2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppSettings.this, Connecton.class);
                startActivity(intent);
                finish();
            }
        });

        l3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppSettings.this, Notifications.class);
                startActivity(intent);
                finish();
            }
        });

        l4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AppSettings.this, Contropanel.class);
                startActivity(intent);
                finish();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        String back = sharedPreferenceClass.getValue_string("BACKPAGE");
        if (back.equals("controlpanel")) {
            Intent intent = new Intent(AppSettings.this, Contropanel.class);
            sharedPreferenceClass.setValue_string("BACKPAGE", "");
            startActivity(intent);
            finish();
        }
        else {
            super.onBackPressed();
        }
    }
}
