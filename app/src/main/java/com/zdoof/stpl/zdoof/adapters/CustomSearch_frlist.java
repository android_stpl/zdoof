package com.zdoof.stpl.zdoof.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.activites.PostListActivity;

/**
 * Created by stpl on 9/5/16.
 */
public class CustomSearch_frlist extends BaseAdapter{
    PostListActivity postList;
    String[] sertype;
    int[] sercimg;
    public CustomSearch_frlist(PostListActivity postList, int spiner_item, String[] sertype, int[] sercimg) {
      this.postList=postList;
      this.sertype=sertype;
      this.sercimg=sercimg;
    }

    @Override
    public int getCount() {
        return sertype.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = postList.getLayoutInflater();
        view = inflater.inflate(R.layout.custom_serdesign, null);
        ImageView image= (ImageView) view.findViewById(R.id.imageView29);
        TextView text= (TextView) view.findViewById(R.id.textView27);
        image.setImageResource(sercimg[position]);
        text.setText(sertype[position]);
        return view;
    }
}
