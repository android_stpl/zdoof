package com.zdoof.stpl.zdoof.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.activites.Profiles;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.adapters.CustomZlikes;
import com.zdoof.stpl.zdoof.utils.AppUtil;
import com.zdoof.stpl.zdoof.utils.ImageListener;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall1;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Ratan on 7/29/2015.
 */
public class Zlikes extends Fragment {

    ListView list;
    SharedPreferenceClass shared;
    String rid,First_name,zfrmdt="",ztodt="";
    int Regd_id,zliketype=0;
    static String aResponse;
    final WebserviceCall1 com1 = new WebserviceCall1();
    ArrayList<Detail>detl;
    /*String[]values={"Ujal last",};*/
    ProgressDialog progressDialog;
    ImageListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (ImageListener) ((Activity) context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.updates_layout,null);
        list= (ListView) rootView.findViewById(R.id.listView2);
        shared= new SharedPreferenceClass(getActivity());
        rid = shared.getValue_string("UDDIDD").toString();
        Regd_id=Integer.parseInt(rid);
        First_name="";
       // progressDialog = ProgressDialog.show(getActivity(), "", "Loading...");


        return rootView;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {

            // If we are becoming invisible, then...
            AsyncCallWS task = new AsyncCallWS();
            task.execute();
            if (!isVisibleToUser) {
                Log.d("MyFragment", "Not visible anymore.  Stopping audio.");
                // TODO stop audio playback
            }
        }
    }
    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressDialog = ProgressDialog.show(getActivity(), "", "Loading...");
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            aResponse = com1.Zlikes("user_zlike",zliketype,zfrmdt,Regd_id,ztodt);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            //progressDialog.dismiss();
            try {
                JSONArray jr = new JSONArray(aResponse);
                detl = new ArrayList<Detail>();
                for (int i = 0; i < jr.length(); i++) {
                    Detail productdto = new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);
                    String likedto=jsonObject.getString("Like_to").toString();
                    String type=jsonObject.getString("typeimg").toString();
                    String likedonn=jsonObject.getString("Created_on").toString();
                    //likedonn = AppUtil.getDateAndTimeFromTimestamp(getActivity(), likedonn);
                    likedonn = convertTime(likedonn);
                    productdto.setLikedto(likedto);
                    productdto.setType(type);
                    productdto.setLikedon(likedonn);


                    detl.add(productdto);
                    CustomZlikes adapter=new CustomZlikes(getActivity(),R.layout.spiner_item,detl);
                    list.setAdapter(adapter);

                }


            }catch (Exception e){

            }

            if (aResponse.equals("0")) {
                Toast.makeText(getActivity(), "No zLikes Availabe", Toast.LENGTH_LONG).show();

            } else if (aResponse.equals("1")) {
                Toast.makeText(getActivity(), "You have successfully Registered !", Toast.LENGTH_LONG).show();

            } else {

               /* Intent intent=new Intent(Profiles.this,Connecton.class);
                startActivity(intent);
                finish();*/
            }

        }

        private String convertTime(String jTime) {
            String returnString = null;
            returnString = jTime.substring(6, jTime.length()-2);
            long epoch = Long.parseLong(returnString);
            //returnString = new java.text.SimpleDateFormat("dd/MMM/yyyy 'at' HH:mm:ss aaa").format(new java.util.Date (epoch));
            Date date = new Date(epoch-5400000);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy 'at' HH:mm:ss aaa");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            returnString = sdf.format(date);
            return returnString;
        }

    }
}
