package com.zdoof.stpl.zdoof.search;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.activites.Connecton;
import com.zdoof.stpl.zdoof.views.Contropanel;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.activites.Home;
import com.zdoof.stpl.zdoof.activites.Notifications;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall2;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by stpl on 13/3/16.
 */
public class Search_all extends Activity implements LocationListener {
    ImageView all, post, grocery, resturant, food, serc;
    ImageView close, home, connect, noti, icon, contlpnl,serch_close;
   // ImageView active1, active2, act3, act4, act5, act6, act7;
    private FloatingActionButton fab;
    ListView list, list1, list2, list3, list4;
    LinearLayout ll1, ll2, ll3, ll4;
    ProgressDialog progressDialog;
    ArrayList<Detail> Values;
    final WebserviceCall2 com2 = new WebserviceCall2();
    static String aResponse;
    String[] bresponce = {};
    String restplaceid = "", groceid = "",zipcode;
    String primg, utype, name,searchvalue;
    static String result;
    SharedPreferenceClass sharedPreferenceClass;
    String desc = "", loATION,des;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    String rest, dish, pep, gros, posts;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    Dialog dialog;
    EditText descc;
    ImageView search;
    ImageView rch,serch;
    String pos,profile_image;
    ListView listt;
    TextView textt;
    LinearLayout ziploc;
    LinearLayout vieew;
    EditText zips;
    Button submit;
    TextView code;


    String sertype[]={"Search All","Search Post","Search Receipe/Dish","Search Resturant","Search Grocery Store","Search Foodie"};
    int sercimg[]={R.drawable.searhicon,R.drawable.sst,R.drawable.ff,R.drawable.sahlist,R.drawable.crt,R.drawable.uur};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_peopl);
        listt= (ListView) findViewById(R.id.li);
        list = (ListView) findViewById(R.id.listviewf);
        descc= (EditText) findViewById(R.id.imageView11);
       /* serc = (ImageView) findViewById(R.id.serc);
        all = (ImageView) findViewById(R.id.image5);
        post = (ImageView) findViewById(R.id.imageVie);
        grocery = (ImageView) findViewById(R.id.grocery);
        resturant = (ImageView) findViewById(R.id.imageViwes);*/
       /* active1 = (ImageView) findViewById(R.id.act1);
        active2 = (ImageView) findViewById(R.id.act2);
        act3 = (ImageView) findViewById(R.id.act3);
        act4 = (ImageView) findViewById(R.id.act4);
        act5 = (ImageView) findViewById(R.id.act5);
        act6 = (ImageView) findViewById(R.id.act6);
        act7 = (ImageView) findViewById(R.id.act7);*/
        serch_close= (ImageView) findViewById(R.id.close);
        serch= (ImageView) findViewById(R.id.serc);
        food = (ImageView) findViewById(R.id.imageVies);
        home = (ImageView) findViewById(R.id.hme);
        ll1 = (LinearLayout) findViewById(R.id.hmj);
        ll2 = (LinearLayout) findViewById(R.id.cnn);
        ll3 = (LinearLayout) findViewById(R.id.ntf);
        ll4 = (LinearLayout) findViewById(R.id.ct);
        home = (ImageView) findViewById(R.id.hme);
        connect = (ImageView) findViewById(R.id.hm);
        rch= (ImageView) findViewById(R.id.sec);
       // icon = (ImageView) findViewById(R.id.icon);
        contlpnl = (ImageView) findViewById(R.id.ctrl);
        list1 = (ListView) findViewById(R.id.listview2);
        list2 = (ListView) findViewById(R.id.listview3);
        list3 = (ListView) findViewById(R.id.listview4);
        list4 = (ListView) findViewById(R.id.listview5);
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header1 = (ViewGroup)inflater.inflate(R.layout.search_view, listt, false);
        listt.addHeaderView(header1, null, false);
        textt= (TextView) findViewById(R.id.textt);
        ziploc= (LinearLayout) findViewById(R.id.ziploc);
        vieew= (LinearLayout) findViewById(R.id.view);
        zips= (EditText) findViewById(R.id.zip);
        submit= (Button) findViewById(R.id.submit);
        code= (TextView) findViewById(R.id.code);
        sharedPreferenceClass=new SharedPreferenceClass(this);

       /* fab = (FloatingActionButton) findViewById(R.id.fab);*/
        try {
            desc = getIntent().getStringExtra("DES").toString();
            descc.setText(desc);
            if(desc.equals("")){
                serch_close.setVisibility(View.GONE);
            }
            else if(desc.equals("0")){
                serch_close.setVisibility(View.GONE);
            }else {
                serch_close.setVisibility(View.VISIBLE);
                sharedPreferenceClass.setValue_string("SEARCHVALUE",desc);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
      /*  LocationManager locationManager = (LocationManager)
                getSystemService(LOCATION_SERVICE);

        // getting GPS status
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
        isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!isGPSEnabled && !isNetworkEnabled) {
            // no network provider is enabled
        } else {
            this.canGetLocation = true;
            if (isNetworkEnabled) {

                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                        if (location != null) {

                            Geocoder gcd = new Geocoder(this, Locale.getDefault());
                            List<Address> list = null;
                            try {
                                list = gcd.getFromLocation(location
                                        .getLatitude(), location.getLongitude(), 1);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if (list != null & list.size() > 0) {
                                Address address = list.get(0);
                                result = address.getLocality();
                                // location1.setText("Checking in to"+ " " + result +"?");
   *//* // Setting latitude and longitude in the TextView tv_location
    location.setText("Latitude:" +  latitude  + ", Longitude:"+ longitude );*//*
                            }
                        }
                    }

                    // if GPS Enabled get lat/long using GPS Services
                    if (isGPSEnabled) {
                        if (location == null) {
                            locationManager.requestLocationUpdates(
                                    LocationManager.GPS_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                            Log.d("GPS Enabled", "GPS Enabled");
                            if (locationManager != null) {
                                location = locationManager
                                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                if (location != null) {
                                    Geocoder gcd = new Geocoder(this, Locale.getDefault());
                                    List<Address> list = null;
                                    try {
                                        list = gcd.getFromLocation(location
                                                .getLatitude(), location.getLongitude(), 1);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    if (list != null & list.size() > 0) {
                                        Address address = list.get(0);
                                        result = address.getLocality();
                                        if (address.getPostalCode() != null) {
                                            zipcode = address.getPostalCode();
                                        }
                                        // location1.setText("Checking in to"+ " " + result +"?");
                                    }
                                }
                            }
                        }
                    }
                    return;
                }

                loATION = result;
*/
        LocationManager locationManager = (LocationManager)
                Search_all.this.getSystemService(LOCATION_SERVICE);
        ActivityCompat.requestPermissions(Search_all.this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);
        // getting GPS status
        isGPSEnabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // getting network status
        isNetworkEnabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);


        if (!isGPSEnabled && !isNetworkEnabled) {
            // no network provider is enabled
        } else {
            this.canGetLocation = true;

            if (isNetworkEnabled) {
                // checkLocationPermission();
                if (ActivityCompat.checkSelfPermission(Search_all.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(Search_all.this,
                                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                        if (location != null) {

                            Geocoder gcd = new Geocoder(Search_all.this, Locale.getDefault());
                            List<Address> list = null;
                            try {
                                list = gcd.getFromLocation(location
                                        .getLatitude(), location.getLongitude(), 1);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if (list != null & list.size() > 0) {
                                Address address = list.get(0);
                                result = address.getLocality();
                                if (address.getPostalCode() != null) {
                                    zipcode = address.getPostalCode();
                                }
                               // locaqtions = result;
                                loATION = result;
                                //location1.setText("Checking in to" + " " + result + "?");
   /* // Setting latitude and longitude in the TextView tv_location
    location.setText("Latitude:" +  latitude  + ", Longitude:"+ longitude );*/
                            }
                        }
                    }

                }


                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        //checkLocationPermission();
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                Geocoder gcd = new Geocoder(Search_all.this, Locale.getDefault());
                                List<Address> list = null;
                                try {
                                    list = gcd.getFromLocation(location
                                            .getLatitude(), location.getLongitude(), 1);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                if (list != null & list.size() > 0) {
                                    Address address = list.get(0);
                                    result = address.getLocality();
                                    if (address.getPostalCode() != null) {
                                        zipcode = address.getPostalCode();
                                    }

                                }
                            }
                        }
                    }
                }
                // postcomment = zipcode;
            }
        }
                code.setText(zipcode);
                submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String zipcodevalue=zips.getText().toString();
                        code.setText(zipcodevalue);
                        ziploc.setVisibility(View.GONE);
                        vieew.setVisibility(View.GONE);
                    }
                });

               // active1.setVisibility(View.VISIBLE);
                progressDialog = ProgressDialog.show(Search_all.this, "", "Loading...");
                AsyncCallWS task = new AsyncCallWS();
                task.execute();

               /* fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                      /*  CustomSearch7 adapter = new CustomSearch7(Search_all.this, R.layout.spiner_item, sertype, sercimg);
                        list.setAdapter(adapter);

                        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                pos = list.getItemAtPosition(position).toString();
                                if (pos.equals("0")) {

                                    AsyncCallWS task = new AsyncCallWS();
                                    task.execute();
                                } else if (pos.equals("1")) {
                                    Intent intent = new Intent(Search_all.this, Search_post.class);
                                    startActivity(intent);
                                    finish();
                                } else if (pos.equals("2")) {
                                    Intent intent = new Intent(Search_all.this, Search_dish.class);
                                    startActivity(intent);
                                    finish();
                                } else if (pos.equals("3")) {
                                    Intent intent = new Intent(Search_all.this, Search_resturant.class);
                                    startActivity(intent);
                                    finish();
                                } else if (pos.equals("4")) {
                                    Intent intent = new Intent(Search_all.this, Search_gstore.class);
                                    startActivity(intent);
                                    finish();
                                } else if (pos.equals("5")) {
                                    Intent intent = new Intent(Search_all.this, Search_people.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        });*/
                     /*  serc.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                desc = descc.getText().toString().trim();
                                AsyncCallWS task = new AsyncCallWS();
                                task.execute();
                                dialog.dismiss();
                            }
                        });
                        descc.setOnKeyListener(new View.OnKeyListener() {
                            public boolean onKey(View v, int keyCode, KeyEvent event) {
                                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                                    switch (keyCode) {
                                        case KeyEvent.KEYCODE_DPAD_CENTER:
                                        case KeyEvent.KEYCODE_ENTER:
                                            desc = descc.getText().toString().trim();

                                            AsyncCallWS task = new AsyncCallWS();
                                            task.execute();
                                            dialog.dismiss();
                                            return true;
                                        default:
                                            break;
                                    }
                                }
                                return false;
                            }
                        });
                        dialog.show();
                    }
                });
*/
                           /* alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }*/

              /*  icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finish();

                    }
                });*/
                textt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ziploc.setVisibility(View.VISIBLE);
                        vieew.setVisibility(View.VISIBLE);
                    }
                });

                serch_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        descc.setText("");
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", "0");
                        serch_close.setVisibility(View.GONE);
                    }
                });
                ll2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // sharedPreferenceClass.setValue_string("SEARCHVALUE",desc);
                        ll2.setBackgroundColor(Color.parseColor("#5ce480"));
                        Intent intent = new Intent(Search_all.this, Connecton.class);
                        // intent.putExtra("EML",det);
                        startActivity(intent);
                        finish();

                    }
                });

                ll3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // ll2.setBackgroundColor(Color.parseColor("#5ce480"));
                        //sharedPreferenceClass.setValue_string("SEARCHVALUE",desc);
                        Intent intent = new Intent(Search_all.this, Notifications.class);
                        // intent.putExtra("EML",det);
                        startActivity(intent);
                        finish();

                    }
                });
                ll4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ll4.setBackgroundColor(Color.parseColor("#5ce480"));
                        Intent intent = new Intent(Search_all.this, Contropanel.class);
                        startActivity(intent);
                        finish();
                    }
                });
                ll1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       // sharedPreferenceClass.setValue_string("SEARCHVALUE",desc);
                        ll1.setBackgroundColor(Color.parseColor("#5ce480"));
                        Intent intent = new Intent(Search_all.this, Home.class);
                        startActivity(intent);
                        finish();
                    }
                });
                rch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(listt.getVisibility()== View.VISIBLE) {
                            listt.setVisibility(View.GONE);
                        }else {
                            listt.setVisibility(View.VISIBLE);
                            CustomSearch7 adapter = new CustomSearch7(Search_all.this, R.layout.spiner_item, sertype, sercimg);
                            listt.setAdapter(adapter);

                            listt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    pos = listt.getItemAtPosition(position).toString();
                                    if (pos.equals("0")) {
                                        Intent intent = new Intent(Search_all.this, Search_all.class);
                                        startActivity(intent);
                                        finish();

                                    } else if (pos.equals("1")) {
                                        Intent intent = new Intent(Search_all.this, Search_post.class);
                                        startActivity(intent);
                                        finish();
                                    } else if (pos.equals("2")) {
                                        desc = descc.getText().toString().trim();
                                        descc.setText(desc);
                                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                                        Intent intent = new Intent(Search_all.this, Search_dish.class);
                                        startActivity(intent);
                                        finish();
                                    } else if (pos.equals("3")) {
                                        Intent intent = new Intent(Search_all.this, Search_resturant.class);
                                        startActivity(intent);
                                        finish();
                                    } else if (pos.equals("4")) {
                                        Intent intent = new Intent(Search_all.this, Search_gstore.class);
                                        startActivity(intent);
                                        finish();
                                    } else if (pos.equals("5")) {
                                        desc = descc.getText().toString().trim();
                                        descc.setText(desc);
                                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                                        Intent intent = new Intent(Search_all.this, Search_people.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            });

                        }
                    }
                });
                /*grocery.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        act5.setVisibility(View.VISIBLE);
                        act4.setVisibility(View.GONE);
                        act3.setVisibility(View.GONE);
                        active2.setVisibility(View.GONE);
                        active1.setVisibility(View.GONE);
                        act6.setVisibility(View.GONE);

                        Intent intent = new Intent(Search_all.this, Search_gstore.class);
                        startActivity(intent);
                        finish();
                    }
                });
                all.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        act6.setVisibility(View.VISIBLE);
                        act5.setVisibility(View.GONE);
                        act4.setVisibility(View.GONE);
                        act3.setVisibility(View.GONE);
                        active2.setVisibility(View.GONE);
                        active1.setVisibility(View.GONE);
                        active1.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(Search_all.this, Search_people.class);
                        startActivity(intent);
                        finish();
                    }
                });
                food.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        act3.setVisibility(View.VISIBLE);
                        active2.setVisibility(View.GONE);
                        active1.setVisibility(View.GONE);
                        act6.setVisibility(View.GONE);
                        act5.setVisibility(View.GONE);
                        act4.setVisibility(View.GONE);
                        Intent intent = new Intent(Search_all.this, Search_dish.class);
                        startActivity(intent);
                        finish();
                    }
                });
                resturant.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        act4.setVisibility(View.VISIBLE);
                        act3.setVisibility(View.GONE);
                        active2.setVisibility(View.GONE);
                        active1.setVisibility(View.GONE);
                        act6.setVisibility(View.GONE);
                        act5.setVisibility(View.GONE);
                        Intent intent = new Intent(Search_all.this, Search_resturant.class);
                        startActivity(intent);
                        finish();
                    }
                });
                post.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        active2.setVisibility(View.VISIBLE);
                        active1.setVisibility(View.GONE);
                        act3.setVisibility(View.GONE);
                        act4.setVisibility(View.GONE);
                        act6.setVisibility(View.GONE);
                        act5.setVisibility(View.GONE);
                        Intent intent = new Intent(Search_all.this, Search_post.class);
                        startActivity(intent);
                        finish();
                    }
                });
                serc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Search_all.this, Search_all.class);
                        startActivity(intent);
                        finish();
                    }
                });
*/

                serch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //description.setCursorVisible(true);
                        des = descc.getText().toString().trim();
                        Intent intent = new Intent(Search_all.this, Search_all.class);
                        intent.putExtra("DES", des);
                        startActivity(intent);

                    }

          /*  @Override
            public boolean onTouch(View v, MotionEvent event) {

                description.setEnabled(false);

                des = description.getText().toString().trim();
                //fullparentscrolling.setEnabled(false);
                Intent intent = new Intent(Home.this, Search_all.class);
                intent.putExtra("DES", des);
                startActivity(intent);
                return true;
            }*/
                });
            }


            @Override
            public void onLocationChanged (Location location){

            }

            @Override
            public void onStatusChanged (String provider,int status, Bundle extras){

            }

            @Override
            public void onProviderEnabled (String provider){

            }

            @Override
            public void onProviderDisabled (String provider){

            }
    @Override
    public void onBackPressed() {
        descc.setText("");
        sharedPreferenceClass.setValue_string("SEARCHVALUE", "0");
        serch_close.setVisibility(View.GONE);

    }
            class AsyncCallWS extends AsyncTask<Void, Void, Void> {
                @Override
                protected Void doInBackground(Void... params) {
                    Log.i("TAG", "doInBackground");

                            aResponse = com2.Serch_All("search_all", desc, loATION, restplaceid, groceid);
                          bresponce=aResponse.split("\\$");
                    return null;
                }

                @Override
                protected void onPostExecute(Void result) {
                    Log.i("TAG", "onPostExecute");
                    try {

                        dish=bresponce[0];
                        rest=bresponce[1];
                        gros=bresponce[2];
                        pep=bresponce[3];
                        posts=bresponce[4];
                        if (posts.equals("0")) {
                           // progressDialog.dismiss();
                            list.setVisibility(View.GONE);

                        } else {
                            JSONArray jr = new JSONArray(posts);

                            Values = new ArrayList<Detail>();

                            for (int i = 0; i < jr.length(); i++) {

                                Detail productdto = new Detail();
                                JSONObject jsonObject = jr.getJSONObject(i);

                                String post_image = jsonObject.getString("mid_img").toString();
                                String postdetails = jsonObject.getString("Details").toString();
                                String creaton = jsonObject.getString("Created_on").toString();
                                String postname = jsonObject.getString("Name").toString();
                                String profile_image = jsonObject.getString("profimg").toString();

                                productdto.setPostimage(post_image);
                                productdto.setPostdetails(postdetails);
                                productdto.setPostname(postname);
                                productdto.setPcreaton(creaton);
                                productdto.setPfimage(profile_image);
                                Values.add(productdto);
                                list.setVisibility(View.VISIBLE);

                                CustomSercall4 adapter = new CustomSercall4(Search_all.this, R.layout.spiner_item, Values);
                                list.setAdapter(adapter);
                                setListViewHeightBasedOnItems(list);
                                progressDialog.dismiss();
                   /* grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        }
                    });*/
                                // progressDialog.dismiss();

                                // progressDialog.dismiss();
                            }
                        }

                    }catch (Exception e){

                    }


                    if (aResponse.equals("0")) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Sorry try again!", Toast.LENGTH_LONG).show();

                    } else if (aResponse.equals("1")) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "You have successfully Registered !", Toast.LENGTH_LONG).show();
              /*  Intent i = new Intent(SignUp.this, Home.class);
                startActivity(i);
                // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                //save auth key
                finish();*/
                    } else {
                        progressDialog.dismiss();
                        //Toast.makeText(getApplicationContext(), "NO data Availabel!", Toast.LENGTH_SHORT).show();
                        //sharedPreferenceClass.setValue_boolean("Loginstatus", true);


                    }
                    try {
                        if (dish.equals("0")) {
                            //progressDialog.dismiss();
                            list1.setVisibility(View.GONE);

                        } else {

                            JSONArray jr = new JSONArray(dish);

                            Values = new ArrayList<Detail>();

                            for (int i = 0; i < jr.length(); i++) {

                                Detail productdto = new Detail();
                                JSONObject jsonObject = jr.getJSONObject(i);
                                String dishname = jsonObject.getString("Dish_name").toString();
                                String dishpic = jsonObject.getString("Dish_picture").toString();
                                // username = jsonObject.getString("User_name").toString();
                                String review = jsonObject.getString("totalreview").toString();
                                String likes = jsonObject.getString("thudish").toString();
                                String ratings = jsonObject.getString("reviewrating").toString();


                                productdto.setDish_name(dishname);
                                productdto.setDish_pic(dishpic);
                                productdto.setReviews(review);
                                productdto.setZlikes(likes);
                                productdto.setRatings(ratings);

                                Values.add(productdto);
                                list1.setVisibility(View.VISIBLE);

                                CustomSercall adapter = new CustomSercall(Search_all.this, R.layout.spiner_item, Values);
                                list1.setAdapter(adapter);
                                setListViewHeightBasedOnItems(list1);
                                progressDialog.dismiss();

                            }
                        }

                    } catch (Exception e) {

                    }
                    if (aResponse.equals("0")) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Sorry try again!", Toast.LENGTH_LONG).show();
                       progressDialog.dismiss();
                    } else if (aResponse.equals("1")) {
                        Toast.makeText(getApplicationContext(), "You have successfully Registered !", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();

                    } else {

                    }try{
                       if (rest.equals("0")) {
                          // progressDialog.dismiss();
                           list2.setVisibility(View.GONE);
                       }


                         else {

                    JSONArray jrr = new JSONArray(rest);

                    Values = new ArrayList<Detail>();

                    for (int i = 0; i < jrr.length(); i++) {

                        Detail productdto = new Detail();
                        JSONObject jsonObject = jrr.getJSONObject(i);

                        String resurant_name = jsonObject.getString("Restaurant_name").toString();
                        String rest_address = jsonObject.getString("Address_line1").toString();
                        // username = jsonObject.getString("User_name").toString();
                        String rest_id = jsonObject.getString("Restaurant_id").toString();
                        String resturant_img = jsonObject.getString("Restaurant_Image").toString();
                        String rest_phone = jsonObject.getString("Restaurant_phone").toString();
                        productdto.setRestaurant_name(resurant_name);
                        productdto.setRest_img(resturant_img);
                        productdto.setRest_address(rest_address);
                        productdto.setRest_id(rest_id);
                        productdto.setRest_phone(rest_phone);


                        Values.add(productdto);

                        list2.setVisibility(View.VISIBLE);
                        CustomSercall1 adapter = new CustomSercall1(Search_all.this, R.layout.spiner_item, Values);
                        list2.setAdapter(adapter);
                        setListViewHeightBasedOnItems(list2);
/*
                            Customres adapter1=new Customres(Search_all.this, R.layout.spiner_item,Values);
                            list1.setAdapter(adapter1);*/
                        progressDialog.dismiss();


                    }
                    }

                } catch (Exception e) {

                }



                if (aResponse.equals("0")) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Sorry try again!", Toast.LENGTH_LONG).show();
                    //progressDialog.dismiss();
                } else if (aResponse.equals("1")) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "You have successfully Registered !", Toast.LENGTH_LONG).show();
                   // progressDialog.dismiss();

                } else {
                    progressDialog.dismiss();


                }try{
                        if (gros.equals("0")) {
                            //progressDialog.dismiss();
                            list3.setVisibility(View.GONE);

                        }

                        else {
                        JSONArray jrr = new JSONArray(gros);

                        Values = new ArrayList<Detail>();

                        for (int i = 0; i < jrr.length(); i++) {

                            Detail productdto = new Detail();
                            JSONObject jsonObject = jrr.getJSONObject(i);

                            String Gstore_name = jsonObject.getString("Gstore_name").toString();
                            String Gstore_address = jsonObject.getString("Address_line1").toString();
                            // username = jsonObject.getString("User_name").toString();
                            String Gsrore_id = jsonObject.getString("Gstore_id").toString();
                            String Gstore_image = jsonObject.getString("Gstore_Image").toString();
                            String Gstore_phone = jsonObject.getString("Gstore_phone").toString();

                            productdto.setGstore_name(Gstore_name);
                            productdto.setGstore_img(Gstore_image);
                            productdto.setGstore_address(Gstore_address);
                            productdto.setGstore_id(Gsrore_id);
                            productdto.setGstore_phone(Gstore_phone);

                            Values.add(productdto);
                            list3.setVisibility(View.VISIBLE);

                            CustomSercall2 adapter = new CustomSercall2(Search_all.this, R.layout.spiner_item, Values);
                            list3.setAdapter(adapter);
                            setListViewHeightBasedOnItems(list3);
                           progressDialog.dismiss();


                       }   // progressDialog.dismiss();
                        }

                    } catch (Exception e) {

                    }



                    if (aResponse.equals("0")) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Sorry try again!", Toast.LENGTH_LONG).show();
                       // progressDialog.dismiss();
                    } else if (aResponse.equals("1")) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "You have successfully Registered !", Toast.LENGTH_LONG).show();
                       // progressDialog.dismiss();

                    } else {
                        progressDialog.dismiss();


                    }
                    try {
                        if (pep.equals("0")) {
                        list4.setVisibility(View.GONE);

                        } else {
                            JSONArray jrr = new JSONArray(pep);

                            Values = new ArrayList<Detail>();

                            for (int i = 0; i < jrr.length(); i++) {

                                Detail productdto = new Detail();
                                JSONObject jsonObject = jrr.getJSONObject(i);

                                String User_id = jsonObject.getString("User_id").toString();
                                String Upf_img = jsonObject.getString("Profile_image").toString();
                                String U_Name=jsonObject.getString("name").toString();
                                String Uzlike=jsonObject.getString("thuuser").toString();
                                String Usertype=jsonObject.getString("UTYP").toString();
                                productdto.setU_Name(U_Name);
                                productdto.setUser_type(Usertype);
                                productdto.setUser_Zlike(Uzlike);
                                productdto.setUserpfimg(Upf_img);
                                productdto.setUser_id(User_id);

                                Values.add(productdto);
                                list4.setVisibility(View.VISIBLE);
                                list.setVisibility(View.GONE);
                                list1.setVisibility(View.GONE);
                                list2.setVisibility(View.GONE);
                                list3.setVisibility(View.GONE);

                                CustomSercall3 adapter = new CustomSercall3(Search_all.this, R.layout.spiner_item, Values);
                                list4.setAdapter(adapter);
                                setListViewHeightBasedOnItems(list4);
                                progressDialog.dismiss();
                            }

                           }

                        }catch(Exception e){

                        }


                    if (aResponse.equals("0")) {
                        Toast.makeText(getApplicationContext(), "Sorry try again!", Toast.LENGTH_LONG).show();
                       // progressDialog.dismiss();
                    } else if (aResponse.equals("1")) {
                        Toast.makeText(getApplicationContext(), "You have successfully Registered !", Toast.LENGTH_SHORT).show();
                       // progressDialog.dismiss();

                    } else {

                        //sharedPreferenceClass.setValue_boolean("Loginstatus", true);

                        progressDialog.dismiss();

                    }
                }
                public  boolean setListViewHeightBasedOnItems(ListView cmntlist) {

                    ListAdapter listAdapter = cmntlist.getAdapter();
                    if (listAdapter != null) {

                        int numberOfItems = listAdapter.getCount();

                        // Get total height of all items.
                        int totalItemsHeight = 0;
                        for (int itemPos = 0; itemPos < numberOfItems;itemPos++) {
                            View item = listAdapter.getView(itemPos, null, cmntlist);
                            item.measure(0, 0);
                            totalItemsHeight += item.getMeasuredHeight();
                        }

                        // Get total height of all item dividers.
                        int totalDividersHeight = cmntlist.getDividerHeight() *
                                (numberOfItems - 1);

                        // Set list height.
                        ViewGroup.LayoutParams params = cmntlist.getLayoutParams();
                        params.height = totalItemsHeight + totalDividersHeight;
                        cmntlist.setLayoutParams(params);
                        cmntlist.requestLayout();
                        return true;

                    } else {
                        return false;
                    }

                }

            }
        }


