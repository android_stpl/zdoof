package com.zdoof.stpl.zdoof.activites;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.utils.AppUtil;
import com.zdoof.stpl.zdoof.views.Contropanel;
import com.zdoof.stpl.zdoof.adapters.Customconection;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.searchadapters.CustomSearch1;
import com.zdoof.stpl.zdoof.database.DatabaseHelper;
import com.zdoof.stpl.zdoof.search.Search_dish;
import com.zdoof.stpl.zdoof.search.Search_gstore;
import com.zdoof.stpl.zdoof.search.Search_people;
import com.zdoof.stpl.zdoof.search.Search_post;
import com.zdoof.stpl.zdoof.search.Search_resturant;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall1;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by stpl on 23/1/16.
 */
public class Connecton extends Activity implements LocationListener{
    ImageView home, connection, notification, contlpnl,bk;
    SharedPreferenceClass sharedPreferenceClass;
    ImageView propic, back,close;
    ImageView icon;
    ImageView serch;
    //DatabaseHelper helper;
    LinearLayout ll1,ll2,ll3,ll4;
    ListView list,drawerList;
    StringBuilder log;


    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;
    LinearLayout headerview,ziploc,vieew;
    // flag for GPS status
    boolean canGetLocation = false;
    TextView Share;
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    // private static final long MY_PERMISSION_LOCATION = 10;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    ImageView addbutton;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    private final int LOCATION_PERMISSION_CODE = 1;

   // String[]option={"Log Out"};
    String name="",proflpic,pic,urid,desc;
    ArrayList<Detail>detl;
    String useriddd;
    private ProgressDialog progressDialog;
    String uid,emailid="",wrkas,gender,contry,city,img,nn,tc;
    int  riid1,Regd_id,Sub_user_type=0, Main_user_type=0,totalcount=0;
    public static String bResponse,dResponse;
    final WebserviceCall1 com1 = new WebserviceCall1();
    static String aResponse;
    EditText description,zips;
    TextView textt,code;
    Button submit;
    int countt;
    String rid,totalconnection,result,zipcode;
    String First_name, Last_name, Createdon, Updated_on, Ip_address,locations;
    String email="",profileimg;
    final WebserviceCall com = new WebserviceCall();
    String connection_id="",from_user_id="",to_user_id="",userid="",username="",
            main_user_type="",user_status="",first_name,last_name="",profileimage="",to_image="",
            utype="",utypimg="",created_onn="",thumbsup="",count="",raceipies="",from_user="",to_user="",
            from_id="";
    String pos,searchvalue;
    String sertype[]={"Search Post","Search Foodie","Search Recipe/Dish","Search Restaurant","Search Grocery Store"};
    int sercimg[]={R.drawable.searhicon,R.drawable.sst,R.drawable.ff,R.drawable.sahlist,R.drawable.crt,R.drawable.uur};
    android.support.v4.widget.DrawerLayout drawerMainSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connection);

        //helper=new DatabaseHelper(this);
        serch= (ImageView) findViewById(R.id.serc);
        description= (EditText) findViewById(R.id.imageView8);
        ll1= (LinearLayout) findViewById(R.id.hmj);
        ll2= (LinearLayout) findViewById(R.id.cnn);
        ll2.setBackgroundColor(Color.parseColor("#1D8168"));
        ll3=(LinearLayout) findViewById(R.id.ntf);
        ll4=(LinearLayout) findViewById(R.id.ct);
        drawerMainSearch = (DrawerLayout) findViewById(R.id.drawerMainSearch);
        list= (ListView) findViewById(R.id.cntction);
        close= (ImageView) findViewById(R.id.close);
        home = (ImageView) findViewById(R.id.hme);
        connection = (ImageView) findViewById(R.id.hm);
        notification = (ImageView) findViewById(R.id.cnct);
        contlpnl= (ImageView) findViewById(R.id.ctrl);
        icon=(ImageView) findViewById(R.id.optn);
        drawerList = (ListView) findViewById(R.id.drawerItemList);
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header1 = (ViewGroup)inflater.inflate(R.layout.search_view, drawerList, false);
        drawerList.addHeaderView(header1, null, false);
        CustomSearch1 drawerAdapter = new CustomSearch1(Connecton.this, R.layout.spiner_item, sertype, sercimg);
        drawerList.setAdapter(drawerAdapter);
        textt= (TextView) findViewById(R.id.textt);
        ziploc= (LinearLayout) findViewById(R.id.ziploc);
        vieew= (LinearLayout) findViewById(R.id.view);
        zips= (EditText) findViewById(R.id.zip);
        submit= (Button) findViewById(R.id.submit);
        code= (TextView) findViewById(R.id.code);
        sharedPreferenceClass=new SharedPreferenceClass(this);
        searchvalue=sharedPreferenceClass.getValue_string("SEARCHVALUE");
        if(searchvalue.equals("")){
            description.setText("");
            close.setVisibility(View.GONE);
        }else if(searchvalue.equals("0")){
            description.setText("");
            close.setVisibility(View.GONE);
        }
        else {
            description.setText(searchvalue);
            close.setVisibility(View.VISIBLE);
        }
        rid = sharedPreferenceClass.getValue_string("UIDD");
        Regd_id=Integer.parseInt(rid);

//        AsyncCallWS3 task1 = new AsyncCallWS3();
//        task1.execute();

        progressDialog = new ProgressDialog(Connecton.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCanceledOnTouchOutside(false);
       /*else{
        AsyncCallWS task=new AsyncCallWS();
        task.execute();

        progressDialog=ProgressDialog.show(Connecton.this,"","Loading...");
        }*/

      //  bk= (ImageView) findViewById(R.id.back);
       /* */

        name=sharedPreferenceClass.getValue_string("NM");
        proflpic=sharedPreferenceClass.getValue_string("PIC");
        First_name = "";
        Last_name = "";
        Createdon = "";
        Updated_on = "";
        Ip_address = "";

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zipcode =zips.getText().toString();
                code.setText(zipcode);
                sharedPreferenceClass.setValue_string("ZZIP", zipcode);
                ziploc.setVisibility(View.GONE);
                vieew.setVisibility(View.GONE);
            }
        });

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                desc = description.getText().toString().trim();
                if (desc.length()<3) {
                    Toast.makeText(Connecton.this, "Please enter atleast 3 letters to search!", Toast.LENGTH_SHORT).show();
                }
                else {
                    pos = drawerList.getItemAtPosition(position).toString();
                    if (pos.equals("0")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(Connecton.this, Search_post.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("1")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(Connecton.this, Search_people.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("2")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(Connecton.this, Search_dish.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("3")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(Connecton.this, Search_resturant.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("4")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(Connecton.this, Search_gstore.class);
                        startActivity(intent);
                        //finish();
                    }
                }
            }
        });
        textt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ziploc.setVisibility(View.VISIBLE);
                vieew.setVisibility(View.VISIBLE);
            }
        });
       /* icon.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               list1.setVisibility(View.VISIBLE);
               CusAdapter adapter=new CusAdapter(Connecton.this,R.layout.support_simple_spinner_dropdown_item,option);
               list1.setAdapter(adapter);
               list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                   @Override
                   public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                       sharedPreferenceClass.setValue_boolean("Loginstatus", false);
                       sharedPreferenceClass.clearData();
                       Intent intent=new Intent(Connecton.this,SignIn.class);
                       startActivity(intent);
                       list1.setVisibility(View.GONE);
                       finish();
                   }

               });

           }
       });*/

        //detl=new ArrayList<Detail>();
        //detl= (ArrayList<Detail>) getIntent().getSerializableExtra("EML");
       /* profpic=(ArrayList<String>) getIntent().getSerializableExtra("PIC");
        tcnt=(ArrayList<String>) getIntent().getSerializableExtra("TCT");
        userid=(ArrayList<String>) getIntent().getSerializableExtra("ID");*/
        //propic = (ImageView) findViewById(R.id.profilePictureView1);
       // CustomConnection
       // back = (ImageView) findViewById(R.id.back);

      /*  bk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Connecton.this, Home.class);
                startActivity(intent);
            }
        });*/

        ll1.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               /* ll2.setBackgroundColor(Color.parseColor("#3D3F63"));
                ll1.setBackgroundColor(Color.parseColor("#5B5F85"));*/
                Intent intent = new Intent(Connecton.this, Home.class);
                startActivity(intent);
                finish();
            }
        });
        ll3.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Connecton.this, Notifications.class);
                startActivity(intent);
                finish();
            }
        });
       ll4.setOnClickListener(new View.OnClickListener() {


           @Override
           public void onClick(View v) {
              /* ll2.setBackgroundColor(Color.parseColor("#3D3F63"));
               ll1.setBackgroundColor(Color.parseColor("#3D3F63"));
               ll4.setBackgroundColor(Color.parseColor("#5B5F85"));*/
               Intent intent = new Intent(Connecton.this, Contropanel.class);
               startActivity(intent);
               finish();
           }
       });
        /*back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Connecton.this, Home.class);
                startActivity(intent);
            }
        });*/
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setText("");
                sharedPreferenceClass.setValue_string("SEARCHVALUE","0");
                close.setVisibility(View.GONE);
            }
        });
        serch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawerMainSearch.isDrawerOpen(GravityCompat.END)) {
                    drawerMainSearch.openDrawer(GravityCompat.END);
                }
                else {
                    drawerMainSearch.closeDrawer(GravityCompat.END);
                }
//                desc = description.getText().toString();
//                if (desc.length()<3) {
//                    Toast.makeText(Connecton.this, "Please enter atleast 3 letters to search!", Toast.LENGTH_SHORT).show();
//                }
//                else {
//
//                }
            }
        });
        description.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId== EditorInfo.IME_ACTION_SEARCH) {
                    serch.performClick();
                    return true;
                }
                return false;
            }
        });
        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                drawerMainSearch.closeDrawer(GravityCompat.END);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setCursorVisible(true);
            }
        });

       /* propic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Connecton.this, Profiles.class);
                startActivity(intent);
            }
        });*/
        AsyncCallWS task=new AsyncCallWS();
        task.execute();
        //getZipcodeOfLocation();
        String z_zip = sharedPreferenceClass.getValue_string("ZZIP");
        if (!z_zip.equals(null) && !z_zip.equals("") && !(z_zip.length()<2)) {
            code.setText(z_zip);
            zipcode = z_zip;
        } else {
            if (AppUtil.GetLocationPermission(this)) {
                GetZipcodeTask zipTask = new GetZipcodeTask();
                zipTask.execute();
            }
            else {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CODE);
            }
        }
    }

    private class GetZipcodeTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... params) {
            getZipcodeOfLocation();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String zzip = sharedPreferenceClass.getValue_string("ZZIP");
            if (!zzip.equals(null) && !zzip.equals("") && !(zzip.length()<2)) {
                code.setText(zzip);
            }
            else {
                code.setText(zipcode);
                sharedPreferenceClass.setValue_string("ZZIP", zipcode);
            }
            zipcode = sharedPreferenceClass.getValue_string("ZZIP");
        }
    }

    private void getZipcodeOfLocation() {
        LocationManager manager = (LocationManager) Connecton.this.getSystemService(LOCATION_SERVICE);
        // getting GPS status
        isGPSEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        isNetworkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Geocoder geocoder = new Geocoder(Connecton.this,Locale.getDefault());
        if (
                ActivityCompat.checkSelfPermission(Connecton.this, Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(Connecton.this, Manifest.permission.ACCESS_COARSE_LOCATION)==PackageManager.PERMISSION_GRANTED) {
            if (isNetworkEnabled) {
                location = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location!=null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    try {
                        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 5);
                        if (addresses!=null) {
                            if (addresses.size()>0) {
                                for (int i=0; i<addresses.size(); i++){
                                    if (addresses.get(i).getPostalCode()!=null) {
                                        if (addresses.get(i).getPostalCode().length()>1) {
                                            zipcode = addresses.get(i).getPostalCode();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        else {
            ActivityCompat.requestPermissions(Connecton.this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_CODE) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //getZipcodeOfLocation();
                GetZipcodeTask zipcodeTask = new GetZipcodeTask();
                zipcodeTask.execute();
            } else {
                Toast.makeText(Connecton.this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        drawerMainSearch.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }


    @Override
    public void onBackPressed() {
        if (drawerMainSearch.isDrawerOpen(GravityCompat.END)) {
            drawerMainSearch.closeDrawer(GravityCompat.END);
        }
        else {
            description.setText("");
            sharedPreferenceClass.setValue_string("SEARCHVALUE","0");
            Intent intent = new Intent(Connecton.this, Home.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    class AsyncCallWS extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            aResponse = com1.Connection("my_connection", First_name, Regd_id);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            try {
                JSONArray jr = new JSONArray(aResponse);
                detl = new ArrayList<Detail>();
                for (int i = 0; i < jr.length(); i++) {
                    Detail productdto = new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);
                    userid = jsonObject.getString("User_id");
                    username = jsonObject.getString("User_name");
                    emailid = jsonObject.getString("Email");
                    name = jsonObject.getString("name");
                    profileimage = jsonObject.getString("Profile_image");
                    utype = jsonObject.getString("UTYP");
                    //count = jsonObject.getString("totcount");
                    //thumbsup = jsonObject.getString("thuuser");
                    //raceipies = jsonObject.getString("totrecipe");
                    /*helper.AddMyconnection(connection_id,from_user_id,
                            to_user_id,userid,username,main_user_type,user_status,emailid,first_name,
                            last_name,name,profileimage,to_image,utype,utypimg,
                            created_onn,thumbsup,count,raceipies,from_user,to_user,from_id);*/
                    productdto.setUserid(userid);
                    productdto.setEmail(emailid);
                    productdto.setName(name);
                    productdto.setPfimage(profileimage);
                    productdto.setUtype(utype);
                    //productdto.setTotalcount(count);
                    //productdto.setThumbsup(thumbsup);
                    //productdto.setRaceipies(raceipies);
                    detl.add(productdto);
                }
                Customconection adapter=new Customconection(Connecton.this,android.R.layout.simple_list_item_1,detl);
                list.setAdapter(adapter);

                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        img = detl.get(position).getPfimage();
                        nn = detl.get(position).getName();
                        tc = detl.get(position).getTotalcount();
                        urid = detl.get(position).getUserid();
                        riid1 = Integer.parseInt(urid);
                        // sharedPreferenceClass.setValue_boolean("JJ", true);
                        sharedPreferenceClass.setValue_boolean("Profiletask", false);

                        //sharedPreferenceClass.setValue_string("TC",tc);
                        Intent intent = new Intent(Connecton.this, Profiles.class);
                        intent.putExtra("USERID", urid);
                        intent.putExtra("VALUE",true);
                        startActivity(intent);


                    }
                });

            }catch (Exception e){
                    Log.e("Connection", "Connection api", e);
            }

            if (aResponse.equals("0")) {
                Toast.makeText(getApplicationContext(), "No data Available !", Toast.LENGTH_LONG).show();
                  progressDialog.dismiss();
            } else if (aResponse.equals("1")) {
                Toast.makeText(getApplicationContext(), "You have successfully Registered !", Toast.LENGTH_LONG).show();

            } else {

                Log.e("Connection", "Connection api");
            }
            progressDialog.dismiss();
        }

    }
   /* class AsyncCallWS1 extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            bResponse = com.Usersearch("user_search", riid1, First_name, Last_name, Main_user_type, Sub_user_type, Createdon, Updated_on, Ip_address);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            try {
                JSONArray jr = new JSONArray(bResponse);

                for (int i = 0; i < jr.length(); i++) {

                    JSONObject jsonObject = jr.getJSONObject(i);
                    useriddd = jsonObject.getString("User_id").toString();
                    // username = jsonObject.getString("User_name").toString();
                    totalconnection = jsonObject.getString("total_conn").toString();
                    name = jsonObject.getString("name").toString();
                    email = jsonObject.getString("User_name").toString();
                    wrkas = jsonObject.getString("Main_utype").toString();
                    profileimg = jsonObject.getString("profimg").toString();
                    contry = jsonObject.getString("Country").toString();
                    gender = jsonObject.getString("gendertext").toString();
                    city = jsonObject.getString("City").toString();

                    sharedPreferenceClass.setValue_string("UDDIDD", useriddd);
                    sharedPreferenceClass.setValue_string("WRK", wrkas);
                    sharedPreferenceClass.setValue_string("EMID", emailid);
                    sharedPreferenceClass.setValue_string("PHOTO", profileimg);
                    sharedPreferenceClass.setValue_string("GEN", gender);
                    sharedPreferenceClass.setValue_string("CON", contry);
                    sharedPreferenceClass.setValue_string("CTY", city);
                    // sharedPreferenceClass.setValue_string("UID",userid);
                    sharedPreferenceClass.setValue_string("LTN", name);
                    sharedPreferenceClass.setValue_string("TC", totalconnection);
                }

            } catch (Exception e) {

            }

            if (bResponse.equals("0")) {
                Toast.makeText(getApplicationContext(), "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (bResponse.equals("1")) {
                Toast.makeText(getApplicationContext(), "You have successfully Registered !", Toast.LENGTH_LONG).show();
             *//*  Intent i = new Intent(SignUp.this, Home.class);
                startActivity(i);
                // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                //save auth key
                finish();*//*
            } else {


            }

        }*/

        class AsyncCallWS3 extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... params) {
                Log.i("TAG", "doInBackground");

               dResponse = com1.connectioncount("total_connection",Regd_id);
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                Log.i("TAG", "onPostExecute");
                try {
                    JSONArray jr = new JSONArray(dResponse);
                    for (int i = 0; i < jr.length(); i++) {
                        JSONObject jsonObject = jr.getJSONObject(i);

                        totalconnection = jsonObject.getString("totcount");
                        totalcount = Integer.parseInt(totalconnection);
                        AsyncCallWS task=new AsyncCallWS();
                        task.execute();
                        if (totalcount > countt) {

                            // progressDialog = ProgressDialog.show(Home.this, "", "Loading...");
                        } else if (totalcount == countt) {
                            // progressDialog.dismiss();
                        }
                        // progressDialog.dismiss();
                    }

                } catch (Exception e) {

                }

                if (dResponse.equals("0")) {
                   // Toast.makeText(Connecton.this, "Sorry try again!", Toast.LENGTH_LONG).show();

                } else if (dResponse.equals("1")) {

                    Toast.makeText(Connecton.this, "You have successfully Registered !", Toast.LENGTH_LONG).show();

                } else {

                    // progressDialog.dismiss();

                }

            }
        }

    }
