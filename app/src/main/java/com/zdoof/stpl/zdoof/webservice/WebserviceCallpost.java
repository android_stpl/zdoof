package com.zdoof.stpl.zdoof.webservice;


import android.widget.Toast;

import com.zdoof.stpl.zdoof.activites.Home;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;

/**
 * @author AndroidExample DotNetWebService Class
 *
 */
public class WebserviceCallpost {

    /**
     * Variable Decleration................
     *
     */
    String namespace = "http://tempuri.org/";
    private String url ="https://www.zdoof.com/web-services-android/zdoof-and-ztimeline.asmx";
    SoapPrimitive resultString;
    String SOAP_ACTION;
    SoapObject request = null, objMessages = null;
    SoapSerializationEnvelope envelope;
    AndroidHttpTransport androidHttpTransport;

    public WebserviceCallpost() {
    }


    /**
     * Set Envelope
     */
    protected void SetEnvelope() {

        try {

            // Creating SOAP envelope			
            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            //You can comment that line if your web service is not .NET one.
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            androidHttpTransport = new AndroidHttpTransport(url);
            androidHttpTransport.debug = true;
           /* androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapObject result = (SoapObject)envelope.getResponse();*/

        } catch (Exception e) {
            System.out.println("Soap Exception---->>>" + e.toString());
        }
    }

   public String Experience(String MethodName,int ischeckin,String description,String post_image,int createby,
                            String createon, String uploadvideolinktxt)

   {

       try {
           SOAP_ACTION = namespace + MethodName;

           //Adding values to request object
           request = new SoapObject(namespace, MethodName);

           //Adding Double value to request object
           PropertyInfo weightProp =new PropertyInfo();
           PropertyInfo weightProp1 =new PropertyInfo();
           PropertyInfo weightProp2 =new PropertyInfo();
           PropertyInfo weightProp3 =new PropertyInfo();
           PropertyInfo weightProp4 =new PropertyInfo();
           PropertyInfo weightProp5 =new PropertyInfo();


           weightProp.setName("Is_checkin");
           weightProp.setValue(ischeckin);
           weightProp.setType(String.class);
           request.addProperty(weightProp);

           weightProp1.setName("Description");
           weightProp1.setValue(description);
           weightProp1.setType(String.class);
           request.addProperty(weightProp1);

           weightProp2.setName("Post_image");
           weightProp2.setValue(post_image);
           weightProp2.setType(String.class);
           request.addProperty(weightProp2);

           weightProp3.setName("Created_by");
           weightProp3.setValue(createby);
           weightProp3.setType(String.class);
           request.addProperty(weightProp3);

           weightProp4.setName("Created_on");
           weightProp4.setValue(createon);
           weightProp4.setType(String.class);
           request.addProperty(weightProp4);

           weightProp5.setName("Videolink");
           weightProp5.setValue(uploadvideolinktxt);
           weightProp5.setType(String.class);
           request.addProperty(weightProp5);

           SetEnvelope();

           try {

               //SOAP calling webservice
               androidHttpTransport.call(SOAP_ACTION, envelope);

               //Got Webservice response
               String result = envelope.getResponse().toString();

               if(result == null ) {

                   Toast.makeText(Home.homeContext,"Please try again..",Toast.LENGTH_SHORT).show();

                   return "";

               }

               else {

                   return result;
               }

           } catch (Exception e) {
               // TODO: handle exception
               return e.toString();
           }
       } catch (Exception e) {
           // TODO: handle exception
           return e.toString();
       }

   }
    public String Enjoydish(String MethodName,int userid,String placename,int dishid,String dishname,String expdate,
                             int posttype,String postcmnt,String postimage,int placetype,int devicetype,int cretby,String creaton)

    {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();
            PropertyInfo weightProp1 =new PropertyInfo();
            PropertyInfo weightProp2 =new PropertyInfo();
            PropertyInfo weightProp3 =new PropertyInfo();
            PropertyInfo weightProp4 =new PropertyInfo();
            PropertyInfo weightProp5 =new PropertyInfo();
            PropertyInfo weightProp6 =new PropertyInfo();
            PropertyInfo weightProp7 =new PropertyInfo();
            PropertyInfo weightProp8 =new PropertyInfo();
            PropertyInfo weightProp9 =new PropertyInfo();
            PropertyInfo weightProp10 =new PropertyInfo();
            PropertyInfo weightProp11 =new PropertyInfo();

            weightProp.setName("User_id");
            weightProp.setValue(userid);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("Place_name");
            weightProp1.setValue(placename);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Dish_id");
            weightProp2.setValue(dishid);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("Dish_name");
            weightProp3.setValue(dishname);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            weightProp4.setName("Experience_date");
            weightProp4.setValue(expdate);
            weightProp4.setType(String.class);
            request.addProperty(weightProp4);

            weightProp5.setName("Post_type");
            weightProp5.setValue(posttype);
            weightProp5.setType(String.class);
            request.addProperty(weightProp5);

            weightProp6.setName("Post_comment");
            weightProp6.setValue(postcmnt);
            weightProp6.setType(String.class);
            request.addProperty(weightProp6);

            weightProp7.setName("Post_image");
            weightProp7.setValue(postimage);
            weightProp7.setType(String.class);
            request.addProperty(weightProp7);

            weightProp8.setName("Place_type");
            weightProp8.setValue(placetype);
            weightProp8.setType(String.class);
            request.addProperty(weightProp8);

            weightProp9.setName("Device_type");
            weightProp9.setValue(devicetype);
            weightProp9.setType(String.class);
            request.addProperty(weightProp9);

            weightProp10.setName("Created_by");
            weightProp10.setValue(cretby);
            weightProp10.setType(String.class);
            request.addProperty(weightProp10);

            weightProp11.setName("Created_on");
            weightProp11.setValue(creaton);
            weightProp11.setType(String.class);
            request.addProperty(weightProp11);

           /* weightProp11.setName("post_image");
            weightProp11.setValue(byt);
            weightProp11.setType(String.class);
            request.addProperty(weightProp11);*/




            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
    // MethodName variable is d*///efine for which webservice function  will call

    public String Postdish(String MethodName, String dishname,String imges,String ingradiant,String instru,String qunt,String desc,int cretby,String crton,String ip)

    {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();
            PropertyInfo weightProp1 =new PropertyInfo();
            PropertyInfo weightProp2 =new PropertyInfo();
            PropertyInfo weightProp3=new PropertyInfo();
            PropertyInfo weightProp4 =new PropertyInfo();
            PropertyInfo weightProp5 =new PropertyInfo();
            PropertyInfo weightProp6=new PropertyInfo();
            PropertyInfo weightProp7 =new PropertyInfo();
            PropertyInfo weightProp8 =new PropertyInfo();

            weightProp.setName("Dish_name");
            weightProp.setValue(dishname);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("DishXmlImg");
            weightProp1.setValue(imges);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Ingredients");
            weightProp2.setValue(ingradiant);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("Cooking_Method");
            weightProp3.setValue(instru);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            weightProp4.setName("XmlValue");
            weightProp4.setValue(qunt);
            weightProp4.setType(String.class);
            request.addProperty(weightProp4);

            weightProp5.setName("Dish_desc");
            weightProp5.setValue(desc);
            weightProp5.setType(String.class);
            request.addProperty(weightProp5);

            weightProp6.setName("Created_by");
            weightProp6.setValue(cretby);
            weightProp6.setType(String.class);
            request.addProperty(weightProp6);

            weightProp7.setName("Created_on");
            weightProp7.setValue(crton);
            weightProp7.setType(String.class);
            request.addProperty(weightProp7);

            weightProp8.setName("Ip_address");
            weightProp8.setValue(ip);
            weightProp8.setType(String.class);
            request.addProperty(weightProp8);


            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
    public String Checkin(String MethodName,int ischeckin,String description,String vd, String post_image,int createby,
                            String createon)

    {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();
            PropertyInfo weightProp1 =new PropertyInfo();
            PropertyInfo weightProp2 =new PropertyInfo();
            PropertyInfo weightProp3 =new PropertyInfo();
            PropertyInfo weightProp4 =new PropertyInfo();
            PropertyInfo weightProp5 =new PropertyInfo();


            weightProp.setName("Is_checkin");
            weightProp.setValue(ischeckin);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("Description");
            weightProp1.setValue(description);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Videolink");
            weightProp2.setValue(vd);
           weightProp2.setType(String.class);
           request.addProperty(weightProp2);

            weightProp3.setName("Post_image");
            weightProp3.setValue(post_image);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            weightProp4.setName("Created_by");
            weightProp4.setValue(createby);
            weightProp4.setType(String.class);
            request.addProperty(weightProp4);

            weightProp5.setName("Created_on");
            weightProp5.setValue(createon);
            weightProp5.setType(String.class);
            request.addProperty(weightProp5);






            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }

    public String Ztimeline(String MethodName, int createdby,String createdon,String updatedon,int pageindex,int pagesize)

    {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();
            PropertyInfo weightProp1 =new PropertyInfo();
            PropertyInfo weightProp2 =new PropertyInfo();
            PropertyInfo weightProp3 =new PropertyInfo();
            PropertyInfo weightProp4 =new PropertyInfo();

            weightProp.setName("Created_by");
            weightProp.setValue(createdby);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("Created_on");
            weightProp1.setValue(createdon);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Updated_on");
            weightProp2.setValue(updatedon);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("PageIndex");
            weightProp3.setValue(pageindex);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            weightProp4.setName("PageSize");
            weightProp4.setValue(pagesize);
            weightProp4.setType(String.class);
            request.addProperty(weightProp4);



            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);


                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
////////////////
public String Addlike(String MethodName,int Post_id,int posttype, int Created_by, String Created_on)
{

    try {
        SOAP_ACTION = namespace + MethodName;

        //Adding values to request object
        request = new SoapObject(namespace, MethodName);

        //Adding Double value to request object
        PropertyInfo weightProp =new PropertyInfo();
        PropertyInfo weightProp1 =new PropertyInfo();
        PropertyInfo weightProp2 =new PropertyInfo();
        PropertyInfo weightProp3 =new PropertyInfo();

        weightProp.setName("Post_id");
        weightProp.setValue(Post_id);
        weightProp.setType(String.class);
        request.addProperty(weightProp);

        weightProp1.setName("Post_type");
        weightProp1.setValue(posttype);
        weightProp1.setType(String.class);
        request.addProperty(weightProp1);

        weightProp2.setName("Created_by");
        weightProp2.setValue(Created_by);
        weightProp2.setType(String.class);
        request.addProperty(weightProp2);

        weightProp3.setName("Created_on");
        weightProp3.setValue(Created_on);
        weightProp3.setType(String.class);
        request.addProperty(weightProp3);



        SetEnvelope();

        try {

            //SOAP calling webservice
            androidHttpTransport.call(SOAP_ACTION, envelope);

            //Got Webservice response
            String result = envelope.getResponse().toString();

            return result;

        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }
    } catch (Exception e) {
        // TODO: handle exception
        return e.toString();
    }

}
    ////
//update commnt
    public String Update_cmnt(String MethodName, int postreview,String comm)

    {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();
            PropertyInfo weightProp1 =new PropertyInfo();



            weightProp.setName("Review_post_id");
            weightProp.setValue(postreview);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("Comment");
            weightProp1.setValue(comm);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
    //////////
    ///////
    //receive comment
    public String comment(String MethodName, int pid,int ptipe)

    {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();
            PropertyInfo weightProp1 =new PropertyInfo();



            weightProp.setName("ID");
            weightProp.setValue(pid);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("PTYPE");
            weightProp1.setValue(ptipe);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
    /////
    public String Addcomment(String MethodName, String Comment, int Post_id,int post_type, int Created_by, String Created_on)

    {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            //PropertyInfo weightProp =new PropertyInfo();
            PropertyInfo weightProp1 =new PropertyInfo();
            PropertyInfo weightProp2 =new PropertyInfo();
            PropertyInfo weightProp3 =new PropertyInfo();
            PropertyInfo weightProp4 =new PropertyInfo();
            PropertyInfo weightProp5 =new PropertyInfo();

           /* weightProp.setName("Star_review");
            weightProp.setValue(Star_review);
            weightProp.setType(String.class);
            request.addProperty(weightProp);*/

            weightProp1.setName("Comment");
            weightProp1.setValue(Comment);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Post_id");
            weightProp2.setValue(Post_id);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("Post_type");
            weightProp3.setValue(post_type);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            weightProp4.setName("Created_by");
            weightProp4.setValue(Created_by);
            weightProp4.setType(String.class);
            request.addProperty(weightProp4);

            weightProp5.setName("Created_on");
            weightProp5.setValue(Created_on);
            weightProp5.setType(String.class);
            request.addProperty(weightProp5);
            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
    public String postcount(String MethodName)

    {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();

            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }

    public String Selectdish(String MethodName)

    {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();






            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }

    public String Review_dish(String MethodName, int ddish_id, int restaurant_id, String place_name,
                              String google_place_id, String ratingg, String review,
                              String imageName, int createb,
                              String createdon,String Place_type, String dish_name) {
        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();
            PropertyInfo weightProp1 =new PropertyInfo();
            PropertyInfo weightProp2 =new PropertyInfo();
            PropertyInfo weightProp3=new PropertyInfo();
            PropertyInfo weightProp4 =new PropertyInfo();
            PropertyInfo weightProp5 =new PropertyInfo();
            PropertyInfo weightProp6=new PropertyInfo();
            PropertyInfo weightProp7 =new PropertyInfo();
            PropertyInfo weightProp8 =new PropertyInfo();
            PropertyInfo weightProp9 =new PropertyInfo();

            weightProp.setName("Dish_id");
            weightProp.setValue(ddish_id);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("Restaurant_id");
            weightProp1.setValue(restaurant_id);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Restaurant_name");
            weightProp2.setValue(place_name);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("Google_place_id");
            weightProp3.setValue(google_place_id);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            weightProp4.setName("Star_review");
            weightProp4.setValue(ratingg);
            weightProp4.setType(String.class);
            request.addProperty(weightProp4);

            weightProp5.setName("Comment");
            weightProp5.setValue(review);
            weightProp5.setType(String.class);
            request.addProperty(weightProp5);

            weightProp6.setName("Dish_img");
            weightProp6.setValue(imageName);
            weightProp6.setType(String.class);
            request.addProperty(weightProp6);

            weightProp7.setName("Created_by");
            weightProp7.setValue(createb);
            weightProp7.setType(String.class);
            request.addProperty(weightProp7);

            weightProp8.setName("Created_on");
            weightProp8.setValue(createdon);
            weightProp8.setType(String.class);
            request.addProperty(weightProp8);

            weightProp9.setName("Place_type");
            weightProp9.setValue(Place_type);
            weightProp9.setType(String.class);
            request.addProperty(weightProp9);

            weightProp9.setName("Dish_name");
            weightProp9.setValue(dish_name);
            weightProp9.setType(String.class);
            request.addProperty(weightProp9);

            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }


    }

    public String reviewss(String MethodName, int resturantid) {
        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();

            weightProp.setName("Restaurant_id");
            weightProp.setValue(resturantid);
            weightProp.setType(String.class);
            request.addProperty(weightProp);



            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
    }

    ///////////////
    /*public String Usersearch(String MethodName, int rid,String fstname,String lname,int mainusertype,int subtype,String creton,String updton,String ip) {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();
            PropertyInfo weightProp2 = new PropertyInfo();
            PropertyInfo weightProp3 = new PropertyInfo();
            PropertyInfo weightProp4 = new PropertyInfo();
            PropertyInfo weightProp5 = new PropertyInfo();
            PropertyInfo weightProp6 = new PropertyInfo();
            PropertyInfo weightProp7 = new PropertyInfo();


            weightProp.setName("Regd_id");
            weightProp.setValue(rid);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("First_name");
            weightProp1.setValue(fstname);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Last_name");
            weightProp2.setValue(lname);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("Main_user_type");
            weightProp3.setValue(mainusertype);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            weightProp4.setName("Sub_user_type");
            weightProp4.setValue(subtype);
            weightProp4.setType(String.class);
            request.addProperty(weightProp4);

            weightProp5.setName("Created_on");
            weightProp5.setValue(creton);
            weightProp5.setType(String.class);
            request.addProperty(weightProp5);

            weightProp6.setName("Updated_on");
            weightProp6.setValue(updton);
            weightProp6.setType(String.class);
            request.addProperty(weightProp6);

            weightProp7.setName("Ip_address");
            weightProp7.setValue(ip);
            weightProp7.setType(String.class);
            request.addProperty(weightProp7);

            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
    //////////////////////
    public String AdminassignTask(String MethodName, String department,String name,String empid,String Date,String task,String type) {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();
            PropertyInfo weightProp2 = new PropertyInfo();
            PropertyInfo weightProp3 = new PropertyInfo();
            PropertyInfo weightProp4 = new PropertyInfo();
            PropertyInfo weightProp5 = new PropertyInfo();


            weightProp.setName("Dep");
            weightProp.setValue(department);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("EmpName");
            weightProp1.setValue(name);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("EmpID");
            weightProp2.setValue(empid);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("Date");
            weightProp3.setValue(Date);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            weightProp4.setName("WhichTask");
            weightProp4.setValue(task);
            weightProp4.setType(String.class);
            request.addProperty(weightProp4);

            weightProp5.setName("EntryBy");
            weightProp5.setValue(type);
            weightProp5.setType(String.class);
            request.addProperty(weightProp5);

            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }
    ///////////////////////////////////
    public String AddDelNotice(String MethodName, String action,String id,String date,String sub,String descrip,String enterby)
    {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp =new PropertyInfo();
            PropertyInfo weightProp1 =new PropertyInfo();
            PropertyInfo weightProp2 =new PropertyInfo();
            PropertyInfo weightProp3 =new PropertyInfo();
            PropertyInfo weightProp4 =new PropertyInfo();
            PropertyInfo weightProp5 =new PropertyInfo();

            weightProp.setName("Action");
            weightProp.setValue(action);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("ID");
            weightProp1.setValue(id);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);

            weightProp2.setName("Date");
            weightProp2.setValue(date);
            weightProp2.setType(String.class);
            request.addProperty(weightProp2);

            weightProp3.setName("Subject");
            weightProp3.setValue(sub);
            weightProp3.setType(String.class);
            request.addProperty(weightProp3);

            weightProp4.setName("Description");
            weightProp4.setValue(descrip);
            weightProp4.setType(String.class);
            request.addProperty(weightProp4);

            weightProp5.setName("EntryBy");
            weightProp5.setValue(enterby);
            weightProp5.setType(String.class);
            request.addProperty(weightProp5);

            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }*/
    /////////////////////////////////////////////////
    /////
	/*public String AddDelNotice1(String MethodName, String action,String id,String date,String sub,String descrip,String enterby)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();
			PropertyInfo weightProp1 =new PropertyInfo();
			PropertyInfo weightProp2 =new PropertyInfo();
			PropertyInfo weightProp3 =new PropertyInfo();
			PropertyInfo weightProp4 =new PropertyInfo();
			PropertyInfo weightProp5 =new PropertyInfo();

			weightProp.setName("Action");
			weightProp.setValue(action);
			weightProp.setType(String.class);
			request.addProperty(weightProp);

			weightProp1.setName("ID");
			weightProp1.setValue(id);
			weightProp1.setType(String.class);
			request.addProperty(weightProp1);

			weightProp2.setName("Date");
			weightProp2.setValue(date);
			weightProp2.setType(String.class);
			request.addProperty(weightProp2);

			weightProp3.setName("Subject");
			weightProp3.setValue(sub);
			weightProp3.setType(String.class);
			request.addProperty(weightProp3);

			weightProp4.setName("Description");
			weightProp4.setValue(descrip);
			weightProp4.setType(String.class);
			request.addProperty(weightProp4);

			weightProp5.setName("EntryBy");
			weightProp5.setValue(enterby);
			weightProp5.setType(String.class);
			request.addProperty(weightProp5);

			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	////

//////////
public String Logout(String MethodName,String emp_id)

{

	try {
		SOAP_ACTION = namespace + MethodName;

		//Adding values to request object
		request = new SoapObject(namespace, MethodName);

		//Adding Double value to request object
		PropertyInfo weightProp =new PropertyInfo();



		weightProp.setName("UserID");
		weightProp.setValue(emp_id);
		weightProp.setType(String.class);
		request.addProperty(weightProp);



		SetEnvelope();

		try {

			//SOAP calling webservice
			androidHttpTransport.call(SOAP_ACTION, envelope);

			//Got Webservice response
			String result = envelope.getResponse().toString();

			return result;

		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}
	} catch (Exception e) {
		// TODO: handle exception
		return e.toString();
	}

}
	//////
	public String Push(String MethodName, String rid)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("RegID");
			weightProp.setValue(rid);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	//////////////
	public String EmDeptWiseName(String MethodName, String dpt)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("Department");
			weightProp.setValue(dpt);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	///////////
	public String EmpeNameWiseid(String MethodName, String name)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("Name");
			weightProp.setValue(name);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}
	///
	public String Alldata(String MethodName,String dd)
	{

		try {
			SOAP_ACTION = namespace + MethodName;

			//Adding values to request object
			request = new SoapObject(namespace, MethodName);

			//Adding Double value to request object
			PropertyInfo weightProp =new PropertyInfo();

			weightProp.setName("RegID");
			weightProp.setValue(dd);
			weightProp.setType(String.class);
			request.addProperty(weightProp);



			SetEnvelope();

			try {

				//SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				//Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}*/
    ///package


    /************************************/

