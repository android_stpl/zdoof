package com.zdoof.stpl.zdoof;

/**
 * Created by stpl on 22/1/16.
 */
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class MyTextView extends TextView {
    public static Typeface FONT_NAME;

    public MyTextView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        if(FONT_NAME == null)
            FONT_NAME = Typeface.createFromAsset(context.getAssets(), "OpenSans-Regular.ttf");

        this.setTypeface(FONT_NAME);
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(FONT_NAME == null)
            FONT_NAME = Typeface.createFromAsset(context.getAssets(), "OpenSans-Regular.ttf");

        this.setTypeface(FONT_NAME);
    }

    public MyTextView(Context context, AttributeSet attrs, int defStyle) {
        // TODO Auto-generated constructor stub
        super(context, attrs, defStyle);
        if(FONT_NAME == null)
            FONT_NAME = Typeface.createFromAsset(context.getAssets(), "OpenSans-Regular.ttf");

        this.setTypeface(FONT_NAME);
    }
}