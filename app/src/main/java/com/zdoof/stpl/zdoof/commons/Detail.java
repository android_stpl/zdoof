package com.zdoof.stpl.zdoof.commons;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by stpl on 17/2/16.
 */
public class Detail {
    private String userid;
    private String name;
    private String email;
    private String pfimage;
    private String utype;
    private String totalcount;
    private String thumbsup;
    private String raceipies;
    private String ingradiant;
    private String quantity;
    private String unit;
    private String id;
    private String utypeimage;
    private String dprf;
    private String dwrkas;
    private String ddtails;
    private String dname;
    private String updateimage;
    private String style;
    private String dshare;
    private String dcreateon;
    private String likedto;
    private String type;
    private String likedon;
    private String zfan;
    private String dish_name;
    private String dish_pic;
    private String reviews;
    private String zlikes;
    private String ratings;
    private String restaurant_name;
    private String rest_img;
    private String rest_id;
    private String rest_address;
    private String rest_phone;
    private String gstore_id;
    private String gstore_name;
    private String gstore_img;
    private String gstore_address;
    private String gstore_phone;
    private String User_id;
    private String Userpfimg;
    private String User_type;
    private String User_Zlike;
    private String U_Name;
    private String postimage;
    private String postdetails;
    private String postname;
    private String pcreaton;
    private String pfimg;
    private String uype;
    private String sname;
    private String idd;
    private String ptype;
    private String comment;
    private String profile;
    private String names;
    private String cre;
    private String Post_like;
    private String iflike;
    private String comments;
    private String Postedcomments;
    private String place_dish;
    private String place_rest;
    private String at;
    private String ribon;
    private String Dish_namee;
    private String Dish_id;
    private String postedby;
    private String dish_picture;
    private String POST_TYPE;
    private String POST_COMMENT;
    private String POSTDATE;
    private String weight;
    private String youtubeID;
    private Bitmap profBitmap;
    private ArrayList<Bitmap> scrollBitmap = new ArrayList<Bitmap>();
    private ArrayList<Tables>tableImages;

    public String connID1;
    public String connID2;
    public String connName1;
    public String connName2;

    public String blogTitleImg;

    public Detail() {
    }

    public ArrayList<Tables> getTableImages() {
        return tableImages;
    }

    public void setTableImages(ArrayList<Tables> tableImages) {
        this.tableImages = tableImages;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getPOST_TYPE() {
        return POST_TYPE;
    }

    public void setPOST_TYPE(String POST_TYPE) {
        this.POST_TYPE = POST_TYPE;
    }

    public String getPOST_COMMENT() {
        return POST_COMMENT;
    }

    public void setPOST_COMMENT(String POST_COMMENT) {
        this.POST_COMMENT = POST_COMMENT;
    }

    public String getPOSTDATE() {
        return POSTDATE;
    }

    public void setPOSTDATE(String POSTDATE) {
        this.POSTDATE = POSTDATE;
    }

    public String getDish_picture() {
        return dish_picture;
    }

    public void setDish_picture(String dish_picture) {
        this.dish_picture = dish_picture;
    }

    public String getPostedby() {
        return postedby;
    }

    public void setPostedby(String postedby) {
        this.postedby = postedby;
    }

    public String getDish_namee() {
        return Dish_namee;
    }

    public void setDish_namee(String dish_namee) {
        Dish_namee = dish_namee;
    }

    public String getDish_id() {
        return Dish_id;
    }

    public void setDish_id(String dish_id) {
        Dish_id = dish_id;
    }

    public String getRibon() {
        return ribon;
    }

    public void setRibon(String ribon) {
        this.ribon = ribon;
    }

    public String getPostedcomments() {
        return Postedcomments;
    }

    public void setPostedcomments(String postedcomments) {
        Postedcomments = postedcomments;
    }

    public String getPlace_dish() {
        return place_dish;
    }

    public void setPlace_dish(String place_dish) {
        this.place_dish = place_dish;
    }

    public String getPlace_rest() {
        return place_rest;
    }

    public void setPlace_rest(String place_rest) {
        this.place_rest = place_rest;
    }

    public String getAt() {
        return at;
    }

    public void setAt(String at) {
        this.at = at;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getIflike() {
        return iflike;
    }

    public void setIflike(String iflike) {
        this.iflike = iflike;
    }

    public String getPost_like() {
        return Post_like;
    }

    public void setPost_like(String post_like) {
        Post_like = post_like;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getCre() {
        return cre;
    }

    public void setCre(String cre) {
        this.cre = cre;
    }

    public String getPtype() {
        return ptype;
    }

    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    public String getIdd() {
        return idd;
    }

    public void setIdd(String idd) {
        this.idd = idd;
    }

    public String getUtypeimage() {
        return utypeimage;
    }

    public void setUtypeimage(String utypeimage) {
        this.utypeimage = utypeimage;
    }

    public String getPfimg() {
        return pfimg;
    }

    public void setPfimg(String pfimg) {
        this.pfimg = pfimg;
    }

    public String getUype() {
        return uype;
    }

    public void setUype(String uype) {
        this.uype = uype;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getPcreaton() {
        return pcreaton;
    }

    public void setPcreaton(String pcreaton) {
        this.pcreaton = pcreaton;
    }

    public String getPostimage() {
        return postimage;
    }

    public void setPostimage(String postimage) {
        this.postimage = postimage;
    }

    public String getPostdetails() {
        return postdetails;
    }

    public void setPostdetails(String postdetails) {
        this.postdetails = postdetails;
    }

    public String getPostname() {
        return postname;
    }

    public void setPostname(String postname) {
        this.postname = postname;
    }

    public String getU_Name() {
        return U_Name;
    }

    public void setU_Name(String u_Name) {
        U_Name = u_Name;
    }

    public String getUser_id() {
        return User_id;
    }

    public void setUser_id(String user_id) {
        User_id = user_id;
    }

    public String getUserpfimg() {
        return Userpfimg;
    }

    public void setUserpfimg(String userpfimg) {
        Userpfimg = userpfimg;
    }

    public String getUser_type() {
        return User_type;
    }

    public void setUser_type(String user_type) {
        User_type = user_type;
    }

    public String getUser_Zlike() {
        return User_Zlike;
    }

    public void setUser_Zlike(String user_Zlike) {
        User_Zlike = user_Zlike;
    }

    public String getGstore_id() {
        return gstore_id;
    }

    public void setGstore_id(String gstore_id) {
        this.gstore_id = gstore_id;
    }

    public String getGstore_name() {
        return gstore_name;
    }

    public void setGstore_name(String gstore_name) {
        this.gstore_name = gstore_name;
    }

    public String getGstore_img() {
        return gstore_img;
    }

    public void setGstore_img(String gstore_img) {
        this.gstore_img = gstore_img;
    }

    public String getGstore_address() {
        return gstore_address;
    }

    public void setGstore_address(String gstore_address) {
        this.gstore_address = gstore_address;
    }

    public String getGstore_phone() {
        return gstore_phone;
    }

    public void setGstore_phone(String gstore_phone) {
        this.gstore_phone = gstore_phone;
    }

    public String getRestaurant_name() {
        return restaurant_name;
    }

    public void setRestaurant_name(String restaurant_name) {
        this.restaurant_name = restaurant_name;
    }

    public String getRest_img() {
        return rest_img;
    }

    public void setRest_img(String rest_img) {
        this.rest_img = rest_img;
    }

    public String getRest_id() {
        return rest_id;
    }

    public void setRest_id(String rest_id) {
        this.rest_id = rest_id;
    }

    public String getRest_address() {
        return rest_address;
    }

    public void setRest_address(String rest_address) {
        this.rest_address = rest_address;
    }

    public String getRest_phone() {
        return rest_phone;
    }

    public void setRest_phone(String rest_phone) {
        this.rest_phone = rest_phone;
    }

    public String getDish_name() {
        return dish_name;
    }

    public void setDish_name(String dish_name) {
        this.dish_name = dish_name;
    }

    public String getDish_pic() {
        return dish_pic;
    }

    public void setDish_pic(String dish_pic) {
        this.dish_pic = dish_pic;
    }

    public String getReviews() {
        return reviews;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }

    public String getZlikes() {
        return zlikes;
    }

    public void setZlikes(String zlikes) {
        this.zlikes = zlikes;
    }

    public String getRatings() {
        return ratings;
    }

    public void setRatings(String ratings) {
        this.ratings = ratings;
    }

    public String getZfan() {
        return zfan;
    }

    public void setZfan(String zfan) {
        this.zfan = zfan;
    }

    public String getLikedto() {
        return likedto;
    }

    public void setLikedto(String likedto) {
        this.likedto = likedto;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLikedon() {
        return likedon;
    }

    public void setLikedon(String likedon) {
        this.likedon = likedon;
    }

    public String getDshare() {
        return dshare;
    }

    public void setDshare(String dshare) {
        this.dshare = dshare;
    }

    public String getDcreateon() {
        return dcreateon;
    }

    public void setDcreateon(String dcreateon) {
        this.dcreateon = dcreateon;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getUpdateimage() {
        return updateimage;
    }

    public void setUpdateimage(String updateimage) {
        this.updateimage = updateimage;
    }

    public String getDprf() {
        return dprf;
    }

    public void setDprf(String dprf) {
        this.dprf = dprf;
    }

    public String getDwrkas() {
        return dwrkas;
    }

    public void setDwrkas(String dwrkas) {
        this.dwrkas = dwrkas;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public String getDdtails() {
        return ddtails;
    }

    public void setDdtails(String ddtails) {
        this.ddtails = ddtails;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getThumbsup() {
        return thumbsup;
    }

    public String getIngradiant() {
        return ingradiant;
    }

    public void setIngradiant(String ingradiant) {
        this.ingradiant = ingradiant;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setThumbsup(String thumbsup) {
        this.thumbsup = thumbsup;
    }

    public String getRaceipies() {
        return raceipies;
    }

    public void setRaceipies(String raceipies) {
        this.raceipies = raceipies;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getPfimage() {
        return pfimage;
    }

    public void setPfimage(String pfimage) {
        this.pfimage = pfimage;
    }

    public String getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(String totalcount) {
        this.totalcount = totalcount;
    }

    public String getYoutubeID() {
        return youtubeID;
    }

    public void setYoutubeID(String youtubeID) {
        this.youtubeID = youtubeID;
    }

    public Bitmap getProfBitmap() {
        return profBitmap;
    }

    public void setProfBitmap(Bitmap profBitmap) {
        this.profBitmap = profBitmap;
    }

    public ArrayList<Bitmap> getScrollBitmap() {
        return scrollBitmap;
    }

    public void setScrollBitmap(ArrayList<Bitmap> scrollBitmap) {
        this.scrollBitmap = scrollBitmap;
    }

}