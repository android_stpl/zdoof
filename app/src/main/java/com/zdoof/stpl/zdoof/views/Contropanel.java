package com.zdoof.stpl.zdoof.views;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.activites.AboutApp;
import com.zdoof.stpl.zdoof.activites.AppSettings;
import com.zdoof.stpl.zdoof.activites.ChangePasswordActivity;
import com.zdoof.stpl.zdoof.activites.Connecton;
import com.zdoof.stpl.zdoof.activites.Home;
import com.zdoof.stpl.zdoof.activites.MyRecipiesActivity;
import com.zdoof.stpl.zdoof.activites.Notifications;
import com.zdoof.stpl.zdoof.activites.PostListActivity;
import com.zdoof.stpl.zdoof.activites.Profiles;
import com.zdoof.stpl.zdoof.activites.SignIn;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.receivers.NotificationAlarmReceiver;
import com.zdoof.stpl.zdoof.search.Search_all;
import com.zdoof.stpl.zdoof.search.Search_dish;
import com.zdoof.stpl.zdoof.search.Search_gstore;
import com.zdoof.stpl.zdoof.search.Search_people;
import com.zdoof.stpl.zdoof.search.Search_post;
import com.zdoof.stpl.zdoof.search.Search_resturant;
import com.zdoof.stpl.zdoof.searchadapters.CustomSearch3;
import com.zdoof.stpl.zdoof.utils.AppUtil;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Locale;

/**
 * Created by sujit on 23/1/16.
 */
public class Contropanel extends Activity implements LocationListener {
    ImageView close, home, connect, noti, icon;
    TextView pro, rcp, pff, post, textt, code;
    LinearLayout sl, profile, mypost, myreceipe, aboutapp, appseting, changepass, logout;
    ImageView serc;
    EditText description, zips;
    LinearLayout ll1, ll2, ll3, ll4, ll5;
    final WebserviceCall com = new WebserviceCall();
    static String bResponse;
    ProgressDialog progressDialog;
    StringBuilder log;
    boolean isGPSEnabled = false;
    final int LOCATION_PERMISSION_CODE = 1;

    ListView drawerList;
    // flag for network status
    boolean isNetworkEnabled = false;
    LinearLayout headerview, ziploc, vieew;
    // flag for GPS status
    boolean canGetLocation = false;
    TextView Share;
    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    // private static final long MY_PERMISSION_LOCATION = 10;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    ImageView addbutton;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    Button submit;
    String main, First_name = "", rid, searchvalue, result, locations;
    int Regd_id, Sub_user_type, Main_user_type;
    String _name, Last_name, Createdon, Updated_on, Ip_address, desc;
    String userid, email, emailid, creaton, profileimg, city, contry, gender, wrkas, creatby, maritaltext, totalconnection;
    String firstname, lname, name, states, propict, mnutype, subutype, pic, tcnt, pic1, pic3, type, password,
            addrsline1, addressln2, zipcode, dob, Marital_status, Mobile;
    Dialog dialog;
    SharedPreferenceClass sharedPreferenceClass;
    String sertype[] = {"Search Post", "Search Foodie", "Search Recipe/Dish", "Search Restaurant", "Search Grocery Store"};
    int sercimg[] = {R.drawable.searhicon, R.drawable.sst, R.drawable.ff, R.drawable.sahlist, R.drawable.crt, R.drawable.uur};
    android.support.v4.widget.DrawerLayout drawerMainSearch;
    String pos, des;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.controlpenal);

        ll1 = (LinearLayout) findViewById(R.id.hmj);
        ll2 = (LinearLayout) findViewById(R.id.cnn);
        ll3 = (LinearLayout) findViewById(R.id.ntf);
        ll4 = (LinearLayout) findViewById(R.id.ct);
        ll5 = (LinearLayout) findViewById(R.id.lay);
        drawerMainSearch = (DrawerLayout) findViewById(R.id.drawerMainSearch);
        profile = (LinearLayout) findViewById(R.id.profile);
        mypost = (LinearLayout) findViewById(R.id.myposst);
        aboutapp = (LinearLayout) findViewById(R.id.aboutapp);
        appseting = (LinearLayout) findViewById(R.id.appseting);
        home = (ImageView) findViewById(R.id.hme);
        ll4.setBackgroundColor(Color.parseColor("#1D8168"));
        connect = (ImageView) findViewById(R.id.hm);
        description = (EditText) findViewById(R.id.imageView11);
        drawerList= (ListView) findViewById(R.id.drawerItemList);
        myreceipe = (LinearLayout) findViewById(R.id.myreceipe);
        //sl= (LinearLayout) findViewById(R.id.ser);
        close = (ImageView) findViewById(R.id.close);
        pff = (TextView) findViewById(R.id.pff);
        icon = (ImageView) findViewById(R.id.imageView4);
        rcp = (TextView) findViewById(R.id.rcp);
        changepass = (LinearLayout) findViewById(R.id.cngpassword);
        // post= (TextView) findViewById(R.id.post);
        serc = (ImageView) findViewById(R.id.serc);
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header1 = (ViewGroup) inflater.inflate(R.layout.search_view, drawerList, false);
        drawerList.addHeaderView(header1, null, false);
        CustomSearch3 drawerAdapter = new CustomSearch3(Contropanel.this, R.layout.spiner_item, sertype, sercimg);
        drawerList.setAdapter(drawerAdapter);
        textt = (TextView) findViewById(R.id.textt);
        ziploc = (LinearLayout) findViewById(R.id.ziploc);
        vieew = (LinearLayout) findViewById(R.id.view);
        zips = (EditText) findViewById(R.id.zip);
        submit = (Button) findViewById(R.id.submit);
        code = (TextView) findViewById(R.id.code);
        sharedPreferenceClass = new SharedPreferenceClass(this);
        close.setVisibility(View.GONE);
        searchvalue = sharedPreferenceClass.getValue_string("SEARCHVALUE");
        if (searchvalue.equals("")) {
            description.setText("");
            close.setVisibility(View.GONE);
        } else if (searchvalue.equals("0")) {
            description.setText("");
            close.setVisibility(View.GONE);
        } else {
            description.setText(searchvalue);
            close.setVisibility(View.VISIBLE);
        }
        rid = sharedPreferenceClass.getValue_string("UIDD");
        Regd_id = Integer.parseInt(rid);
        logout = (LinearLayout) findViewById(R.id.logout);

        sharedPreferenceClass.setValue_string("BACKPAGE","");
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zipcode = zips.getText().toString();
                code.setText(zipcode);
                sharedPreferenceClass.setValue_string("ZZIP", zipcode);
                ziploc.setVisibility(View.GONE);
                vieew.setVisibility(View.GONE);
            }
        });

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                desc = description.getText().toString().trim();
                if (desc.length() < 3) {
                    Toast.makeText(Contropanel.this, "Please enter atleast 3 letters to search!", Toast.LENGTH_SHORT).show();
                } else {
                    pos = drawerList.getItemAtPosition(position).toString();
                    if (pos.equals("0")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(Contropanel.this, Search_post.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("1")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(Contropanel.this, Search_people.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("2")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(Contropanel.this, Search_dish.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("3")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(Contropanel.this, Search_resturant.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("4")) {
                        desc = description.getText().toString().trim();
                        description.setText(desc);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                        Intent intent = new Intent(Contropanel.this, Search_gstore.class);
                        startActivity(intent);
                        //finish();
                    }
                }
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferenceClass.setValue_string("SEARCHVALUE", "0");
                description.setText("");
                close.setVisibility(View.GONE);
            }
        });
        serc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawerMainSearch.isDrawerOpen(GravityCompat.END)) {
                    drawerMainSearch.openDrawer(GravityCompat.END);
                }
                else {
                    drawerMainSearch.closeDrawer(GravityCompat.END);
                }
            }
        });
        description.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId== EditorInfo.IME_ACTION_SEARCH) {
                    serc.performClick();
                    return true;
                }
                return false;
            }
        });

        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                drawerMainSearch.closeDrawer(GravityCompat.END);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setCursorVisible(true);
            }
        });
        textt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ziploc.setVisibility(View.VISIBLE);
                vieew.setVisibility(View.VISIBLE);
            }
        });
        aboutapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Contropanel.this, AboutApp.class);
                startActivity(intent);
            }
        });
        appseting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Contropanel.this, AppSettings.class);
                sharedPreferenceClass.setValue_string("BACKPAGE", "controlpanel");
                startActivity(intent);
                finish();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //logout.setBackgroundColor(Color.parseColor("#1D8168"));
                logoutDialog();
            }
        });
        mypost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mypost.setBackgroundColor(Color.parseColor("#1D8168"));
                Intent intent = new Intent(Contropanel.this, PostListActivity.class);
                intent.putExtra("USERID", rid);
                sharedPreferenceClass.setValue_string("BACKPAGE", "controlpanel");
                startActivity(intent);
                finish();
            }
        });
        myreceipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //myreceipe.setBackgroundColor(Color.parseColor("#1D8168"));
                Intent intent = new Intent(Contropanel.this, MyRecipiesActivity.class);
                sharedPreferenceClass.setValue_string("BACKPAGE", "controlpanel");
                startActivity(intent);
                finish();
            }
        });
       /* sl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Contropanel.this,Search_all.class);
                startActivity(intent);
                finish();
            }
        });
*/
        changepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Contropanel.this, ChangePasswordActivity.class);
                startActivity(intent);
                finish();
            }
        });
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll4.setBackgroundColor(Color.parseColor("#2D9363"));
                ll1.setBackgroundColor(Color.parseColor("#1D8168"));
                Intent intent = new Intent(Contropanel.this, Home.class);
                startActivity(intent);
                finish();
            }
        });

        ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll1.setBackgroundColor(Color.parseColor("#2D9363"));
                ll2.setBackgroundColor(Color.parseColor("#1D8168"));
                Intent intent = new Intent(Contropanel.this, Connecton.class);
                startActivity(intent);
                finish();
            }
        });

        ll3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Contropanel.this, Notifications.class);
                startActivity(intent);
                finish();
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //profile.setBackgroundColor(Color.parseColor("#1D8168"));
                Intent intent = new Intent(Contropanel.this, Profiles.class);
                intent.putExtra("USERID", rid);
                intent.putExtra("Profiletask", true);
                intent.putExtra("HTM", "su");
                startActivity(intent);
                sharedPreferenceClass.setValue_boolean("Loginstatus", true);
                sharedPreferenceClass.setValue_string("BACKPAGE", "controlpanel");
                finish();
            }
        });

        //getZipcodeOfLocation();
        String z_zip = sharedPreferenceClass.getValue_string("ZZIP");
        if (!z_zip.equals(null) && !z_zip.equals("") && !(z_zip.length()<2)) {
            code.setText(z_zip);
            zipcode = z_zip;
        } else {
            if (AppUtil.GetLocationPermission(this)) {
                GetZipcodeTask zipTask = new GetZipcodeTask();
                zipTask.execute();
            }
            else {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CODE);
            }
        }

    }

    private void cancelAlarm() {
        Intent intent = new Intent(getApplicationContext(), NotificationAlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, NotificationAlarmReceiver.ALARM_REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pIntent);
    }

    private void logoutDialog() {
        dialog = new Dialog(Contropanel.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        ImageView cross = (ImageView) dialog.findViewById(R.id.logoutCross);
        Button yes = (Button) dialog.findViewById(R.id.logoutYes);
        Button no = (Button) dialog.findViewById(R.id.logoutNo);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferenceClass.setValue_boolean("Loginstatus", false);
                sharedPreferenceClass.clearData();
                cancelAlarm();
                Boolean autoisSet = sharedPreferenceClass.getValue_boolean("AUTO_LOGOUT");
                if (autoisSet) {
                    sharedPreferenceClass.setValue_boolean("AUTO_LOGOUT", autoisSet);
                }
                Intent intent = new Intent(Contropanel.this, SignIn.class);
                startActivity(intent);
                finish();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private class GetZipcodeTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... params) {
            getZipcodeOfLocation();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String zzip = sharedPreferenceClass.getValue_string("ZZIP");
            if (!zzip.equals(null) && !zzip.equals("") && !zzip.equals("0") && !(zzip.length()<2)) {
                code.setText(zzip);
            } else {
                code.setText(zipcode);
                sharedPreferenceClass.setValue_string("ZZIP", zipcode);
            }
            zipcode = sharedPreferenceClass.getValue_string("ZZIP");
        }
    }

    private void getZipcodeOfLocation() {
        LocationManager manager = (LocationManager) Contropanel.this.getSystemService(LOCATION_SERVICE);
        // getting GPS status
        isGPSEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        isNetworkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Geocoder geocoder = new Geocoder(Contropanel.this,Locale.getDefault());
        if (
                ActivityCompat.checkSelfPermission(Contropanel.this, Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(Contropanel.this, Manifest.permission.ACCESS_COARSE_LOCATION)==PackageManager.PERMISSION_GRANTED) {
            if (isNetworkEnabled) {
                location = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location!=null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    try {
                        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 5);
                        if (addresses!=null) {
                            if (addresses.size()>0) {
                                for (int i=0; i<addresses.size(); i++){
                                    if (addresses.get(i).getPostalCode()!=null) {
                                        if (addresses.get(i).getPostalCode().length()>1) {
                                            zipcode = addresses.get(i).getPostalCode();
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        else {
            ActivityCompat.requestPermissions(Contropanel.this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CODE);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_CODE) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                //getZipcodeOfLocation();
                GetZipcodeTask zipcodeTask = new GetZipcodeTask();
                zipcodeTask.execute();
            } else {
                Toast.makeText(Contropanel.this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        drawerMainSearch.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onBackPressed() {
        if (drawerMainSearch.isDrawerOpen(GravityCompat.END)) {
            drawerMainSearch.closeDrawer(GravityCompat.END);
        } else {
            description.setText("");
            sharedPreferenceClass.setValue_string("SEARCHVALUE", "0");
            Intent intent = new Intent(Contropanel.this, Home.class);
            startActivity(intent);
            finish();
        }
    }
}


