package com.zdoof.stpl.zdoof.search;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageView;

import com.zdoof.stpl.zdoof.activites.Connecton;
import com.zdoof.stpl.zdoof.views.Contropanel;
import com.zdoof.stpl.zdoof.activites.Home;
import com.zdoof.stpl.zdoof.R;

/**
 * Created by stpl on 11/3/16.
 */
public class Custfood extends Activity {
    ImageView all,post,grocery,dish,food,serc;
    ImageView close,home,connect,noti,icon,contlpnl;
    private FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_search);
        serc=(ImageView) findViewById(R.id.serc);
        all= (ImageView) findViewById(R.id.image5);
        post= (ImageView) findViewById(R.id.imageVie);
        grocery=(ImageView) findViewById(R.id.imageView4);
        dish=(ImageView) findViewById(R.id.imageViwes);
        food= (ImageView) findViewById(R.id.imageVies);
        home= (ImageView) findViewById(R.id.hme);
        connect= (ImageView) findViewById(R.id.hm);
        icon=(ImageView) findViewById(R.id.imageView6);
        contlpnl = (ImageView) findViewById(R.id.ctrl);
        fab= (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Custfood.this, Search_all.class);
                startActivity(intent);
                finish();
            }

        });
        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Custfood.this, Connecton.class);
                // intent.putExtra("EML",det);
                startActivity(intent);
                finish();

            }
        });

        contlpnl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Custfood.this, Contropanel.class);
                startActivity(intent);
                finish();
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Custfood.this,Home.class);
                startActivity(intent);
                finish();
            }
        });
        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Custfood.this, Customall.class);
                startActivity(intent);
                finish();
            }
        });
       /* food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Custfood.this, Custfood.class);
                startActivity(intent);
                finish();
            }
        });*/
       /* dish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Custfood.this, Custdish.class);
                startActivity(intent);
                finish();
            }
        });*/
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Custfood.this, Custpost.class);
                startActivity(intent);
                finish();
            }
        });
        serc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Custfood.this, Search_all.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
