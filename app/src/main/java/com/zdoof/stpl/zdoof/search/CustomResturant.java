package com.zdoof.stpl.zdoof.search;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.search.Details.Resturant_design;
import com.zdoof.stpl.zdoof.webservice.PicConstant;

import java.util.ArrayList;

/**
 * Created by stpl on 13/3/16.
 */
public class CustomResturant extends BaseAdapter{
    Search_resturant search_resturant;
    ArrayList<Detail> values;
    String pic,pic2,zlikes,review;
    public CustomResturant(Search_resturant search_resturant, int spiner_item, ArrayList<Detail> values) {
      this.search_resturant=search_resturant;
        this.values=values;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vgrid;
        LayoutInflater inflater = search_resturant.getLayoutInflater();
        vgrid = inflater.inflate(R.layout.custom_resturant, null);
        ImageView img= (ImageView) vgrid.findViewById(R.id.imageView13);
        RatingBar rateBar = (RatingBar) vgrid.findViewById(R.id.res_ratings);
        TextView text= (TextView) vgrid.findViewById(R.id.textView8);
        TextView text1= (TextView) vgrid.findViewById(R.id.textView10);
        TextView text2= (TextView) vgrid.findViewById(R.id.textView12);
        TextView text3= (TextView) vgrid.findViewById(R.id.textView19);
        TextView text4= (TextView) vgrid.findViewById(R.id.textView20);
        TextView viewdetails= (TextView) vgrid.findViewById(R.id.viewdetails);
        zlikes=values.get(position).getThumbsup();
        review=values.get(position).getReviews();
        text3.setText(review);
        text4.setText(zlikes);
        Float rating = Float.parseFloat(values.get(position).getRatings());
        rateBar.setRating(rating);
        rateBar.setIsIndicator(true);
        pic=values.get(position).getRest_img();

        pic2= PicConstant.PROFILE_URL1+pic;
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(search_resturant).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                        // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.noimg)
                .showImageOnLoading(R.drawable.uute).build();
        imageLoader.displayImage(pic2, img, options);
       /* Uri myUri = Uri.parse(pic2);
        Picasso.with(search_resturant)
                .load(myUri)
                .placeholder(R.drawable.load)
                .error(R.drawable.blankpeople)
                .into(img);
*/
        text.setText(values.get(position).getRestaurant_name());
        text1.setText(values.get(position).getRest_address());
        text2.setText(values.get(position).getRest_phone());

        viewdetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String resturant_id=values.get(position).getRest_id();
                Intent in=new Intent(search_resturant, Resturant_design.class);
                in.putExtra("RID",resturant_id);
                search_resturant.startActivity(in);


            }
        });
      /* pic3=values.get(position+1).getRest_img();
        pic4=PicConstant.PROFILE_URL1+pic3;
        imageLoader.displayImage(pic4, img1, options);
       *//* Uri myUri1 = Uri.parse(pic4);
        Picasso.with(search_resturant)
                .load(myUri1)
                .placeholder(R.drawable.load)
                .error(R.drawable.blankpeople)
                .into(img1);*//*
        text3.setText(values.get(position+1).getRestaurant_name());
        text4.setText(values.get(position+1).getRest_address());
        text5.setText(values.get(position+1).getRest_phone());*/
        return vgrid;
    }
    }

