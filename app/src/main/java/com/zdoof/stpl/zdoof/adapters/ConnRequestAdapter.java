package com.zdoof.stpl.zdoof.adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.CircularNetworkImageView;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.activites.Home;
import com.zdoof.stpl.zdoof.utils.ConnectionObject;
import com.zdoof.stpl.zdoof.webservice.Webservicereceipe_post;

import java.util.ArrayList;

/**
 * Created by Shiftu Bd team1 on 24-Feb-2017.
 */

public class ConnRequestAdapter extends BaseAdapter {

    ArrayList<ConnectionObject> connList;
    Context context;
    LayoutInflater inflater;
    final Webservicereceipe_post com_post = new Webservicereceipe_post();
    static String aResponse = "", bResponse = "";
    ProgressDialog progressDialog;
    Dialog dialog;
    int from_id, to_id, connID;
    int temp_pos;

    public ConnRequestAdapter(ArrayList<ConnectionObject> connList, Context context) {
        this.connList = connList;
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.progressDialog = new ProgressDialog(context);
        this.progressDialog.setMessage("Loading...Please wait.");
        this.progressDialog.setCancelable(false);
        this.progressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public int getCount() {
        return connList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.conn_request_item, null);
        CircularNetworkImageView profimg = (CircularNetworkImageView) view.findViewById(R.id.connReqImg);
        TextView name = (TextView) view.findViewById(R.id.connReqName);
        Button accept = (Button) view.findViewById(R.id.connReqAccept);
        Button decline = (Button) view.findViewById(R.id.connReqDecline);

        name.setText(connList.get(position).getFrom_username());
        if (Home.profileCache.get(connList.get(position).getFrom_userid())!=null) {
            profimg.setImageBitmap(Home.profileCache.get(connList.get(position).getFrom_userid()));
        }

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptRequestDialog(position);
            }
        });
        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                declineRequest(position);
            }
        });
        return view;
    }

    private void acceptRequestDialog(final int pos) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setContentView(R.layout.conn_adapter_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        TextView msg = (TextView) dialog.findViewById(R.id.conn_message);
        ImageView close = (ImageView) dialog.findViewById(R.id.conn_close);
        Button yes = (Button) dialog.findViewById(R.id.acceptYes);
        Button no = (Button) dialog.findViewById(R.id.acceptNo);
        msg.setText("Do you want to accept the connection request send by " +connList.get(pos).getFrom_username()+"?");
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connID = Integer.parseInt(connList.get(pos).getConnection_id());
                temp_pos = pos;
                AcceptRequestTask task = new AcceptRequestTask();
                task.execute();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void declineRequest(final int pos) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().getAttributes().width = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.setContentView(R.layout.conn_adapter_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        TextView msg = (TextView) dialog.findViewById(R.id.conn_message);
        ImageView close = (ImageView) dialog.findViewById(R.id.conn_close);
        Button yes = (Button) dialog.findViewById(R.id.acceptYes);
        Button no = (Button) dialog.findViewById(R.id.acceptNo);
        msg.setText("Do you want to decline the connection request send by " +connList.get(pos).getFrom_username()+"?");
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                from_id = Integer.parseInt(connList.get(pos).getFrom_userid());
                to_id = Integer.parseInt(connList.get(pos).getTo_userid());
                temp_pos = pos;
                DisconnectTask task = new DisconnectTask();
                task.execute();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private class DisconnectTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            aResponse = com_post.disconnect("disconnect", from_id, to_id);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (aResponse!=null) {
                progressDialog.dismiss();
                if (aResponse.equals("3")) {
                    dialog.dismiss();
                    Toast.makeText(context,"Request declined successfully!",Toast.LENGTH_SHORT).show();
                    connList.remove(temp_pos);
                    notifyDataSetChanged();
                }
                else {
                    Toast.makeText(context,"Something went wrong please try again!",Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(context,"Something went wrong please try again!",Toast.LENGTH_SHORT).show();
            }
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    class AcceptRequestTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            bResponse = com_post.acceptConnection("acceptConnection", connID);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (bResponse!=null) {
                progressDialog.dismiss();
                String[] responseCell = bResponse.split(",");
                if (responseCell[0].equals("2")) {
                    dialog.dismiss();
                    Toast.makeText(context,"Connected successfully!",Toast.LENGTH_SHORT).show();
                    connList.remove(temp_pos);
                    notifyDataSetChanged();
                }
                else {
                    Toast.makeText(context,"Something went wrong please try again!",Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(context,"Something went wrong please try again!",Toast.LENGTH_SHORT).show();
            }
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }
}
