package com.zdoof.stpl.zdoof.search.Details;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.webservice.PicConstant;
import com.zdoof.stpl.zdoof.webservice.Webservicereceipe_post;
import com.zdoof.stpl.zdoof.webservice.webservice_searchdetails;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by stpl on 22/6/16.
 */
public class View_dish_Deatails extends Activity{
    LinearLayout recdetails,ingradiants;
    TextView dname,review,likes,postby,cmethod,ctg,lvl,ckt,cmnalerg,rcpdtl,text,textingrad;
    ImageView dishpic;
    String pic,pic2,postedby,userid,dish_id,Dish_Name,Preferred_time,Cooking_Method,Dish_desc,thudish,total_review_dish,details,
            Dish_picture,Name,Allergy_info,Expdate,dish, names,type,date,pfimg,comments,zlikes,rvondish,restneme;
    int disid,resturant_idd=0;
    String resturant_name,comment,createdby,Resturant_id,resturant_id,resturant_namee,ratingg,star,Star_review;
    ArrayList<String>Dish,Dish2;
    ArrayList<Detail>Dish1;
    ArrayList<String> values;
    TextView rtng;
    RatingBar ratingBar,rating;
    EditText yrreview;
    AutoCompleteTextView dishname;
    Button riv;
    ListView list,rev_list;
    int riid1,dishid;
    static String bResponse,gResponse,cResponse;
    String[] bresponce = {};
    SharedPreferenceClass sharedPreferenceClass;
    String createby;
    final Webservicereceipe_post com1 = new Webservicereceipe_post();
    final webservice_searchdetails com2 = new webservice_searchdetails();
    ProgressDialog reviewRatingDilog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewdetail_new);
        reviewRatingDilog = new ProgressDialog(View_dish_Deatails.this);
        reviewRatingDilog.setMessage("Loading... Please wait.");
        dishpic= (ImageView) findViewById(R.id.imageView13);
        dname= (TextView) findViewById(R.id.textView18);
        review=(TextView) findViewById(R.id.textView19);
        likes=(TextView) findViewById(R.id.textView20);
        postby=(TextView) findViewById(R.id.textView1);
        cmethod=(TextView) findViewById(R.id.textVi);
        ctg=(TextView) findViewById(R.id.cat_name);
        lvl=(TextView) findViewById(R.id.lev_item);
        ckt=(TextView) findViewById(R.id.cook_tym);
        cmnalerg=(TextView) findViewById(R.id.cmn_all);
        rcpdtl=(TextView) findViewById(R.id.rcpdtl);
        recdetails= (LinearLayout) findViewById(R.id.recpdtls);
        ingradiants= (LinearLayout) findViewById(R.id.ingrad);
        //textingrad=(TextView) findViewById(R.id.ingarien);
        text=(TextView) findViewById(R.id.recp);
        rating= (RatingBar) findViewById(R.id.ratings);
        rating.setIsIndicator(true);
        list= (ListView) findViewById(R.id.listView);
        rev_list= (ListView) findViewById(R.id.rev_list);
        sharedPreferenceClass=new SharedPreferenceClass(View_dish_Deatails.this);
        createby=sharedPreferenceClass.getValue_string("UIDD");
        values=new ArrayList<String>();
        yrreview= (EditText) findViewById(R.id.review);
        dishname= (AutoCompleteTextView) findViewById(R.id.dishname);
        riv= (Button) findViewById(R.id.viw);
        rtng= (TextView)findViewById(R.id.ratetyp);
        ratingBar= (RatingBar) findViewById(R.id.rating);
        AsyncCallWS task = new AsyncCallWS();
        task.execute();
         AsyncCallWS3 task1 = new AsyncCallWS3();
         task1.execute();
        String format = "dd/MMM/yyyy H:mm:ss";
        final SimpleDateFormat sdf = new SimpleDateFormat(format);

        riv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment=yrreview.getText().toString().trim();
                resturant_namee = dishname.getText().toString().trim();
                Float rate = ratingBar.getRating();
                ratingg = String.format("%.1f",rate);
                if (comment.length()>150 || resturant_namee.length()>150) {
                    Toast.makeText(View_dish_Deatails.this,"Your review should be less than 150 charecters!",Toast.LENGTH_SHORT).show();
                }
                else {
                    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Expdate = sdf.format(new Date());
                    yrreview.setText("");
                    dishname.setText("");
                    ratingBar.setRating(0.0F);
                    AsyncCallWS2 task = new AsyncCallWS2();
                    task.execute();
                }
            }
        });

        Dish_Name=getIntent().getStringExtra("Dname");
        Preferred_time=getIntent().getStringExtra("PFTM");
        Cooking_Method=getIntent().getStringExtra("CKM");
        Dish_desc=getIntent().getStringExtra("Dsc");
        thudish=getIntent().getStringExtra("Tl");
        total_review_dish=getIntent().getStringExtra("TR");
        Name=getIntent().getStringExtra("Nm");
        Allergy_info=getIntent().getStringExtra("algf");
        pic=getIntent().getStringExtra("Dp");
        details=getIntent().getStringExtra("Details");
        values=getIntent().getStringArrayListExtra("ARRAY");
        dish=getIntent().getStringExtra("dsid");
        star=getIntent().getStringExtra("STAR");
        //ingradiant_name=getIntent().getStringExtra("InG");
       /* in_id=getIntent().getStringExtra("InGG");
        quantity=getIntent().getStringExtra("qnty");
        wgt=getIntent().getStringExtra("WHT");
        dish=getIntent().getStringExtra("dsid");*/
       // createdby=getIntent().getStringExtra("UID");
        riid1=Integer.parseInt(createby);
        disid=Integer.parseInt(dish);
       // value=(ingradiant_name+" " + in_id+" " +quantity+" " + wgt).toUpperCase();
        pic2= PicConstant.PROFILE_URL1+pic.substring(1);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(View_dish_Deatails.this).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.noimg)
                .showImageOnLoading(R.drawable.uute).build();
        imageLoader.displayImage(pic2, dishpic, options);
        dname.setText(Dish_Name);
        //review.setText(total_review_dish + "reviews");
        review.setText(total_review_dish);
        likes.setText(thudish);
        postby.setText(Name);
        ckt.setText(Preferred_time);
        cmnalerg.setText(Allergy_info);
        cmethod.setText(Html.fromHtml(details));
        rating.setRating(Float.parseFloat(star));
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rtng.setText(String.valueOf(rating));
                //ratingg=String.valueOf(rating);
            }
        });
        rcpdtl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recdetails.setVisibility(View.VISIBLE);
                ingradiants.setVisibility(View.VISIBLE);
                ArrayAdapter<String> adapter=new ArrayAdapter<String>(View_dish_Deatails.this,R.layout.spiner_item,values);
                list.setAdapter(adapter);
                setListViewHeightBasedOnItems(list);
               // textingrad.setText(value);
                text.setText(Html.fromHtml(Cooking_Method));
            }
        });

    }
    class AsyncCallWS2 extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            reviewRatingDilog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");
            if (ratingg==null || ratingg.equals("null")){
                ratingg = "0";
            }
            bResponse = com2.AddReview("add_review_dish", disid,resturant_idd,resturant_namee,ratingg,comment,riid1,Expdate);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            reviewRatingDilog.dismiss();
            if (bResponse.equals("0")) {
                Toast.makeText(View_dish_Deatails.this, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (bResponse.equals("1")) {
                //Toast.makeText(View_dish_Deatails.this, "S!", Toast.LENGTH_LONG).show();
                AsyncCallWS3 task = new AsyncCallWS3();
                task.execute();
            } else {

            }

        }
    }

    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            gResponse = com2.resturant_autocomp("rest_auto_complete");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            try {
                JSONArray jr = new JSONArray(gResponse);
                Dish=new ArrayList<String>();
                Dish2=new ArrayList<String>();
                for (int i = 0; i < jr.length(); i++) {
                    JSONObject jsonObject = jr.getJSONObject(i);
                    resturant_name = jsonObject.getString("Restaurant_name");
                    Resturant_id=jsonObject.getString("Restaurant_id");
                    Dish.add(resturant_name);
                    Dish2.add(Resturant_id);
                }
                ArrayAdapter<String> adapter=new ArrayAdapter<String>(View_dish_Deatails.this,android.R.layout.simple_list_item_1,Dish);
                dishname.setAdapter(adapter);
                dishname.setThreshold(1);
                dishname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        resturant_namee=Dish.get(position).toString();
                        resturant_id=Dish2.get(position);
                        resturant_idd=Integer.parseInt(resturant_id);

                    }
                });

                // progressDialog.dismiss();

            } catch (Exception e) {

            }

        }

    }
    class AsyncCallWS3 extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            cResponse = com2.review("ret_dish_review", disid);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            try {
                JSONArray jr = new JSONArray(cResponse);
                Dish1=new ArrayList<Detail>();
                for (int i = 0; i < jr.length(); i++) {
                    Detail pro=new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);
                    names = jsonObject.getString("Name");
                    comments = jsonObject.getString("Comment");
                    type = jsonObject.getString("DRvtext");
                    if (jsonObject.has("thurevwdish")) {
                        zlikes = jsonObject.getString("thurevwdish");
                    }
                    date=jsonObject.getString("Date");
                    pfimg=jsonObject.getString("profimg");
                    restneme=jsonObject.getString("onreview_rest");
                    Star_review=jsonObject.getString("Star_review");
                    pro.setName(names);
                    pro.setComment(comments);
                    pro.setWeight(type);
                    pro.setDcreateon(date);
                    pro.setPost_like(zlikes);
                    pro.setPfimage(pfimg);
                    pro.setRestaurant_name(restneme);
                    pro.setAt(Star_review);
                    Dish1.add(pro);
                }

                Cut_dish_review adapter=new Cut_dish_review(View_dish_Deatails.this,R.layout.spiner_item,Dish1);
                rev_list.setAdapter(adapter);
                setListViewHeightBasedOnItems(rev_list);
            }catch (Exception e){
                Log.v("","");
            }

                if (cResponse.equals("0")) {
                    Toast.makeText(View_dish_Deatails.this, "Sorry try again!", Toast.LENGTH_LONG).show();

                } else if (cResponse.equals("1")) {
                    //Toast.makeText(View_dish_Deatails.this, "S!", Toast.LENGTH_LONG).show();


                } else {

                }
            }
        }

    public  boolean setListViewHeightBasedOnItems(ListView cmntlist) {

        ListAdapter listAdapter = cmntlist.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems;itemPos++) {
                View item = listAdapter.getView(itemPos, null, cmntlist);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = cmntlist.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = cmntlist.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            cmntlist.setLayoutParams(params);
            cmntlist.requestLayout();
            return true;

        } else {
            return false;
        }

    }

    @Override
    protected void onStop() {
        if (reviewRatingDilog!=null){
            reviewRatingDilog.cancel();
        }
        super.onStop();
    }
}
