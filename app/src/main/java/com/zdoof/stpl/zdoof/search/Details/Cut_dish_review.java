package com.zdoof.stpl.zdoof.search.Details;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.CircularNetworkImageView;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.webservice.PicConstant;

import java.util.ArrayList;

/**
 * Created by stpl on 27/6/16.
 */
public class Cut_dish_review extends BaseAdapter{
    View_dish_Deatails view_dish_deatails;
    ArrayList<Detail> dish1;
    String pic,pic1,comment,names,dates,star_riview;

    public Cut_dish_review(View_dish_Deatails view_dish_deatails, int spiner_item, ArrayList<Detail> dish1) {
        this.view_dish_deatails=view_dish_deatails;
        this.dish1=dish1;
    }
    @Override
    public int getCount() {
        return dish1.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = view_dish_deatails.getLayoutInflater();
        view = inflater.inflate(R.layout.custom_coment_detaitail, null);
        CircularNetworkImageView circularImageView = (CircularNetworkImageView) view.findViewById(R.id.profilePictureVie);
        TextView nam = (TextView) view.findViewById(R.id.textVie1);
        TextView types = (TextView) view.findViewById(R.id.textV1);
        TextView zlikes=(TextView) view.findViewById(R.id.textView20);
        TextView rst= (TextView) view.findViewById(R.id.textV11);
        TextView datess=(TextView) view.findViewById(R.id.textV12);
        RatingBar ratingBar= (RatingBar) view.findViewById(R.id.ratings);
        //ImageView down= (ImageView) view.findViewById(R.id.fdd);
        TextView cmty=(TextView) view.findViewById(R.id.tb);
        
        pic=dish1.get(position).getPfimage();
        pic1= PicConstant.PROFILE_URL1 + pic;

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(view_dish_deatails).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.blankpeople)
                .showImageOnLoading(R.drawable.uute).build();
        imageLoader.displayImage(pic1, circularImageView, options);
        comment=dish1.get(position).getComment();
        names=dish1.get(position).getName();
        dates=dish1.get(position).getDcreateon();
        star_riview=dish1.get(position).getAt();
        nam.setText(names);
        types.setText(dish1.get(position).getWeight());
        rst.setText(dish1.get(position).getRestaurant_name());
        datess.setText("on"+ " " +dates);
        cmty.setText(comment);
        zlikes.setText(dish1.get(position).getPost_like());
        ratingBar.setRating(Float.parseFloat(star_riview));
        ratingBar.setIsIndicator(true);

       /*down.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               ArrayAdapter adapter=new ArrayAdapter(comments,R.layout.spiner_item,gg);
                sp.setAdapter(adapter);
           }

       });*/

        return view;

    }
}
