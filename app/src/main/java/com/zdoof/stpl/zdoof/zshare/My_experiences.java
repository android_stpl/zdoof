package com.zdoof.stpl.zdoof.zshare;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.activites.Home;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.adapters.CusAdapters2;
import com.zdoof.stpl.zdoof.webservice.WebserviceCallpost;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by stpl on 13/4/16.
 */
public class My_experiences extends Fragment {
    int[]img={R.drawable.friend,R.drawable.frnds_frnds,R.drawable.pblic};
    String[]catgory={"Friends","Friends of friends","Public"};
    String imageFilePath,imageFilePath1,imageFilePath2,Expdate;
    int SELECT_FILE = 1;
    int REQUEST_CAMERA = 2;
    int SELECT_FILE1 = 3;
    int REQUEST_CAMERA1 = 4;
    LinearLayout camera,send;
    EditText exper;
    LinearLayout ll1, ll2, ll3, ll4;
    String imageName,imageName1,imageName2;
    String yourexp,uid;
    TextView Share,photo;
    ProgressDialog progressDialog;
    int uerid,dishid=0,posttype=2,placetype=0,createdby,divicetype=1;
    Spinner friends;
    ImageView im1,im2,ip1,ip2,is1,is2,chek,active1,active2,active3,active4,send2;
    ImageView enjoish,postdish,dishnrcp,checkin,close1;
    ImageView close,addimage,files,files1,files2,files3,enjoydish,Enjoydish,frsticn,sndicn,trdicn;
    static String aResponse;
    String placename="",dishname="",postcomment,createdon,ImageNName="";
    SharedPreferenceClass sharedPreferenceClass;
    //Context context;
    final WebserviceCallpost comp = new WebserviceCallpost();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View rootView =  inflater.inflate(R.layout.myexperiences,null);

            sharedPreferenceClass=new SharedPreferenceClass(getActivity());
            uid=sharedPreferenceClass.getValue_string("UIDD");
            friends = (Spinner)rootView.findViewById(R.id.spinner4);
            // close1= (ImageView) findViewById(R.id.close1);
            send=(LinearLayout) rootView.findViewById(R.id.pst);
            enjoish= (ImageView)rootView.findViewById(R.id.enjd);
            dishnrcp= (ImageView)rootView.findViewById(R.id.postd);
            checkin= (ImageView)rootView.findViewById(R.id.chkin);
            chek= (ImageView)rootView.findViewById(R.id.imageView1);
            camera= (LinearLayout)rootView.findViewById(R.id.rl);
            exper= (EditText)rootView.findViewById(R.id.editTextRefractor1);
            frsticn= (ImageView)rootView.findViewById(R.id.frst);
            sndicn= (ImageView)rootView.findViewById(R.id.secnd);
            trdicn= (ImageView)rootView.findViewById(R.id.trd);
            close = (ImageView)rootView.findViewById(R.id.close);
            photo= (TextView) rootView.findViewById(R.id.pho);
            // send1 = (RelativeLayout) findViewById(R.id.pst);
            Share= (TextView) rootView.findViewById(R.id.hgh);
            /*ll1 = (LinearLayout)rootView.findViewById(R.id.hmj);
            ll2 = (LinearLayout)rootView.findViewById(R.id.cnn);
            ll3 = (LinearLayout)rootView.findViewById(R.id.ntf);
            ll4 = (LinearLayout)rootView.findViewById(R.id.ct);*/
            //  unit = (Spinner) findViewById(R.id.spinner5);
            //addimage = (ImageView)rootView.findViewById(R.id.addimg);
            files = (ImageView) rootView.findViewById(R.id.image);
            files1 = (ImageView) rootView.findViewById(R.id.images);
            files2 = (ImageView) rootView.findViewById(R.id.images1);
            send2= (ImageView) rootView.findViewById(R.id.pst1);

            uerid=Integer.parseInt(uid);
            createdby=uerid;
       /* Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        Expdate = df.format(c.getTime());*/
            String format = "dd/MMM/yyyy H:mm:ss";
            final SimpleDateFormat sdf = new SimpleDateFormat(format);
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            Expdate = sdf.format(new Date());
            createdon=Expdate;
            CusAdapters2 adapter4=new CusAdapters2(getActivity(),R.layout.spiner_item,catgory,img);
            friends.setAdapter(adapter4);
       /* ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll2.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent = new Intent(getActivity(), Connecton.class);
                // intent.putExtra("EML",det);
                getActivity().startActivity(intent);
                getActivity().finish();

            }
        });
        ll3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), Notifications.class);
                // intent.putExtra("EML",det);
                getActivity().startActivity(intent);
                getActivity().finish();

            }
        });

        ll4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll4.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent = new Intent(getActivity(), Contropanel.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll1.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent = new Intent(getActivity(), Home.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });*/

          /*  enjoish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    sharedPreferenceClass.setValue_boolean("MYEX", true);
                    Intent intent = new Intent(getActivity(), Enjoyeddish.class);
                   getActivity().startActivity(intent);
                    getActivity().finish();
                }
            });
            dishnrcp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // active1.setVisibility(View.INVISIBLE);
                    active3.setVisibility(View.VISIBLE);
                    sharedPreferenceClass.setValue_boolean("DISH", true);
                    Intent intent = new Intent(My_experience.this, Post.class);
                    startActivity(intent);

                }
            });
            checkin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    active1.setVisibility(View.INVISIBLE);
                    active4.setVisibility(View.VISIBLE);
                    sharedPreferenceClass.setValue_boolean("CHK",true);
                    Intent intent = new Intent(My_experience.this, Chekin.class);
                    startActivity(intent);
                }
            });*/


            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Share.setTextColor(Color.parseColor("#4CAF50"));
                    yourexp=exper.getText().toString();
                    if(yourexp.equals("") /*|| imageFilePath==null *//*|| imageFilePath1==null || imageFilePath2==null*/){
                        Toast.makeText(getActivity(), "This Field is requried", Toast.LENGTH_SHORT).show();
                    }else{
                        AsyncCallWS task = new AsyncCallWS();
                        task.execute();
                        progressDialog = ProgressDialog.show(getActivity(), "", "Loading...");
                    }
                }
            });
          /*  close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(My_experience.this,Home.class);
                    startActivity(intent);
                    finish();
                }
            });*/
            camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    photo.setTextColor(Color.parseColor("#4CAF50"));
                    if(imageFilePath==null || imageFilePath1==null || imageFilePath2==null) {
                        final CharSequence[] items = {"Take Photo", "Choose from Library",
                                "Cancel"};

                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("Add Photo!");
                        builder.setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                if (items[item].equals("Take Photo")) {
                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intent, REQUEST_CAMERA);
                                } else if (items[item].equals("Choose from Library")) {
                                    Intent intent = new Intent(
                                            Intent.ACTION_PICK,
                                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    intent.setType("image/*");
                                    startActivityForResult(
                                            Intent.createChooser(intent, "Select File"), SELECT_FILE);

                                } else if (items[item].equals("Cancel")) {
                                    dialog.dismiss();
                                }

                            }
                        });

                        builder.show();
                    }else{
                        Toast.makeText(getActivity(), "You Have already Choose Three Images", Toast.LENGTH_SHORT).show();
                    }
                }
            });
return rootView;
        }
        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {

            if (resultCode == Activity.RESULT_OK && requestCode == SELECT_FILE){
                onSelectFromGalleryResult(data);
            }
       /*else if(resultCode == Activity.RESULT_OK && requestCode == SELECT_FILE1){
            onSelectFromGalleryResult1(data);
        }*/
            else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CAMERA){
                onCaptureImageResult(data);
            }
        /*else if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CAMERA1){
            onCaptureImageResult1(data);
        }*/
            else{
                Toast.makeText(getActivity(), "Please try again !!!", Toast.LENGTH_SHORT).show();
            }


            super.onActivityResult(requestCode, resultCode, data);
        }

    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        if(imageFilePath==null){
            imageFilePath = destination.toString();


            File imagefile = new File(imageFilePath);
            FileInputStream fis = null;
                   /* try {
                        fis = new FileInputStream(imagefile);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    Bitmap bm = BitmapFactory.decodeStream(fis);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100 , baos);
                    byte[] b = baos.toByteArray();
                    imageName = Base64.encodeToString(b, Base64.DEFAULT);*/
            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            files.setImageBitmap(thumbnail);
            byte[] b = bytes.toByteArray();
            imageName = Base64.encodeToString(b, Base64.DEFAULT);

            frsticn.setVisibility(View.VISIBLE);
            frsticn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    files.setImageBitmap(null);
                    imageFilePath=null;
                    frsticn.setVisibility(View.INVISIBLE);
                }
            });
        }else if(imageFilePath1==null){
            imageFilePath1 = destination.toString();


            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            files1.setImageBitmap(thumbnail);
            byte[] b = bytes.toByteArray();
            imageName1 = Base64.encodeToString(b, Base64.DEFAULT);
            sndicn.setVisibility(View.VISIBLE);
            sndicn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    files1.setImageBitmap(null);
                    imageFilePath1=null;
                    sndicn.setVisibility(View.INVISIBLE);
                }
            });
        }else if(imageFilePath2==null){
            imageFilePath2 = destination.toString();


            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            files2.setImageBitmap(thumbnail);
            byte[] b = bytes.toByteArray();
            imageName2 = Base64.encodeToString(b, Base64.DEFAULT);
            trdicn.setVisibility(View.VISIBLE);
            trdicn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    files2.setImageBitmap(null);
                    imageFilePath2=null;
                    trdicn.setVisibility(View.INVISIBLE);
                }
            });
        }
        if(imageName!=null){
            ImageNName=imageName;
        }else if(imageName1!=null){
            ImageNName=imageName +"$"+imageName1;
        }else if(imageName2!=null){
            ImageNName=imageName +"$"+imageName1 +"$" +imageName2;
        }else{
            ImageNName="";
        }


    }

    /* private void onSelectFromGalleryResult(Intent data) {
         Uri selectedImageUri = data.getData();
         String[] projection = {MediaStore.MediaColumns.DATA};
         Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                 null);
         int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
         cursor.moveToFirst();

         imageFilePath = cursor.getString(column_index).toString();

         BitmapFactory.decodeFile(imageFilePath);

         Bitmap bm = BitmapFactory.decodeFile(imageFilePath);

         files.setImageBitmap(bm);

     }*/
    private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = getActivity().managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        if(imageFilePath==null){
            imageFilePath = cursor.getString(column_index).toString();

            BitmapFactory.decodeFile(imageFilePath);
            Bitmap bm1 = BitmapFactory.decodeFile(imageFilePath);
            File imagefile = new File(imageFilePath);
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(imagefile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            Bitmap bm = BitmapFactory.decodeStream(fis);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();
            imageName = Base64.encodeToString(b, Base64.DEFAULT);
            // ImageNName="@" + imageName;
            files.setImageBitmap(bm1);
            frsticn.setVisibility(View.VISIBLE);
            frsticn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    files.setImageBitmap(null);
                    imageFilePath=null;
                    frsticn.setVisibility(View.INVISIBLE);
                }
            });
        }
        else if(imageFilePath1==null){
            imageFilePath1 = cursor.getString(column_index).toString();

            BitmapFactory.decodeFile(imageFilePath1);
            Bitmap bm1 = BitmapFactory.decodeFile(imageFilePath1);
            File imagefile = new File(imageFilePath1);
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(imagefile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            Bitmap bm = BitmapFactory.decodeStream(fis);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();
            imageName1 = Base64.encodeToString(b, Base64.DEFAULT);
            files1.setImageBitmap(bm1);
            sndicn.setVisibility(View.VISIBLE);
            sndicn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    files1.setImageBitmap(null);
                    imageFilePath1=null;
                    sndicn.setVisibility(View.INVISIBLE);
                }
            });
        }
        else if(imageFilePath2==null){
            imageFilePath2 = cursor.getString(column_index).toString();

            BitmapFactory.decodeFile(imageFilePath2);
            Bitmap bm1 = BitmapFactory.decodeFile(imageFilePath2);
            File imagefile = new File(imageFilePath2);
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(imagefile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            Bitmap bm = BitmapFactory.decodeStream(fis);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();
            imageName2 = Base64.encodeToString(b, Base64.DEFAULT);
            files2.setImageBitmap(bm1);
            trdicn.setVisibility(View.VISIBLE);
            trdicn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    files2.setImageBitmap(null);
                    imageFilePath2=null;
                    trdicn.setVisibility(View.INVISIBLE);
                }
            });

        }
        if(imageName!=null){
            ImageNName=imageName;
        }else if(imageName1!=null){
            ImageNName=imageName +"$"+imageName1;
        }else if(imageName2!=null){
            ImageNName=imageName +"$"+imageName1 +"$" +imageName2;
        }else{
            ImageNName="";
        }
       /*else if(imageFilePath3.equals(""){
           imageFilePath3 = cursor.getString(column_index).toString();

           BitmapFactory.decodeFile(imageFilePath);

           Bitmap bm = BitmapFactory.decodeFile(imageFilePath);

           files3.setImageBitmap(bm);
       }*/
    }
    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

          //  aResponse = comp.Experience("insert_post", uerid, placename,dishid,dishname,Expdate,posttype,yourexp,ImageNName,placetype,divicetype,createdby,createdon);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");


            if (aResponse.equals("0")) {
                Toast.makeText(getActivity(), "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (aResponse.equals("1")) {
                progressDialog.dismiss();
               // Toast.makeText(getActivity(), "You have successfully Posted !", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), Home.class);
                getActivity().startActivity(intent);
                getActivity().finish();

            } else {

            }
        }

    }
}
