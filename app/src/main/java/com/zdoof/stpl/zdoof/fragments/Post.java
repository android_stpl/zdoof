package com.zdoof.stpl.zdoof.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.zdoof.stpl.zdoof.activites.Profiles;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.adapters.Custom_post;
import com.zdoof.stpl.zdoof.utils.AppUtil;
import com.zdoof.stpl.zdoof.utils.ImageListener;
import com.zdoof.stpl.zdoof.webservice.Webservicereceipe_post;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Ratan on 7/29/2015.
 */
public class Post extends Fragment {
    ListView pos;
    final Webservicereceipe_post com1 = new Webservicereceipe_post();
    static String bResponse;
    int Loggin_id;

    ProgressDialog progressDialog;
    ArrayList<Detail>Post;
    SharedPreferenceClass sharedPreferenceClass;
    ImageListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (ImageListener) ((Activity) context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView=inflater.inflate(R.layout.postt,null);


        pos=(ListView)rootView.findViewById(R.id.cntction1);

        sharedPreferenceClass= new SharedPreferenceClass(getActivity());
        String rid = sharedPreferenceClass.getValue_string("UDDIDD").toString();
        Loggin_id=Integer.parseInt(rid);

        return rootView;
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            AsyncCallWS2 task = new AsyncCallWS2();
            task.execute();

            if (!isVisibleToUser) {
                Log.d("MyFragment", "Not visible anymore.  Stopping audio.");
                // TODO stop audio playback
            }
        }
    }
    class AsyncCallWS2 extends AsyncTask<Void, Void, Void> {
       @Override
        protected void onPreExecute() {
            super.onPreExecute();
           //progressDialog = ProgressDialog.show(getActivity(), "", "Loading...");
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");
            bResponse = com1.post("post_report",Loggin_id);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
           //progressDialog.dismiss();
            try {
                JSONArray jr = new JSONArray(bResponse);
                Post = new ArrayList<Detail>();

                for (int i = 0; i < jr.length(); i++) {
                    JSONObject jsonObject = jr.getJSONObject(i);
                    Detail pr=new Detail();
                    String post_type = jsonObject.getString("Post_type");
                    String Post_cmmnt = jsonObject.getString("Details");
                    String Creton = jsonObject.getString("Created_on");
                    //Creton = AppUtil.getDateAndTimeFromTimestamp(getActivity(), Creton);
                    Creton = convertTime(Creton);
                    pr.setPOST_TYPE(post_type);
                    pr.setPOST_COMMENT(Post_cmmnt);
                    pr.setPOSTDATE(Creton);
                    Post.add(pr);
                }
                Custom_post adapter=new Custom_post(getActivity(),R.layout.spiner_item,Post);
                pos.setAdapter(adapter);
                setListViewHeightBasedOnChildren(pos);


                   /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(Home.this, android.R.layout.simple_list_item_1, Dish);
                    item.setAdapter(adapter);
                    item.setThreshold(1);
                    item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            diSh_id = Dish1.get(position);
                            ddish_id = Integer.parseInt(diSh_id);
                        }
                    });*/

                // progressDialog.dismiss();

            } catch (Exception e) {
                Log.e("Post", "Error", e);
            }
        }

        private String convertTime(String jTime) {
            String returnString = null;
            returnString = jTime.substring(6, jTime.length()-2);
            long epoch = Long.parseLong(returnString);
            //returnString = new java.text.SimpleDateFormat("dd/MMM/yyyy 'at' HH:mm:ss aaa").format(new java.util.Date (epoch));
            Date date = new Date(epoch-5400000);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy 'at' HH:mm:ss aaa");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            returnString = sdf.format(date);
            return returnString;
        }
    }
   public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }


}
