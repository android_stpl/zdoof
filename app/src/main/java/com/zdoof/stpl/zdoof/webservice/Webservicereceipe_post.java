package com.zdoof.stpl.zdoof.webservice;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.AndroidHttpTransport;

/**
 * Created by stpl on 9/6/16.
 */
public class Webservicereceipe_post {
    String namespace = "http://tempuri.org/";
    private String url = "https://www.zdoof.com/web-services-android/zdoof-and-myreports.asmx";
    SoapPrimitive resultString;
    String SOAP_ACTION;
    SoapObject request = null, objMessages = null;
    SoapSerializationEnvelope envelope;
    AndroidHttpTransport androidHttpTransport;

    public Webservicereceipe_post() {

    }

    /**
     * Set Envelope
     */
    protected void SetEnvelope() {

        try {

            // Creating SOAP envelope
            envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            //You can comment that line if your web service is not .NET one.
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            androidHttpTransport = new AndroidHttpTransport(url);
            androidHttpTransport.debug = true;
           /* androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapObject result = (SoapObject)envelope.getResponse();*/

        } catch (Exception e) {
            System.out.println("Soap Exception---->>>" + e.toString());
        }
    }


    //////////////////////
    public String receipe(String MethodName, int login_id, int user_id) {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();


            weightProp.setName("Logged_id");
            weightProp.setValue(login_id);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            weightProp1.setName("User_id");
            weightProp1.setValue(user_id);
            weightProp1.setType(String.class);
            request.addProperty(weightProp1);


            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }

      public String getUserDtl(String MethodName,int Logged_id,int User_id){

          try {
              SOAP_ACTION = namespace + MethodName;

              //Adding values to request object
              request = new SoapObject(namespace, MethodName);

              //Adding Double value to request object
              PropertyInfo weightProp = new PropertyInfo();
              PropertyInfo weightProp1 = new PropertyInfo();

              weightProp1.setName("Logged_id");
              weightProp1.setValue(Logged_id);
              weightProp1.setType(String.class);
              request.addProperty(weightProp1);

              weightProp.setName("User_id");
              weightProp.setValue(User_id);
              weightProp.setType(String.class);
              request.addProperty(weightProp);
              SetEnvelope();

              try {

                  //SOAP calling webservice
                  androidHttpTransport.call(SOAP_ACTION, envelope);

                  //Got Webservice response
                  String result = envelope.getResponse().toString();

                  return result;

              } catch (Exception e) {
                  // TODO: handle exception
                  return e.toString();
              }
          } catch (Exception e) {
              // TODO: handle exception
              return e.toString();
          }
      }

    public String acceptConnection(String MethodName,int connectionid){

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();

            weightProp.setName("connectionid");
            weightProp.setValue(connectionid);
            weightProp.setType(String.class);
            request.addProperty(weightProp);

            SetEnvelope();
            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }
    }

    public String disconnect(String MethodName, int logged_id, int user_id){

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();

            weightProp.setName("Logged_id");
            weightProp.setValue(logged_id);
            weightProp.setType(Integer.class);
            request.addProperty(weightProp);

            weightProp1.setName("User_id");
            weightProp1.setValue(user_id);
            weightProp1.setType(Integer.class);
            request.addProperty(weightProp1);

            SetEnvelope();
            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }
    }

    public String retConnection(String MethodName, int logged_id){

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();

            weightProp.setName("Logged_id");
            weightProp.setValue(logged_id);
            weightProp.setType(Integer.class);
            request.addProperty(weightProp);


            SetEnvelope();
            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }
    }

    public String post(String MethodName, int login_id) {

        try {
            SOAP_ACTION = namespace + MethodName;

            //Adding values to request object
            request = new SoapObject(namespace, MethodName);

            //Adding Double value to request object
            PropertyInfo weightProp = new PropertyInfo();
            PropertyInfo weightProp1 = new PropertyInfo();

            weightProp.setName("Created_by");
            weightProp.setValue(login_id);
            weightProp.setType(String.class);
            request.addProperty(weightProp);


            SetEnvelope();

            try {

                //SOAP calling webservice
                androidHttpTransport.call(SOAP_ACTION, envelope);

                //Got Webservice response
                String result = envelope.getResponse().toString();

                return result;

            } catch (Exception e) {
                // TODO: handle exception
                return e.toString();
            }
        } catch (Exception e) {
            // TODO: handle exception
            return e.toString();
        }

    }


}
