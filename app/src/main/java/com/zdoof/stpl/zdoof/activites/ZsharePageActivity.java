package com.zdoof.stpl.zdoof.activites;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.fragmentpageradapter.Zshare_page_adapter;
import com.zdoof.stpl.zdoof.views.Contropanel;

/**
 * Created by stpl on 13/4/16.
 */
public class ZsharePageActivity extends AppCompatActivity{
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int int_items = 5 ;
    LinearLayout ll1, ll2, ll3, ll4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myexp);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.cook));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.review));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.dish));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.check));
        
        ll1 = (LinearLayout)findViewById(R.id.hmj);
        ll2 = (LinearLayout)findViewById(R.id.cnn);
        ll3 = (LinearLayout)findViewById(R.id.ntf);
        ll4 = (LinearLayout)findViewById(R.id.ct);

        ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll2.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent = new Intent(ZsharePageActivity.this, Connecton.class);
                // intent.putExtra("EML",det);
              startActivity(intent);
              finish();

            }
        });
        ll3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ZsharePageActivity.this, Notifications.class);
                // intent.putExtra("EML",det);
               startActivity(intent);
                finish();

            }
        });
        ll4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll4.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent = new Intent(ZsharePageActivity.this, Contropanel.class);
               startActivity(intent);
                finish();
            }
        });
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll1.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent = new Intent(ZsharePageActivity.this, Home.class);
              startActivity(intent);
             finish();
            }
        });
        //tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.menu));
        // tabLayout.addTab(tabLayout.newTab().setText("Experience"));
        //  tabLayout.addTab(tabLayout.newTab().setText("Make Payment"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        Zshare_page_adapter adapter = new Zshare_page_adapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }
        });
    }
}
