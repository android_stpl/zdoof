package com.zdoof.stpl.zdoof.activites;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.adapters.AndroidImageAdapter;

public class AndroidImageSlider extends Activity {
    public static Context context;
    public String ImageURLS;
    public static String[] URLS =null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activit_image_slider);
        context = this;
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        LinearLayout layout = (LinearLayout)findViewById(R.id.imageSlider);
        layout.getLayoutParams().height = (int)(height * 0.7);
        Bundle extras = getIntent().getExtras();
        if(extras == null){
            ImageURLS = null;

        } else {
            ImageURLS = extras.getString("ImageURLS");
            String newImgURLS = ImageURLS.substring(1,ImageURLS.length()-1);
            URLS = newImgURLS.split(",");
            ImageURLS = null;

        }
        ViewPager mViewPager = (ViewPager) findViewById(R.id.viewPageAndroid);

        AndroidImageAdapter adapterView = new AndroidImageAdapter(this);
        mViewPager.setAdapter(adapterView);

        ImageButton imageButton = (ImageButton)findViewById(R.id.btnClose);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ImageButton imageButton1 = (ImageButton)findViewById(R.id.btnMaximise);
        imageButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Gets linearlayout
                LinearLayout layout = (LinearLayout)findViewById(R.id.imageSlider);
                layout.getLayoutParams().height = 1100;

                layout.requestLayout();

            }
        });

        ImageButton imageButton2 = (ImageButton)findViewById(R.id.btnMinimise);
        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout layout = (LinearLayout)findViewById(R.id.imageSlider);
                layout.getLayoutParams().height = 1000;

                layout.requestLayout();
            }
        });

    }

}
