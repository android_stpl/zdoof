package com.zdoof.stpl.zdoof.activites;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by stpl on 9/2/16.
 */
public class ForgotPasswordActivity extends Activity {
    EditText email,newpass,confpass;
    Button submit;
    String _email;
    final WebserviceCall com = new WebserviceCall();
    static String aResponse,bResponse;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgetpassword);
        email= (EditText) findViewById(R.id.eml);

       /* newpass= (EditText) findViewById(R.id.newpas);
        confpass= (EditText) findViewById(R.id.confrm);*/
        submit= (Button) findViewById(R.id.submt);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _email=email.getText().toString();
                if(_email.equals("") || _email.equals(" ")){
                    Toast.makeText(ForgotPasswordActivity.this,"Invalid Email id!!",Toast.LENGTH_SHORT).show();

                }else if(!isValidEmail(_email)) {
                    email.setError("Invalid Email");
                    email.requestFocus();
                }
                else {
                    AsyncCallWS task = new AsyncCallWS();
                    task.execute();
                }
            }
        });

    }
    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            aResponse = com.Forgot_pass("user_forgot_password",_email);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            if (aResponse.equals("0")) {
                Toast.makeText(getApplicationContext(), "Sorry try again!", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(getApplicationContext(), "SucessFully sent to your mail id !", Toast.LENGTH_SHORT).show();
            }

        }
    }
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
