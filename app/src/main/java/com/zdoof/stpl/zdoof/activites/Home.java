package com.zdoof.stpl.zdoof.activites;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.LruCache;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.webkit.URLUtil;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.zdoof.stpl.zdoof.CircularNetworkImageView;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.adapters.CusAdapters5;
import com.zdoof.stpl.zdoof.adapters.CustomHomeadapter;
import com.zdoof.stpl.zdoof.commons.Tables;
import com.zdoof.stpl.zdoof.receivers.NotificationAlarmReceiver;
import com.zdoof.stpl.zdoof.searchadapters.CustomSearch;
import com.zdoof.stpl.zdoof.database.DatabaseHelper;
import com.zdoof.stpl.zdoof.googleautocomp.Http;
import com.zdoof.stpl.zdoof.googleautocomp.Places;
import com.zdoof.stpl.zdoof.search.Search_dish;
import com.zdoof.stpl.zdoof.search.Search_gstore;
import com.zdoof.stpl.zdoof.search.Search_people;
import com.zdoof.stpl.zdoof.search.Search_post;
import com.zdoof.stpl.zdoof.search.Search_resturant;
import com.zdoof.stpl.zdoof.services.ConnectionNotificationService;
import com.zdoof.stpl.zdoof.utils.AppUtil;
import com.zdoof.stpl.zdoof.views.Contropanel;
import com.zdoof.stpl.zdoof.webservice.PicConstant;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall;
import com.zdoof.stpl.zdoof.webservice.WebserviceCallpost;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;

/**
 * Created by stpl on 23/1/16.
 */
public class Home extends Activity implements LocationListener, AbsListView.OnScrollListener, View.OnClickListener {
    ImageView home, connection, notification, contlpnl;
    ImageView close;
    ImageView serch;
    String zdate;
    String ImageNName = "";
    ImageView cookactive, cook, reviewactive, review, cackeactive, cake;
    //Permision code that will be checked in the method onRequestPermissionsResult
    private int STORAGE_PERMISSION_CODE = 23;
    private final int LOCATION_PERMISSION_CODE = 24;

    public static final String LOADING_CACHE = "Home.LOADING_CACHE";

    public static Boolean watcher;
    Boolean isSwiped = false;
    SimpleDateFormat sdf;
    RelativeLayout rrr;
    String des;
    ImageView addimage, files3, enjoydish, Enjoydish, frsticn, sndicn, trdicn;
    int couunt = 7;
    LinearLayout ll1, ll2, ll3, ll4, uploadvideobutton;
    LinearLayout reviews, reciepes, upload, checkin, cokking;

    DrawerLayout drawerMainSearch;
    String sub, uidd, Place_type = "restaurant";
    EditText description, _wtonmnd, reviewr, zips, uploadvideolinkEditText;
    ListView list2, drawerList;
    AutoCompleteTextView place, item;
    Button gotop;
    int pagesize = 7;
    int pxProfImg,pxScrollImg;
    // RecyclerView list2;
    int detailStart,detailEnd;
    ArrayList<Detail> Values;
    ArrayList<Detail> Val;
    ArrayList<String> Dish;
    ArrayList<String> Dishid;
    ArrayList<String> place_id;
    int newcount = 1;
    Button post, post1, post2, submit;
    int[] img = {R.drawable.earths};
    String[] catgory = {"Public"};
    String cookingmethod = "";
    TextView wrs, ratrings;
    String totalpost;
    CustomHomeadapter adapter1;
    int is_checkin = 1;
    ProgressDialog progressDialog;
    String main, First_name = "", createdonn = "";
    TextView nam;
    int Regd_id, Sub_user_type, Main_user_type, pid, ptipe, is_checkinn = 2;
    int createb;
    int Restaurant_id = 0;
    String _uid, Last_name, Createdon, Updated_on, Ip_address;
    final WebserviceCall com = new WebserviceCall();
    final WebserviceCallpost com1 = new WebserviceCallpost();
    static String bResponse, cResponse, dResponse, eResponse, gResponse;
    CircularNetworkImageView circularImageView;
    int riid1, previouscount = 0;
    int count = 0, totalcount = 0;
    Spinner friends;
    ScrollView scroll;
    String value, if_like = "", result, uploadvideolinktxt;
    DatabaseHelper helper;

    String dprf = "", dwrkas = "", ddtailss = "", dname = "", updateimage = "", dstyle = "", dshare = "", dcreateon = "", utypeimage = "",
            posttedcomments = "", imagess = "", youtubeID = "",blogImg="";

    boolean isGPSEnabled = false;
    // flag for network status
    boolean isNetworkEnabled = false;
    LinearLayout headerview, ziploc, vieew;
    // flag for GPS status
    boolean canGetLocation = false;
    TextView Share;
    Location location; // location

    // private static final long MY_PERMISSION_LOCATION = 10;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    ImageView addbutton;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    ImageView active1, active2, active3, active4, enjoish, dishnrcp, exp, send2;
    int ddish_id = 0;
    EditText dname1, insgriant, instruction;

    static String  fResponse, hResponse;
    String  placename="", dishname = "", postcomment, vdlink= "", createdon, uids, zipcodes = "", locations, Post_image = "", Dish_Name = "", Dish_id = "";
    SharedPreferenceClass sharedPreferenceClass;
    final WebserviceCallpost comp = new WebserviceCallpost();
    int SELECT_FILE = 1;
    int REQUEST_CAMERA = 2;
    String diSh_id = "", Google_place_id = "";
    RatingBar rating1;
    String ratingg = "3.5";
    RelativeLayout rlcooking, rlreview, rlcake;
    TextView textt, code;
    String Place_Name, Item_name, Review, dishname1, insgriants, instructions, quant = "", ipaddress = "";
    String userid, email, Expdate, emailid, creaton, profileimg, city, contry, gender, wrkas, creatby, maritaltext, totalconnection;
    String firstname, lname, name = "", states, propict, mnutype, subutype, pic, tcnt, pic1, pic3, type, password, addrsline1, addressln2, zipcode="", dob, Marital_status, Mobile;
    String ptype = "", rebcol = "", idd = "", uid, names, profimg, dishurl = "",
            resturl = "", place_dish = "", place_rest = "", at = "", details = "", ribon = "",
            utpy, utpyimg, orderdt = "", created_on, mid_img, style, share, commentstyle = "", sub_post = "", Post_like = "";
    String pos, searchvalue, comment, Review_post_id;
    String sertype[] = {"Search Post", "Search Foodie", "Search Recipe/Dish", "Search Restaurant", "Search Grocery Store",};
    int sercimg[] = {R.drawable.searhicon, R.drawable.sst, R.drawable.ff, R.drawable.sahlist, R.drawable.crt, R.drawable.uur};
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private static final String GOOGLE_API_KEY = "AIzaSyClVSlqhlOZvvYhN_707ZoP70PGmAJJP6s";
    int lastItem;
    AutoCompleteTextView placeText;
    double latitude = 0;
    double longitude = 0;
    private int PROXIMITY_RADIUS = 50000;
    ArrayList<String> place_name_;

    private static final int POST_TYPE_UPLOAD_IMAGE = 1;
    private static final int POST_TYPE_UPLOAD_VIDEO_LINK = 2;
    private static final int POST_TYPE_CHECK_IN = 3;
    private int post_type = -1;

    private HorizontalScrollView imageupload;
    private LinearLayout layoutHorizontalScrollViewChild;
    List<String> imageFilePathList;
    private static final int UPLOAD_PICTURE_COUNT = 5;
    private String totalAllPicBase64;
    ProgressDialog dialog,dialogShowHomeData;
    private Boolean scrolled;
    PlacesDisplayTask placesDisplayTask;
    ProgressDialog placeDisplayProgressDilog;
    public static Context homeContext;
    public static LruCache<String, Bitmap> lodingCache = new LruCache<String, Bitmap>(50*1024*1024);
    public static LruCache<String, Bitmap> profileCache = new LruCache<String, Bitmap>(30*1024*1024);
    public static LruCache<String, Bitmap> scrollCache = new LruCache<String, Bitmap>(70*1024*1024);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        homeContext = this;
        if(isReadStorageAllowed()) {
            requestStoragePermission();
        }
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        if (watcher==null || !watcher) {
            watcher = true;
            startWatcherService();
        }
        initUI();
        initListener();
        AsyncCallWS2 task = new AsyncCallWS2();
        task.execute();
        String z_zip = sharedPreferenceClass.getValue_string("ZZIP");
        if (!z_zip.equals(null) && !z_zip.equals("") && !(z_zip.length()<2)) {
            code.setText(z_zip);
            zipcode = z_zip;
            postcomment = z_zip;
        } else {
            if (AppUtil.GetLocationPermission(this)) {
                GetZipcodeTask zipTask = new GetZipcodeTask();
                zipTask.execute();
            }
            else {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CODE);
            }
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.uploadvideo:
                if (uploadvideolinkEditText != null && uploadvideolinkEditText.getVisibility() == View.VISIBLE) {
                    uploadvideolinkEditText.setVisibility(View.GONE);
                    post_type = -1;
                } else if (uploadvideolinkEditText != null
                        && (uploadvideolinkEditText.getVisibility() == View.GONE
                        || uploadvideolinkEditText.getVisibility() == View.INVISIBLE)) {
                    uploadvideolinkEditText.setVisibility(View.VISIBLE);
                    post_type = POST_TYPE_UPLOAD_VIDEO_LINK;
                }
                break;
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        // do nothing
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (SCROLL_STATE_TOUCH_SCROLL == scrollState) {
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                currentFocus.clearFocus();
            }
        }
    }

    private void find() {
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlacesUrl.append("location=" + latitude + "," + longitude);
        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&types=" + "restaurant");
        googlePlacesUrl.append("&sensor=true");
        googlePlacesUrl.append("&key=" + GOOGLE_API_KEY);

        GooglePlacesReadTask googlePlacesReadTask = new GooglePlacesReadTask();
        Object[] toPass = new Object[2];
        toPass[1] = googlePlacesUrl.toString();
        googlePlacesReadTask.execute(toPass);
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Home Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.


       // if(client !=null) {

            client.connect();
            AppIndex.AppIndexApi.start(client, getIndexApiAction());
        //}
    }

    @Override
    public void onStop() {
        if (progressDialog!=null){
            progressDialog.cancel();
        }
        if (dialog!=null){
            dialog.cancel();
        }
        if (dialogShowHomeData!=null){
            dialogShowHomeData.cancel();
        }
        if (placesDisplayTask!=null){
            placesDisplayTask.cancel(true);
        }
        if (placeDisplayProgressDilog!=null && placeDisplayProgressDilog.isShowing()){
            placeDisplayProgressDilog.cancel();
        }
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    public class GooglePlacesReadTask extends AsyncTask<Object, Integer, String> {
        String googlePlacesData = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading....");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(Object... inputObj) {
            try {
                String googlePlacesUrl = (String) inputObj[1];
                Http http = new Http();
                googlePlacesData = http.read(googlePlacesUrl);
            } catch (Exception e) {
                Log.d("Google Place Read Task", e.toString());
            }
            return googlePlacesData;
        }

        @Override
        protected void onPostExecute(String result) {
            placesDisplayTask = new PlacesDisplayTask(Home.this);
            Object[] toPass = new Object[2];
            toPass[1] = result;
            placesDisplayTask.execute(toPass);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }

    public class PlacesDisplayTask extends AsyncTask<Object, Integer, List<HashMap<String, String>>> {
        private WeakReference<Home> weakReference;
        public PlacesDisplayTask(Home home){
            weakReference = new WeakReference<Home>(home);
        }
        JSONObject googlePlacesJson;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            placeDisplayProgressDilog.setMessage("Loading....");
            placeDisplayProgressDilog.setCancelable(false);
            placeDisplayProgressDilog.show();
        }

        @Override
        protected List<HashMap<String, String>> doInBackground(Object... inputObj) {
            List<HashMap<String, String>> googlePlacesList = null;
            Places placeJsonParser = new Places();
            try {
                googlePlacesJson = new JSONObject((String) inputObj[1]);
                googlePlacesList = placeJsonParser.parse(googlePlacesJson);
            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return googlePlacesList;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> list) {
            if (weakReference.get() != null){
                placeDisplayProgressDilog.dismiss();
                place_name_ = new ArrayList<String>();
                place_id = new ArrayList<String>();
                for (int i = 0; i < list.size(); i++) {

                    HashMap<String, String> googlePlace = list.get(i);
                    //double lat = Double.parseDouble(googlePlace.get("lat"));
                    //double lng = Double.parseDouble(googlePlace.get("lng"));
                    //String id = googlePlace.get("place_id");
                    //String placeName = googlePlace.get("place_name");
                    place_name_.add(googlePlace.get("place_name"));
                    place_id.add(googlePlace.get("place_id"));
                    //String vicinity = googlePlace.get("vicinity");

                }
                ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(Home.this, android.R.layout.simple_list_item_1, place_name_);
                place.setAdapter(stringArrayAdapter);
                place.setThreshold(1);
                place.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        TextView temptxt = (TextView) view;
                        placename = temptxt.getText().toString();
                        int index = place_name_.indexOf(placename);
                        Google_place_id = place_id.get(index);
                        Place_Name = placename;
                    }
                });
                if (placeDisplayProgressDilog != null) {
                    placeDisplayProgressDilog.dismiss();
                }
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        drawerMainSearch.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void onBackPressed() {
        if (drawerMainSearch.isDrawerOpen(GravityCompat.END)) {
            drawerMainSearch.closeDrawer(GravityCompat.END);
        }
        else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    Home.this);

            // set title
            alertDialogBuilder.setTitle("Do you want to exit From Zdoof !!!");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Click yes to exit!")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // if this button is clicked, close
                            // current activity
                            Boolean autoisSet = sharedPreferenceClass.getValue_boolean("AUTO_LOGOUT");
                            if (autoisSet) {
                                sharedPreferenceClass.setValue_boolean("Loginstatus", false);
                                sharedPreferenceClass.clearData();
                                cancelAlarm();
                                sharedPreferenceClass.setValue_boolean("AUTO_LOGOUT", autoisSet);
                            }
                            Intent intent = new Intent(Intent.ACTION_MAIN);
                            intent.addCategory(Intent.CATEGORY_HOME);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    class AsyncCallWS2 extends AsyncTask<Void, Void, Void> {
        Boolean timelineSuccess = false;
        @Override
        protected void onPreExecute() {

            dialogShowHomeData.setMessage("Loading...Please wait.");
            dialogShowHomeData.show();

            if(newcount==1) {
                scrolled = false;
                Values.clear();
            }
            /*if (Values!=null && Values.size()>0 && adapter1!=null){
                Values.clear();
                adapter1.clear();
            }*/
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            cResponse = com1.Ztimeline("ztimeline_bind", createb, createdonn, Updated_on, newcount, pagesize);
            return null;
        }



        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");

            try {
                JSONObject responseObject=new JSONObject(cResponse);
                JSONObject  newDataset=responseObject.getJSONObject("NewDataSet");
                JSONArray jr=newDataset.getJSONArray("Table");
                for (int i=0; i < jr.length(); i++) {
                    Detail productdto = new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);
                    ptype = jsonObject.getString("PTYPE");
                    idd = jsonObject.getString("ID");
                    userid = jsonObject.getString("UID");
                    if (jsonObject.has("mid_img")) {
                        dprf = jsonObject.getString("mid_img");
                    }
                    else {dprf=null;}
                    utypeimage = jsonObject.getString("UTYPIMG");
                    updateimage = jsonObject.getString("profimg");
                    ddtailss = jsonObject.getString("Details");
                    dwrkas = jsonObject.getString("UTYP");
                    dname = jsonObject.getString("Name");
                    dstyle = jsonObject.getString("style");
                    dshare = jsonObject.getString("Share");
                    Post_like = jsonObject.getString("Postlike");
                    zdate = jsonObject.getString("zDate");
                    //zdate=AppUtil.getDateAndTimeFromTimestamp(Home.this, zdate);
                    dcreateon = jsonObject.getString("Created_on");
                    dcreateon = convertTime(dcreateon);
                    //Log.e("The Date is::::::::::","The Date is::::::::::"+dcreateon);
                    if (jsonObject.has("Blog_Img")) {
                        blogImg = jsonObject.getString("Blog_Img");
                    }
                    if (jsonObject.has("Video_link")) {
                        youtubeID = jsonObject.getString("Video_link");
                    }
                    else {youtubeID = "null";}
                    if (youtubeID!=null && youtubeID!="" && youtubeID!="null"){
                        String[] ytemp = youtubeID.split("embed/");
                        youtubeID = ytemp[1];
                    }
                    if_like = jsonObject.getString("Likecss");
                    posttedcomments = jsonObject.getString("Postcomment");
                    if (jsonObject.has("Place_dish")) {
                        place_dish = jsonObject.getString("Place_dish");
                    }
                    place_rest = jsonObject.getString("Place_rest");
                    at = jsonObject.getString("at");
                    rebcol = jsonObject.getString("Rebcol");
                    pid = Integer.parseInt(idd);
                    ptipe = Integer.parseInt(ptype);
                    if(jsonObject.has("Table2")){
                        ArrayList<Tables> tables = new ArrayList<>();
                        Object o=jsonObject.get("Table2");
                        if(o instanceof JSONArray) {
                            JSONArray table2 = jsonObject.getJSONArray("Table2");
                            if (table2 != null) {
                                for (int k = 0; k < table2.length(); k++) {
                                    JSONObject jsonObject1 = table2.getJSONObject(k);
                                    Tables tables1 = Tables.createTableFromJson(jsonObject1);
                                    if (tables1 != null) {
                                        tables.add(tables1);
                                    }
                                }
                                productdto.setTableImages(tables);

                            }
                        }
                        else if(o instanceof JSONObject){
                            JSONObject object=(JSONObject)o;
                            Tables tables1=Tables.createTableFromJson(object);
                            if(tables1!=null){
                                tables.add(tables1);
                            }
                            productdto.setTableImages(tables);


                        }

                    }
                    if (ptype.equals("3")) {
                        String[] uidArr = ddtailss.split("user-id=");
                        String[] u1Arr = uidArr[1].split("\">");
                        String[] u2Arr = uidArr[2].split("\">");
                        productdto.connID1 = u1Arr[0];
                        productdto.connID2 = u2Arr[0];
                        productdto.connName1 = u1Arr[1].split("</a>")[0];
                        productdto.connName2 = u2Arr[1].split("</a>")[0];
                    }
                    else if (ptype.equals("7")) {
                        //ddtailss = checkFontSize(ddtailss);
                        ddtailss = checkImages(ddtailss);
                    }
                    productdto.setUtypeimage(utypeimage);
                    productdto.setDprf(dprf);
                    productdto.setPtype(ptype);
                    productdto.setUpdateimage(updateimage);
                    productdto.setDwrkas(dwrkas);
                    productdto.setDname(dname);
                    productdto.setStyle(dstyle);
                    productdto.setDdtails(ddtailss);
                    productdto.setDshare(dshare);
                    productdto.setDcreateon(dcreateon);
                    productdto.setPlace_dish(place_dish);
                    productdto.setPlace_rest(place_rest);
                    productdto.setAt(at);
                    productdto.setUserid(userid);
                    productdto.setIdd(idd);
                    productdto.setPost_like(Post_like);
                    productdto.setIflike(if_like);
                    productdto.setPostedcomments(posttedcomments);
                    productdto.setRibon(rebcol);
                    productdto.setYoutubeID(youtubeID);
                    productdto.blogTitleImg = blogImg;
                    Values.add(productdto);
                    if (i==0) {
                        detailStart =  Values.indexOf(productdto);
                    }
                    else if (i==(jr.length()-1)) {
                        detailEnd = Values.indexOf(productdto);
                    }
                }
                if (newcount==1) {
                    adapter1 = new CustomHomeadapter(Home.this, R.layout.spiner_item, Values);
                    list2.setAdapter(adapter1);
                }else {
                    adapter1.notifyDataSetChanged();
                }

                list2.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState) {


                        if(scrollState==1){
                            View currentFocus = ((Activity)Home.homeContext).getCurrentFocus();
                            if (currentFocus != null) {
                                currentFocus.clearFocus();
                            }
                        }

                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        // Toast.makeText(Home.this,String.valueOf(firstVisibleItem),Toast.LENGTH_SHORT).show();
                        if (firstVisibleItem >= 1) {
                            //plus.setVisibility(View.VISIBLE);
                        } else {
                            //plus.setVisibility(View.GONE);
                        }
                        lastItem = firstVisibleItem + visibleItemCount;
                        if (lastItem == totalItemCount) {
                            //boolean x = sharedPreferenceClass.getValue_boolean("lazy");
                            if(!scrolled) {
                                scrolled = true;
                                newcount= newcount + 1;
                                AsyncCallWS2 task1 = new AsyncCallWS2();
                                task1.execute();
                            }
                            //sharedPreferenceClass.setValue_boolean("lazy", false);
                        }

                    }
                });
                String temp_zip = sharedPreferenceClass.getValue_string("ZZIP");
                if (!temp_zip.equals("000000")) {
                    LoadImageTask imageTask = new LoadImageTask();
                    imageTask.execute();
                    timelineSuccess = true;
                }

            } catch (Exception e) {
                e.printStackTrace();
                if (dialogShowHomeData != null && dialogShowHomeData.isShowing()) {
                    dialogShowHomeData.dismiss();
                }
                e.printStackTrace();
            }

            if (cResponse.equals("0")) {

                Toast.makeText(getApplicationContext(), "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (cResponse.equals("1")) {

                Toast.makeText(getApplicationContext(), "You have successfully Registered !", Toast.LENGTH_LONG).show();

            } else {
                sharedPreferenceClass.setValue_boolean("Loginstatus", true);

            }

            if (scrolled) {
                scrolled=false;
            }

            if (dialogShowHomeData != null && dialogShowHomeData.isShowing() && !timelineSuccess) {
                dialogShowHomeData.dismiss();
            }
        }

        private String checkFontSize(String data) {
            if (data.contains("<span style=\"font-size: xx-small;\">")) {
                String[] d_arr = data.split("<span style=\"font-size: xx-small;\">");
                String temp = "";
                for (int g=0; g<d_arr.length; g++) {
                    if (g==0) {
                        temp = temp + d_arr[g];
                    }
                    else {
                        String[] remain_arr = d_arr[g].split("</span>");
                        String remain = "";
                        for (int h=1; h<remain_arr.length; h++) {
                            if (h==1){
                                remain = remain + remain_arr[h];
                            }
                            else {
                                remain = remain + "</span>" + remain_arr[h];
                            }
                        }
                        temp = temp + "<small><small><small>"+remain_arr[0]+"</small></small></small>" + remain;
                    }
                }
                data = temp;
            }

            if (data.contains("<span style=\"font-size: x-small;\">")) {
                String[] d_arr = data.split("<span style=\"font-size: x-small;\">");
                String temp = "";
                for (int g=0; g<d_arr.length; g++) {
                    if (g==0) {
                        temp = temp + d_arr[g];
                    }
                    else {
                        String[] remain_arr = d_arr[g].split("</span>");
                        String remain = "";
                        for (int h=1; h<remain_arr.length; h++) {
                            if (h==1){
                                remain = remain + remain_arr[h];
                            }
                            else {
                                remain = remain + "</span>" + remain_arr[h];
                            }
                        }
                        temp = temp + "<small><small>"+remain_arr[0]+"</small></small>" + remain;
                    }
                }
                data = temp;
            }

            if (data.contains("<span style=\"font-size: small;\">")) {
                String[] d_arr = data.split("<span style=\"font-size: small;\">");
                String temp = "";
                for (int g=0; g<d_arr.length; g++) {
                    if (g==0) {
                        temp = temp + d_arr[g];
                    }
                    else {
                        String[] remain_arr = d_arr[g].split("</span>");
                        String remain = "";
                        for (int h=1; h<remain_arr.length; h++) {
                            if (h==1){
                                remain = remain + remain_arr[h];
                            }
                            else {
                                remain = remain + "</span>" + remain_arr[h];
                            }
                        }
                        temp = temp + "<small>"+remain_arr[0]+"</small>" + remain;
                    }
                }
                data = temp;
            }

            if (data.contains("<span style=\"font-size: large;\">")) {
                String[] d_arr = data.split("<span style=\"font-size: large;\">");
                String temp = "";
                for (int g=0; g<d_arr.length; g++) {
                    if (g==0) {
                        temp = temp + d_arr[g];
                    }
                    else {
                        String[] remain_arr = d_arr[g].split("</span>");
                        String remain = "";
                        for (int h=1; h<remain_arr.length; h++) {
                            if (h==1){
                                remain = remain + remain_arr[h];
                            }
                            else {
                                remain = remain + "</span>" + remain_arr[h];
                            }
                        }
                        temp = temp + "<big>"+remain_arr[0]+"</big>" + remain;
                    }
                }
                data = temp;
            }

            if (data.contains("<span style=\"font-size: x-large;\">")) {
                String[] d_arr = data.split("<span style=\"font-size: x-large;\">");
                String temp = "";
                for (int g=0; g<d_arr.length; g++) {
                    if (g==0) {
                        temp = temp + d_arr[g];
                    }
                    else {
                        String[] remain_arr = d_arr[g].split("</span>");
                        String remain = "";
                        for (int h=1; h<remain_arr.length; h++) {
                            if (h==1){
                                remain = remain + remain_arr[h];
                            }
                            else {
                                remain = remain + "</span>" + remain_arr[h];
                            }
                        }
                        temp = temp + "<big><big>"+remain_arr[0]+"</big></big>" + remain;
                    }
                }
                data = temp;
            }

            if (data.contains("<span style=\"font-size: xx-large;\">")) {
                String[] d_arr = data.split("<span style=\"font-size: xx-large;\">");
                String temp = "";
                for (int g=0; g<d_arr.length; g++) {
                    if (g==0) {
                        temp = temp + d_arr[g];
                    }
                    else {
                        String[] remain_arr = d_arr[g].split("</span>");
                        String remain = "";
                        for (int h=1; h<remain_arr.length; h++) {
                            if (h==1){
                                remain = remain + remain_arr[h];
                            }
                            else {
                                remain = remain + "</span>" + remain_arr[h];
                            }
                        }
                        temp = temp + "<big><big><big>"+remain_arr[0]+"</big></big></big>" + remain;
                    }
                }
                data = temp;
            }
            return data;
        }

        private String checkImages (String data) {
            if (data.contains("<img style=\"")) {
                String[] d_arr = data.split("<img style=\"");
                String temp = "";
                for (int g=0; g<d_arr.length; g++) {
                    if (g==0) {
                        temp = temp + d_arr[g];
                    }
                    else {
                        String[] remain_arr = d_arr[g].split(" alt=\"\" />");
                        String core = remain_arr[0];
                        String link = core.split(" src=\"")[1];
                        core = core.split(" src=\"")[0]+ " src=\"" + PicConstant.PROFILE_URL1 + link;
                        temp = temp + "<img style=\""+core+" alt=\"\" />" + remain_arr[1];
                    }
                }
                data = temp;
            }
            return data;
        }

        private String convertTime(String jTime) {
            String returnString = null;
            returnString = jTime.substring(0, jTime.length()-6);
            String[] temp = returnString.split("T");
            returnString = temp[0] + " " + temp[1];
            try {
                long epoch = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(returnString).getTime();
                //epoch = epoch+19800000L;
                Date date = new Date(epoch);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy 'at' HH:mm:ss.SSS");
                String timezoneID = TimeZone.getDefault().getID();
                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                String t1 = sdf.format(date);
                sdf.setTimeZone(TimeZone.getTimeZone(timezoneID));
                String t2 = sdf.format(date);
                long diff = ((new SimpleDateFormat("dd-MMM-yyyy 'at' HH:mm:ss.SSS").parse(t2).getTime())-(new SimpleDateFormat("dd-MMM-yyyy 'at' HH:mm:ss.SSS").parse(t1).getTime()))*1L;
                long c_poch = epoch + diff;
                Date c_date = new Date(c_poch);
                sdf = new SimpleDateFormat("dd-MMM-yyyy 'at' HH:mm:ss aaa");
                returnString = sdf.format(c_date);
            } catch (ParseException e) {
                returnString = "0";
            }
            return returnString;
        }
    }

    class LoadImageTask extends AsyncTask<Void, Void, Void> {

        private Bitmap b = null;
        @Override
        protected void onPreExecute() {
//            dialogShowHomeData.setMessage("Loading...Please wait.");
//            dialogShowHomeData.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i=detailStart;i<=detailEnd;i++) {
                Detail d = Values.get(i);
                if (profileCache.get(Values.get(i).getUserid())==null) {
                    try {
                        URL url = new URL(PicConstant.PROFILE_URL1+ d.getUpdateimage());
                        lodingCache.put(LOADING_CACHE, BitmapFactory.decodeStream(url.openConnection().getInputStream()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        lodingCache.put(LOADING_CACHE,null);
                    }
                    if (lodingCache.get(LOADING_CACHE)==null) {
                        lodingCache.put(LOADING_CACHE,BitmapFactory.decodeResource(getResources(),R.drawable.blankpeople));
                    }
                    lodingCache.put(LOADING_CACHE,scaleImage(lodingCache.get(LOADING_CACHE), pxProfImg));
                    //Values.get(i).setProfBitmap(lodingCache.get(LOADING_CACHE).copy(lodingCache.get(LOADING_CACHE).getConfig(),lodingCache.get(LOADING_CACHE).isMutable()));
                    profileCache.put(Values.get(i).getUserid(),lodingCache.get(LOADING_CACHE).copy(lodingCache.get(LOADING_CACHE).getConfig(),lodingCache.get(LOADING_CACHE).isMutable()));
                    lodingCache.get(LOADING_CACHE).recycle();
                    //lodingCache.put(LOADING_CACHE,null);
                }

                ArrayList<Tables> t = d.getTableImages();
                if (t!=null) {
                    if (t.size()>0) {
                        //ArrayList<Bitmap> bArr = new ArrayList<Bitmap>();
                        Values.get(i).setScrollBitmap(new ArrayList<Bitmap>());
                        for (int j=0;j<t.size();j++) {
                            String cachekey = Values.get(i).getPtype()+"."+Values.get(i).getIdd()+"."+j;
                            if (scrollCache.get(cachekey)==null) {
                                try {
                                    URL url = new URL(PicConstant.PROFILE_URL1+ t.get(j).getMid_img());
                                    lodingCache.put(LOADING_CACHE, BitmapFactory.decodeStream(url.openConnection().getInputStream()));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    lodingCache.put(LOADING_CACHE,null);
                                }
                                if (lodingCache.get(LOADING_CACHE)==null) {
                                    lodingCache.put(LOADING_CACHE,BitmapFactory.decodeResource(getResources(),R.drawable.blankpeople));
                                }

                                lodingCache.put(LOADING_CACHE,scaleImage(lodingCache.get(LOADING_CACHE), pxScrollImg));
                                //Values.get(i).getScrollBitmap().add(lodingCache.get(LOADING_CACHE).copy(lodingCache.get(LOADING_CACHE).getConfig(),lodingCache.get(LOADING_CACHE).isMutable()));

                                scrollCache.put(cachekey,lodingCache.get(LOADING_CACHE).copy(lodingCache.get(LOADING_CACHE).getConfig(),lodingCache.get(LOADING_CACHE).isMutable()));

                                lodingCache.get(LOADING_CACHE).recycle();
                                //lodingCache.put(LOADING_CACHE, null);
                            }

                        }
                    }
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            adapter1.notifyDataSetChanged();
            dialogShowHomeData.dismiss();
        }
    }

    private Bitmap scaleImage(Bitmap bitmap,int newDpHeight){
        int newHeight=newDpHeight;
        int newWidth=(int) (newHeight * bitmap.getWidth()/((double) bitmap.getHeight()));
        return Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
//
//        float scaleX = newWidth / (float) bitmap.getWidth();
//        float scaleY = newHeight / (float) bitmap.getHeight();
//        float pivotX = 0;
//        float pivotY = 0;
//
//        Matrix scaleMatrix = new Matrix();
//        scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);
//
//        Canvas canvas = new Canvas(scaledBitmap);
//        canvas.setMatrix(scaleMatrix);
//        canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));
    }

    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Home.this);
            progressDialog.setMessage("Loading....");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            bResponse = comp.Checkin("insert_post", is_checkin,postcomment,vdlink,Post_image, createb, createdon);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");


            if (bResponse.equals("0")) {
                Toast.makeText(Home.this, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (bResponse.equals("1")) {

                // Toast.makeText(getActivity(), "You have successfully Posted !", Toast.LENGTH_LONG).show();
//                scrolled = false;
//                AsyncCallWS2 task = new AsyncCallWS2();
//                task.execute();

                Intent intent = new Intent(Home.this, Home.class);
                startActivity(intent);
                finish();

            } else {

            }

            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }

    class AsyncCallWS5 extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressDialog = new ProgressDialog(Home.this);
            dialog.setMessage("Posting.... ");
            dialog.setCancelable(false);
            dialog.show();
            //progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            eResponse = comp.Experience("insert_post", is_checkinn, value, ImageNName, createb, createdon, uploadvideolinktxt);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");


            if (eResponse.equals("0")) {
                Toast.makeText(Home.this, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (eResponse.equals("1")) {
                //progressDialog.dismiss();
                //sharedPreferenceClass.setValue_boolean("LOAD",false);
                //recreate();
                if (imageFilePathList!=null && layoutHorizontalScrollViewChild !=null) {
                    imageFilePathList.clear();
                    layoutHorizontalScrollViewChild.removeAllViews();
                }
                dialog.dismiss();
//                scrolled = false;
//                AsyncCallWS2 task1 = new AsyncCallWS2();
//                task1.execute();
                Intent intent = new Intent(Home.this, Home.class);
                startActivity(intent);
                finish();
                //changessssss

            } else {

            }

            if (dialog != null) {
                dialog.dismiss();
            }
            uploadvideolinktxt = "";
        }
    }

    class AsyncCallWS6 extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Home.this);
            progressDialog.setMessage("Loading....");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            fResponse = comp.Postdish("insert_dish", dishname1, totalAllPicBase64, insgriants, cookingmethod, quant, instructions, createb, createdon, ipaddress);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");

            if (fResponse.equals("0")) {
                Toast.makeText(Home.this, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (fResponse.equals("1")) {

                //recreate();
//                scrolled = false;
//                AsyncCallWS2 task1 = new AsyncCallWS2();
//                task1.execute();
                Intent intent = new Intent(Home.this, Home.class);
                startActivity(intent);
                finish();


            } else {

            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

    }

    class AsyncCallWS7 extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Home.this);
            progressDialog.setMessage("Loading....");
            progressDialog.setCancelable(false);
            progressDialog.dismiss();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            gResponse = com1.Selectdish("select_all_dish");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            try {
                JSONArray jr = new JSONArray(gResponse);
                Dish = new ArrayList<String>();
                Dishid = new ArrayList<String>();
                for (int i = 0; i < jr.length(); i++) {
                    JSONObject jsonObject = jr.getJSONObject(i);
                    Dish_Name = jsonObject.getString("Dish_name");
                    Dish_id = jsonObject.getString("Dish_id");
                    Dish.add(Dish_Name);
                    Dishid.add(Dish_id);
                }
                final ArrayAdapter<String> adapter = new ArrayAdapter<String>(Home.this, android.R.layout.simple_list_item_1, Dish);
                item.setAdapter(adapter);
                item.setThreshold(1);
                item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        //diSh_id = Dishid.get(position);
                        TextView temptxt = (TextView) view;
                        String txt = temptxt.getText().toString();
                        //int i = Dish.indexOf(txt);
                        String did = Dishid.get(Dish.indexOf(txt));
                        ddish_id = Integer.parseInt(did);
                        dishname = txt;
                    }
                });

                // progressDialog.dismiss();

            } catch (Exception e) {

            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

    }

    class AsyncCallWS8 extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(Home.this);
            progressDialog.setMessage("Loading....");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            hResponse = com1.Review_dish("add_review_dish", ddish_id, Restaurant_id, Place_Name,
                    Google_place_id, ratingg, Review,
                    totalAllPicBase64, createb, createdon, Place_type, dishname);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            //  progressDialog.dismiss();
            if (hResponse.equals("0")) {
                Toast.makeText(Home.this, "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (hResponse.equals("1")) {
                //sharedPreferenceClass.setValue_boolean("LOAD",true);
                //recreate();
//                scrolled = false;
//                AsyncCallWS2 task = new AsyncCallWS2();
//                task.execute();

                Intent intent = new Intent(Home.this, Home.class);
                startActivity(intent);
                finish();

            } else {

            }
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == SELECT_FILE) {
            onSelectFromGalleryResult(data);
        }

        else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CAMERA) {
            onCaptureImageResult(data);
        }

        else {
            Toast.makeText(Home.this, "Please try again !!!", Toast.LENGTH_SHORT).show();
        }


    }
    private void addToHorizentalScrollView(Bitmap bitmap, String base64) {
        int added_layout_size = layoutHorizontalScrollViewChild.getChildCount();
        View view = LayoutInflater.from(Home.this).inflate(R.layout.img_upload_layout, null);
        ImageView image = (ImageView) view.findViewById(R.id.image);
        image.setImageBitmap(bitmap);
        ImageView image_close = (ImageView) view.findViewById(R.id.close);
        image_close.setTag(added_layout_size);
        image_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (Integer) v.getTag();
                Log.v("Pos",""+position);
                layoutHorizontalScrollViewChild.removeViewAt(position);
                imageFilePathList.remove(position);
            }
        });
        layoutHorizontalScrollViewChild.addView(view);
        imageFilePathList.add(base64);
    }

    private void onCaptureImageResult(Intent data) {
        imageupload.setVisibility(View.VISIBLE);
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        String file_path = destination.toString();
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] b = bytes.toByteArray();
        String imageName = Base64.encodeToString(b, Base64.DEFAULT);
        addToHorizentalScrollView(thumbnail, imageName);
    }

    private void onSelectFromGalleryResult(Intent data) {
        imageupload.setVisibility(View.VISIBLE);
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.Images.Media.DATA};
        //Cursor cursor = Home.this.managedQuery(selectedImageUri, projection, null, null, null);
        // @SuppressWarnings("deprecation")
        //  Cursor cursor =getContentResolver().query(uri, projection, null, null, null);
        Cursor cursor = Home.this.getContentResolver().query(selectedImageUri, projection, null, null, null);
        int column_index;
        if (cursor != null) {
            column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();

            String imageFilePath = cursor.getString(column_index).toString();
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            BitmapFactory.decodeFile(imageFilePath,options);
            Bitmap bm1 = BitmapFactory.decodeFile(imageFilePath,options);
            File imagefile = new File(imageFilePath);
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(imagefile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            Bitmap bm = BitmapFactory.decodeStream(fis);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 90, baos);
            byte[] b = baos.toByteArray();
            String base64 = Base64.encodeToString(b, Base64.DEFAULT);
            // ImageNName="@" + totalAllPicBase64;
            upload.setVisibility(View.VISIBLE);
            addToHorizentalScrollView(bm1, base64);
        }
        else {
            Toast.makeText(homeContext,"Please try again",Toast.LENGTH_SHORT).show();

        }
    }



    /* @Override
     protected void onRestart(){
         super.onRestart();
         Intent i=new Intent(Home.this,Home.class);
         startActivity(i);
         finish();
     }
*/
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    private void startWatcherService() {
        Intent intent = new Intent(getApplicationContext(), NotificationAlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, NotificationAlarmReceiver.ALARM_REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarm.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+ConnectionNotificationService.REPEAT_INTERVAL, pIntent);
        }
        else {
            alarm.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+ConnectionNotificationService.REPEAT_INTERVAL, pIntent);
        }
    }

    private void cancelAlarm() {
        Intent intent = new Intent(getApplicationContext(), NotificationAlarmReceiver.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, NotificationAlarmReceiver.ALARM_REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pIntent);
    }

    public void initUI(){
        pxProfImg = getResources().getDimensionPixelSize(R.dimen.pxProfImg);
        pxScrollImg = getResources().getDimensionPixelSize(R.dimen.pxScrollImg);
        scrolled = false;
        placeDisplayProgressDilog = new ProgressDialog(Home.this);
        dialog = new ProgressDialog(Home.this);
        dialogShowHomeData = new ProgressDialog(Home.this);
        dialogShowHomeData.setCancelable(false);
        progressDialog = new ProgressDialog(Home.this);
        progressDialog.setCancelable(false);
        helper = new DatabaseHelper(this);
        sharedPreferenceClass = new SharedPreferenceClass(this);
        sharedPreferenceClass.setValue_boolean("LOAD", false);
        list2 = (ListView) findViewById(R.id.listView2);
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.header, list2, false);
        list2.addHeaderView(header, null, false);
        imageFilePathList = new ArrayList<String>();

        //swipeRefresh.setColorSchemeResources(R.color.zdoofgreen);

        drawerMainSearch = (DrawerLayout) findViewById(R.id.drawerMainSearch);
        ll1 = (LinearLayout) findViewById(R.id.hmj);
        ll2 = (LinearLayout) findViewById(R.id.cnn);
        ll3 = (LinearLayout) findViewById(R.id.ntf);
        ll4 = (LinearLayout) findViewById(R.id.ct);
        ll4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                des = description.getText().toString().trim();
                description.setText(des);
                sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                Intent intent = new Intent(Home.this, Contropanel.class);
                startActivity(intent);
                finish();
            }
        });
        home = (ImageView) findViewById(R.id.hme);
        rating1 = (RatingBar) findViewById(R.id.rating);
        ratrings = (TextView) findViewById(R.id.textviewss);
        close = (ImageView) findViewById(R.id.close);
        scroll = (ScrollView) findViewById(R.id.scroll);
        ll1.setBackgroundColor(Color.parseColor("#1D8168"));
        connection = (ImageView) findViewById(R.id.hm);
        notification = (ImageView) findViewById(R.id.cnct);
        friends = (Spinner) findViewById(R.id.friends);
        frsticn = (ImageView) findViewById(R.id.frst);
        sndicn = (ImageView) findViewById(R.id.secnd);
        trdicn = (ImageView) findViewById(R.id.trd);
        headerview = (LinearLayout) findViewById(R.id.headerview);
        imageupload = (HorizontalScrollView) findViewById(R.id.imageupload);
        OverScrollDecoratorHelper.setUpOverScroll(imageupload);
        layoutHorizontalScrollViewChild = (LinearLayout) findViewById(R.id.layoutHorizontalScrollViewChild);

        cokking = (LinearLayout) findViewById(R.id.cooking);
        //plus = (ImageView) findViewById(R.id.addbtn);
        rlcooking = (RelativeLayout) findViewById(R.id.relcook);
        rlreview = (RelativeLayout) findViewById(R.id.rlreview);
        rlcake = (RelativeLayout) findViewById(R.id.rlcake);
        place = (AutoCompleteTextView) findViewById(R.id.plcnm);
        item = (AutoCompleteTextView) findViewById(R.id.itmes);
        reviewr = (EditText) findViewById(R.id.reviewr);
        cookactive = (ImageView) findViewById(R.id.cook);
        cook = (ImageView) findViewById(R.id.cook1);
        reviewactive = (ImageView) findViewById(R.id.review1);
        review = (ImageView) findViewById(R.id.review);
        cackeactive = (ImageView) findViewById(R.id.dish1);
        cake = (ImageView) findViewById(R.id.dish);
        reviews = (LinearLayout) findViewById(R.id.reviews);
        reciepes = (LinearLayout) findViewById(R.id.receipe);
        upload = (LinearLayout) findViewById(R.id.upload);
        checkin = (LinearLayout) findViewById(R.id.chekin);
        dname1 = (EditText) findViewById(R.id.dishname);
        insgriant = (EditText) findViewById(R.id.ingradiant);
        instruction = (EditText) findViewById(R.id.instuction);
        post = (Button) findViewById(R.id.post);
        post1 = (Button) findViewById(R.id.post1);
        post2 = (Button) findViewById(R.id.post2);
        _wtonmnd = (EditText) findViewById(R.id.whryrmnd);

        contlpnl = (ImageView) findViewById(R.id.ctrl);

        //list2= (RecyclerView) findViewById(R.id.listView2);
        serch = (ImageView) findViewById(R.id.serc);
        description = (EditText) findViewById(R.id.imageView11);
        drawerList = (ListView) findViewById(R.id.drawerItemList);
        LayoutInflater inflater11 = getLayoutInflater();
        ViewGroup header1 = (ViewGroup) inflater11.inflate(R.layout.search_view, drawerList, false);
        drawerList.addHeaderView(header1, null, false);
        textt = (TextView) findViewById(R.id.textt);
        ziploc = (LinearLayout) findViewById(R.id.ziploc);
        vieew = (LinearLayout) findViewById(R.id.view);
        zips = (EditText) findViewById(R.id.zip);
        submit = (Button) findViewById(R.id.submit);
        code = (TextView) findViewById(R.id.code);
        gotop = (Button) findViewById(R.id.gotop);

        uploadvideobutton = (LinearLayout) findViewById(R.id.uploadvideo);
        uploadvideobutton.setOnClickListener(this);
        uploadvideolinkEditText = (EditText) findViewById(R.id.uploadvideolink);

        _uid = sharedPreferenceClass.getValue_string("UIDD");
        createb = Integer.parseInt(_uid);
        //createb = Integer.parseInt(_uid);
        searchvalue = sharedPreferenceClass.getValue_string("SEARCHVALUE");
        close.setVisibility(View.GONE);

        if (searchvalue.equals("")) {
            description.setText("");
            close.setVisibility(View.GONE);
        } else if (searchvalue.equals("0")) {
            description.setText("");
            close.setVisibility(View.GONE);
        } else {
            description.setText(searchvalue);
            close.setVisibility(View.VISIBLE);
        }
        CusAdapters5 adapter = new CusAdapters5(Home.this, R.layout.spiner_item, catgory, img);
        friends.setAdapter(adapter);
        CustomSearch drawerAdapter = new CustomSearch(Home.this, R.layout.spiner_item, sertype, sercimg);
        drawerList.setAdapter(drawerAdapter);
        sharedPreferenceClass.setValue_string("BACKPAGE","");
    }

    public void initListener() {
        sharedPreferenceClass.setValue_boolean("lazy", true);
        Values = new ArrayList<Detail>();
        textt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ziploc.setVisibility(View.VISIBLE);
                vieew.setVisibility(View.VISIBLE);
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zipcode = zips.getText().toString();
                code.setText(zipcode);
                sharedPreferenceClass.setValue_string("ZZIP", zipcode);
                ziploc.setVisibility(View.GONE);
                vieew.setVisibility(View.GONE);
            }
        });
        /*plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list2.smoothScrollToPosition(0);
            }
        });*/
        cook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadvideobutton.setVisibility(View.VISIBLE);
                upload.setVisibility(View.VISIBLE);
                cokking.setVisibility(View.VISIBLE);
                reviewactive.setVisibility(View.GONE);
                review.setVisibility(View.VISIBLE);
                checkin.setVisibility(View.VISIBLE);
                cackeactive.setVisibility(View.GONE);
                reviews.setVisibility(View.GONE);
                reciepes.setVisibility(View.GONE);
                cake.setVisibility(View.VISIBLE);
                post.setVisibility(View.VISIBLE);
                post1.setVisibility(View.GONE);
                post2.setVisibility(View.GONE);
                cookactive.setVisibility(View.VISIBLE);
                cook.setVisibility(View.GONE);
                rlcooking.setBackgroundColor(Color.parseColor("#ffffff"));
                rlreview.setBackgroundColor(Color.parseColor("#dddddd"));
                rlcake.setBackgroundColor(Color.parseColor("#dddddd"));
            }
        });
        cake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadvideobutton.setVisibility(View.GONE);
                uploadvideolinkEditText.setVisibility(View.GONE);
                upload.setVisibility(View.VISIBLE);
                reviewactive.setVisibility(View.GONE);
                review.setVisibility(View.VISIBLE);
                checkin.setVisibility(View.GONE);
                cackeactive.setVisibility(View.VISIBLE);
                reviews.setVisibility(View.GONE);
                reciepes.setVisibility(View.VISIBLE);
                cake.setVisibility(View.GONE);
                cokking.setVisibility(View.GONE);
                post.setVisibility(View.GONE);
                post1.setVisibility(View.GONE);
                reciepes.setVisibility(View.VISIBLE);
                post2.setVisibility(View.VISIBLE);
                cookactive.setVisibility(View.VISIBLE);
                cookactive.setVisibility(View.GONE);
                cook.setVisibility(View.VISIBLE);
                rlcooking.setBackgroundColor(Color.parseColor("#dddddd"));
                rlreview.setBackgroundColor(Color.parseColor("#dddddd"));
                rlcake.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        });
        /*swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(true);
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        swipeRefresh.setRefreshing(false);
//                    }
//                }, 5000L);
                newcount = 1;
                scrolled = false;
                isSwiped = true;
                Values.clear();
                AsyncCallWS2 task = new AsyncCallWS2();
                task.execute();
            }
        });*/
        review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadvideobutton.setVisibility(View.GONE);
                upload.setVisibility(View.GONE);
                uploadvideolinkEditText.setVisibility(View.GONE);
                cokking.setVisibility(View.GONE);
                reviewactive.setVisibility(View.VISIBLE);
                checkin.setVisibility(View.VISIBLE);
                review.setVisibility(View.GONE);
                reviews.setVisibility(View.VISIBLE);
                post.setVisibility(View.GONE);
                post2.setVisibility(View.GONE);
                reciepes.setVisibility(View.GONE);
                post1.setVisibility(View.VISIBLE);
                cook.setVisibility(View.VISIBLE);
                cookactive.setVisibility(View.GONE);
                cake.setVisibility(View.VISIBLE);
                cackeactive.setVisibility(View.GONE);
                rlcooking.setBackgroundColor(Color.parseColor("#dddddd"));
                rlreview.setBackgroundColor(Color.parseColor("#ffffff"));
                rlcake.setBackgroundColor(Color.parseColor("#dddddd"));
            }
        });
        rating1.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratrings.setText(String.valueOf(rating));
                ratingg = String.valueOf(rating);
            }
        });

        post2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dishname1 = dname1.getText().toString();
                insgriants = insgriant.getText().toString();
                instructions = instruction.getText().toString();
                dname1.setText("");
                insgriant.setText("");
                instruction.setText("");
                imageupload.setVisibility(View.GONE);

                if (description.equals("") || instructions.equals("")) {
                    Toast.makeText(Home.this, "Please give inputs", Toast.LENGTH_SHORT).show();
                }
                else if (description.length()>150 || instructions.length()>150) {
                    Toast.makeText(Home.this, "Your Recipe size should be less than 150 characters!", Toast.LENGTH_SHORT).show();
                }
                else {
                    for (int i = 0; i < imageFilePathList.size(); i++) {
                        String path = imageFilePathList.get(i);
                        if (i != 0) {
                            totalAllPicBase64 = totalAllPicBase64 + "$" + path;
                        } else {
                            totalAllPicBase64 = path;
                        }
                    }
                    Expdate = sdf.format(new Date());
                    createdon = Expdate;
                    AsyncCallWS6 task = new AsyncCallWS6();
                    task.execute();

                }
            }
        });
        post1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Place_Name = place.getText().toString().trim();
                Item_name = item.getText().toString().trim();
                Review = reviewr.getText().toString().trim();
                place.setText("");
                item.setText("");
                reviewr.setText("");
                //ddish_id = Integer.parseInt(did);
                //dishname = txt;
                //Dish_id = "0";
                //ddish_id = 0;
                imageupload.setVisibility(View.GONE);
                if (Place_Name.equals("") || Item_name.equals("") || Review.equals("")) {
                    Toast.makeText(Home.this, "This Field is requried", Toast.LENGTH_SHORT).show();
                }
                else if (Place_Name.length()>150 || Item_name.length()>150 || Review.length()>150) {
                    Toast.makeText(Home.this, "Your review should be less than 150 characters!", Toast.LENGTH_SHORT).show();
                }
                else {
                    if (!Item_name.equals(dishname)) {
                        ddish_id = 0;
                        dishname = Item_name;
                    }
                    if (!Place_Name.equals(placename)) {
                        Google_place_id = "";
                    }
                    Expdate = sdf.format(new Date());
                    createdon = Expdate;
                    AsyncCallWS8 task = new AsyncCallWS8();
                    task.execute();
                    // progressDialog = ProgressDialog.show(Home.this, "", "Loading...");
                }
            }
        });
        post.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        value = _wtonmnd.getText().toString().trim();
                        String link = uploadvideolinkEditText.getText().toString().trim();
                        Boolean isID = false;
                        if (link.split("watch?").length>1) {
                            if (link.split("feature=").length>1) {
                                String t1 = link.split("&feature=")[0];
                                uploadvideolinktxt = t1.split("v=")[1];
                            } else {
                                uploadvideolinktxt = link.split("v=")[1];
                            }
                            isID = true;
                        }
                        else if (link.split("/embed/").length>1) {
                            uploadvideolinktxt = link.split("/embed/")[1];
                            isID = true;
                        }
                        else if (link.split("//youtu.be/").length>1) {
                            if (link.split("t=").length>1) {
                                String t1 = link.split("t=")[0];
                                uploadvideolinktxt = t1.split("//youtu.be/")[1].substring(0,t1.split("v=")[1].length()-1);
                            } else {
                                uploadvideolinktxt = link.split("//youtu.be/")[1];
                            }
                            isID = true;
                        }
                        else {
                            uploadvideolinktxt="";
                            isID = false;
                        }

                        if (value.equals("") || value.length()<1) {
                            Toast.makeText(Home.this, "Enter what's new with you.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        else if (value.length()>150){
                            Toast.makeText(Home.this, "Your post should be less than 150 characters!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        switch (post_type) {
                            case POST_TYPE_UPLOAD_IMAGE:
                                break;
                            case POST_TYPE_UPLOAD_VIDEO_LINK:
                                if (link == null || link.length() <= 0) {
                                    Toast.makeText(Home.this, "Enter a video link to upload", Toast.LENGTH_SHORT).show();
                                    return;
                                } else if (URLUtil.isValidUrl(link) == false || !isID) {
                                    Toast.makeText(Home.this, "Enter a valid YouTube video link to upload", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                //uploadvideolinktxt = "";

                                break;
                            case POST_TYPE_CHECK_IN:
                                break;
                        }
                        _wtonmnd.setText("");
                        uploadvideolinkEditText.setText("");
                        imageupload.setVisibility(View.GONE);

                        for (int i = 0; i < imageFilePathList.size(); i++) {
                            String path = imageFilePathList.get(i);
                            if (i != 0) {
                                ImageNName = ImageNName + "$" + path;
                            } else {
                                ImageNName = path;
                            }
                        }
                        Expdate = sdf.format(new Date());
                        createdon = Expdate;
                        AsyncCallWS5 task = new AsyncCallWS5();
                        task.execute();
                        // progressDialog = ProgressDialog.show(Home.this, "", "Loading...");
                    }
                });

        /////
        /*AsyncCallWS7 task1 = new AsyncCallWS7();
        task1.execute();*/
        String format = "dd/MMM/yyyy H:mm:ss";
        sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Expdate = sdf.format(new Date());
        createdon = Expdate;

        //getZipcodeOfLocation();

        ///////


        //checkin click
        description.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //Log.e("KeyCode:::",""+actionId);
                if (actionId== EditorInfo.IME_ACTION_SEARCH) {
                    serch.performClick();
                    return true;
                }
                return false;
            }
        });
        description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                drawerMainSearch.closeDrawer(GravityCompat.END);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setCursorVisible(true);

            }
        });

        checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationManager lm = (LocationManager) Home.this.getSystemService(Context.LOCATION_SERVICE);
                boolean gps_enabled = false;
                try {
                    gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                } catch (Exception e) {}
                if (!gps_enabled) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Home.this);
                    builder.setTitle("GPS is swiched off !!!")
                            .setMessage("For check-in feature GPS needs to be switched on. Please switch on your GPS and try again.")
                            .setCancelable(false)
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    builder.show();
                } else {
                    post_type = POST_TYPE_CHECK_IN;
                    AsyncCallWS task = new AsyncCallWS();
                    task.execute();
                }

            }
        });
        //image upload code
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                post_type = POST_TYPE_UPLOAD_IMAGE;

                // Max 5 photo can upload at a time
                if (imageFilePathList.size() < UPLOAD_PICTURE_COUNT) {
                    final CharSequence[] items = {"Take Photo", "Choose from Library",
                            "Cancel"};

                    AlertDialog.Builder builder = new AlertDialog.Builder(Home.this);
                    builder.setTitle("Add Photo!");
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (items[item].equals("Take Photo")) {
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, REQUEST_CAMERA);
                            } else if (items[item].equals("Choose from Library")) {
                                //First checking if the app is already having the permission
                                if(!isReadStorageAllowed()){
                                    requestStoragePermission();

                                }

                                //If the app has not the permission then asking for the permission
                         Intent intent = new Intent(Intent.ACTION_PICK,
                                 android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                               // startActivityForResult(intent, SELECT_FILE);
                              //  intent.setType("image/*");
                                startActivityForResult(
                                        Intent.createChooser(intent, "Select File"), SELECT_FILE);

                            } else if (items[item].equals("Cancel")) {
                                dialog.dismiss();
                            }
                        }
                    });

                    builder.show();
                } else {
                    Toast.makeText(Home.this, "You Have already Choose five Images", Toast.LENGTH_SHORT).show();
                }
            }
        });

        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                des = description.getText().toString().trim();
                if (des.length()<3) {
                    Toast.makeText(Home.this, "Please enter atleast 3 letters to search!", Toast.LENGTH_SHORT).show();
                } else {
                    pos = drawerList.getItemAtPosition(position).toString();
                    if (pos.equals("0")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(Home.this, Search_post.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("1")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(Home.this, Search_people.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("2")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(Home.this, Search_dish.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("3")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(Home.this, Search_resturant.class);
                        startActivity(intent);
                        //finish();
                    } else if (pos.equals("4")) {
                        des = description.getText().toString().trim();
                        description.setText(des);
                        sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                        Intent intent = new Intent(Home.this, Search_gstore.class);
                        startActivity(intent);
                        //finish();
                    }
                }
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                description.setText("");
                sharedPreferenceClass.setValue_string("SEARCHVALUE", "0");
                close.setVisibility(View.GONE);
            }
        });

        serch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!drawerMainSearch.isDrawerOpen(GravityCompat.END)) {
                    drawerMainSearch.openDrawer(GravityCompat.END);
                }
                else {
                    drawerMainSearch.closeDrawer(GravityCompat.END);
                }
            }
        });

        Updated_on = "";
        ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                des = description.getText().toString().trim();
                description.setText(des);
                sharedPreferenceClass.setValue_string("SEARCHVALUE", des);
                Intent intent = new Intent(Home.this, Connecton.class);
                startActivity(intent);
                finish();
            }
        });
        ll3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Home.this, Notifications.class);
                startActivity(intent);
                finish();
            }

        });

        gotop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list2.smoothScrollToPosition(0, 0);
            }
        });

    }

    private class GetZipcodeTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... params) {
            getZipcodeOfLocation();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String zzip = sharedPreferenceClass.getValue_string("ZZIP");
            if (!zzip.equals(null) && !zzip.equals("") && !zzip.equals("0") && !(zzip.length()<2)) {
                code.setText(zzip);
            } else {
                code.setText(zipcode);
                sharedPreferenceClass.setValue_string("ZZIP", zipcode);
            }
            zipcode = sharedPreferenceClass.getValue_string("ZZIP");
            postcomment = zipcode;
        }
    }

    private void getZipcodeOfLocation() {
        LocationManager manager = (LocationManager) Home.this.getSystemService(LOCATION_SERVICE);
        // getting GPS status
        isGPSEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        isNetworkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Geocoder geocoder = new Geocoder(Home.this,Locale.getDefault());
        if (
                ActivityCompat.checkSelfPermission(Home.this, Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(Home.this, Manifest.permission.ACCESS_COARSE_LOCATION)==PackageManager.PERMISSION_GRANTED) {
            if (isNetworkEnabled) {
                location = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location!=null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    try {
                        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 5);
                        if (addresses!=null) {
                            if (addresses.size()>0) {
                                for (int i=0; i<addresses.size(); i++){
                                    if (addresses.get(i).getPostalCode()!=null) {
                                        if (addresses.get(i).getPostalCode().length()>1) {
                                            zipcode = addresses.get(i).getPostalCode();
                                            postcomment = zipcode;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        else {
            ActivityCompat.requestPermissions(Home.this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CODE);
        }

    }

    //We are calling this method to check the permission status
    private boolean isReadStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //We are calling this method to check the permission status
    private boolean isGPSEnabled() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        //If permission is granted returning true
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        //If permission is not granted returning false
        return false;
    }

    //Requesting permission
    private void requestStoragePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_EXTERNAL_STORAGE)){
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }

        //And finally ask for the permission
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},STORAGE_PERMISSION_CODE);
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if(requestCode == STORAGE_PERMISSION_CODE){

            //If permission is granted
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                //Displaying a toast
                //Toast.makeText(this,"Permission granted now you can read the storage",Toast.LENGTH_LONG).show();
            }else{
                //Displaying another toast if permission is not granted
                Toast.makeText(Home.this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }
        }
        else if (requestCode == LOCATION_PERMISSION_CODE) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                GetZipcodeTask zipcodeTask = new GetZipcodeTask();
                zipcodeTask.execute();
            } else {
                Toast.makeText(Home.this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }

        }
    }


}

