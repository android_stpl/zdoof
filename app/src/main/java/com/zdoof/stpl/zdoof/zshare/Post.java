package com.zdoof.stpl.zdoof.zshare;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.adapters.Customdish;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.activites.Home;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.adapters.CusAdapters;
import com.zdoof.stpl.zdoof.database.DatabaseHelper;
import com.zdoof.stpl.zdoof.webservice.WebserviceCallpost;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by sujit on 24/2/16.
 */
public class Post extends Fragment{
    Spinner friends,unit;
    Button add;
    String weight_value;
    ListView list;
    LinearLayout camera,send;
    String imageName,imageName1,imageName2;
    DatabaseHelper helper;
    AutoCompleteTextView text;
    ArrayList<Detail>val;
    String ingradient,quantity,unit1;
    String[]units={"GMS","LTR","LBS","OZ","CUP","DROPS","OTHERS"};
    String[]catgory={"Friends","Frinds of friends","Public"};
    String[] Ingredient={"ZUCCHINI","CHANNA DAL","SPINCH PASTE","CHICKEN BONELESS","KASHMIRI MIRCH"};
    int[]img={R.drawable.friend,R.drawable.frnds_frnds,R.drawable.pblic};
    EditText dname,desc,ingradiant2,qunity,instruction;
    ImageView close,addimage,files,files1,files2,files3,Enjoydish,experience,exp1,chk,chk1,frsticn,sndicn,trdicn;
    protected static final int CAMERA_REQUEST = 0;
    protected static final int GALLERY_PICTURE = 1;
    private Intent pictureActionIntent = null;
    Bitmap bitmap;
    String selectedImagePath,dishname1,description,instructions,ipaddress="";
    String quant,images,createdon1;
    LinearLayout ll1, ll2, ll3, ll4;
    EditText dnam,descr,inst;
    int createdby=0;
    int SELECT_FILE = 1;
    int REQUEST_CAMERA = 2;
    int SELECT_FILE1 = 3;
    int REQUEST_CAMERA1 = 4;
    String imageFilePath,imageFilePath1,imageFilePath2;
    static String aResponse;
    ImageView active1,active2,active3,active4,enjoish,dishnrcp,checkin,exp,send2;
    SharedPreferenceClass sharedPreferenceClass;
    final WebserviceCallpost comp = new WebserviceCallpost();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View rootView =  inflater.inflate(R.layout.post_receipes,null);
        helper = new DatabaseHelper(getActivity());

        send=(LinearLayout) rootView.findViewById(R.id.pst);
       // send2=(ImageView) rootView.findViewById(R.id.pst1);
        friends = (Spinner) rootView.findViewById(R.id.spinner4);
        dname = (EditText) rootView.findViewById(R.id.dise);
        list = (ListView) rootView.findViewById(R.id.listView);
        descr = (EditText)rootView.findViewById(R.id.ddescription);
        qunity = (EditText) rootView.findViewById(R.id.qnt);
        close= (ImageView) rootView.findViewById(R.id.close);
        active1= (ImageView) rootView.findViewById(R.id.act1);
        active2=(ImageView) rootView.findViewById(R.id.act2);
        active3=(ImageView) rootView.findViewById(R.id.act3);
        active4=(ImageView) rootView.findViewById(R.id.act4);
        val = new ArrayList<Detail>();
     /*   ll1= (LinearLayout)rootView.findViewById(R.id.hmj);
        ll2= (LinearLayout) rootView.findViewById(R.id.cnn);
        ll3=(LinearLayout) rootView.findViewById(R.id.ntf);
        ll4=(LinearLayout) rootView.findViewById(R.id.ct);*/
        camera= (LinearLayout) rootView.findViewById(R.id.kk);
        frsticn= (ImageView) rootView.findViewById(R.id.frst);
        sndicn= (ImageView) rootView.findViewById(R.id.secnd);
        trdicn= (ImageView) rootView.findViewById(R.id.trd);
        exp = (ImageView) rootView.findViewById(R.id.myex);
        enjoish = (ImageView) rootView.findViewById(R.id.enjd);
        checkin= (ImageView) rootView.findViewById(R.id.chkin);
        exp1=(ImageView) rootView.findViewById(R.id.imageView8);
        chk=(ImageView) rootView.findViewById(R.id.imageView21);
        chk1=(ImageView) rootView.findViewById(R.id.imageView1);
        // ingredient1= (EditText) findViewById(R.id.ingradient);
        text = (AutoCompleteTextView) rootView.findViewById(R.id.autoCompleteTextView);
       /* ingradiant2= (EditText) findViewById(R.id.ingradient2);
        quality= (EditText) findViewById(R.id.qulity);*/
       // Enjoydish = (ImageView)rootView.findViewById(R.id.enjactive);
        enjoish = (ImageView)rootView.findViewById(R.id.enjd);
        //add = (Button) findViewById(R.id.add);
        inst= (EditText)rootView.findViewById(R.id.instruction);
        //close = (ImageView) findViewById(R.id.close);
        sharedPreferenceClass=new SharedPreferenceClass(getActivity());
       // unit = (Spinner) findViewById(R.id.spinner5);
        files = (ImageView)rootView.findViewById(R.id.image);
        files1 = (ImageView)rootView.findViewById(R.id.images);
        files2 = (ImageView)rootView.findViewById(R.id.images1);
       //active3.setVisibility(View.VISIBLE);
       /* ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll2.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent = new Intent(getActivity(), Connecton.class);
                // intent.putExtra("EML",det);
                getActivity().startActivity(intent);
                getActivity().finish();

            }
        });
        ll3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), Notifications.class);
                // intent.putExtra("EML",det);
                getActivity().startActivity(intent);
                getActivity().finish();

            }
        });
        ll4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll4.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent = new Intent(getActivity(), Contropanel.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll1.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent = new Intent(getActivity(), Home.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });*/
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dishname1=dname.getText().toString();
                 description=descr.getText().toString();
                 instructions=inst.getText().toString();

                if (description.equals("") || instructions.equals("") /*|| imageFilePath==null || imageFilePath1==null || imageFilePath2==null*/){
                    Toast.makeText(getActivity(),"Please give inputs",Toast.LENGTH_SHORT).show();
                }else {

                    AsyncCallWS task = new AsyncCallWS();
                    task.execute();

                }
            }
        });

       /* exp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                active3.setVisibility(View.INVISIBLE);
                active1.setVisibility(View.VISIBLE);
                Intent intent=new Intent(getActivity(),My_experience.class);
                startActivity(intent);
                finish();
            }
        });
        checkin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                active3.setVisibility(View.INVISIBLE);
                active4.setVisibility(View.VISIBLE);
                sharedPreferenceClass.setValue_boolean("CHK2",true);
                Intent intent = new Intent(Post.this, Chekin.class);
                startActivity(intent);
                finish();
            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sharedPreferenceClass.getValue_boolean("DISH")){
                    Intent intent=new Intent(Post.this,Home.class);
                    startActivity(intent);


                }else if(sharedPreferenceClass.getValue_boolean("DISH1")){
                    Intent intent=new Intent(Post.this,Home.class);
                    startActivity(intent);

                    finish();
                }else if(sharedPreferenceClass.getValue_boolean("POST")){
                    Intent intent=new Intent(Post.this,Home.class);
                    startActivity(intent);
                    finish();
                }

            }
        });
        enjoish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                active3.setVisibility(View.INVISIBLE);
                active2.setVisibility(View.VISIBLE);
                sharedPreferenceClass.setValue_boolean("ENJ",true);
                Intent intent=new Intent(Post.this,Enjoyeddish.class);
                startActivity(intent);
            }
        });*/
        try {
            val = helper.getTag();
        } catch (Exception e) {

        }
        if (val==null) {

        } else{
            Customdish adapter = new Customdish(getActivity(), R.layout.spiner_item, val);
        list.setAdapter(adapter);

            list.setOnTouchListener(new View.OnTouchListener() {
                // Setting on Touch Listener for handling the touch inside ScrollView
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // Disallow the touch request for parent scroll on touch of child view
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;


                }
            });
    }

        String format = "dd/MMM/yyyy H:mm:ss";
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        createdon1 = sdf.format(new Date());

       // files3=(ImageView) findViewById(R.id.images2);
       /* add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // list.setVisibility(View.VISIBLE);
                ingradient = text.getText().toString();
                quantity = qunity.getText().toString();

                if (ingradient.equals("") || quantity.equals("") || unit1.equals("")) {
                    Toast.makeText(getActivity(), "Plese give Input", Toast.LENGTH_SHORT).show();
                } else {
                    helper.Addetails(ingradient, quantity, unit1);
                    val = helper.getTag();
                }
                Customdish adapter = new Customdish(getActivity(), android.R.layout.simple_list_item_1, val);
                list.setAdapter(adapter);
                quant = "0" + "," + ingradient + "," + quantity + "," + weight_value + "," + unit1;
            }
        });*/
        ArrayAdapter adapter2 = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,units);

      /*  unit.setAdapter(adapter2);
        unit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                unit1 = unit.getItemAtPosition(position).toString();
                if (unit1.equals("GMS")) {
                    weight_value = "0";
                } else if (unit1.equals("LTR")) {
                    weight_value = "1";
                } else if (unit1.equals("LBS")) {
                    weight_value = "2";
                } else if (unit1.equals("OZ")) {
                    weight_value = "3";
                } else if (unit1.equals("TEASPOON")) {
                    weight_value = "4";
                } else if (unit1.equals("TEASPOON")) {
                    weight_value = "4";
                } else if (unit1.equals("CUP")) {
                    weight_value = "5";
                } else if (unit1.equals("DROPS")) {
                    weight_value = "6";
                } else if (unit1.equals("OTHERS")) {
                    weight_value = "7";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter adapter1 = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,Ingredient);

        text.setAdapter(adapter1);
        text.setThreshold(1);*/

        CusAdapters adapter3=new CusAdapters(getActivity(),R.layout.spiner_item,catgory,img);
        friends.setAdapter(adapter3);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(imageFilePath==null || imageFilePath1==null || imageFilePath2==null) {
                    final CharSequence[] items = {"Take Photo", "Choose from Library",
                            "Cancel"};

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Add Photo!");
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (items[item].equals("Take Photo")) {
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, REQUEST_CAMERA);
                            } else if (items[item].equals("Choose from Library")) {
                                Intent intent = new Intent(
                                        Intent.ACTION_PICK,
                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                intent.setType("image/*");
                                startActivityForResult(
                                        Intent.createChooser(intent, "Select File"), SELECT_FILE);

                            } else if (items[item].equals("Cancel")) {
                                dialog.dismiss();
                            }

                        }
                    });

                    builder.show();
                }else{
                   Toast.makeText(getActivity(),"You Have already Choose Three Images",Toast.LENGTH_SHORT).show();
                }
            }
        });
return rootView;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && requestCode == SELECT_FILE){
            onSelectFromGalleryResult(data);
        }
       /*else if(resultCode == Activity.RESULT_OK && requestCode == SELECT_FILE1){
            onSelectFromGalleryResult1(data);
        }*/
        else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CAMERA){
            onCaptureImageResult(data);
        }
        /*else if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CAMERA1){
            onCaptureImageResult1(data);
        }*/
        else{
            Toast.makeText(getActivity(), "Please try again !!!", Toast.LENGTH_SHORT).show();
        }


        super.onActivityResult(requestCode, resultCode, data);
    }

    private void onCaptureImageResult(Intent data) {

        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        if(imageFilePath==null){
            imageFilePath = destination.toString();


            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            files.setImageBitmap(thumbnail);
            byte[] b = bytes.toByteArray();
            imageName = Base64.encodeToString(b, Base64.DEFAULT);
            frsticn.setVisibility(View.VISIBLE);
            frsticn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    files.setImageBitmap(null);
                    imageFilePath=null;
                    frsticn.setVisibility(View.INVISIBLE);
                }
            });
        }else if(imageFilePath1==null){
            imageFilePath1 = destination.toString();
            File f = new File(imageFilePath1);

            imageName1 = f.getName();

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            files1.setImageBitmap(thumbnail);
            sndicn.setVisibility(View.VISIBLE);
            sndicn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    files1.setImageBitmap(null);
                    imageFilePath1=null;
                    sndicn.setVisibility(View.INVISIBLE);
                }
            });
        }else if(imageFilePath2==null){
            imageFilePath2 = destination.toString();
            File f = new File(imageFilePath2);

            imageName2 = f.getName();

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            files2.setImageBitmap(thumbnail);
            trdicn.setVisibility(View.VISIBLE);
            trdicn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    files2.setImageBitmap(null);
                    imageFilePath2=null;
                    trdicn.setVisibility(View.INVISIBLE);
                }
            });
        }



    }

   /* private void onSelectFromGalleryResult(Intent data) {
        Uri selectedImageUri = data.getData();
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        imageFilePath = cursor.getString(column_index).toString();

        BitmapFactory.decodeFile(imageFilePath);

        Bitmap bm = BitmapFactory.decodeFile(imageFilePath);

        files.setImageBitmap(bm);

    }*/
   private void onSelectFromGalleryResult(Intent data) {
       Uri selectedImageUri = data.getData();
       String[] projection = {MediaStore.MediaColumns.DATA};
       Cursor cursor = getActivity().managedQuery(selectedImageUri, projection, null, null,
               null);
       int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
       cursor.moveToFirst();

       if(imageFilePath==null){
           imageFilePath = cursor.getString(column_index).toString();

           BitmapFactory.decodeFile(imageFilePath);
           Bitmap bm1 = BitmapFactory.decodeFile(imageFilePath);
           File imagefile = new File(imageFilePath);
           FileInputStream fis = null;
           try {
               fis = new FileInputStream(imagefile);
           } catch (FileNotFoundException e) {
               e.printStackTrace();
           }

           Bitmap bm = BitmapFactory.decodeStream(fis);
           ByteArrayOutputStream baos = new ByteArrayOutputStream();
           bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
           byte[] b = baos.toByteArray();
           imageName = Base64.encodeToString(b, Base64.DEFAULT);
           // ImageNName="@" + imageName;
           files.setImageBitmap(bm1);
           frsticn.setVisibility(View.VISIBLE);
           frsticn.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   files.setImageBitmap(null);
                   imageFilePath=null;
                   frsticn.setVisibility(View.INVISIBLE);
               }
           });
       }
       else if(imageFilePath1==null){
           imageFilePath1 = cursor.getString(column_index).toString();
           File f = new File(imageFilePath1);

           imageName1 = f.getName();
           BitmapFactory.decodeFile(imageFilePath1);

           Bitmap bm = BitmapFactory.decodeFile(imageFilePath1);

           files1.setImageBitmap(bm);
           sndicn.setVisibility(View.VISIBLE);
           sndicn.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   files1.setImageBitmap(null);
                   imageFilePath1=null;
                   sndicn.setVisibility(View.INVISIBLE);
               }
           });
       }
       else if(imageFilePath2==null){
           imageFilePath2 = cursor.getString(column_index).toString();
           File f = new File(imageFilePath2);

           imageName2 = f.getName();
           BitmapFactory.decodeFile(imageFilePath2);

           Bitmap bm = BitmapFactory.decodeFile(imageFilePath2);

           files2.setImageBitmap(bm);
           trdicn.setVisibility(View.VISIBLE);
           trdicn.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   files2.setImageBitmap(null);
                   imageFilePath2=null;
                   trdicn.setVisibility(View.INVISIBLE);
               }
           });

       }

       /*else if(imageFilePath3.equals(""){
           imageFilePath3 = cursor.getString(column_index).toString();

           BitmapFactory.decodeFile(imageFilePath);

           Bitmap bm = BitmapFactory.decodeFile(imageFilePath);

           files3.setImageBitmap(bm);
       }*/
      // images=imageName+ "$" + imageName1+ "$" + imageName2 ;
   }
    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

           // aResponse = comp.Postdish("insert_dish",dishname1,imageName,instructions,quant,description,createdby,createdon1,ipaddress);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");


            if (aResponse.equals("0")) {
                Toast.makeText(getActivity(), "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (aResponse.equals("1")) {

                Toast.makeText(getActivity(), "You have successfully Posted !", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), Home.class);
                getActivity().startActivity(intent);
                getActivity().finish();
            } else {

            }
        }

    }
    /*public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems, itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }*/

   }
