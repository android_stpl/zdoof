package com.zdoof.stpl.zdoof.adapters;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.CircularNetworkImageView;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.activites.Home;
import com.zdoof.stpl.zdoof.activites.Profiles;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.activites.Comments;
import com.zdoof.stpl.zdoof.database.DatabaseHelper;
import com.zdoof.stpl.zdoof.webservice.PicConstant;
import com.zdoof.stpl.zdoof.webservice.WebserviceCallpost;

import java.util.ArrayList;

/**
 * Created by sujit on 3/3/16.
 */
public class CustomHomeadapter1 extends BaseAdapter {

    Home home;
    SharedPreferenceClass sharedPreferenceClass;
    int riid1, Sub_user_type = 0, Main_user_type = 0, Post_id, pty;
    String name1, usertype_img, idd = "", ptype = "", ifliked = "", posted_comments = "", place_of_dish = "", place_of_rest = "";
    String email, likesss;
    Typeface typeface;
    ViewHolder holder;
    String gg, post_userid, comments, share, riboncol, pic6;
    DatabaseHelper helper;

    static String cResponse, eResponse;
    final WebserviceCallpost com = new WebserviceCallpost();
    String imd_img, prfimg, name, type, description, pic3, style, pic4, pic5, shares, creaton, user, imd_img1, likess, Dtails;
    ArrayList<Detail> values;
    ArrayList<Detail> cmt;
    ArrayList<Detail> cmmnt;
    String lik, likk, comment, Review_post_id;
    String[] Liked;
    int total;
    ArrayList<String> cmn;
    //int pos;

    public CustomHomeadapter1(Home home, int spiner_item, ArrayList<Detail> values) {
        this.home = home;
        this.values = values;
        notifyDataSetChanged();
        sharedPreferenceClass = new SharedPreferenceClass(home);
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        //if (convertView == null) {
        LayoutInflater inflater = home.getLayoutInflater();
        convertView = inflater.inflate(R.layout.custom_new_ztimeline, null);
        holder = new ViewHolder();
        helper = new DatabaseHelper(home);
        //pos=position;
        holder.circularImageView = (CircularNetworkImageView) convertView.findViewById(R.id.profilePictureVie);
        Typeface custom_font = Typeface.createFromAsset(home.getAssets(), "OpenSans-Italic.ttf");
        holder.imd = (ImageView) convertView.findViewById(R.id.imageView23);
        holder.nam = (TextView) convertView.findViewById(R.id.textVie1);
        holder.types = (TextView) convertView.findViewById(R.id.textV1);
        holder.imm = (ImageView) convertView.findViewById(R.id.img);
        holder.descrip = (TextView) convertView.findViewById(R.id.tcc);
        holder.dshares = (TextView) convertView.findViewById(R.id.textyV1);
        holder.dcrton = (TextView) convertView.findViewById(R.id.postTime);
        holder.likeactive = (ImageView) convertView.findViewById(R.id.btnYum);
        holder.like = (ImageView) convertView.findViewById(R.id.kk);
        holder.texth = (TextView) convertView.findViewById(R.id.text7);
        holder.texth_red = (TextView) convertView.findViewById(R.id.textred);
        holder.likes = (TextView) convertView.findViewById(R.id.likes);
        holder.cmntlist = (ListView) convertView.findViewById(R.id.lsst);
        holder.cment = (TextView) convertView.findViewById(R.id.coments);
        holder.place_dish = (TextView) convertView.findViewById(R.id.place);
        holder.place_rest = (TextView) convertView.findViewById(R.id.resturant);
        holder.at = (TextView) convertView.findViewById(R.id.at);
        // holder.mDemoSlider = (SliderLayout)convertView.findViewById(R.id.slider);
        // RelativeLayout rl = (RelativeLayout) convertView.findViewById(R.id.rr);
        holder.orange = (ImageView) convertView.findViewById(R.id.orange);
        holder.blue = (ImageView) convertView.findViewById(R.id.blue);
        holder.red = (ImageView) convertView.findViewById(R.id.red);
        holder.green = (ImageView) convertView.findViewById(R.id.green);

        holder.workimg = (ImageView) convertView.findViewById(R.id.workimg);
        holder.ll = (LinearLayout) convertView.findViewById(R.id.ll);
        holder.ll1 = (LinearLayout) convertView.findViewById(R.id.ll1);
        //holder.allclick = (LinearLayout) convertView.findViewById(R.id.allclick);
        //holder.relative = (RelativeLayout) convertView.findViewById(R.id.rll);
        // convertView.setTag(holder);
      /*  }else{
            holder = (ViewHolder) convertView.getTag();
        }*/
        post_userid = sharedPreferenceClass.getValue_string("UIDD");

        // shares = values.get(position).getDshare();
        user = values.get(position).getUserid();
        creaton = values.get(position).getDcreateon();
        usertype_img = values.get(position).getUtypeimage();
        prfimg = values.get(position).getUpdateimage();
        imd_img = values.get(position).getDprf();
        name1 = values.get(position).getDname();
        type = values.get(position).getDwrkas();
        likess = values.get(position).getPost_like();
        posted_comments = values.get(position).getPostedcomments();
        share = values.get(position).getDshare();
        Dtails = values.get(position).getDdtails();
        riboncol = values.get(position).getRibon();
        if (riboncol.equals("ribbon-blue")) {
            holder.blue.setVisibility(View.VISIBLE);
        } else if (riboncol.equals("ribbon-red")) {
            holder.red.setVisibility(View.VISIBLE);
        } else if (riboncol.equals("ribbon-green")) {
            holder.green.setVisibility(View.VISIBLE);
        } else {
            holder.orange.setVisibility(View.VISIBLE);
        }
        place_of_dish = values.get(position).getPlace_dish();
        place_of_rest = values.get(position).getPlace_rest();
        if (place_of_dish.equals("")) {

        } else if (place_of_dish.equals("null")) {

        } else {

            holder.place_dish.setVisibility(View.VISIBLE);
            // holder.at.setVisibility(View.VISIBLE);
            holder.place_dish.setText(place_of_dish + " ");
        }
        if (place_of_rest.equals("")) {

        } else {
            holder.place_rest.setVisibility(View.VISIBLE);
            holder.at.setVisibility(View.VISIBLE);
            holder.place_rest.setText(" " + place_of_rest);
        }
        // comments=values.get(pos).getComments();

        idd = values.get(position).getIdd();
        ptype = values.get(position).getPtype();

        try {
            Post_id = Integer.parseInt(idd);
            pty = Integer.parseInt(ptype);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        ifliked = values.get(position).getIflike();
        if (ifliked == null) {

        } else if (ifliked.equals("blog-btn choose")) {
            holder.likeactive.setVisibility(View.VISIBLE);
            holder.like.setVisibility(View.GONE);
            holder.texth.setVisibility(View.GONE);
            holder.texth_red.setVisibility(View.VISIBLE);

        } else {

            holder.likeactive.setVisibility(View.GONE);
            holder.like.setVisibility(View.VISIBLE);
            holder.texth.setVisibility(View.VISIBLE);
            holder.texth_red.setVisibility(View.GONE);
        }
        style = values.get(position).getStyle();
        user = values.get(position).getUserid();
        holder.likes.setText(likess);
        holder.cment.setText(posted_comments);

        try {

            riid1 = Integer.parseInt(post_userid);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        pic5 = PicConstant.PROFILE_URL1 + usertype_img;

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(home).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .resetViewBeforeLoading(true)

                // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.blankpeople)
                .showImageOnLoading(R.drawable.uute).build();
//initialize image view
//download and display image from url
        imageLoader.displayImage(pic5, holder.workimg, options);

        pic3 = PicConstant.PROFILE_URL1 + prfimg;
        /*pic6=PicConstant.PROFILE_URL2;
        imageLoader.displayImage(pic6, holder.circularImageView, options);*/
        imageLoader.displayImage(pic3, holder.circularImageView, options);
//initialize image view
//download and displamDemoSlider = (SliderLayout)findViewById(R.id.slider);
        ///////

        //////
        if (style == null) {

        } else if (style.equals("display: none")) {

        } else if (style.equals("")) {
            if (imd_img.contains("/zdoof-admin/dish/post-image/")) {
                holder.imm.setVisibility(View.VISIBLE);
                pic4 = PicConstant.PROFILE_URL1 + imd_img;
                imageLoader.displayImage(pic4, holder.imm, options);
                // post_pic=Integer.parseInt(pic4);
            } else if (imd_img.contains("/zdoof-admin/dish/dish-image/")) {
                holder.imm.setVisibility(View.VISIBLE);
                pic4 = PicConstant.PROFILE_URL1 + imd_img;
                imageLoader.displayImage(pic4, holder.imm, options);
                // ArrayList<String> file_maps = new ArrayList<String>();
                /*ArrayList<String> file_image = new ArrayList<String>();

                file_image.add(pic4);
                for(int i=0;i<file_image.size();i++){
                    TextSliderView textSliderView = new TextSliderView(home);
                    // initialize a SliderLayout

                    textSliderView

                            .image(file_image.get(i))
                            .setScaleType(BaseSliderView.ScaleType.Fit)
                            .setOnSliderClickListener((BaseSliderView.OnSliderClickListener) this);

                    //add your extra information
                    textSliderView.bundle(new Bundle());

                    holder.mDemoSlider.addSlider(textSliderView);
                }
                holder.mDemoSlider.setPresetTransformer(SliderLayout.Transformer.DepthPage);
                holder.mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                holder.mDemoSlider.setCustomAnimation(new DescriptionAnimation());
                holder.mDemoSlider.setDuration(4000);
                holder.mDemoSlider.addOnPageChangeListener((ViewPagerEx.OnPageChangeListener) this);
                imageLoader.displayImage(pic3, holder.circularImageView, options);
                imageLoader.displayImage(pic4, holder.imm, options);*/
            }
        }
        holder.nam.setText(values.get(position).getDname());
        /*if(type==null){
            holder.types.setText("N/A");
        }
        else if (type.equals("null")) {
            holder.types.setText("N/A");
        } else {
            holder.types.setText(type);
        }*/
        holder.descrip.setText(Html.fromHtml(values.get(position).getDdtails()));
        holder.dshares.setTypeface(custom_font);
        holder.dshares.setText(values.get(position).getDshare());
        holder.dcrton.setText(values.get(position).getDcreateon());
//        holder.relative.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String rrid = values.get(position).getUserid();
//                riid1 = Integer.parseInt(rrid);
//                Intent intent = new Intent(home, Profiles.class);
//                intent.putExtra("USERID", rrid);
//                sharedPreferenceClass.setValue_string("BACKPAGE", "home");
//                home.startActivity(intent);
//            }
//        });

//        holder.allclick.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String rrid = values.get(position).getUserid();
//                riid1 = Integer.parseInt(rrid);
//                Intent intent = new Intent(home, Profiles.class);
//                intent.putExtra("USERID", rrid);
//                sharedPreferenceClass.setValue_string("BACKPAGE", "home");
//                home.startActivity(intent);
//            }
//        });
        holder.texth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ptype = values.get(position).getPtype();
                pty = Integer.parseInt(ptype);
                idd = values.get(position).getIdd();
                Post_id = Integer.parseInt(idd);
                values.get(position).setIflike("blog-btn choose");
                likess = values.get(position).getPost_like();
                likess = likess.replace(" yums", "");
                // 0 yums
                try {
                    if (likess != null && likess.trim().length() > 0 && likess.contains(" ")) {
                        String[] spilt = likess.split(" ");
                        String num = spilt[0];
                        if (num != null && num.trim().length() > 0 && TextUtils.isDigitsOnly(num)) {
                            total = Integer.parseInt(likess) + 1;
                        } else {
                            total = 0;
                        }
                    }
                } catch (Exception exception) {
                    Log.e("CustomHomeAdapter", "Exception", exception);
                }

                values.get(position).setPost_like(String.valueOf(total) + " yums");
                likesss = values.get(position).getPost_like();
                helper.upDate(idd, likesss, ifliked);
               /* ifliked="blog-btn choose";
                likess=String.valueOf(total)+" yums";*/
                // helper.upDate(idd,ifliked,likess);
                // values.get(position).setPost_like(likess);
                  /*  holder.likeactive.setVisibility(View.VISIBLE);
                    holder.like.setVisibility(View.GONE);
                    holder.texth.setTextColor(Color.parseColor("#ff0000"));*/
                notifyDataSetChanged();
               /* } else {
                    values.get(position).setIflike("blog-btn");
                    //values.get(pos).setPost_like(likess);
                   *//* holder.likeactive.setVisibility(View.GONE);
                    holder.like.setVisibility(View.VISIBLE);
                    holder.texth.setTextColor(Color.parseColor("#DDDDDD"));*//*
                    notifyDataSetChanged();
                }*/

                //   Toast.makeText(home, String.valueOf(position), Toast.LENGTH_SHORT).show();

                AsyncCallWS2 task = new AsyncCallWS2(holder.likes);
                task.execute();
            }
        });
        holder.texth_red.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ptype = values.get(position).getPtype();
                pty = Integer.parseInt(ptype);
                idd = values.get(position).getIdd();
                Post_id = Integer.parseInt(idd);
                values.get(position).setIflike("blog-btn");
                likess = values.get(position).getPost_like();
                likess = likess.replace(" yums", "");
                total = Integer.parseInt(likess) - 1;
                values.get(position).setPost_like(String.valueOf(total) + " yums");
                likesss = values.get(position).getPost_like();
                helper.upDate(idd, likesss, ifliked);

                // likess=String.valueOf(total)+" yums";
                //helper.upDate(idd,ifliked,likess);
                // values.get(position).setPost_like(likess);
                  /*  holder.likeactive.setVisibility(View.VISIBLE);
                    holder.like.setVisibility(View.GONE);
                    holder.texth.setTextColor(Color.parseColor("#ff0000"));*/
                notifyDataSetChanged();
               /* } else {
                    values.get(position).setIflike("blog-btn");
                    //values.get(pos).setPost_like(likess);
                   *//* holder.likeactive.setVisibility(View.GONE);
                    holder.like.setVisibility(View.VISIBLE);
                    holder.texth.setTextColor(Color.parseColor("#DDDDDD"));*//*
                    notifyDataSetChanged();
                }*/

                // Toast.makeText(home, String.valueOf(position), Toast.LENGTH_SHORT).show();

                AsyncCallWS2 task = new AsyncCallWS2(holder.likes);
                task.execute();
            }
        });

        holder.likeactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ptype = values.get(position).getPtype();
                pty = Integer.parseInt(ptype);
                idd = values.get(position).getIdd();
                Post_id = Integer.parseInt(idd);
                values.get(position).setIflike("blog-btn");
                likess = values.get(position).getPost_like();
                likess = likess.replace(" yums", "");
                total = Integer.parseInt(likess) - 1;
                values.get(position).setPost_like(String.valueOf(total) + " yums");
                // values.get(position).setPost_like(likess);
                  /*  holder.likeactive.setVisibility(View.VISIBLE);
                    holder.like.setVisibility(View.GONE);
                    holder.texth.setTextColor(Color.parseColor("#ff0000"));*/
               /* ifliked="blog-btn";
                likess=String.valueOf(total)+" yums";*/
                //helper.upDate(idd,ifliked,likess);
                likesss = values.get(position).getPost_like();
                helper.upDate(idd, likesss, ifliked);

                notifyDataSetChanged();

               /* } else {
                    values.get(position).setIflike("blog-btn");
                    //values.get(pos).setPost_like(likess);
                   *//* holder.likeactive.setVisibility(View.GONE);
                    holder.like.setVisibility(View.VISIBLE);
                    holder.texth.setTextColor(Color.parseColor("#DDDDDD"));*//*
                    notifyDataSetChanged();
                }*/


                //   Toast.makeText(home, String.valueOf(position), Toast.LENGTH_SHORT).show();

                AsyncCallWS2 task = new AsyncCallWS2(holder.likes);
                task.execute();
            }
        });
        holder.like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ptype = values.get(position).getPtype();
                pty = Integer.parseInt(ptype);
                idd = values.get(position).getIdd();
                Post_id = Integer.parseInt(idd);
                values.get(position).setIflike("blog-btn choose");
                likess = values.get(position).getPost_like();
                likess = likess.replace(" yums", "");
                total = Integer.parseInt(likess) + 1;
                values.get(position).setPost_like(String.valueOf(total) + " yums");
                likesss = values.get(position).getPost_like();
                helper.upDate(idd, likesss, ifliked);
                /*ifliked="blog-btn choose";
                likess=String.valueOf(total)+" yums";*/
                // helper.upDate(idd,ifliked,likess);
                // values.get(position).setPost_like(likess);
                  /*  holder.likeactive.setVisibility(View.VISIBLE);
                    holder.like.setVisibility(View.GONE);
                    holder.texth.setTextColor(Color.parseColor("#ff0000"));*/


                notifyDataSetChanged();
               /* } else {
                    values.get(position).setIflike("blog-btn");
                    //values.get(pos).setPost_like(likess);
                   *//* holder.likeactive.setVisibility(View.GONE);
                    holder.like.setVisibility(View.VISIBLE);
                    holder.texth.setTextColor(Color.parseColor("#DDDDDD"));*//*
                    notifyDataSetChanged();
                }*/

             /*   ptype = values.get(pos).getPtype();
                pty = Integer.parseInt(ptype);
                idd = values.get(pos).getIdd();
                Post_id = Integer.parseInt(idd);*/

                //    Toast.makeText(home, String.valueOf(position), Toast.LENGTH_SHORT).show();

                AsyncCallWS2 task = new AsyncCallWS2(holder.likes);
                task.execute();
            }
        });
        holder.ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ptype = values.get(position).getPtype();
                idd = values.get(position).getIdd();
                Post_id = Integer.parseInt(idd);
                pty = Integer.parseInt(ptype);
                prfimg = values.get(position).getUpdateimage();
                name1 = values.get(position).getDname();
                imd_img = values.get(position).getDprf();
                user = values.get(position).getUserid();
                creaton = values.get(position).getDcreateon();
                type = values.get(position).getDwrkas();
                style = values.get(position).getStyle();
                share = values.get(position).getDshare();
                Dtails = values.get(position).getDdtails();
                // usertype_img = values.get(position).getUtypeimage();
                Intent intent = new Intent(home, Comments.class);

                intent.putExtra("POST_ID", Post_id);
                intent.putExtra("_USER", riid1);
                intent.putExtra("CREATE_ON", creaton);
                intent.putExtra("PTYPE", pty);
                intent.putExtra("PFIMG", prfimg);
                intent.putExtra("NMA", name1);
                intent.putExtra("MID", imd_img);
                intent.putExtra("USRID", user);
                intent.putExtra("WRK", user);
                intent.putExtra("STYL", style);
                intent.putExtra("DSHR", share);
                intent.putExtra("DTL", Dtails);
                home.startActivity(intent);
                home.finish();
                //  Toast.makeText(home, "Clicked on comment", Toast.LENGTH_SHORT).show();
            }
        });

       /* try {
            idd = values.get(pos).getIdd();

            AsyncCallWS4 task4 = new AsyncCallWS4();
            task4.execute();
            cmt = helper.getcomments(idd);

            gg = cmt.get(pos).getComment();
            if (gg.equals("")) {

            } else {
                holder.cment.setVisibility(View.VISIBLE);
                holder.cment.setText(gg);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }*/

        return convertView;
    }

    public void add(ArrayList<Detail> val) {
        this.values = val;
    }


    ////////////////
    class AsyncCallWS2 extends AsyncTask<Void, Void, Void> {

        TextView tt;
        public AsyncCallWS2(TextView tt){
            this.tt = tt;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");

            cResponse = com.Addlike("post_like", Post_id, pty, riid1, creaton);

            Log.i("HomeAdapter Response", cResponse);

            Liked = cResponse.split(",");
            lik = Liked[0];
            likess = Liked[1];
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");

            if (lik.equals("1")) {

                tt.setText(likess);
                notifyDataSetChanged();
/*
                ifliked="blog-btn choose";
                likess=String.valueOf(total)+" yums";
             */
                // Toast.makeText(home, "You liked the post!", Toast.LENGTH_SHORT).show();
                //notifyDataSetChanged();
            } else if (lik.equals("2")) {
                 tt.setText(likess);
                 notifyDataSetChanged();
               /* ifliked="blog-btn";
                likess=String.valueOf(total)+" yums";
               helper.upDate(idd,ifliked,likess);
*/
                // Toast.makeText(home, "You Dislike the post!", Toast.LENGTH_LONG).show();

            } else {
                sharedPreferenceClass.setValue_boolean("HOME", true);
                //  home.startActivity(intent);

            }

        }
    }

    private static class ViewHolder {
        CircularNetworkImageView circularImageView;
        ImageView imm, like, likeactive;
        LinearLayout ll;
        RelativeLayout relative;
        LinearLayout ll1;
        TextView nam, texth, texth_red;
        TextView likes, cment, descrip, dshares, dcrton, place_dish, place_rest, at;
        ImageView imd, workimg;
        SliderLayout mDemoSlider;
        ImageView orange, blue, red, green;
        TextView types;
        LinearLayout allclick;
        ListView cmntlist;

    }


    public boolean setListViewHeightBasedOnItems(ListView cmntlist) {

        ListAdapter listAdapter = cmntlist.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, cmntlist);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = cmntlist.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = cmntlist.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            cmntlist.setLayoutParams(params);
            cmntlist.requestLayout();
            return true;

        } else {
            return false;
        }

    }
}
