package com.zdoof.stpl.zdoof.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.zdoof.stpl.zdoof.commons.Detail;

import java.util.ArrayList;

/**
 * Created by STPL-011 on 29-10-2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "zdoof";
    private static final String TABLE_INGREDIANT = "ingrediant";
    private static final String ID = "id";
    private static final String INGREDIAENT = "ingredient";
    private static final String QUANTITY = "quantity";
    private static final String UNITS = "units";
//Ztimeline table
    private static final String TABLE_ZTIMELINE = "ztimeline";
    private static final String PTYPE= "ptype";
    private static final String REBCOL = "rebcol";
    private static final String IDD = "idd";
    private static final String UID = "uid";
    private static final String NAME = "name";
    private static final String PROFMIG = "profimg";
    private static final String DISHURL = "dishurl";
    private static final String RESTURL = "resturl";
    private static final String PLACE_DISH = "place_dish";
    private static final String PLACE_REST = "place_rest";
    private static final String AT = "at";
    private static final String DETAILS = "details";
    private static final String UTYPE = "utype";
    private static final String UTYPE_IMAGE = "utype_image";
    private static final String ORDERDT = "orderdt";
    private static final String CREATED_ON = "created_on";
    private static final String MID_MIG = "mid_mig";
    private static final String STYLE = "style";
    private static final String SHARE = "share";
    private static final String COMMENTSTYLE = "commentstyle";
    private static final String POST_LIKE = "post_like";
    private static final String IF_LIKE = "if_like";
    private static final String POSTED_COMMENTS = "posted_comments";
    private static final String SUB_POST = "sub_post";

//Connection table
private static final String TABLE_MYCONNECTION = "myconnection";
    private static final String CONNECTION_ID= "connection_id";
    private static final String FROM_USER_ID = "from_user_id";
    private static final String TO_USER_ID = "to_user_id";
    private static final String USER_ID = "user_id";
    private static final String USER_NAME = "user_name";
    private static final String MAIN_USER_TYPE = "main_user_type";
    private static final String USER_STATUS = "user_status";
    private static final String EMAIL = "email";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String NAMEE = "namee";
    private static final String PROFILE_IMAGE = "profile_image";
    private static final String TO_IMAGE = "to_image";
    private static final String UTYP = "utyp";
    private static final String UTYPIMG = "utypimg";
    private static final String CREATED_ONN = "created_onn";
    private static final String THUUSER = "thuuser";
    private static final String TOTCOUNT = "totcount";
    private static final String TOTRECIPE = "totrecipe";
    private static final String FROM_USER = "from_user";
    private static final String TO_USER = "to_user";
    private static final String FROM_ID = "from_id";
//comment table
private static final String TABLE_COMMENT = "comment";
    private static final String COMMENT= "comment";
    private static final String POST_ID = "post_id";
    private static final String PTYPES= "ptypes";
    private static final String CREATEBY = "createby";
    private static final String CREATE_ON = "create_on";
    private static final String PROFILEPIC = "profilepic";
    private static final String NAMES = "names";
    static Cursor cursoor = null;
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_COMMENT = "CREATE TABLE IF NOT EXISTS " + TABLE_COMMENT +
                "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COMMENT + " TEXT,"
                + POST_ID + " TEXT,"
                + PTYPES + " TEXT,"
                + CREATEBY + " TEXT,"
                + CREATE_ON + " TEXT,"
                + PROFILEPIC + " TEXT,"
                + NAMES + " TEXT)";

        db.execSQL(CREATE_TABLE_COMMENT);

        String CREATE_TABLE_INGREDIANT = "CREATE TABLE IF NOT EXISTS " + TABLE_INGREDIANT +
                "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + INGREDIAENT + " TEXT,"
                + QUANTITY + " TEXT,"
                + UNITS + " TEXT)";

        db.execSQL(CREATE_TABLE_INGREDIANT);
       String CREATE_TABLE_ZTIMELINE = "CREATE TABLE IF NOT EXISTS " + TABLE_ZTIMELINE+
                "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + PTYPE + " TEXT,"
                + REBCOL + " TEXT,"
                + IDD + " TEXT,"
                + UID + " TEXT, "
                + NAME + " TEXT, "
                + PROFMIG + " TEXT, "
                + DISHURL + " TEXT, "
                + RESTURL + " TEXT, "
                + PLACE_DISH + " TEXT, "
                + PLACE_REST + " TEXT, "
                + AT + " TEXT, "
                + DETAILS + " TEXT, "
                + UTYPE + " TEXT, "
               + UTYPE_IMAGE + " TEXT, "
               + ORDERDT + " TEXT, "
               + CREATED_ON + " TEXT, "
               + MID_MIG + " TEXT, "
               + STYLE + " TEXT, "
               + SHARE + " TEXT, "
               + COMMENTSTYLE + " TEXT, "
               + POST_LIKE + " TEXT, "
               + IF_LIKE+ " TEXT, "
               + POSTED_COMMENTS+ " TEXT, "
                + SUB_POST + " TEXT)";

        db.execSQL(CREATE_TABLE_ZTIMELINE);

    String CREATE_TABLE_MYCONNECTION = "CREATE TABLE IF NOT EXISTS " + TABLE_MYCONNECTION+
            "("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + CONNECTION_ID + " TEXT,"
            + FROM_USER_ID + " TEXT,"
            + TO_USER_ID + " TEXT,"
            + USER_ID + " TEXT, "
            + USER_NAME + " TEXT, "
            + MAIN_USER_TYPE + " TEXT, "
            + USER_STATUS + " TEXT, "
            + EMAIL + " TEXT, "
            + FIRST_NAME + " TEXT, "
            + LAST_NAME + " TEXT, "
            + NAMEE + " TEXT, "
            + PROFILE_IMAGE + " TEXT, "
            + TO_IMAGE + " TEXT, "
            + UTYPE_IMAGE + " TEXT, "
            + UTYP + " TEXT, "
            + UTYPIMG + " TEXT, "
            + CREATED_ONN + " TEXT, "
            + THUUSER + " TEXT, "
            + TOTCOUNT + " TEXT, "
            + TOTRECIPE + " TEXT, "
            + FROM_USER + " TEXT, "
            + TO_USER + " TEXT, "
            + COMMENTSTYLE + " TEXT, "
            + FROM_ID + " TEXT)";
    db.execSQL(CREATE_TABLE_MYCONNECTION);
}
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INGREDIANT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ZTIMELINE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMENT);
        onCreate(db);
    }
//add value of Ztimeline
public boolean AddZtimeline(String ptype, String rebcol, String idd, String uid, String name,
                               String profmig, String dishurl, String restul, String place_dish,
                               String place_rest, String at, String details, String utype,
                               String utype_image, String orderdt, String creared_on, String mid_mig, String style, String share,
                            String commentstyle,String post_like,String if_like,String poste_comments,String sub_post) {

    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();
    values.put(PTYPE,ptype);
    values.put(REBCOL,rebcol);
    values.put(IDD, idd);
    values.put(UID, uid);
    values.put(NAME, name);
    values.put(PROFMIG, profmig);
    values.put(DISHURL, dishurl);
    values.put(RESTURL, restul);
    values.put(PLACE_DISH, place_dish);
    values.put(PLACE_REST, place_rest);
    values.put(AT,at);
    values.put(DETAILS, details);
    values.put(UTYPE, utype);
    values.put(UTYPE_IMAGE, utype_image);
    values.put(ORDERDT, orderdt);
    values.put(CREATED_ON, creared_on);
    values.put(MID_MIG, mid_mig);
    values.put(STYLE, style);
    values.put(SHARE, share);
    values.put(COMMENTSTYLE, commentstyle);
    values.put(POST_LIKE,post_like);
    values.put(IF_LIKE,if_like);
    values.put(POSTED_COMMENTS,poste_comments);
    values.put(SUB_POST, sub_post);
    db.insert(TABLE_ZTIMELINE, null, values);
    return true;

}

    public int getNewCounts() {
        String countQuery = "SELECT  * FROM " + TABLE_ZTIMELINE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }
    /*public int getNewCounts() {
        String countQuery = "SELECT count(*) FROM " + TABLE_ZTIMELINE*//* + " WHERE " + UID + " = '" + uid+"'"*//*;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        return cursor.getCount();
    }*/
    //Insert data of My connection
    public boolean AddMyconnection(String connection_id, String from_user_id, String to_user_id, String user_id,
                                   String user_name,
                                   String main_user_type, String user_status, String email, String first_name,
                                   String last_name, String namee, String profile_image, String to_image,
                                   String utyp, String utypimg, String created_onn, String thuuser,
                                   String totcount,String totrecipe,String from_user,String to_user,String from_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(CONNECTION_ID,connection_id);
        values.put(FROM_USER_ID,from_user_id);
        values.put(TO_USER_ID, to_user_id);
        values.put(USER_ID, user_id);
        values.put(USER_NAME, user_name);
        values.put(MAIN_USER_TYPE, main_user_type);
        values.put(USER_STATUS, user_status);
        values.put(EMAIL, email);
        values.put(FIRST_NAME, first_name);
        values.put(LAST_NAME, last_name);
        values.put(NAMEE,namee);
        values.put(PROFILE_IMAGE, profile_image);
        values.put(TO_IMAGE, to_image);
        values.put(UTYP, utyp);
        values.put(UTYPIMG, utypimg);
        values.put(CREATED_ONN, created_onn);
        values.put(THUUSER, thuuser);
        values.put(TOTCOUNT, totcount);
        values.put(TOTRECIPE, totrecipe);
        values.put(FROM_USER, from_user);
        values.put(TO_USER, to_user);
        values.put(FROM_ID, from_id);
        db.insert(TABLE_MYCONNECTION, null, values);
        return true;

    }
    //getcount of connection
    public int getNewCountconntion() {
        String countQuery = "SELECT * FROM " + TABLE_MYCONNECTION;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        return cursor.getCount();
    }
    //get values of connection
    public ArrayList<Detail> getConnections() {
        // TODO Auto-generated method stub

        ArrayList<Detail> list = new ArrayList<Detail>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_MYCONNECTION;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {

                Detail pro = new Detail();
                pro.setUserid(cursor.getString(4));
                pro.setEmail(cursor.getString(8));
                pro.setName(cursor.getString(11));
                pro.setPfimage(cursor.getString(12));
                pro.setUtype(cursor.getString(15));
                pro.setTotalcount(cursor.getString(19));
                pro.setThumbsup(cursor.getString(18));
                pro.setRaceipies(cursor.getString(20));
                list.add(pro);

            } while (cursor.moveToNext());

            return list;
        }
        return null;
    }

    //insert comments
    public boolean Addcomnt(String comment,String postid, String posttype,String createby,String creaton,String profilepic,String names) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COMMENT,comment);
        values.put(POST_ID,postid);
        values.put(PTYPES,posttype);
        values.put(CREATEBY, createby);
        values.put(CREATE_ON, creaton);
        values.put(PROFILEPIC, profilepic);
        values.put(NAMES, names);
        db.insert(TABLE_COMMENT, null, values);
        return true;

    }
    //Getcount of comment
    public int getCmntcount(String postid) {
        String countQuery = "SELECT * FROM " + TABLE_COMMENT  + " WHERE " + POST_ID + " = '" + postid+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        return cursor.getCount();
    }
    //get value from comment table

    public  ArrayList<Detail>getcomments(String postid){
        ArrayList<Detail>ll=new ArrayList<Detail>();
        String selectQuery = "SELECT * FROM " + TABLE_COMMENT  + " WHERE " + POST_ID + " = '" + postid+"'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                Detail pro = new Detail();
                pro.setComment(cursor.getString(1));
                pro.setCre(cursor.getString(5));
                pro.setProfile(cursor.getString(6));
                pro.setNames(cursor.getString(7));
                String cmmen = cursor.getString(1);
                String profilepic=cursor.getString(6);
                String name=cursor.getString(7);
                ll.add(pro);

            } while (cursor.moveToNext());

            return ll;
        }
        return null;
    }
    public void upDatebyid(String idd,String comment){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COMMENT, comment);


     /*db.update(TABLE_REMINDER, values, TITLE+"="+name, null);*/
        db.update(TABLE_COMMENT, values, POST_ID + "= ?", new String[] {idd});
    }
    public void upDate(String idd,String post_like,String if_like){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(POST_LIKE, post_like);
        values.put(IF_LIKE, if_like);


	    /*db.update(TABLE_REMINDER, values, TITLE+"="+name, null);*/
        db.update(TABLE_ZTIMELINE, values, IDD + "= ?",new String[] {idd});
    }
    public boolean Addetails(String ingadient,String quantity, String units) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(INGREDIAENT,ingadient);
        values.put(QUANTITY,quantity);
        values.put(UNITS, units);
        db.insert(TABLE_INGREDIANT, null, values);
        return true;

    }
    public ArrayList<Detail> getTags() {
        // TODO Auto-generated method stub

        ArrayList<Detail> list = new ArrayList<Detail>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_ZTIMELINE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                Detail pro = new Detail();
                pro.setPtype(cursor.getString(1));
                pro.setRibon(cursor.getString(2));
                pro.setIdd(cursor.getString(3));
                String j=cursor.getString(3);
                pro.setUserid(cursor.getString(4));
                pro.setPlace_dish(cursor.getString(9));
                pro.setPlace_rest(cursor.getString(10));
                pro.setAt(cursor.getString(11));
                pro.setDprf(cursor.getString(17));
                pro.setUtypeimage(cursor.getString(14));
                String ss =cursor.getString(14);
                pro.setUpdateimage(cursor.getString(6));
                String s =cursor.getString(6);
                pro.setDwrkas(cursor.getString(13));
                pro.setDname(cursor.getString(5));
                pro.setStyle(cursor.getString(18));
                pro.setDdtails(cursor.getString(12));
                pro.setDshare(cursor.getString(19));
                pro.setDcreateon(cursor.getString(16));
                pro.setPost_like(cursor.getString(21));
                String jj=cursor.getString(21);
                pro.setIflike(cursor.getString(22));
                String pp=cursor.getString(22);
                pro.setPostedcomments(cursor.getString(23));
                String ppt=cursor.getString(23);
                list.add(pro);

            } while (cursor.moveToNext());

            return list;
        }
        return null;
    }

    /*public boolean Addetails1(String pid,String name, String price, String description, String date,String image,String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PID,pid);
        values.put(NAME,name);
        values.put(DATE, date);
        values.put(PRICE, price);
        values.put(IMAGE,image);
        values.put(DESCRIPTION, description);
        values.put(USERNAME, username);
        db.insert(TABLE_WISH, null, values);
        return true;

    }*/

  /*  public int getAddToCartCount() {
        String countQuery = "SELECT * FROM " + TABLE_PRODUCT;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        return cursor.getCount();
    }*/
   /* public int getCount() {
        String selectQuery = "SELECT  * FROM  " + TABLE_PRODUCT;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // return count
        return cursor.getCount();
    }*/

       public int getNewCount() {
           String countQuery = "SELECT * FROM " + TABLE_INGREDIANT/* + " WHERE " + USERNAME + " = '" + username+"'"*/;
           SQLiteDatabase db = this.getReadableDatabase();
           Cursor cursor = db.rawQuery(countQuery, null);
           return cursor.getCount();
       }
   /* public int getNewCount1(String username) {
        String countQuery = "SELECT * FROM " + TABLE_WISH + " WHERE " + USERNAME + " = '"+ username+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        return cursor.getCount();
    }*/

   public ArrayList<Detail> getTag() {
        // TODO Auto-generated method stub

        ArrayList<Detail> list = new ArrayList<Detail>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_INGREDIANT;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                Detail pro = new Detail();
                pro.setId(cursor.getString(0));
                pro.setIngradiant(cursor.getString(1));
                String r=cursor.getString(1);
                pro.setQuantity(cursor.getString(2));
                pro.setUnit(cursor.getString(3));
              /*  pro.setName(cursor.getString(3));
                pro.setPrice(cursor.getString(4));
                pro.setImage(cursor.getString(5));
                pro.setDate(cursor.getString(6));*/
                String u=cursor.getString(1);

               /* String n=cursor.getString(2);
                String i=cursor.getString(3);
                String a=cursor.getString(4);
                String t=cursor.getString(5);
               String x=cursor.getString(6);*/

                list.add(pro);

            } while (cursor.moveToNext());

            return list;
        }
        return null;
    }
    public boolean DeleteSingle(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        long i= db.delete(TABLE_COMMENT, POST_ID + " = ?", new String[]{id});
        if(i>0){
            return true;
        }else{
            return false;

        }

    }
    public void Delete() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ZTIMELINE,null,null);
        db.close();

    }
    public void upDate_byid(String id,String ingradiant,String quantity,String unit){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(INGREDIAENT, ingradiant);
        values.put(QUANTITY, quantity);
        values.put(UNITS, unit);

     /*db.update(TABLE_REMINDER, values, TITLE+"="+name, null);*/
        db.update(TABLE_INGREDIANT, values, ID + "= ?", new String[] {id});
    }
/*
    public ArrayList<Product> getTag1() {
        // TODO Auto-generated method stub

        ArrayList<Product> list1 = new ArrayList<Product>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_WISH;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                Product pro = new Product();
                pro.setId(cursor.getString(1));
                pro.setUsername(cursor.getString(6));
                pro.setDescription(cursor.getString(5));
                pro.setName(cursor.getString(2));
                pro.setPrice(cursor.getString(4));
                pro.setImage(cursor.getString(7));
                pro.setDate(cursor.getString(3));

                String r = cursor.getString(1);
                String n=cursor.getString(2);
                String i=cursor.getString(3);
                String a=cursor.getString(4);
                String t=cursor.getString(5);
                String x=cursor.getString(6);



                list1.add(pro);


            } while (cursor.moveToNext());

            return list1;
        }
        return null;
    }

  public ArrayList<Product> getTag() {
      // TODO Auto-generated method stub

      ArrayList<Product> list = new ArrayList<Product>();
      // Select All Query
      String selectQuery = "SELECT * FROM " + TABLE_PRODUCT;

      SQLiteDatabase db = this.getReadableDatabase();
      Cursor cursor = db.rawQuery(selectQuery, null);

      if (cursor.getCount() > 0) {
          cursor.moveToFirst();
          do {
              Product pro = new Product();
              pro.setId(cursor.getString(1));
              pro.setUsername(cursor.getString(6));
              pro.setDescription(cursor.getString(5));
              pro.setName(cursor.getString(2));
              pro.setPrice(cursor.getString(4));
              pro.setImage(cursor.getString(7));
              pro.setDate(cursor.getString(3));

              String r = cursor.getString(1);
              String n=cursor.getString(2);
              String i=cursor.getString(3);
              String a=cursor.getString(4);
              String t=cursor.getString(5);
              String x=cursor.getString(6);

              list.add(pro);

          } while (cursor.moveToNext());

          return list;
      }
      return null;
  }
    public void DeleteAllAddToCart() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PRODUCT, null, null);
        db.close();
    }
   public void DeleteAllAddToCart1() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_WISH, null, null);
        db.close();
    }
    public boolean checkNameExistance(String pid) {
        SQLiteDatabase db = this.getReadableDatabase();

        try {
            cursoor = db.query(TABLE_PRODUCT, new String[]{"*"}, PID + " = '" + pid + "'",
                    null,
                    null,
                    null,
                    null
            );
        } catch (Exception e) {
        } finally {
            if (cursoor.moveToFirst()) {
                cursoor.close();
                return true;

            }
            cursoor.close();
            return false;
        }
    }
    public boolean DeleteSingleAddToCart1(String pid) {
        SQLiteDatabase db = this.getWritableDatabase();
        long i= db.delete(TABLE_WISH, PID + " = ?",new String[] { pid });
        if(i>0){
            return true;
        }else{
            return false;

        }

    }


    public boolean checkNameExistance1(String pid){
        SQLiteDatabase db = this.getReadableDatabase();

        try{
            cursoor= db.query(TABLE_WISH,new String[] { "*" },PID+" = '"+pid+"'",
                    null,
                    null,
                    null,
                    null
            );
        }
        catch(Exception e){}
        finally{
            if(cursoor.moveToFirst())
            {
                cursoor.close();
                return true;

            }
            cursoor.close();
            return false;
        }

    }
    public boolean DeleteSingleAddToCart(String pid) {
        SQLiteDatabase db = this.getWritableDatabase();
       long i= db.delete(TABLE_PRODUCT, PID + " = ?",new String[] { pid });
      if(i>0){
          return true;
      }else{
          return false;

      }
    }
  */
/*  public DeleteSingleAddToCart1(String pid) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_WISH, PID + " = ?",new String[] { pid });
        db.close();
    }*//*


*/

}



