package com.zdoof.stpl.zdoof.search;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.activites.Profiles;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.webservice.PicConstant;

import java.util.ArrayList;

/**
 * Created by stpl on 13/3/16.
 */
public class CustomSerchPost extends BaseAdapter{
    Search_post search_post;
    ArrayList<Detail> values;
    String pic,pic1,pic2,pic3,review;
    private Context context;
    public CustomSerchPost(Search_post search_post, int spiner_item, ArrayList<Detail> values,Context context) {
        this.search_post=search_post;
        this.values=values;
        this.context = context;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vgrid;
        LayoutInflater inflater = search_post.getLayoutInflater();
        vgrid = inflater.inflate(R.layout.custom_searchfood, null);
        ImageView img= (ImageView) vgrid.findViewById(R.id.post);
        ImageView img1=(ImageView) vgrid.findViewById(R.id.post1);
        TextView text= (TextView) vgrid.findViewById(R.id.textView5);
        TextView text1= (TextView) vgrid.findViewById(R.id.textView6);
        TextView text2= (TextView) vgrid.findViewById(R.id.textView7);
        TextView text3=(TextView) vgrid.findViewById(R.id.textView19);
        TextView txtViewDetails = (TextView)vgrid.findViewById(R.id.txtViewDetails);
        txtViewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Detail detail = values.get(position);
                if (!TextUtils.isEmpty(detail.getUser_id())){
                    Intent intent = new Intent(context, Profiles.class);
                    intent.putExtra("USERID",detail.getUser_id());
                    context.startActivity(intent);
                }
            }
        });
        /* TextView text3= (TextView) vgrid.findViewById(R.id.textView13);
        TextView text4= (TextView) vgrid.findViewById(R.id.textView15);
        TextView text5= (TextView) vgrid.findViewById(R.id.textView17);*/
        //style=values.get(position).getStyle();
        pic=values.get(position).getPostimage();
        //review=values.get(position).getReviews();
        //text3.setText(review);
        pic1= PicConstant.PROFILE_URL1+pic;

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(search_post).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.noimg)
                .showImageOnLoading(R.drawable.uute).build();
        imageLoader.displayImage(pic1, img1, options);
        pic2=values.get(position).getPfimage();

        pic3= PicConstant.PROFILE_URL1+pic2;
        imageLoader.displayImage(pic3, img, options);
       /* Uri myUri = Uri.parse(pic1);
        Picasso.with(search_post)
                .load(myUri)
                .placeholder(R.drawable.load)
                .error(R.drawable.noimg)
                .into(img);*/

        text.setText(values.get(position).getPostname());
        text1.setText(Html.fromHtml(values.get(position).getPostdetails()));
        text2.setText(values.get(position).getPcreaton());

        return vgrid;
    }
}