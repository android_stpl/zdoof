package com.zdoof.stpl.zdoof.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.activites.Profiles;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.utils.ImageListener;
import com.zdoof.stpl.zdoof.utils.ZdoofConstants;
import com.zdoof.stpl.zdoof.webservice.PicConstant;
import com.zdoof.stpl.zdoof.webservice.Webservicereceipe_post;

import org.json.JSONArray;
import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Ratan on 7/29/2015.
 */
public class About extends Fragment {
    TextView wrk, types, gen, date, email;
    LinearLayout phBar, emailBar;
    SharedPreferenceClass sharedPreferenceClass;
    String city, conty, eml, works, frsn, lstn, name,dob;
    int Sub_user_type, Main_user_type;
    String _name, Last_name, Createdon, Updated_on, Ip_address, userid;
    static String bResponse;
    final Webservicereceipe_post com1 = new Webservicereceipe_post();
    String First_name, states, propict, mnutype, subutype, pic1, pic3, type, password, addrsline1, addressln2, zipcode,  Marital_status, Mobile;
    int Reg_Id, Logged_id, User_id;
    ProgressDialog progressDialog;
    String rid,imageUrl;
    ImageListener listener;
    SharedPreferences userSharedPreff;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (ImageListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ImageListner");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.social_layout, null);
        wrk = (TextView) rootView.findViewById(R.id.wrkgas);
        types = (TextView) rootView.findViewById(R.id.chef);
        date = (TextView) rootView.findViewById(R.id.textView5);
        gen = (TextView) rootView.findViewById(R.id.textView6);
        email = (TextView) rootView.findViewById(R.id.ee);
        phBar = (LinearLayout) rootView.findViewById(R.id.phNoBar);
        emailBar = (LinearLayout) rootView.findViewById(R.id.emailAddBar);
        sharedPreferenceClass = new SharedPreferenceClass(getActivity());
        Reg_Id = Integer.parseInt(sharedPreferenceClass.getValue_string("UDDIDD"));


        rid = sharedPreferenceClass.getValue_string("UIDD");
        Logged_id=Integer.parseInt(rid);
        userid=sharedPreferenceClass.getValue_string("UDDIDD");
        User_id=Integer.parseInt(userid);
        if (Logged_id!=User_id) {
            phBar.setVisibility(View.GONE);
            emailBar.setVisibility(View.GONE);
        } else {
            phBar.setVisibility(View.VISIBLE);
            emailBar.setVisibility(View.VISIBLE);
        }
        AsyncCallWS task = new AsyncCallWS();
        task.execute();
      /*
        city=sharedPreferenceClass.getValue_string("CTY");
        conty=sharedPreferenceClass.getValue_string("CON");
        gender=sharedPreferenceClass.getValue_string("GEN");
        works=sharedPreferenceClass.getValue_string("WRK").toUpperCase();
        if(conty.equals("0")){
            lives.setText("N/A");
        }else{
            lives.setText(conty);
        }if(city.equals("null")){
            hometwn.setText("N/A");
        }
        else {
            hometwn.setText(city);
        }if(gender.equals("null"))
        {
            gen.setText("N/A");
        }else {
            gen.setText(gender);
        }
        wrk.setText(works);
        //works=sharedPreferenceClass.getValue_string("");
      *//*  frsn=sharedPreferenceClass.getValue_string("FSN");
        lstn=sharedPreferenceClass.getValue_string("LTN");
        name=frsn+lstn;*/
        userSharedPreff = getActivity().getSharedPreferences(ZdoofConstants.EXTRA_USER_PREFF, Context.MODE_PRIVATE);
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {

            // If we are becoming invisible, then...
            AsyncCallWS task = new AsyncCallWS();
            task.execute();

            if (!isVisibleToUser) {
                Log.d("MyFragment", "Not visible anymore.  Stopping audio.");
                // TODO stop audio playback
            }
        }
    }

    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* progressDialog = ProgressDialog.show(getActivity(), "", "Loading...");*/
            Profiles.aboutLoaded = false;
            if (!listener.getProgressDialog().isShowing()) {
                listener.getProgressDialog().show();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");
            // progressDialog.dismiss();
            bResponse = com1.getUserDtl("getUserDtl", Logged_id, User_id);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            // progressDialog.dismiss();
            try {
                JSONArray jr = new JSONArray(bResponse);
                for (int i=0; i<jr.length(); i++) {
                    JSONObject jsonObject = jr.getJSONObject(i);
                    //  userid = jsonObject.getString("User_id").toString();
                    name = jsonObject.getString("UTYPE").toString().toUpperCase();
                    dob = jsonObject.getString("Created_on").toString();
                    conty = jsonObject.getString("Country").toString();
                    works = jsonObject.getString("Mobile").toString();
                    eml = jsonObject.getString("Email").toString();
                    imageUrl = PicConstant.PROFILE_URL1+jsonObject.getString("profimg");


                    String userName= jsonObject.getString("name");
                    String connStatus = jsonObject.getString("Connection_status");
                    String conndetails = jsonObject.getString("Connection_details");
                    String[] connDtl = conndetails.split(",");
                    if (!conndetails.equals("0")) {
                        for (int j=0; j<connDtl.length; j++) {
                            connDtl[j] = connDtl[j].split("=")[1];
                        }
                    }

                    if (!name.toLowerCase().equals("null")) {
                        types.setText(name);
                    }
                    else {types.setText("N/A");}
                    if (!dob.toLowerCase().equals("null")) {
                        date.setText(dob);
                    }
                    else {date.setText("N/A");}
                    if (!conty.toLowerCase().equals("null")) {
                        gen.setText(conty);
                    }
                    else {gen.setText("N/A");}
                    if (!works.toLowerCase().equals("null")) {
                        wrk.setText(works);
                    }
                    else {wrk.setText("N/A");}
                    if (!eml.toLowerCase().equals("null")) {
                        email.setText(eml);
                    }
                    else {email.setText("N/A");}
                    listener.loadImage(imageUrl);
                    listener.loadName(userName);
                    listener.connectionStatus(connStatus, connDtl);
                    listener.loadUType(name);
                    SharedPreferences.Editor editor = userSharedPreff.edit();
                    editor.putString(ZdoofConstants.EXTRA_USER_ID,""+jsonObject.getString("User_id"));
                    editor.putString(ZdoofConstants.EXTRA_MAIN_USER_TYPE,jsonObject.getString("Main_user_type"));
                    editor.putString(ZdoofConstants.EXTRA_SUBUSER_TYPE,""+jsonObject.getString("Sub_user_type"));
                    editor.putString(ZdoofConstants.EXTRA_USER_STATUS,""+jsonObject.getString("User_status"));
                    editor.putString(ZdoofConstants.EXTRA_USER_FIRST_NAME,jsonObject.getString("First_name"));
                    editor.putString(ZdoofConstants.EXTRA_USER_LAST_NAME,jsonObject.getString("Last_name"));
                    editor.putString(ZdoofConstants.EXTRA_USER_EMAIL,jsonObject.getString("Email"));
                    editor.putString(ZdoofConstants.EXTRA_USER_NAME,jsonObject.getString("name"));
                    editor.putString(ZdoofConstants.EXTRA_USER_ADDRESS_ONE,jsonObject.getString("Address_line1"));
                    editor.putString(ZdoofConstants.EXTRA_USER_ADDRESS_TWO,jsonObject.getString("Address_line2"));
                    editor.putString(ZdoofConstants.EXTRA_USER_ZIP_CODE,jsonObject.getString("Zip_code"));
                    editor.putString(ZdoofConstants.EXTRA_USER_CUTY,jsonObject.getString("City"));
                    editor.putString(ZdoofConstants.EXTRA_USER_STATE,jsonObject.getString("State"));
                    editor.putString(ZdoofConstants.EXTRA_USER_COUNTRY,jsonObject.getString("Country"));
                    editor.putString(ZdoofConstants.EXTRA_USER_GENDER,jsonObject.getString("gen"));
                    editor.putString(ZdoofConstants.EXTRA_USER_MOBILE,jsonObject.getString("Mobile"));
                    editor.putString(ZdoofConstants.EXTRA_USER_DOB,jsonObject.getString("DOB"));
                    editor.putString(ZdoofConstants.EXTRA_USER_PROFILE_IMAGE,jsonObject.getString("profimg"));
                    editor.putString(ZdoofConstants.EXTRA_USER_TYPE,jsonObject.getString("UTYPE"));
                    editor.putString(ZdoofConstants.EXTRA_USER_CREATED_ON,jsonObject.getString("Created_on"));
                    editor.putString(ZdoofConstants.EXTRA_USER_MARITIAL_STATUS,jsonObject.getString("mstat"));
                    editor.putString(ZdoofConstants.EXTRA_USER_ABOUT,jsonObject.getString("About_user"));
                    editor.commit();
                }


            } catch (Exception e) {
                Log.v("errozr",e.getMessage());
                e.printStackTrace();
            }

            if (bResponse.equals("0")) {
                Toast.makeText(getActivity(), "Sorry try again!", Toast.LENGTH_LONG).show();

            } else {}
            Profiles.aboutLoaded = true;
            listener.dismissProgressDialog();

        }


    }


}