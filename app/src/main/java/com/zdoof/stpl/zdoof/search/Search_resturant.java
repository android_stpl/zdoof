package com.zdoof.stpl.zdoof.search;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.zdoof.stpl.zdoof.activites.Connecton;
import com.zdoof.stpl.zdoof.utils.AppUtil;
import com.zdoof.stpl.zdoof.views.Contropanel;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.activites.Home;
import com.zdoof.stpl.zdoof.activites.Notifications;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.commons.SharedPreferenceClass;
import com.zdoof.stpl.zdoof.webservice.WebserviceCall2;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by stpl on 13/3/16.
 */
public class Search_resturant extends Activity implements LocationListener {
    ImageView all,post,grocery,dish,food,serc;
    ImageView close,home,connect,noti,icon,contlpnl,search_close;
    ImageView active1,active2,act3,act4,act5,act6,act7;
    private FloatingActionButton fab;
    ListView grid;
    LinearLayout ll1,ll2,ll3,ll4,mainBody;
    ProgressDialog progressDialog;
    ArrayList<Detail> Values;
    final WebserviceCall2 com2 = new WebserviceCall2();
    static String aResponse;
    String description="",locationn="";
    String resurant_name,resturant_img,rest_address,rest_phone,rest_id,searchvalue;
    Dialog dialog;
    EditText descc;
    SharedPreferenceClass sharedPreferenceClass;
   // String desc,des;
    ImageView search;
    ImageView rch;
    String pos;

    ListView listt;
    TextView textt;
    LinearLayout ziploc;
    LinearLayout vieew;
    EditText zips;
    Button submit;
    TextView code;
    static String result;
    String zipcode;
    static String zipcodevalue;
    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double latitude; // latitude
    double longitude; // longitude
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
   // String rest, dish, pep, gros, posts;
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    String desc = "", loATION,des,zlikes,review;
    String sertype[]={"Search Post","Search Foodie","Search Recipe/Dish","Search Restaurant","Search Grocery Store"};
    int sercimg[]={R.drawable.searhicon,R.drawable.sst,R.drawable.ff,R.drawable.sahlist,R.drawable.crt,R.drawable.uur};
    private final int LOCATION_PERMISSION_CODE=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_resturant);
        grid= (ListView) findViewById(R.id.gridView);
        listt= (ListView) findViewById(R.id.li);
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header1 = (ViewGroup)inflater.inflate(R.layout.search_view, listt, false);
        listt.addHeaderView(header1, null, false);
        serc= (ImageView) findViewById(R.id.serc);
        textt= (TextView) findViewById(R.id.textt);
        ziploc= (LinearLayout) findViewById(R.id.ziploc);
        vieew= (LinearLayout) findViewById(R.id.view);
        zips= (EditText) findViewById(R.id.zip);
        submit= (Button) findViewById(R.id.submit);
        code= (TextView) findViewById(R.id.code);
        rch= (ImageView) findViewById(R.id.sec);
        descc= (EditText) findViewById(R.id.imageView11);
        search_close= (ImageView) findViewById(R.id.close);
        ll1= (LinearLayout) findViewById(R.id.hmj);
        ll2= (LinearLayout) findViewById(R.id.cnn);
        ll3=(LinearLayout) findViewById(R.id.ntf);
        ll4=(LinearLayout) findViewById(R.id.ct);
        mainBody = (LinearLayout) findViewById(R.id.resMainBody);
        food= (ImageView) findViewById(R.id.imageVies);
        home= (ImageView) findViewById(R.id.hme);
        connect= (ImageView) findViewById(R.id.hm);
        icon=(ImageView) findViewById(R.id.icon);
        contlpnl = (ImageView) findViewById(R.id.ctrl);
       /* fab= (FloatingActionButton) findViewById(R.id.fab);
        act4.setVisibility(View.VISIBLE);*/
        sharedPreferenceClass=new SharedPreferenceClass(Search_resturant.this);
        searchvalue=sharedPreferenceClass.getValue_string("SEARCHVALUE");
        descc.setText(searchvalue);
       if(searchvalue.equals("") || searchvalue.equals("0")){
            search_close.setVisibility(View.GONE);
        }else {
            search_close.setVisibility(View.VISIBLE);
            sharedPreferenceClass.setValue_string("SEARCHVALUE",desc);
           description = searchvalue;
        }

        search_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descc.setText("");
                sharedPreferenceClass.setValue_string("SEARCHVALUE", "0");
                search_close.setVisibility(View.GONE);
            }
        });

        ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll2.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent = new Intent(Search_resturant.this, Connecton.class);
                // intent.putExtra("EML",det);
                startActivity(intent);
                finish();
            }
        });
        mainBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listt.getVisibility()== View.VISIBLE) {
                    listt.setVisibility(View.GONE);
                }
            }
        });
        ll4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll4.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent = new Intent(Search_resturant.this, Contropanel.class);
                startActivity(intent);
                finish();
            }
        });
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll1.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent=new Intent(Search_resturant.this,Home.class);
                startActivity(intent);
                finish();
            }
        });
        ll3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ll1.setBackgroundColor(Color.parseColor("#5ce480"));
                Intent intent=new Intent(Search_resturant.this,Notifications.class);
                startActivity(intent);
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zipcodevalue=zips.getText().toString();
                code.setText(zipcodevalue);
                sharedPreferenceClass.setValue_string("ZZIP", zipcodevalue);
                //sharedPreferenceClass.setValue_string("X",zipcodevalue);
                ziploc.setVisibility(View.GONE);
                vieew.setVisibility(View.GONE);

            }
        });

        rch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listt.getVisibility()== View.VISIBLE) {
                    listt.setVisibility(View.GONE);
                }else {
                    listt.setVisibility(View.VISIBLE);
                    CustomSearch8 adapter = new CustomSearch8(Search_resturant.this, R.layout.spiner_item, sertype, sercimg);
                    listt.setAdapter(adapter);
                    listt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            desc = descc.getText().toString().trim();
                            if (desc.length()<3) {
                                Toast.makeText(Search_resturant.this, "Please enter atleast 3 letters to search!", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                pos = listt.getItemAtPosition(position).toString();
                                if (pos.equals("0")) {
                                    desc = descc.getText().toString().trim();
                                    descc.setText(desc);
                                    sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                                    Intent intent = new Intent(Search_resturant.this, Search_post.class);
                                    startActivity(intent);
                                    finish();
                                } else if (pos.equals("1")) {
                                    desc = descc.getText().toString().trim();
                                    descc.setText(desc);
                                    sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                                    Intent intent = new Intent(Search_resturant.this, Search_people.class);
                                    startActivity(intent);
                                    finish();
                                } else if (pos.equals("2")) {
                                    desc = descc.getText().toString().trim();
                                    descc.setText(desc);
                                    sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                                    Intent intent = new Intent(Search_resturant.this, Search_dish.class);
                                    startActivity(intent);
                                    finish();
                                } else if (pos.equals("3")) {
                                    desc = descc.getText().toString().trim();
                                    descc.setText(desc);
                                    sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                                    //zipcodevalue=sharedPreferenceClass.getValue_string("X");
                                    //sharedPreferenceClass.setValue_string("Y", zipcodevalue);
                                    Intent intent = new Intent(Search_resturant.this, Search_resturant.class);
                                    startActivity(intent);
                                    finish();
                                } else if (pos.equals("4")) {
                                    desc = descc.getText().toString().trim();
                                    descc.setText(desc);
                                    sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                                    Intent intent = new Intent(Search_resturant.this, Search_gstore.class);
                                    startActivity(intent);
                                    finish();
                                } /*else if (pos.equals("5")) {}*/
                                listt.setVisibility(View.GONE);
                            }
                        }
                    });
                }
            }
        });

        textt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ziploc.setVisibility(View.VISIBLE);
                vieew.setVisibility(View.VISIBLE);
            }
        });

        serc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //description.setCursorVisible(true);
                des = descc.getText().toString().trim();
                if (des.length()<3) {
                    Toast.makeText(Search_resturant.this, "Please enter atleast 3 letters to search!", Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent(Search_resturant.this, Search_post.class);
                    sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                    startActivity(intent);
                }
            }

          /*  @Override
            public boolean onTouch(View v, MotionEvent event) {

                description.setEnabled(false);

                des = description.getText().toString().trim();
                //fullparentscrolling.setEnabled(false);
                Intent intent = new Intent(Home.this, Search_all.class);
                intent.putExtra("DES", des);
                startActivity(intent);
                return true;
            }*/
        });
        descc.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            desc = descc.getText().toString().trim();

                            Intent intent = new Intent(Search_resturant.this, Search_post.class);
                            sharedPreferenceClass.setValue_string("SEARCHVALUE", desc);
                            startActivity(intent);
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
        descc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descc.setCursorVisible(true);
            }
        });
        AsyncCallWS task = new AsyncCallWS();
        task.execute();
        String z_zip = sharedPreferenceClass.getValue_string("ZZIP");
        if (!z_zip.equals(null) && !z_zip.equals("") && !(z_zip.length()<2)) {
            code.setText(z_zip);
            zipcode = z_zip;
            zipcodevalue=z_zip;
        } else {
            if (AppUtil.GetLocationPermission(this)) {
                GetZipcodeTask zipTask = new GetZipcodeTask();
                zipTask.execute();
            }
            else {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CODE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(listt.getVisibility()== View.VISIBLE) {
            listt.setVisibility(View.GONE);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_CODE) {
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                GetZipcodeTask zipcodeTask = new GetZipcodeTask();
                zipcodeTask.execute();
            } else {
                Toast.makeText(Search_resturant.this,"Oops you just denied the permission",Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private class GetZipcodeTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... params) {
            getZipcodeOfLocation();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            String zzip = sharedPreferenceClass.getValue_string("ZZIP");
            if (!zzip.equals(null) && !zzip.equals("") && !zzip.equals("0") && !(zzip.length()<2)) {
                code.setText(zzip);
            } else {
                code.setText(zipcode);
                sharedPreferenceClass.setValue_string("ZZIP", zipcode);
            }
            zipcode = sharedPreferenceClass.getValue_string("ZZIP");
            zipcodevalue = zipcode;
        }
    }

    private void getZipcodeOfLocation() {
        LocationManager manager = (LocationManager) Search_resturant.this.getSystemService(LOCATION_SERVICE);
        // getting GPS status
        isGPSEnabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        isNetworkEnabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        Geocoder geocoder = new Geocoder(Search_resturant.this, Locale.getDefault());
        if (
                ActivityCompat.checkSelfPermission(Search_resturant.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(Search_resturant.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (isNetworkEnabled) {
                location = manager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    try {
                        List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 5);
                        if (addresses != null) {
                            if (addresses.size() > 0) {
                                for (int i = 0; i < addresses.size(); i++) {
                                    if (addresses.get(i).getPostalCode() != null) {
                                        if (addresses.get(i).getPostalCode().length() > 1) {
                                            zipcode = addresses.get(i).getPostalCode();
                                            zipcodevalue = zipcode;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        } else {
            ActivityCompat.requestPermissions(Search_resturant.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION_CODE);
        }
    }

    class AsyncCallWS extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(Search_resturant.this, "", "Loading...");
        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("TAG", "doInBackground");
            aResponse = com2.Serch_resturant("restaurant_search", description, zipcodevalue);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Log.i("TAG", "onPostExecute");
            progressDialog.dismiss();
            try {
                JSONArray jr = new JSONArray(aResponse);

                Values=new ArrayList<Detail>();

                for (int i=0;i<jr.length();i++) {

                    Detail productdto = new Detail();
                    JSONObject jsonObject = jr.getJSONObject(i);

                    resurant_name = jsonObject.getString("Restaurant_name").toString();
                    rest_address = jsonObject.getString("Address_line1").toString();
                    // username = jsonObject.getString("User_name").toString();
                    zlikes=jsonObject.getString("thurest").toString();
                    review=jsonObject.getString("totalreview").toString();
                    rest_id=jsonObject.getString("Restaurant_id").toString();
                    resturant_img=jsonObject.getString("Restaurant_Image").toString();
                    rest_phone=jsonObject.getString("Restaurant_phone").toString();
                    String rating = jsonObject.getString("reviewrating");
                    productdto.setRestaurant_name(resurant_name);
                    productdto.setRest_img(resturant_img);
                    productdto.setRest_address(rest_address);
                    productdto.setRest_id(rest_id);
                    productdto.setRest_phone(rest_phone);
                    productdto.setThumbsup(zlikes);
                    productdto.setReviews(review);
                    productdto.setRatings(rating);
                    Values.add(productdto);
                }
                CustomResturant adapter=new CustomResturant(Search_resturant.this, R.layout.spiner_item,Values);
                grid.setAdapter(adapter);
                progressDialog.dismiss();
                grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    }
                });
            }catch (Exception e){
                Log.v("ERR..",e.getMessage());
            }

            if (aResponse.equals("0")) {
                Toast.makeText(getApplicationContext(), "Sorry try again!", Toast.LENGTH_LONG).show();

            } else if (aResponse.equals("1")) {
                Toast.makeText(getApplicationContext(), "You have successfully Registered !", Toast.LENGTH_LONG).show();
              /*  Intent i = new Intent(SignUp.this, Home.class);
                startActivity(i);
                // overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                //save auth key
                finish();*/
            } else {

                //sharedPreferenceClass.setValue_boolean("Loginstatus", true);

            }

        }


    }
    }

