package com.zdoof.stpl.zdoof.search;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.activites.Profiles;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.webservice.PicConstant;

import java.util.ArrayList;

/**
 * Created by stpl on 13/3/16.
 */
public class CustomSerchpeople extends BaseAdapter{
    Search_people search_ple;
    ArrayList<Detail> values;
    String pic,pic1;
    Context context;
    public CustomSerchpeople(Context context, Search_people search_ple, int spiner_item, ArrayList<Detail> values) {
       this.search_ple=search_ple;
        this.values=values;
        this.context = context;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View vgrid;
        LayoutInflater inflater = search_ple.getLayoutInflater();
        vgrid = inflater.inflate(R.layout.custom_searchfoodie, null);
        ImageView img= (ImageView) vgrid.findViewById(R.id.imageView13);
        ImageView u_type_img = (ImageView) vgrid.findViewById(R.id.uTypeImg);
        TextView viewD = (TextView) vgrid.findViewById(R.id.viewD);
        TextView text= (TextView) vgrid.findViewById(R.id.textView2);
        TextView text1= (TextView) vgrid.findViewById(R.id.textView4);
       TextView text2= (TextView) vgrid.findViewById(R.id.textView);

        viewD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Detail detail = values.get(position);
                if (!TextUtils.isEmpty(detail.getUser_id())){
                    Intent intent = new Intent(context, Profiles.class);
                    intent.putExtra("USERID",detail.getUser_id());
                    context.startActivity(intent);
                }
            }
        });
        /* TextView text3= (TextView) vgrid.findViewById(R.id.textView13);
        TextView text4= (TextView) vgrid.findViewById(R.id.textView15);
        TextView text5= (TextView) vgrid.findViewById(R.id.textView17);*/

        pic=values.get(position).getUserpfimg();

        pic1= PicConstant.PROFILE_URL1+pic;
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(search_ple).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                        // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.noimg)
                .showImageOnLoading(R.drawable.uute).build();
        imageLoader.displayImage(pic1, img, options);
       /* Uri myUri = Uri.parse(pic1);
        Picasso.with(search_ple)
                .load(myUri)
                .placeholder(R.drawable.load)
                .error(R.drawable.blankpeople)
                .into(img);*/
        String uType = values.get(position).getUser_type();
        if(uType.equals("Foodie")) {
            u_type_img.setImageResource(R.drawable.utype2);
        }
        else if (uType.equals("Culinary hobbyist")) {
            u_type_img.setImageResource(R.drawable.utype3);
        }
        else if (uType.equals("Chef")) {
            u_type_img.setImageResource(R.drawable.utype1);
        }


        text2.setText(values.get(position).getU_Name());
        text.setText(values.get(position).getUser_type());
        text1.setText(values.get(position).getUser_Zlike());

        return vgrid;
    }
}
