package com.zdoof.stpl.zdoof.adapters;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zdoof.stpl.zdoof.R;

/**
 * Created by stpl on 24/2/16.
 */
public class CusAdapters extends BaseAdapter{
    FragmentActivity post;
    String[] catgory;
    int[] img;
    public CusAdapters(FragmentActivity post, int spiner_item, String[] catgory, int[] img) {
        this.post=post;
        this.catgory=catgory;
        this.img=img;
    }

    @Override
    public int getCount() {
        return catgory.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = post.getLayoutInflater();
        view = inflater.inflate(R.layout.customview, null);
        TextView text= (TextView) view.findViewById(R.id.textView16);
        ImageView image= (ImageView) view.findViewById(R.id.imageView25);
        text.setText(catgory[position]);
        text.setTextColor(Color.WHITE);
        image.setImageResource(img[position]);

        return view;
    }
}
