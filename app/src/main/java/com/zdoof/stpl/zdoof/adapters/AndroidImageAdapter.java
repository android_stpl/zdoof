package com.zdoof.stpl.zdoof.adapters;

/**
 * Created by UBN on 06-02-2017.
 */

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.zdoof.stpl.zdoof.activites.AndroidImageSlider;
import com.zdoof.stpl.zdoof.webservice.PicConstant;

public class AndroidImageAdapter extends PagerAdapter {
    Context mContext;

    public AndroidImageAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return AndroidImageSlider.URLS.length;
    }
 @Override
    public boolean isViewFromObject(View v, Object obj) {
        return v == ((ImageView) obj);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int i) {
        ImageView mImageView = new ImageView(mContext);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(0, 0, 0, 0);

        mImageView.setLayoutParams(lp);
        mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        Picasso.with(AndroidImageSlider.context).load(AndroidImageSlider.URLS[i].trim()).into(mImageView);

        container.addView(mImageView, 0);
        mImageView.invalidate();
        return mImageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int i, Object obj) {
        ((ViewPager) container).removeView((ImageView) obj);
    }
}
