package com.zdoof.stpl.zdoof.commons;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Nutan on 07-01-2017.
 */
/*
"IMG_ID": "1725",
            "Post_type": "1",
            "Post_id": "5008",
            "Post_image": "PI99131944.png",
            "mid_img": "/zdoof-admin/dish/post-image/PI99131944.png"
 */

public class Tables {
    private String IMG_ID;
    private String Post_type;

    public String getPost_id() {
        return Post_id;
    }

    public void setPost_id(String post_id) {
        Post_id = post_id;
    }

    public String getIMG_ID() {
        return IMG_ID;
    }

    public void setIMG_ID(String IMG_ID) {
        this.IMG_ID = IMG_ID;
    }

    public String getPost_type() {
        return Post_type;
    }

    public void setPost_type(String post_type) {
        Post_type = post_type;
    }

    public String getPost_image() {
        return Post_image;
    }

    public void setPost_image(String post_image) {
        Post_image = post_image;
    }

    public String getMid_img() {
        return mid_img;
    }

    public void setMid_img(String mid_img) {
        this.mid_img = mid_img;
    }

    private String Post_id;
    private String Post_image;
    private String mid_img;
    public static Tables createTableFromJson(JSONObject jsonObject){
        try{
            Tables tables=new Tables();
            tables.setIMG_ID(jsonObject.getString("IMG_ID"));
            tables.setMid_img(jsonObject.getString("mid_img"));
            tables.setPost_id(jsonObject.getString("Post_id"));
            tables.setPost_image(jsonObject.getString("Post_image"));
            tables.setPost_type(jsonObject.getString("Post_type"));
            return tables;
        }
        catch (Exception je){
            je.printStackTrace();
            return null;
        }
    }
}
