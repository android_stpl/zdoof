package com.zdoof.stpl.zdoof.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zdoof.stpl.zdoof.CircularNetworkImageView;
import com.zdoof.stpl.zdoof.commons.Detail;
import com.zdoof.stpl.zdoof.R;
import com.zdoof.stpl.zdoof.activites.Comments;
import com.zdoof.stpl.zdoof.webservice.PicConstant;

import java.util.ArrayList;

/**
 * Created by stpl on 26/4/16.
 */
public class Custom_Comment extends BaseAdapter{
    ArrayList<Detail> hh;
    Comments comments;
    Spinner sp;
    String pic,pic1,names,dates,comment;

    public Custom_Comment(Comments comments, int spiner_item, ArrayList<Detail> hh) {
        this.hh=hh;
        this.comments=comments;
    }

    @Override
    public int getCount() {
        return hh.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        LayoutInflater inflater = comments.getLayoutInflater();
        view = inflater.inflate(R.layout.custom_comment, null);
        CircularNetworkImageView circularImageView = (CircularNetworkImageView) view.findViewById(R.id.profilePictureVie);
        TextView nam = (TextView) view.findViewById(R.id.textVie1);
        TextView types = (TextView) view.findViewById(R.id.textV1);
        //ImageView down= (ImageView) view.findViewById(R.id.fdd);
        TextView cmty=(TextView) view.findViewById(R.id.tb);

        pic=hh.get(position).getProfile();
        pic1= PicConstant.PROFILE_URL1 + pic;

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(comments).build();
        ImageLoader imageLoader = ImageLoader.getInstance();
        ImageLoader.getInstance().init(config);
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                        // .showImageForEmptyUri(fallback)
                .showImageOnFail(R.drawable.blankpeople)
                .showImageOnLoading(R.drawable.uute).build();
        imageLoader.displayImage(pic1, circularImageView, options);
        comment=hh.get(position).getComment();
        names=hh.get(position).getNames();
        dates=hh.get(position).getCre();
        nam.setText(names);
        types.setText(dates);
        cmty.setText(comment);
       /*down.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               ArrayAdapter adapter=new ArrayAdapter(comments,R.layout.spiner_item,gg);
                sp.setAdapter(adapter);
           }
       });*/
        return view;
    }
}
